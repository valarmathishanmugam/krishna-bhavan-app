"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var CustomerOrderList = (function () {
    function CustomerOrderList(routerExtensions) {
        this.routerExtensions = routerExtensions;
        this.orderDetails = [];
    }
    CustomerOrderList.prototype.ngOnInit = function () {
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
        this.orderDetails.push({ id: "ash34283980938", shop: "Kanniga Parameshwari Store", img: "~/Images/green_dot.png", date: "8/2/18" });
    };
    CustomerOrderList.prototype.orderdetail = function () {
        this.routerExtensions.navigate(["/orderdetail"]);
    };
    return CustomerOrderList;
}());
CustomerOrderList = __decorate([
    core_1.Component({
        selector: "cus-order-list",
        moduleId: module.id,
        templateUrl: "cusorderlist.component.html"
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions])
], CustomerOrderList);
exports.CustomerOrderList = CustomerOrderList;
