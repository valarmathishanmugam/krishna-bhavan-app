"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var SalesRepList = (function () {
    function SalesRepList(routerExtensions, page, location) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.title = "Sales Rep List";
        this.salerepdetails = [];
    }
    SalesRepList.prototype.ngOnInit = function () {
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   data.cancel = true;
        //   //alert("Back Pressed")
        //   //this.location.back();
        //   this.routerExtensions.backToPreviousPage();
        // });
    };
    SalesRepList.prototype.salesrepdetail = function () {
        this.routerExtensions.navigate(["/salesrepdetail"]);
    };
    return SalesRepList;
}());
SalesRepList = __decorate([
    core_1.Component({
        selector: "sales-rep-list",
        moduleId: module.id,
        templateUrl: "salesreplist.component.html",
        styleUrls: ["salesreplist.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page, common_1.Location])
], SalesRepList);
exports.SalesRepList = SalesRepList;
