"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_service_1 = require("../../Services/customer.service");
var customer_model_1 = require("../../Models/customer.model");
var saveorder_service_1 = require("../../Services/saveorder.service");
var login_service_1 = require("../../Services/login.service");
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var router_1 = require("nativescript-angular/router");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var PushNotifications = require("nativescript-push-notifications");
//import * as pushPlugin from "nativescript-push-notifications";
var localStorage = require("nativescript-localstorage");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var LoginComponent = (function () {
    // private pushSettings = {
    //   senderID: "472232624874", // Required: setting with the sender/project number
    //   notificationCallbackAndroid: (
    //     stringifiedData: String,
    //     fcmNotification: any
    //   ) => {
    //     const notificationBody = fcmNotification && fcmNotification.getBody();
    //     console.log(
    //       "Message received!\n" + notificationBody + "\n" + stringifiedData
    //     );
    //   }
    // };
    function LoginComponent(routerExtensions, page, loginservice, savecustomer, customerservice, savecustomerdetails, location, saveemployee, http) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.loginservice = loginservice;
        this.savecustomer = savecustomer;
        this.customerservice = customerservice;
        this.savecustomerdetails = savecustomerdetails;
        this.location = location;
        this.saveemployee = saveemployee;
        this.http = http;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.usernamevalid = "collapse";
        this.passwordvalid = "collapse";
        this.incorrectmsg = "collapse";
        this.servermsg = "";
        this.storeusername = "";
        this.storepassword = "";
        this.loader = new LoadingIndicator();
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        // application.android.on(
        //   AndroidApplication.activityBackPressedEvent,
        //   (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     //this.exitApp(data.cancel);
        //   }
        // );
        var settings = {
            senderID: "472232624874",
            notificationCallbackAndroid: function (data, notification) {
                console.log("DATA: " + JSON.stringify(data));
                console.log("NOTIFICATION: " + JSON.stringify(notification));
                var notificationBody = notification && notification.getBody();
                console.log("notificationBody: " + notificationBody);
                alert("New Order: \n" + notificationBody);
                //this.updateMessage("Message received!\n" + notificationBody + "\n" + data);
            }
        };
        //   pushPlugin.register(this.pushSettings, (token: String) => {
        //     alert("Device registered. Access token: " + token);;
        // }, function() { });
        //PushNotifications.onTokenRefresh
        //Commented by Balaji
        PushNotifications.register(settings, function (data) {
            console.log("REGISTRATION ID: " + data);
            _this.regid = data;
            PushNotifications.onMessageReceived(settings.notificationCallbackAndroid);
        }, function (error) {
            console.log("Firebase Error: " + error);
        });
        //Commented by Balaji
        /*var currentdate = new Date();
          console.log("Date: " + currentdate.getDate())
          var date = currentdate.toISOString();
          console.log("ISO Date: " + date)
          */
        if (localStorage.getItem("local username") !== null) {
            //console.log("username not null")
            this.storeusername = localStorage.getItem("local username");
        }
        else {
            //console.log("username is null")
        }
        if (localStorage.getItem("local password") !== null) {
            //console.log("password not null")
            this.storepassword = localStorage.getItem("local password");
        }
        else {
            //console.log("password is null")
        }
        //console.log("username Storage: " + this.storeusername + " password storage: " + this.storepassword);
    };
    LoginComponent.prototype.submit = function (username, password, args) {
        //username="9500231761";
        //password = "dummypass";
        var _this = this;
        this.usernamevalid = "collapse";
        this.passwordvalid = "collapse";
        this.incorrectmsg = "collapse";
        //console.log("username: "+ username + "password: "+ password);
        this.checkvalue = this.FirstCheckBox.nativeElement.checked;
        console.log("Checked value: " + this.checkvalue);
        if (this.checkvalue == true) {
            //console.log("Its Truee...")
            localStorage.setItem("local username", username);
            localStorage.setItem("local password", password);
        }
        else if (this.checkvalue == false) {
            //console.log("Its False...")
        }
        if (username === "") {
            console.log("username is empty");
            this.usernamevalid = "visible";
        }
        else if (password === "") {
            console.log("password is empty");
            this.passwordvalid = "visible";
        }
        else {
            this.showLoadingIndicator();
            console.log("Call Login Service");
            this.loginservice.login(username, password).subscribe(function (data) {
                _this.loader.hide();
                console.log("Login Data: " + data.text());
                _this.incorrectmsg = "visible";
                _this.servermsg = data.text();
                _this.parseusers(username, password, data.json());
            }, function (err) {
                console.log("Login Error: " + err);
            });
        }
        //(error) => {
        // console.log("Login error: " + error)
        //}
        //this.routerExtensions.navigate(["/customerlist"]);
        //this.routerExtensions.navigate(["/productlist"]);
        //let view = <TextField.textfield>args.object;
        //const tip = new ToolTip(view,{text:"Some Text",backgroundColor:"pink",textColor:"black"});
        //tip.show();
    };
    LoginComponent.prototype.showLoadingIndicator = function () {
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
    };
    LoginComponent.prototype.parseusers = function (username, password, data) {
        var _this = this;
        console.log("loginId: " + data.id);
        var temprole = data.role;
        this.loginservice.setScope(data);
        if (temprole === "customer") {
            console.log("customer");
            this.routerExtensions.navigate(["/productlist"]);
            this.savecustomer.setScope(data.id);
            this.customerservice
                .getcustomerbyid(data.id)
                .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        }
        else if (temprole === "rep") {
            console.log("rep");
            this.saveemployee.setScope(data.id);
            this.routerExtensions.navigate(["/customerlist"]);
        }
        else if (temprole === "manager") {
            console.log("manager");
            this.saveemployee.setScope(data.id);
            this.routerExtensions.navigate(["/managerorderlist"]);
        }
        // else if (temprole === "admin") {
        //   console.log("admin");
        //   this.saveemployee.setScope(data.id);
        //   this.routerExtensions.navigate(["/productlist"]);
        // }
        this.data = {
            mobile_no: username,
            password: password,
            registration_id: this.regid
        };
        console.log("REG ID: " + this.regid);
        console.log("Reg data: " + JSON.stringify(this.data));
        this.http
            .post("https://app.krishnabhavanfoods.in/api/user/addregistrationid", this.data, { headers: this.getCommonHeaders() })
            .subscribe(function (result) {
            //this.customerreport(result.json())
            console.log("Reg Result: " + JSON.stringify(result.json()));
        }, function (error) {
            console.log("Reg Error: " + error);
        });
    };
    LoginComponent.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
        this.savecustomerdetails.setScope(this.customer);
        this.saveemployee.setScope(data.employee_id.$oid);
    };
    LoginComponent.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    return LoginComponent;
}());
__decorate([
    core_1.ViewChild("cb1"),
    __metadata("design:type", core_1.ElementRef)
], LoginComponent.prototype, "FirstCheckBox", void 0);
LoginComponent = __decorate([
    core_1.Component({
        selector: "login-comp",
        moduleId: module.id,
        templateUrl: "login.component.html",
        styleUrls: ["login.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        login_service_1.loginService,
        saveorder_service_1.SaveCustomer,
        customer_service_1.CustomerService,
        saveorder_service_1.SaveCustomerDetails,
        common_1.Location,
        saveorder_service_1.SaveEmployeeId,
        http_1.Http])
], LoginComponent);
exports.LoginComponent = LoginComponent;
