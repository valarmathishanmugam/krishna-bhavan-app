"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var router_1 = require("nativescript-angular/router");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var AccountComponent = (function () {
    function AccountComponent(routerExtensions, page, location, http) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.http = http;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = false;
        // application.android.on(
        //   AndroidApplication.activityBackPressedEvent,
        //   (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     //this.exitApp(data.cancel);
        //   }
        // );   
    };
    return AccountComponent;
}());
AccountComponent = __decorate([
    core_1.Component({
        selector: "account-comp",
        moduleId: module.id,
        templateUrl: "account.component.html",
        styleUrls: ["account.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page, common_1.Location,
        http_1.Http])
], AccountComponent);
exports.AccountComponent = AccountComponent;
