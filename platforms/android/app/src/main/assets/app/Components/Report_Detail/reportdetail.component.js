"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var http_1 = require("@angular/http");
var order_model_1 = require("../../Models/order.model");
var login_service_1 = require("../../Services/login.service");
var reports_service_1 = require("../../Services/reports.service");
var ReportDetail = (function () {
    function ReportDetail(page, location, routerExtensions, loginservice, http, reportservice) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.loginservice = loginservice;
        this.http = http;
        this.reportservice = reportservice;
        this.big_total_count = "";
        this.big_total_amount = "";
        this.reports = new Array();
        this.detailvisible = "visible";
        this.bigvisible = "collapse";
    }
    ReportDetail.prototype.ngOnInit = function () {
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   data.cancel = true;
        //   //alert("Back Pressed")
        //   //this.location.back();
        //   this.routerExtensions.backToPreviousPage();
        // });
        this.tempuser = this.loginservice.getScope();
        this.getuserreport();
        this.getdate = this.reportservice.getdate();
        this.setfromdate = this.getdate["from"];
        this.settodate = this.getdate["to"];
        console.log("Tempfromdate: " + this.setfromdate);
    };
    ReportDetail.prototype.getuserreport = function () {
        var _this = this;
        if (this.tempuser.role === "manager") {
            //console.log("Manager Id: " + this.tempuser.id)
            //alert("Manager Id: " + this.tempuser.id)
            this.reportservice.managerReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.getmanagerreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Manager report Error: " + error);
            });
        }
        else if (this.tempuser.role === "rep") {
            //console.log("Rep Id: " + this.tempuser.id)
            //alert("Rep Id: " + this.tempuser.id)
            this.reportservice.repReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.salesrepreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Rep report Error: " + error);
            });
        }
        else if (this.tempuser.role === "customer") {
            //console.log("Customer Id: " + this.tempuser.id)
            //alert("Customer Id: " + this.tempuser.id)
            this.reportservice.customerReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.customerreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Customer Error: " + error);
            });
        }
    };
    ReportDetail.prototype.getmanagerreport = function (result) {
        console.log("Manager Result: " + JSON.stringify(result));
        console.log("Dummy Result: " + result);
        console.log("Manager Report Details: " + result.salesrep_orders);
        //alert("Manager Result: " + JSON.stringify(result))
        this.bigvisible = "visible";
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for (var i = 0; i < result.salesrep_orders.length; i++) {
            //console.log("Manager Report Details: " + result.salesrep_orders.length)
            //alert("Manager Report Details: " + result.salesrep_orders.length)
            this.reportdetail = new order_model_1.Report();
            this.reportdetail.emp_id = result.salesrep_orders[i].salesrep_id;
            this.reportdetail.order_count = result.salesrep_orders[i].order_count;
            this.reportdetail.order_total = result.salesrep_orders[i].order_total;
            this.reports.push(this.reportdetail);
        }
    };
    ReportDetail.prototype.salesrepreport = function (result) {
        //console.log("rep Result: " + JSON.stringify(result));
        //alert("rep Result: " + JSON.stringify(result))
        this.bigvisible = "visible";
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for (var i = 0; i < result.customer_orders_details.length; i++) {
            console.log("Rep Report Details: " + result.customer_orders_details.length);
            //alert("Manager Report Details: " + result.customer_orders_details.length)
            this.reportdetail = new order_model_1.Report();
            this.reportdetail.emp_id = result.customer_orders_details[i].customer_id;
            this.reportdetail.order_count = result.customer_orders_details[i].order_count;
            this.reportdetail.order_total = result.customer_orders_details[i].order_total;
            this.reports.push(this.reportdetail);
        }
    };
    ReportDetail.prototype.customerreport = function (result) {
        console.log("customer Result: " + JSON.stringify(result));
        //alert("customer Result: " + JSON.stringify(result))+++++++++
        this.bigvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
    };
    ReportDetail.prototype.goback = function () {
        //alert("Back Back...")
        this.routerExtensions.navigate(["/salesrepreport"]);
    };
    return ReportDetail;
}());
ReportDetail = __decorate([
    core_1.Component({
        selector: "report-detail",
        moduleId: module.id,
        templateUrl: "reportdetail.component.html",
        styleUrls: ["reportdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions,
        login_service_1.loginService, http_1.Http, reports_service_1.ReportService])
], ReportDetail);
exports.ReportDetail = ReportDetail;
