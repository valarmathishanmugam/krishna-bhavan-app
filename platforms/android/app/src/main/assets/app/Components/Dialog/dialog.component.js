"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var Dialog = (function () {
    function Dialog(params) {
        this.params = params;
        this.quantity = [];
        this.rack = params.context.rack;
        this.order = params.context.order;
    }
    Dialog.prototype.ok = function (result1, result2) {
        /* Commented BY Balaji
        if(result1 === "" || result2 === ""){
            alert("Enter Value!!!");
        }
        else if(result1 === "0" || result2 === "0"){
            alert("Enter Valid Value!!!");
        }

        else if(isNaN(Number(result1)) == true || isNaN(Number(result2)) == true){
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else{
            console.log("Res1: "+result1+"Res2: "+result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);
        }
        */
        if (result1 === "") {
            result1 = "0";
        }
        if (result2 === "" || result2 === "0") {
            alert("Enter Value!!!");
        }
        else if (isNaN(Number(result1)) == true || isNaN(Number(result2)) == true) {
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else {
            console.log("Res1: " + result1 + "Res2: " + result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);
        }
    };
    Dialog.prototype.close = function (result) {
        console.log("Cancelled");
        console.log("Res: " + result);
        this.params.closeCallback(result);
    };
    return Dialog;
}());
Dialog = __decorate([
    core_1.Component({
        selector: "dialog",
        moduleId: module.id,
        templateUrl: "dialog.component.html",
        styleUrls: ["dialog.component.css"]
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams])
], Dialog);
exports.Dialog = Dialog;
