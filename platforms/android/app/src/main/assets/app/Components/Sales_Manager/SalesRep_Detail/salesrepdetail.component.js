"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var SalesRepDetail = (function () {
    function SalesRepDetail(page, location, routerExtensions) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.title = "Sales Rep Detail";
        this.name = "Ramasamy";
        this.region = "Thiruvallur Region";
        this.mobile = 91 + 901236547;
    }
    SalesRepDetail.prototype.ngOnInit = function () {
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });
    };
    return SalesRepDetail;
}());
SalesRepDetail = __decorate([
    core_1.Component({
        selector: "salesrep-detail",
        moduleId: module.id,
        templateUrl: "salesrepdetail.component.html",
        styleUrls: ["salesrepdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions])
], SalesRepDetail);
exports.SalesRepDetail = SalesRepDetail;
