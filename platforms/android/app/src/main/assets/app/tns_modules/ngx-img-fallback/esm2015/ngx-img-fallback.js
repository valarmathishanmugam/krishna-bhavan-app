import { Directive, ElementRef, Input, Output, EventEmitter, Renderer2, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ImgFallbackDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     */
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.loaded = new EventEmitter();
        this.isApplied = false;
        this.ERROR_EVENT_TYPE = 'error';
        this.LOAD_EVENT_TYPE = 'load';
        this.nativeElement = el.nativeElement;
        this.onError = this.onError.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.addEvents();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.removeErrorEvent();
        this.removeOnLoadEvent();
    }
    /**
     * @return {?}
     */
    onError() {
        if (this.nativeElement.getAttribute('src') !== this.imgSrc) {
            this.isApplied = true;
            this.renderer.setAttribute(this.nativeElement, 'src', this.imgSrc);
        }
        else {
            this.removeOnLoadEvent();
        }
    }
    /**
     * @return {?}
     */
    onLoad() {
        this.loaded.emit(this.isApplied);
    }
    /**
     * @return {?}
     */
    removeErrorEvent() {
        if (this.cancelOnError) {
            this.cancelOnError();
        }
    }
    /**
     * @return {?}
     */
    removeOnLoadEvent() {
        if (this.cancelOnLoad) {
            this.cancelOnLoad();
        }
    }
    /**
     * @return {?}
     */
    addEvents() {
        this.cancelOnError = this.renderer.listen(this.nativeElement, this.ERROR_EVENT_TYPE, this.onError);
        this.cancelOnLoad = this.renderer.listen(this.nativeElement, this.LOAD_EVENT_TYPE, this.onLoad);
    }
}
ImgFallbackDirective.decorators = [
    { type: Directive, args: [{
                selector: '[src-fallback]'
            },] },
];
/** @nocollapse */
ImgFallbackDirective.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer2, },
];
ImgFallbackDirective.propDecorators = {
    "imgSrc": [{ type: Input, args: ['src-fallback',] },],
    "loaded": [{ type: Output, args: ['loaded',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ImgFallbackModule {
}
ImgFallbackModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ImgFallbackDirective],
                exports: [ImgFallbackDirective]
            },] },
];
/** @nocollapse */
ImgFallbackModule.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Created by Vadym Yatsyuk on 28.01.18
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Created by Vadym Yatsyuk on 28.01.18
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { ImgFallbackModule, ImgFallbackDirective };
//# sourceMappingURL=ngx-img-fallback.js.map
