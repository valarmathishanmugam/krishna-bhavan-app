"use strict";
/**
 * Created by vadimdez on 27/11/2016.
 */
var core_1 = require('@angular/core');
var ng2_img_fallback_directive_1 = require('./ng2-img-fallback.directive');
var Ng2ImgFallbackModule = (function () {
    function Ng2ImgFallbackModule() {
    }
    Ng2ImgFallbackModule.decorators = [
        { type: core_1.NgModule, args: [{
                    declarations: [ng2_img_fallback_directive_1.Ng2ImgFallbackDirective],
                    exports: [ng2_img_fallback_directive_1.Ng2ImgFallbackDirective]
                },] },
    ];
    /** @nocollapse */
    Ng2ImgFallbackModule.ctorParameters = [];
    return Ng2ImgFallbackModule;
}());
exports.Ng2ImgFallbackModule = Ng2ImgFallbackModule;
//# sourceMappingURL=ng2-img-fallback.module.js.map