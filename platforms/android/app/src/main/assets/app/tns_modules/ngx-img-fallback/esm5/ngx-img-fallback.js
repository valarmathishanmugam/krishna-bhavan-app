import { Directive, ElementRef, Input, Output, EventEmitter, Renderer2, NgModule } from '@angular/core';

var ImgFallbackDirective = /** @class */ (function () {
    function ImgFallbackDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.loaded = new EventEmitter();
        this.isApplied = false;
        this.ERROR_EVENT_TYPE = 'error';
        this.LOAD_EVENT_TYPE = 'load';
        this.nativeElement = el.nativeElement;
        this.onError = this.onError.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.addEvents();
    }
    ImgFallbackDirective.prototype.ngOnDestroy = function () {
        this.removeErrorEvent();
        this.removeOnLoadEvent();
    };
    ImgFallbackDirective.prototype.onError = function () {
        if (this.nativeElement.getAttribute('src') !== this.imgSrc) {
            this.isApplied = true;
            this.renderer.setAttribute(this.nativeElement, 'src', this.imgSrc);
        }
        else {
            this.removeOnLoadEvent();
        }
    };
    ImgFallbackDirective.prototype.onLoad = function () {
        this.loaded.emit(this.isApplied);
    };
    ImgFallbackDirective.prototype.removeErrorEvent = function () {
        if (this.cancelOnError) {
            this.cancelOnError();
        }
    };
    ImgFallbackDirective.prototype.removeOnLoadEvent = function () {
        if (this.cancelOnLoad) {
            this.cancelOnLoad();
        }
    };
    ImgFallbackDirective.prototype.addEvents = function () {
        this.cancelOnError = this.renderer.listen(this.nativeElement, this.ERROR_EVENT_TYPE, this.onError);
        this.cancelOnLoad = this.renderer.listen(this.nativeElement, this.LOAD_EVENT_TYPE, this.onLoad);
    };
    return ImgFallbackDirective;
}());
ImgFallbackDirective.decorators = [
    { type: Directive, args: [{
                selector: '[src-fallback]'
            },] },
];
ImgFallbackDirective.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: Renderer2, },
]; };
ImgFallbackDirective.propDecorators = {
    "imgSrc": [{ type: Input, args: ['src-fallback',] },],
    "loaded": [{ type: Output, args: ['loaded',] },],
};
var ImgFallbackModule = /** @class */ (function () {
    function ImgFallbackModule() {
    }
    return ImgFallbackModule;
}());
ImgFallbackModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ImgFallbackDirective],
                exports: [ImgFallbackDirective]
            },] },
];
ImgFallbackModule.ctorParameters = function () { return []; };

export { ImgFallbackModule, ImgFallbackDirective };
//# sourceMappingURL=ngx-img-fallback.js.map
