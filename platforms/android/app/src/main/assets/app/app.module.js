"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var orderapproval_service_1 = require("./Services/orderapproval.service");
var getorder_service_1 = require("./Services/getorder.service");
var sendorder_service_1 = require("./Services/sendorder.service");
var product_service_1 = require("./Services/product.service");
var login_service_1 = require("./Services/login.service");
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var login_component_1 = require("./Components/Login/login.component");
var account_component_1 = require("./Components/Account/account.component");
var element_registry_1 = require("nativescript-angular/element-registry");
var customer_service_1 = require("./Services/customer.service");
var http_1 = require("nativescript-angular/http");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var saveorder_service_1 = require("./Services/saveorder.service");
var dialog_component_1 = require("./Components/Dialog/dialog.component");
var dialogshipping_component_1 = require("./Components/Dialog_shippingaddress/dialogshipping.component");
var reports_service_1 = require("./Services/reports.service");
var saveorder_service_2 = require("./Services/saveorder.service");
// import { LimitToDirective } from './Services/limit-to.directive';
element_registry_1.registerElement("CardView", function () { return require("nativescript-cardview").CardView; });
element_registry_1.registerElement("CheckBox", function () { return require("nativescript-checkbox").CheckBox; });
element_registry_1.registerElement("Fab", function () { return require("nativescript-floatingactionbutton").Fab; });
element_registry_1.registerElement("DropDown", function () { return require("nativescript-drop-down").DropDown; });
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            nativescript_module_1.NativeScriptModule,
            angular_1.NativeScriptUISideDrawerModule,
            router_1.NativeScriptRouterModule,
            router_1.NativeScriptRouterModule.forRoot(app_routing_1.routes),
            http_1.NativeScriptHttpModule
        ],
        declarations: [
            app_component_1.AppComponent,
            login_component_1.LoginComponent,
            app_routing_1.navigatableComponents,
            dialog_component_1.Dialog,
            dialogshipping_component_1.DialogShipping,
            account_component_1.AccountComponent
            // LimitToDirective
        ],
        providers: [
            customer_service_1.CustomerService,
            saveorder_service_1.SaveOrderService,
            login_service_1.loginService,
            product_service_1.ProductService,
            saveorder_service_1.SaveCustomer,
            sendorder_service_1.SendOrder,
            saveorder_service_1.SaveTotal,
            product_service_1.SaveProducts,
            saveorder_service_1.SaveCustomerDetails,
            saveorder_service_1.SaveShipping,
            saveorder_service_1.SaveNotes,
            getorder_service_1.GetOrder,
            orderapproval_service_1.OrderApproval,
            product_service_1.SaveOffers,
            saveorder_service_1.SaveEmployeeId,
            reports_service_1.ReportService,
            saveorder_service_2.SaveUpdateOrderId,
            saveorder_service_2.SaveUpdateOrder_Id
        ],
        entryComponents: [
            dialog_component_1.Dialog,
            dialogshipping_component_1.DialogShipping
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
