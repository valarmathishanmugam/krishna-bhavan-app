"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("./Services/login.service");
var saveorder_service_1 = require("./Services/saveorder.service");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var core_1 = require("@angular/core");
// import { RouterExtensions } from 'nativescript-angular/router';
var nativescript_angular_1 = require("nativescript-angular");
var router_1 = require("@angular/router");
var connectivity = require("connectivity");
var page_1 = require("tns-core-modules/ui/page/page");
var dialogs = require("ui/dialogs");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var nativescript_exit_1 = require("nativescript-exit");
var AppComponent = (function () {
    function AppComponent(_router, _changeDetectionRef, routerExtensions, page, saveorderservice, router, savecustomer, loginservice, location) {
        this._router = _router;
        this._changeDetectionRef = _changeDetectionRef;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.saveorderservice = saveorderservice;
        this.router = router;
        this.savecustomer = savecustomer;
        this.loginservice = loginservice;
        this.location = location;
        this.gesture = "false";
        this.offerlistvisible = "visible";
        this.productlistvisible = "visible";
        this.customerlistvisible = "visible";
        this.managerorderlist = "visible";
        this.orderlist = "visible";
        this.approvecus = "visible";
        this.reportlistvisible = "visible";
        page.bindingContext = { actionbartitle: "Krishna Bhavan" };
        this.connectionType = "Unknown";
    }
    AppComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            _this.drawer.closeDrawer();
            // alert("Back Pressed")
            //this.location.back();
            _this.temperaryuser = _this.loginservice.getScope();
            if (_this.temperaryuser["role"] === "rep") {
                if (_this._router.url === '/customerlist') {
                    _this.getDialog();
                }
                else {
                    _this.routerExtensions.back();
                }
            }
            else if (_this.temperaryuser["role"] === "manager") {
                if (_this._router.url === '/managerorderlist') {
                    _this.getDialog();
                }
                else {
                    _this.routerExtensions.back();
                }
            }
            else if (_this.temperaryuser["role"] === "customer") {
                if (_this._router.url === '/productlist') {
                    _this.getDialog();
                }
                else {
                    _this.routerExtensions.back();
                }
            }
        });
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.connectionType = this.connectionToString(connectivity.getConnectionType());
        connectivity.startMonitoring(function (connectionType) {
            _this.connectionType = _this.connectionToString(connectionType);
            if (connectionType == 1 || connectionType == 2) {
                console.log("Connected");
                _this.routerExtensions.navigate(["/login"], { clearHistory: true });
            }
            else {
                console.log("DisConnected");
                _this.routerExtensions.navigate(["/test"], { clearHistory: true });
            }
        });
    };
    AppComponent.prototype.usercustomnavbar = function () {
        this.tempuser = this.loginservice.getScope();
        if (this.tempuser["role"] === "manager") {
            //this.productlistvisible = "collapse"
            this.orderlist = "collapse";
        }
        else if (this.tempuser["role"] === "rep") {
            this.managerorderlist = "collapse";
            this.approvecus = "collapse";
        }
        else if (this.tempuser["role"] === "customer") {
            console.log("Navbar:" + this.tempuser["role"]);
            this.customerlistvisible = "collapse";
            this.reportlistvisible = "collapse";
            this.managerorderlist = "collapse";
            this.approvecus = "collapse";
        }
    };
    AppComponent.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case connectivity.connectionType.none:
                return "No Connection!";
            case connectivity.connectionType.wifi:
                return "Connected to WiFi!";
            case connectivity.connectionType.mobile:
                return "Connected to Cellular!";
            default:
                return "Unknown";
        }
    };
    AppComponent.prototype.openDrawer = function () {
        this.drawer.toggleDrawerState();
        this.usercustomnavbar();
    };
    AppComponent.prototype.orderdetails = function () {
        this.tempuser = this.loginservice.getScope();
        this.products = this.saveorderservice.getScope();
        this.customerid = this.savecustomer.getScope();
        console.log("Cart:" + this.tempuser.role);
        console.log("Products: " + this.products + " Customer: " + this.customerid);
        if (this.customerid !== false && this.products !== false) {
            if (this.customerid.length == 0) {
                //alert("select Products");
                dialogs.alert({
                    title: "Empty ",
                    message: "Select Customer",
                    okButtonText: "Ok"
                }).then(function () {
                });
            }
            else if (this.products.length == 0) {
                //alert("select Products");
                dialogs.alert({
                    title: "Empty",
                    message: "Add Products",
                    okButtonText: "Ok"
                }).then(function () {
                });
            }
            else {
                console.log("Not Null");
                console.log("Length: " + this.products.length);
                this.routerExtensions.navigate(["/orderreview"]);
            }
        }
        else if (this.tempuser["role"] === "customer") {
            console.log("Go For Shop");
            dialogs.alert({
                title: "ORDER",
                message: "Select Products",
                okButtonText: "Ok"
            }).then(function () {
            });
        }
        else if (this.tempuser["role"] === "rep") {
            console.log("Go For Shop");
            dialogs.alert({
                title: "ORDER",
                message: "Select Customer and Products",
                okButtonText: "Ok"
            }).then(function () {
            });
        }
        else if (this.tempuser["role"] === "manager") {
            console.log("Go For Shop");
            dialogs.alert({
                title: "ORDER",
                message: "Select Customer and Products",
                okButtonText: "Ok"
            }).then(function () {
            });
        }
    };
    AppComponent.prototype.close = function () {
        this.drawer.closeDrawer();
        //this.routerExtensions.navigate(["/managerorderlist"]);
    };
    AppComponent.prototype.exitApp = function () {
        this.drawer.closeDrawer();
        // if (!this.routerExtensions.canGoBackToPreviousPage()) {              
        var options = {
            title: "Exit App",
            message: "Are you sure？",
            okButtonText: "OK",
            cancelButtonText: "Cancel"
        };
        dialogs.confirm(options).then(function (result) {
            console.log(result);
            if (result) {
                console.log("Exit App");
                //frameModule.topmost().goBack();
                nativescript_exit_1.exit();
            }
            else if (typeof result == "undefined" || result == false) {
                return;
            }
        });
        // }
    };
    AppComponent.prototype.getDialog = function () {
        var _this = this;
        if (!this.routerExtensions.canGoBackToPreviousPage()) {
            var options = {
                title: "Exit App",
                message: "Are you sure？",
                okButtonText: "OK",
                cancelButtonText: "Cancel"
            };
            dialogs.confirm(options).then(function (result) {
                console.log(result);
                if (result) {
                    _this.exitApp();
                }
                else if (typeof result == "undefined" || result == false) {
                    return;
                }
            });
        }
    };
    return AppComponent;
}());
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], AppComponent.prototype, "drawerComponent", void 0);
AppComponent = __decorate([
    core_1.Component({
        selector: "main",
        moduleId: module.id,
        templateUrl: "app.component.html",
        styles: ["\n  ActionBar, .sideStackLayout{\n      color: #fff;\n  }\n  .navdata{\n    padding-left: 5%;\n    padding-top: 10%;\n    font-size: 20;\n  }\n  "]
    }),
    __metadata("design:paramtypes", [router_1.Router, core_1.ChangeDetectorRef, nativescript_angular_1.RouterExtensions,
        page_1.Page, saveorder_service_1.SaveOrderService, router_1.Router,
        saveorder_service_1.SaveCustomer, login_service_1.loginService, common_1.Location])
], AppComponent);
exports.AppComponent = AppComponent;
