"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var approve_customer_detail_component_1 = require("./Components/Approve_Customer_Detail/approve_customer_detail.component");
var approve_customer_list_component_1 = require("./Components/Approve_Customer/approve_customer_list.component");
var salesrepreport_component_1 = require("./Components/Sales_rep/SalesRepReport/salesrepreport.component");
var managerorderreview_component_1 = require("./Components/Sales_Manager/Manager_OrderReview/managerorderreview.component");
var actionbar_component_1 = require("./Components/ActionBar/actionbar.component");
var test_component_1 = require("./Components/test/test.component");
var productdetail_component_1 = require("./Components/Product_Detail/productdetail.component");
var customerinfo_component_1 = require("./Components/Customer_Info/customerinfo.component");
var cusorderlist_component_1 = require("./Components/customer/Order_List/cusorderlist.component");
var customerdetail_component_1 = require("./Components/Sales_rep/CustomerDetail/customerdetail.component");
var reporderreview_component_1 = require("./Components/Sales_rep/Order_Review/reporderreview.component");
var orderdetail_component_1 = require("./Components/Sales_rep/Order_details/orderdetail.component");
var reporderlist_component_1 = require("./Components/Sales_rep/Order_List/reporderlist.component");
var login_component_1 = require("./Components/Login/login.component");
var CustomerList_component_1 = require("./Components/Sales_rep/Customer_List/CustomerList.component");
var ProductList_component_1 = require("./Components/Product_List/ProductList.component");
var managercustomerdetail_component_1 = require("./Components/Sales_Manager/Customer_Detail/managercustomerdetail.component");
var managerorderlist_component_1 = require("./Components/Sales_Manager/Manager_OrderList/managerorderlist.component");
var createcustomer_component_1 = require("./Components/CreateCustomer/createcustomer.component");
var productoffers_component_1 = require("./Components/Product_Offers/productoffers.component");
var ordernotes_component_1 = require("./Components/Order_Notes/ordernotes.component");
var reportdetail_component_1 = require("./Components/Report_Detail/reportdetail.component");
var account_component_1 = require("./Components/Account/account.component");
exports.routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: login_component_1.LoginComponent },
    { path: "customerlist", component: CustomerList_component_1.CustomerList },
    { path: "productlist", component: ProductList_component_1.ProductList },
    { path: "orderlist", component: reporderlist_component_1.RepOrderList },
    { path: "orderdetail/:id/:id2", component: orderdetail_component_1.OrderDetail },
    { path: "orderreview", component: reporderreview_component_1.RepOrderReview },
    { path: "customerdetail/:id", component: customerdetail_component_1.CustomerDetail },
    { path: "customerorderlist", component: cusorderlist_component_1.CustomerOrderList },
    { path: "customerinfo", component: customerinfo_component_1.CustomerInfo },
    { path: "productdetail", component: productdetail_component_1.ProductDetail },
    { path: "managercustomerdetail", component: managercustomerdetail_component_1.ManagerCustomerDetail },
    { path: "test", component: test_component_1.Test },
    { path: "managerorderlist", component: managerorderlist_component_1.ManagerOrderList },
    { path: "actionbar", component: actionbar_component_1.ActionBar },
    { path: "managerorderreview/:id", component: managerorderreview_component_1.ManagerOrderReview },
    { path: "salesrepreport", component: salesrepreport_component_1.SalesRepReport },
    { path: "createcustomer", component: createcustomer_component_1.CreateCustomer },
    { path: "approvecustomerlist", component: approve_customer_list_component_1.ApproveCustomerList },
    { path: "approvecustomerdetail/:id", component: approve_customer_detail_component_1.ApproveCustomerDetail },
    { path: "productoffers", component: productoffers_component_1.ProductOffers },
    { path: "ordernotes", component: ordernotes_component_1.OrderNotes },
    { path: "reportdetail", component: reportdetail_component_1.ReportDetail },
    { path: "accountdetails", component: account_component_1.AccountComponent },
];
exports.navigatableComponents = [
    login_component_1.LoginComponent,
    CustomerList_component_1.CustomerList,
    ProductList_component_1.ProductList,
    reporderlist_component_1.RepOrderList,
    orderdetail_component_1.OrderDetail,
    reporderreview_component_1.RepOrderReview,
    customerdetail_component_1.CustomerDetail,
    cusorderlist_component_1.CustomerOrderList,
    customerinfo_component_1.CustomerInfo,
    productdetail_component_1.ProductDetail,
    managercustomerdetail_component_1.ManagerCustomerDetail,
    managerorderlist_component_1.ManagerOrderList,
    test_component_1.Test,
    actionbar_component_1.ActionBar,
    managerorderreview_component_1.ManagerOrderReview,
    salesrepreport_component_1.SalesRepReport,
    createcustomer_component_1.CreateCustomer,
    approve_customer_list_component_1.ApproveCustomerList,
    approve_customer_detail_component_1.ApproveCustomerDetail,
    productoffers_component_1.ProductOffers,
    ordernotes_component_1.OrderNotes,
    reportdetail_component_1.ReportDetail,
    account_component_1.AccountComponent
];
