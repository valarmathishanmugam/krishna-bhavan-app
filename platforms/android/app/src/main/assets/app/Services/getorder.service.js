"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var login_service_1 = require("./login.service");
var GetOrder = (function () {
    function GetOrder(http, loginservice) {
        this.http = http;
        this.loginservice = loginservice;
    }
    GetOrder.prototype.getorderbyid = function (id) {
        //console.log("Order_Id: "+ id)
        return this.http.get("https://app.krishnabhavanfoods.in/api/orders/get_orders/id/" + id);
    };
    GetOrder.prototype.getCommonHeaders = function () {
        var headers = new Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    GetOrder.prototype.getmanagerorders = function (id) {
        this.tempuser = this.loginservice.getScope();
        if (id != undefined) {
            console.log("requser service id: " + id);
            return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/" + id + "/orders");
        }
        else {
            id = this.tempuser.id;
            console.log("user service id: " + id);
            if (this.tempuser.role === "manager") {
                return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getmanager/id/" + this.tempuser.id + "/orders");
            }
            else if (this.tempuser.role === "rep") {
                return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getsalesrep/id/" + this.tempuser.id + "/orders");
            }
            else if (this.tempuser.role === "customer") {
                //console.log("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/"+this.tempuser.id+"/orders")
                return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/" + this.tempuser.id + "/orders");
            }
            // else if(this.tempuser.role === "admin"){
            //   return this.http.get("https://app.krishnabhavanfoods.in/api/orders/getorders/")
            // }
        }
        //return this.http.get("https://app.krishnabhavanfoods.in/api/orders/getorders/");
    };
    return GetOrder;
}());
GetOrder = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, login_service_1.loginService])
], GetOrder);
exports.GetOrder = GetOrder;
