"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//import { Observable as RxObservable } from "rxjs/Observable";
var http_1 = require("@angular/http");
var ProductService = (function () {
    function ProductService(http) {
        this.http = http;
        this.getproductsUrl = "https://app.krishnabhavanfoods.in/api/product/getproducts";
    }
    ProductService.prototype.getproduct = function () {
        return this.http.get(this.getproductsUrl);
    };
    ProductService.prototype.getDiscounts = function () {
        return this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts");
    };
    return ProductService;
}());
ProductService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ProductService);
exports.ProductService = ProductService;
var SaveProducts = (function () {
    function SaveProducts() {
        this.scope = false;
    }
    SaveProducts.prototype.getScope = function () {
        return this.scope;
    };
    SaveProducts.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveProducts;
}());
exports.SaveProducts = SaveProducts;
var SaveOffers = (function () {
    function SaveOffers() {
        this.scope = false;
    }
    SaveOffers.prototype.getScope = function () {
        return this.scope;
    };
    SaveOffers.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveOffers;
}());
exports.SaveOffers = SaveOffers;
