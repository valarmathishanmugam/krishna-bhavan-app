/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.facebook.drawee;

public final class R {
    public static final class attr {
        public static final int actualImageScaleType = 0x7f030022;
        public static final int actualImageUri = 0x7f030023;
        public static final int backgroundImage = 0x7f03002e;
        public static final int fadeDuration = 0x7f030090;
        public static final int failureImage = 0x7f030091;
        public static final int failureImageScaleType = 0x7f030092;
        public static final int overlayImage = 0x7f0300dd;
        public static final int placeholderImage = 0x7f0300ea;
        public static final int placeholderImageScaleType = 0x7f0300eb;
        public static final int pressedStateOverlayImage = 0x7f0300f3;
        public static final int progressBarAutoRotateInterval = 0x7f0300f5;
        public static final int progressBarImage = 0x7f0300f6;
        public static final int progressBarImageScaleType = 0x7f0300f7;
        public static final int retryImage = 0x7f030101;
        public static final int retryImageScaleType = 0x7f030102;
        public static final int roundAsCircle = 0x7f030105;
        public static final int roundBottomLeft = 0x7f030106;
        public static final int roundBottomRight = 0x7f030107;
        public static final int roundTopLeft = 0x7f030108;
        public static final int roundTopRight = 0x7f030109;
        public static final int roundWithOverlayColor = 0x7f03010a;
        public static final int roundedCornerRadius = 0x7f03010b;
        public static final int roundingBorderColor = 0x7f03010c;
        public static final int roundingBorderPadding = 0x7f03010d;
        public static final int roundingBorderWidth = 0x7f03010e;
        public static final int viewAspectRatio = 0x7f030177;
    }
    public static final class id {
        public static final int center = 0x7f080020;
        public static final int centerCrop = 0x7f080021;
        public static final int centerInside = 0x7f080022;
        public static final int fitCenter = 0x7f080044;
        public static final int fitEnd = 0x7f080045;
        public static final int fitStart = 0x7f080046;
        public static final int fitXY = 0x7f080047;
        public static final int focusCrop = 0x7f080049;
        public static final int none = 0x7f080067;
    }
    public static final class styleable {
        public static final int[] GenericDraweeHierarchy = { 0x7f030022, 0x7f03002e, 0x7f030090, 0x7f030091, 0x7f030092, 0x7f0300dd, 0x7f0300ea, 0x7f0300eb, 0x7f0300f3, 0x7f0300f5, 0x7f0300f6, 0x7f0300f7, 0x7f030101, 0x7f030102, 0x7f030105, 0x7f030106, 0x7f030107, 0x7f030108, 0x7f030109, 0x7f03010a, 0x7f03010b, 0x7f03010c, 0x7f03010d, 0x7f03010e, 0x7f030177 };
        public static final int GenericDraweeHierarchy_actualImageScaleType = 0;
        public static final int GenericDraweeHierarchy_backgroundImage = 1;
        public static final int GenericDraweeHierarchy_fadeDuration = 2;
        public static final int GenericDraweeHierarchy_failureImage = 3;
        public static final int GenericDraweeHierarchy_failureImageScaleType = 4;
        public static final int GenericDraweeHierarchy_overlayImage = 5;
        public static final int GenericDraweeHierarchy_placeholderImage = 6;
        public static final int GenericDraweeHierarchy_placeholderImageScaleType = 7;
        public static final int GenericDraweeHierarchy_pressedStateOverlayImage = 8;
        public static final int GenericDraweeHierarchy_progressBarAutoRotateInterval = 9;
        public static final int GenericDraweeHierarchy_progressBarImage = 10;
        public static final int GenericDraweeHierarchy_progressBarImageScaleType = 11;
        public static final int GenericDraweeHierarchy_retryImage = 12;
        public static final int GenericDraweeHierarchy_retryImageScaleType = 13;
        public static final int GenericDraweeHierarchy_roundAsCircle = 14;
        public static final int GenericDraweeHierarchy_roundBottomLeft = 15;
        public static final int GenericDraweeHierarchy_roundBottomRight = 16;
        public static final int GenericDraweeHierarchy_roundTopLeft = 17;
        public static final int GenericDraweeHierarchy_roundTopRight = 18;
        public static final int GenericDraweeHierarchy_roundWithOverlayColor = 19;
        public static final int GenericDraweeHierarchy_roundedCornerRadius = 20;
        public static final int GenericDraweeHierarchy_roundingBorderColor = 21;
        public static final int GenericDraweeHierarchy_roundingBorderPadding = 22;
        public static final int GenericDraweeHierarchy_roundingBorderWidth = 23;
        public static final int GenericDraweeHierarchy_viewAspectRatio = 24;
        public static final int[] SimpleDraweeView = { 0x7f030023 };
        public static final int SimpleDraweeView_actualImageUri = 0;
    }
}
