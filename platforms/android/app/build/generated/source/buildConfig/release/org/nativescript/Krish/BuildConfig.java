/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.nativescript.Krish;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "org.nativescript.Krish";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "1.10";
}
