"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var order_model_1 = require("../../../Models/order.model");
var customer_model_1 = require("../../../Models/customer.model");
var getorder_service_1 = require("../../../Services/getorder.service");
var product_model_1 = require("../../../Models/product.model");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var router_2 = require("@angular/router");
var common_1 = require("@angular/common");
var OrderDetail = (function () {
    function OrderDetail(routerExtensions, page, getorderservice, route, loginservice, location) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.getorderservice = getorderservice;
        this.route = route;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Order Review";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.allnotes = new Array();
    }
    OrderDetail.prototype.ngOnInit = function () {
        var _this = this;
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        var id = this.route.snapshot.params["id"];
        this.formattedOrderId = this.route.snapshot.params["id2"];
        this.getorderservice
            .getorderbyid(id)
            .subscribe(function (data) { return _this.getallorders(data.json()); });
        var tempcustomer = this.loginservice.getScope();
        this.checkcustomer(tempcustomer);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   data.cancel = true;
        //   //alert("Back Pressed")
        //   //this.location.back();
        //   this.routerExtensions.backToPreviousPage();
        // });
    };
    OrderDetail.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            this.ifcustomer = "visible";
        }
    };
    OrderDetail.prototype.getallorders = function (data) {
        console.log("All Data: " + JSON.stringify(data));
        console.log("products.length: " + data[0].line_items.length);
        //data = data._body;
        this.order_id = data[0]._id.$oid;
        this.order_date = data[0].order_date;
        /*
        console.log("Actual Date: " + this.tempdate);
        this.order_date = new Date();
        this.order_date = this.tempdate.getDate();
        console.log("Changed Date: " + this.order_date);
        */
        //this.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
        var tempstatus = data[0].status;
        this.customer.shopname = data[0].shop_name;
        console.log("shopname: " + this.customer.shopname);
        this.customer.contactname = data[0].customer_name;
        this.address.shippingmode = data[0].preferred_shipping;
        this.address.area = data[0].shipping_address.area;
        this.address.city = data[0].shipping_address.city;
        this.address.door_no = data[0].shipping_address.door_no;
        this.address.pin = data[0].shipping_address.pin;
        this.address.street_name = data[0].shipping_address.street_name;
        this.address.district = data[0].shipping_address.district;
        this.address.state = data[0].shipping_address.state;
        this.customer.address = this.address;
        this.total_amount = data[0].total_amount;
        if (data[0].notes) {
            //console.log("My Notes")
            console.log(data[0].customer_name + " : " + data[0].notes.length);
            for (var a = 0; a < data[0].notes.length; a++) {
                this.notes = new order_model_1.Note();
                console.log("My Notes: " + data[0].notes[a].note);
                this.notes.created_date = data[0].notes[a].created_date;
                this.notes.note = data[0].notes[a].note;
                this.notes.created_by = data[0].notes[a].created_by;
                this.allnotes.push(this.notes);
            }
            if (data[0].notes.length == 0) {
                if (data[0].current_note) {
                    this.notes = new order_model_1.Note();
                    console.log("Current Notes: " + data[0].current_note.note);
                    this.notes.created_date = data[0].current_note.created_date;
                    this.notes.note = data[0].current_note.note;
                    this.notes.created_by = data[0].current_note.created_by;
                    this.allnotes.push(this.notes);
                }
                else { }
            }
        }
        // else{}
        for (var i = 0; i < data[0].line_items.length; i++) {
            this.product = new product_model_1.Product();
            // this.product.offer = new Offer(); // commented by valar for hide discount
            this.product.product_id = data[0].line_items[i].product_id.$oid;
            console.log("productname: " + data[0].line_items[i].product_name);
            this.product.sku_id = data[0].line_items[i].sku_id.$oid;
            this.product.product_name = data[0].line_items[i].product_name;
            this.product.line_total = data[0].line_items[i].line_total;
            this.product.rack_quantity = data[0].line_items[i].rack_quantity;
            this.product.quantity = data[0].line_items[i].quantity;
            // commented by valar for hide discount - start
            // if(data[0].line_items[i].discount_id){
            //   var nodiscount = false;
            //   this.product.discount_id = data[0].line_items[i].discount_id.$oid;
            //   if(data[0].line_items[i].discount_details){
            //   this.product.discount_product_quantity = data[0].line_items[i].discount_product_quantity;
            //   this.product.offer.discount_product_name = data[0].line_items[i].discount_details[1].discount_product_name;
            //   }
            // }
            // else{
            //   var nodiscount = true;
            // }
            // commented by valar for hide discount - end
            this.product.product_price = data[0].line_items[i].product_price;
            this.allproducts.push(this.product);
        }
        console.log("All Product details: " + JSON.stringify(this.productDetails));
        if (tempstatus.toLowerCase() === "hold") {
            console.log("status: Hold");
            this.order_status = "~/Images/hold.png";
        }
        else if (tempstatus.toLowerCase() === "pending") {
            console.log("status: not_reviewed");
            this.order_status = "~/Images/not_reviewed.png";
        }
        else if (tempstatus.toLowerCase() === "processed") {
            console.log("status: completed");
            this.order_status = "~/Images/completed.png";
        }
        else if (tempstatus.toLowerCase() === "approved") {
            console.log("status: approved");
            this.order_status = "~/Images/approved.png";
        }
    };
    return OrderDetail;
}());
OrderDetail = __decorate([
    core_1.Component({
        selector: "order-detail",
        moduleId: module.id,
        templateUrl: "orderdetail.component.html",
        styleUrls: ["orderdetail.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        login_service_1.loginService,
        common_1.Location])
], OrderDetail);
exports.OrderDetail = OrderDetail;
