"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var application_1 = require("application");
var application = require("application");
var http_1 = require("@angular/http");
var login_service_1 = require("../../Services/login.service");
var EmailValidator = require("email-validator");
var dialogs = require("ui/dialogs");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var CreateCustomer = (function () {
    function CreateCustomer(routerExtensions, location, http, loginservice) {
        this.routerExtensions = routerExtensions;
        this.location = location;
        this.http = http;
        this.loginservice = loginservice;
        this.disableCreateFAB = true;
        this.loader = new LoadingIndicator();
    }
    CreateCustomer.prototype.ngOnInit = function () {
        var _this = this;
        console.log(" In Add Customer 1");
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
        this.tempuser = this.loginservice.getScope();
    };
    CreateCustomer.prototype.createcustomer = function (name, shopname, primarymobile, region, contactperson, landlineno, homemobileno, workmobileno, email, gst) {
        var _this = this;
        if (gst) {
            gst = String(gst).toUpperCase();
        }
        var tf1 = this.tempbdoor.nativeElement;
        this.bdoorno = tf1.text;
        var tf2 = this.tempbstreet.nativeElement;
        this.bstreet = tf2.text;
        var tf3 = this.tempbarea.nativeElement;
        this.barea = tf3.text;
        var tf4 = this.tempbcity.nativeElement;
        this.bcity = tf4.text;
        var tf5 = this.tempbdistrict.nativeElement;
        this.bdistrict = tf5.text;
        var tf6 = this.tempbstate.nativeElement;
        this.bstate = tf6.text;
        var tf7 = this.tempbpin.nativeElement;
        this.bpin = tf7.text;
        var tf8 = this.tempsdoor.nativeElement;
        this.sdoorno = tf8.text;
        var tf9 = this.tempsstreet.nativeElement;
        this.sstreet = tf9.text;
        var tf10 = this.tempsarea.nativeElement;
        this.sarea = tf10.text;
        var tf11 = this.tempscity.nativeElement;
        this.scity = tf11.text;
        var tf12 = this.tempsdistrict.nativeElement;
        this.sdistrict = tf12.text;
        var tf13 = this.tempsstate.nativeElement;
        this.sstate = tf13.text;
        var tf14 = this.tempspin.nativeElement;
        this.spin = tf14.text;
        var emailIsValid = EmailValidator.validate(email);
        var errorFlag = false;
        console.log("EMAIL VALIDATION" + emailIsValid);
        if (name === "") {
            alert("Enter Name");
            errorFlag = true;
        }
        else if (shopname === "") {
            alert("Enter Shop Name");
            errorFlag = true;
        }
        else if (primarymobile === "") {
            alert("Enter Primary Mobile Number");
            errorFlag = true;
        }
        else if (primarymobile.length < 10 || primarymobile.length > 10) {
            alert("Primary Mobile Number Should be 10 digits");
            errorFlag = true;
        }
        else if (region === "") {
            alert("Enter Region");
            errorFlag = true;
        }
        else if (contactperson === "") {
            alert("Enter Contact Person");
            errorFlag = true;
        }
        else if (email === "") {
            alert("Enter Email");
            errorFlag = true;
        }
        else if (emailIsValid == false) {
            alert("Enter Valid Email Address");
            errorFlag = true;
        }
        else if (gst === "") {
            alert("Enter GST");
            errorFlag = true;
        }
        else if (gst.length < 15 || gst.length > 15) {
            alert("GST Number Length Should be 15");
            errorFlag = true;
        }
        else if (this.bdoorno === "") {
            alert("Enter Billing Address Door No");
            errorFlag = true;
        }
        else if (this.bstreet === "") {
            alert("Enter Billing Address Street");
            errorFlag = true;
        }
        else if (this.barea === "") {
            alert("Enter Billing Address Area");
            errorFlag = true;
        }
        else if (this.bcity === "") {
            alert("Enter Billing Address City");
            errorFlag = true;
        }
        else if (this.bdistrict === "") {
            alert("Enter Billing Address District");
            errorFlag = true;
        }
        else if (this.bstate === "") {
            alert("Enter Billing Address State");
            errorFlag = true;
        }
        else if (this.bpin === "") {
            alert("Enter Billing Address Pin");
            errorFlag = true;
        }
        else if (this.bpin.length < 6 || this.bpin.length > 6) {
            alert("Pin Number Should be 6 digits");
            errorFlag = true;
        }
        if (homemobileno) {
            if (homemobileno.length < 10 || homemobileno.length > 10) {
                alert("Home Mobile Number Should be 10 digits");
                errorFlag = true;
            }
        }
        if (workmobileno) {
            if (workmobileno.length < 10 || workmobileno.length > 10) {
                alert("Work Mobile Number Should be 10 digits");
                errorFlag = true;
            }
        }
        /*
        else if(this.sdoorno === ""){
          //alert("Enter this.bdoorno")
          this.sdoorno = this.bdoorno
        }
        else if(this.sstreet === ""){
          //alert("Enter name")
          this.sstreet = this.bstreet
        }
        else if(this.sarea === ""){
          //alert("Enter name")
          this.sarea = this.barea
        }
        else if(this.scity === ""){
          //alert("Enter name")
          this.scity = this.bcity
        }
        else if(this.sdistrict === ""){
         // alert("Enter name")
          this.sdistrict = this.bdistrict
        }
        else if(this.sstate === ""){
         //alert("Enter name")
         this.sstate = this.bstate
        }
        else if(this.spin === ""){
          alert("Enter name")
          this.spin = this.bpin
        }
    */
        if (!errorFlag) {
            //code for loading indicator start here
            var options = {
                message: 'Loading...',
                progress: 0.65,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    details: "Additional detail note!",
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#4B9ED6",
                    mode: "" // see iOS specific options below
                }
            };
            this.loader.show(options);
            //code for loading indicator end here
            console.log("In post");
            this.data = {
                name: name,
                gst_no: gst,
                employee_id: this.tempuser.id,
                shop_name: shopname,
                region: region,
                status: "pending",
                contact_details: {
                    work_mobile_no: workmobileno,
                    home_mobile_no: homemobileno,
                    landline_no: landlineno,
                    email: email,
                    contact_person: contactperson
                },
                primary_mobile_no: primarymobile,
                address: [
                    {
                        door_no: this.bdoorno,
                        street_name: this.bstreet,
                        area: this.barea,
                        city: this.bcity,
                        district: this.bdistrict,
                        state: this.bstate,
                        status: "active",
                        pin: this.bpin
                    },
                    {
                        door_no: this.bdoorno,
                        street_name: this.bstreet,
                        area: this.barea,
                        city: this.bcity,
                        district: this.bdistrict,
                        state: this.bstate,
                        status: "active",
                        pin: this.bpin
                    }
                ]
            };
            console.log("Full Customer Data: " + JSON.stringify(this.data));
            return this.http
                .post("https://app.krishnabhavanfoods.in/api/customer/addcustomer/", this.data, { headers: this.getCommonHeaders() })
                .subscribe(function (result) {
                _this.getresult(result);
                //console.log("Customer Result: " + JSON.stringify(result.json))
            }, function (error) {
                console.log("Customer Error: " + error);
                _this.customerCreateerror(error);
            });
        }
    };
    CreateCustomer.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    CreateCustomer.prototype.getresult = function (result) {
        var _this = this;
        this.loader.hide();
        this.disableCreateFAB = false;
        dialogs.alert("Customer Is Created Successfully").then(function () {
            console.log("Customer Result: " + JSON.stringify(result));
            if (_this.tempuser["role"] === "customer") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "rep") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "manager") {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
            }
        });
    };
    CreateCustomer.prototype.customerCreateerror = function (error) {
        var _this = this;
        dialogs.alert("Customer Is Not Created").then(function () {
            if (_this.tempuser["role"] === "customer") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "rep") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "manager") {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
            }
        });
    };
    return CreateCustomer;
}());
__decorate([
    core_1.ViewChild("bdoorno"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbdoor", void 0);
__decorate([
    core_1.ViewChild("bstreet"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbstreet", void 0);
__decorate([
    core_1.ViewChild("barea"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbarea", void 0);
__decorate([
    core_1.ViewChild("bcity"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbcity", void 0);
__decorate([
    core_1.ViewChild("bdistrict"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbdistrict", void 0);
__decorate([
    core_1.ViewChild("bstate"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbstate", void 0);
__decorate([
    core_1.ViewChild("bpin"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbpin", void 0);
__decorate([
    core_1.ViewChild("sdoorno"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsdoor", void 0);
__decorate([
    core_1.ViewChild("sstreet"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsstreet", void 0);
__decorate([
    core_1.ViewChild("sarea"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsarea", void 0);
__decorate([
    core_1.ViewChild("scity"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempscity", void 0);
__decorate([
    core_1.ViewChild("sdistrict"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsdistrict", void 0);
__decorate([
    core_1.ViewChild("sstate"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsstate", void 0);
__decorate([
    core_1.ViewChild("spin"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempspin", void 0);
CreateCustomer = __decorate([
    core_1.Component({
        selector: "create-customer",
        moduleId: module.id,
        templateUrl: "createcustomer.component.html",
        styleUrls: ["createcustomer.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        common_1.Location,
        http_1.Http,
        login_service_1.loginService])
], CreateCustomer);
exports.CreateCustomer = CreateCustomer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlY3VzdG9tZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY3JlYXRlY3VzdG9tZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWlFO0FBRWpFLDBDQUEyQztBQUMzQyxzREFBK0Q7QUFDL0QsMkNBR3FCO0FBQ3JCLHlDQUEyQztBQUMzQyxzQ0FBd0Q7QUFDeEQsOERBQTREO0FBQzVELGdEQUFrRDtBQUNsRCxvQ0FBc0M7QUFDdEMsSUFBSSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztBQVF0RixJQUFhLGNBQWM7SUErQ3pCLHdCQUNVLGdCQUFrQyxFQUNsQyxRQUFrQixFQUNsQixJQUFVLEVBQ1YsWUFBMEI7UUFIMUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQXRCcEMscUJBQWdCLEdBQVksSUFBSSxDQUFDO1FBQ2pDLFdBQU0sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7SUFzQjdCLENBQUM7SUFFSixpQ0FBUSxHQUFSO1FBQUEsaUJBWUM7UUFYQyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDbEMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQ3BCLGdDQUFrQixDQUFDLHdCQUF3QixFQUMzQyxVQUFDLElBQXlDO1lBQ3hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUNGLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFDRSxJQUFJLEVBQ0osUUFBUSxFQUNSLGFBQWEsRUFDYixNQUFNLEVBQ04sYUFBYSxFQUNiLFVBQVUsRUFDVixZQUFZLEVBQ1osWUFBWSxFQUNaLEtBQUssRUFDTCxHQUFHO1FBVkwsaUJBdU9DO1FBM05DLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDUCxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xDLENBQUM7UUFDRCxJQUFJLEdBQUcsR0FBYyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUNsRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxHQUFHLEdBQWMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7UUFDcEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ3hCLElBQUksR0FBRyxHQUFjLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDO1FBQ2xELElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLEdBQUcsR0FBYyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxHQUFHLEdBQWMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7UUFDdEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQzFCLElBQUksR0FBRyxHQUFjLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBQ25ELElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLEdBQUcsR0FBYyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQztRQUNqRCxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDckIsSUFBSSxHQUFHLEdBQWMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7UUFDbEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ3hCLElBQUksR0FBRyxHQUFjLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztRQUN4QixJQUFJLElBQUksR0FBYyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUNuRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdkIsSUFBSSxJQUFJLEdBQWMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7UUFDbkQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksSUFBSSxHQUFjLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMzQixJQUFJLElBQUksR0FBYyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztRQUNwRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxJQUFJLEdBQWMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDbEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RCLElBQUksWUFBWSxHQUFHLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFbEQsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBRXRCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsWUFBWSxDQUFDLENBQUM7UUFDL0MsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEIsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3BCLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMzQixLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUN6QixTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsYUFBYSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDckMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsRUFBRSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNsRSxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztZQUNuRCxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3RCLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxhQUFhLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNoQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUM5QixTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEIsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3JCLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNqQyxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdEIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ25CLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDOUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDeEMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMvQixLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztZQUN2QyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQy9CLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQ3RDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0IsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDcEMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM3QixLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUNwQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1lBQ3hDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDOUIsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDckMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM1QixLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEQsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7WUFDdkMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNqQixFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2dCQUNoRCxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ25CLENBQUM7UUFDSCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNqQixFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2dCQUNoRCxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ25CLENBQUM7UUFDSCxDQUFDO1FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BNkJGO1FBQ0UsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2YsdUNBQXVDO1lBQ3ZDLElBQUksT0FBTyxHQUFHO2dCQUNaLE9BQU8sRUFBRSxZQUFZO2dCQUNyQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxPQUFPLEVBQUU7b0JBQ1AsYUFBYSxFQUFFLElBQUk7b0JBQ25CLFVBQVUsRUFBRSxLQUFLO29CQUNqQixHQUFHLEVBQUUsR0FBRztvQkFDUixvQkFBb0IsRUFBRSxTQUFTO29CQUMvQixxQkFBcUIsRUFBRSxJQUFJO29CQUMzQixhQUFhLEVBQUUsQ0FBQztvQkFDaEIsaUJBQWlCLEVBQUUsQ0FBQztpQkFDckI7Z0JBQ0QsR0FBRyxFQUFFO29CQUNILE9BQU8sRUFBRSx5QkFBeUI7b0JBQ2xDLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRSxFQUFFO29CQUNWLGFBQWEsRUFBRSxJQUFJO29CQUNuQixLQUFLLEVBQUUsU0FBUztvQkFDaEIsSUFBSSxFQUFFLEVBQUUsQ0FBQSxpQ0FBaUM7aUJBQzFDO2FBQ0YsQ0FBQztZQUVGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pCLHFDQUFxQztZQUV0QyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXZCLElBQUksQ0FBQyxJQUFJLEdBQUc7Z0JBQ1YsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDN0IsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLE1BQU0sRUFBQyxNQUFNO2dCQUNiLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixlQUFlLEVBQUU7b0JBQ2YsY0FBYyxFQUFFLFlBQVk7b0JBQzVCLGNBQWMsRUFBRSxZQUFZO29CQUM1QixXQUFXLEVBQUUsVUFBVTtvQkFDdkIsS0FBSyxFQUFFLEtBQUs7b0JBQ1osY0FBYyxFQUFFLGFBQWE7aUJBQzlCO2dCQUNELGlCQUFpQixFQUFFLGFBQWE7Z0JBQ2hDLE9BQU8sRUFBRTtvQkFDUDt3QkFDRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87d0JBQ3JCLFdBQVcsRUFBRSxJQUFJLENBQUMsT0FBTzt3QkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLO3dCQUNoQixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUs7d0JBQ2hCLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUzt3QkFDeEIsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUNsQixNQUFNLEVBQUUsUUFBUTt3QkFDaEIsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJO3FCQUNmO29CQUNEO3dCQUNFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTzt3QkFDckIsV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPO3dCQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUs7d0JBQ2hCLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSzt3QkFDaEIsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTO3dCQUN4QixLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU07d0JBQ2xCLE1BQU0sRUFBRSxRQUFRO3dCQUNoQixHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUk7cUJBQ2Y7aUJBQ0Y7YUFDRixDQUFDO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBRWhFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSTtpQkFDYixJQUFJLENBQ0gsNkRBQTZELEVBQzdELElBQUksQ0FBQyxJQUFJLEVBQ1QsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FDckM7aUJBQ0EsU0FBUyxDQUNSLFVBQUEsTUFBTTtnQkFDSixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN2QixnRUFBZ0U7WUFDbEUsQ0FBQyxFQUNELFVBQUEsS0FBSztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEMsQ0FBQyxDQUNGLENBQUM7UUFDTixDQUFDO0lBQ0gsQ0FBQztJQUVELHlDQUFnQixHQUFoQjtRQUNFLElBQUksT0FBTyxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUNoRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFDRCxrQ0FBUyxHQUFULFVBQVUsTUFBTTtRQUFoQixpQkFhQztRQVpDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixPQUFPLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzFELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDakQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2pELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1lBQzNELENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCw0Q0FBbUIsR0FBbkIsVUFBb0IsS0FBSztRQUF6QixpQkFVQztRQVRDLE9BQU8sQ0FBQyxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDNUMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUN6QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDakQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7WUFDM0QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNILHFCQUFDO0FBQUQsQ0FBQyxBQTNVRCxJQTJVQztBQTNTdUI7SUFBckIsZ0JBQVMsQ0FBQyxTQUFTLENBQUM7OEJBQVksaUJBQVU7aURBQUM7QUFDdEI7SUFBckIsZ0JBQVMsQ0FBQyxTQUFTLENBQUM7OEJBQWMsaUJBQVU7bURBQUM7QUFDMUI7SUFBbkIsZ0JBQVMsQ0FBQyxPQUFPLENBQUM7OEJBQVksaUJBQVU7aURBQUM7QUFDdEI7SUFBbkIsZ0JBQVMsQ0FBQyxPQUFPLENBQUM7OEJBQVksaUJBQVU7aURBQUM7QUFDbEI7SUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7OEJBQWdCLGlCQUFVO3FEQUFDO0FBQzdCO0lBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDOzhCQUFhLGlCQUFVO2tEQUFDO0FBQ3pCO0lBQWxCLGdCQUFTLENBQUMsTUFBTSxDQUFDOzhCQUFXLGlCQUFVO2dEQUFDO0FBQ2xCO0lBQXJCLGdCQUFTLENBQUMsU0FBUyxDQUFDOzhCQUFZLGlCQUFVO2lEQUFDO0FBQ3RCO0lBQXJCLGdCQUFTLENBQUMsU0FBUyxDQUFDOzhCQUFjLGlCQUFVO21EQUFDO0FBQzFCO0lBQW5CLGdCQUFTLENBQUMsT0FBTyxDQUFDOzhCQUFZLGlCQUFVO2lEQUFDO0FBQ3RCO0lBQW5CLGdCQUFTLENBQUMsT0FBTyxDQUFDOzhCQUFZLGlCQUFVO2lEQUFDO0FBQ2xCO0lBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDOzhCQUFnQixpQkFBVTtxREFBQztBQUM3QjtJQUFwQixnQkFBUyxDQUFDLFFBQVEsQ0FBQzs4QkFBYSxpQkFBVTtrREFBQztBQUN6QjtJQUFsQixnQkFBUyxDQUFDLE1BQU0sQ0FBQzs4QkFBVyxpQkFBVTtnREFBQztBQTdDN0IsY0FBYztJQU4xQixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGlCQUFpQjtRQUMzQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLCtCQUErQjtRQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztLQUM1QyxDQUFDO3FDQWlENEIseUJBQWdCO1FBQ3hCLGlCQUFRO1FBQ1osV0FBSTtRQUNJLDRCQUFZO0dBbkR6QixjQUFjLENBMlUxQjtBQTNVWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgVGV4dEZpZWxkIH0gZnJvbSBcInVpL3RleHQtZmllbGRcIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7XHJcbiAgQW5kcm9pZEFwcGxpY2F0aW9uLFxyXG4gIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhXHJcbn0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCB7IGxvZ2luU2VydmljZSB9IGZyb20gXCIuLi8uLi9TZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlXCI7XHJcbmltcG9ydCAqIGFzIEVtYWlsVmFsaWRhdG9yIGZyb20gXCJlbWFpbC12YWxpZGF0b3JcIjtcclxuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidWkvZGlhbG9nc1wiO1xyXG52YXIgTG9hZGluZ0luZGljYXRvciA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtbG9hZGluZy1pbmRpY2F0b3ItbmV3XCIpLkxvYWRpbmdJbmRpY2F0b3I7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJjcmVhdGUtY3VzdG9tZXJcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcImNyZWF0ZWN1c3RvbWVyLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJjcmVhdGVjdXN0b21lci5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDcmVhdGVDdXN0b21lciB7XHJcbiAgbmFtZTogYW55O1xyXG4gIHNob3BuYW1lO1xyXG4gIHByaW1hcnltb2JpbGU7XHJcbiAgcmVnaW9uO1xyXG4gIGNvbnRhY3RwZXJzb247XHJcbiAgbGFuZGxpbmVubztcclxuICBob21lbW9iaWxlbm87XHJcbiAgd29ya21vYmlsZW5vO1xyXG4gIGVtYWlsO1xyXG4gIGdzdDtcclxuICBiZG9vcm5vO1xyXG4gIGJzdHJlZXQ7XHJcbiAgYmFyZWE7XHJcbiAgYmNpdHk7XHJcbiAgYmRpc3RyaWN0O1xyXG4gIGJzdGF0ZTtcclxuICBicGluO1xyXG4gIHNkb29ybm87XHJcbiAgc3N0cmVldDtcclxuICBzYXJlYTtcclxuICBzY2l0eTtcclxuICBzZGlzdHJpY3Q7XHJcbiAgc3N0YXRlO1xyXG4gIHNwaW47XHJcbiAgY3VzbmFtZTtcclxuICB0ZW1wZ3N0O1xyXG4gIGRhdGE6IGFueTtcclxuICB0ZW1wdXNlcjtcclxuICBkaXNhYmxlQ3JlYXRlRkFCIDpib29sZWFuID0gdHJ1ZTtcclxuICBsb2FkZXIgPSBuZXcgTG9hZGluZ0luZGljYXRvcigpO1xyXG5cclxuICBAVmlld0NoaWxkKFwiYmRvb3Jub1wiKSB0ZW1wYmRvb3I6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcImJzdHJlZXRcIikgdGVtcGJzdHJlZXQ6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcImJhcmVhXCIpIHRlbXBiYXJlYTogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwiYmNpdHlcIikgdGVtcGJjaXR5OiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJiZGlzdHJpY3RcIikgdGVtcGJkaXN0cmljdDogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwiYnN0YXRlXCIpIHRlbXBic3RhdGU6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcImJwaW5cIikgdGVtcGJwaW46IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcInNkb29ybm9cIikgdGVtcHNkb29yOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJzc3RyZWV0XCIpIHRlbXBzc3RyZWV0OiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJzYXJlYVwiKSB0ZW1wc2FyZWE6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcInNjaXR5XCIpIHRlbXBzY2l0eTogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwic2Rpc3RyaWN0XCIpIHRlbXBzZGlzdHJpY3Q6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcInNzdGF0ZVwiKSB0ZW1wc3N0YXRlOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJzcGluXCIpIHRlbXBzcGluOiBFbGVtZW50UmVmO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uLFxyXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwLFxyXG4gICAgcHJpdmF0ZSBsb2dpbnNlcnZpY2U6IGxvZ2luU2VydmljZVxyXG4gICkge31cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBJbiBBZGQgQ3VzdG9tZXIgMVwiKTtcclxuICAgIGFwcGxpY2F0aW9uLmFuZHJvaWQub24oXHJcbiAgICAgIEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsXHJcbiAgICAgIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgIGRhdGEuY2FuY2VsID0gdHJ1ZTtcclxuICAgICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMuYmFja1RvUHJldmlvdXNQYWdlKCk7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgICB0aGlzLnRlbXB1c2VyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZWN1c3RvbWVyKFxyXG4gICAgbmFtZSxcclxuICAgIHNob3BuYW1lLFxyXG4gICAgcHJpbWFyeW1vYmlsZSxcclxuICAgIHJlZ2lvbixcclxuICAgIGNvbnRhY3RwZXJzb24sXHJcbiAgICBsYW5kbGluZW5vLFxyXG4gICAgaG9tZW1vYmlsZW5vLFxyXG4gICAgd29ya21vYmlsZW5vLFxyXG4gICAgZW1haWwsXHJcbiAgICBnc3RcclxuICApIHtcclxuICAgIGlmKGdzdCkge1xyXG4gICAgICBnc3QgPSBTdHJpbmcoZ3N0KS50b1VwcGVyQ2FzZSgpO1xyXG4gICAgfVxyXG4gICAgbGV0IHRmMSA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wYmRvb3IubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuYmRvb3JubyA9IHRmMS50ZXh0O1xyXG4gICAgbGV0IHRmMiA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wYnN0cmVldC5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5ic3RyZWV0ID0gdGYyLnRleHQ7XHJcbiAgICBsZXQgdGYzID0gPFRleHRGaWVsZD50aGlzLnRlbXBiYXJlYS5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5iYXJlYSA9IHRmMy50ZXh0O1xyXG4gICAgbGV0IHRmNCA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wYmNpdHkubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuYmNpdHkgPSB0ZjQudGV4dDtcclxuICAgIGxldCB0ZjUgPSA8VGV4dEZpZWxkPnRoaXMudGVtcGJkaXN0cmljdC5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5iZGlzdHJpY3QgPSB0ZjUudGV4dDtcclxuICAgIGxldCB0ZjYgPSA8VGV4dEZpZWxkPnRoaXMudGVtcGJzdGF0ZS5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5ic3RhdGUgPSB0ZjYudGV4dDtcclxuICAgIGxldCB0ZjcgPSA8VGV4dEZpZWxkPnRoaXMudGVtcGJwaW4ubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuYnBpbiA9IHRmNy50ZXh0O1xyXG4gICAgbGV0IHRmOCA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wc2Rvb3IubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuc2Rvb3JubyA9IHRmOC50ZXh0O1xyXG4gICAgbGV0IHRmOSA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wc3N0cmVldC5uYXRpdmVFbGVtZW50O1xyXG4gICAgdGhpcy5zc3RyZWV0ID0gdGY5LnRleHQ7XHJcbiAgICBsZXQgdGYxMCA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wc2FyZWEubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuc2FyZWEgPSB0ZjEwLnRleHQ7XHJcbiAgICBsZXQgdGYxMSA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wc2NpdHkubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuc2NpdHkgPSB0ZjExLnRleHQ7XHJcbiAgICBsZXQgdGYxMiA9IDxUZXh0RmllbGQ+dGhpcy50ZW1wc2Rpc3RyaWN0Lm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB0aGlzLnNkaXN0cmljdCA9IHRmMTIudGV4dDtcclxuICAgIGxldCB0ZjEzID0gPFRleHRGaWVsZD50aGlzLnRlbXBzc3RhdGUubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuc3N0YXRlID0gdGYxMy50ZXh0O1xyXG4gICAgbGV0IHRmMTQgPSA8VGV4dEZpZWxkPnRoaXMudGVtcHNwaW4ubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuc3BpbiA9IHRmMTQudGV4dDtcclxuICAgIGxldCBlbWFpbElzVmFsaWQgPSBFbWFpbFZhbGlkYXRvci52YWxpZGF0ZShlbWFpbCk7XHJcblxyXG4gICAgbGV0IGVycm9yRmxhZyA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKFwiRU1BSUwgVkFMSURBVElPTlwiICsgZW1haWxJc1ZhbGlkKTtcclxuICAgIGlmIChuYW1lID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgTmFtZVwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoc2hvcG5hbWUgPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBTaG9wIE5hbWVcIik7XHJcbiAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKHByaW1hcnltb2JpbGUgPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBQcmltYXJ5IE1vYmlsZSBOdW1iZXJcIik7XHJcbiAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKHByaW1hcnltb2JpbGUubGVuZ3RoIDwgMTAgfHwgcHJpbWFyeW1vYmlsZS5sZW5ndGggPiAxMCkge1xyXG4gICAgICBhbGVydChcIlByaW1hcnkgTW9iaWxlIE51bWJlciBTaG91bGQgYmUgMTAgZGlnaXRzXCIpO1xyXG4gICAgICBlcnJvckZsYWcgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmIChyZWdpb24gPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBSZWdpb25cIik7XHJcbiAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKGNvbnRhY3RwZXJzb24gPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBDb250YWN0IFBlcnNvblwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoZW1haWwgPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBFbWFpbFwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoZW1haWxJc1ZhbGlkID09IGZhbHNlKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgVmFsaWQgRW1haWwgQWRkcmVzc1wiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoZ3N0ID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgR1NUXCIpO1xyXG4gICAgICBlcnJvckZsYWcgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmIChnc3QubGVuZ3RoIDwgMTUgfHwgZ3N0Lmxlbmd0aCA+IDE1KSB7XHJcbiAgICAgIGFsZXJ0KFwiR1NUIE51bWJlciBMZW5ndGggU2hvdWxkIGJlIDE1XCIpO1xyXG4gICAgICBlcnJvckZsYWcgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmJkb29ybm8gPT09IFwiXCIpIHtcclxuICAgICAgYWxlcnQoXCJFbnRlciBCaWxsaW5nIEFkZHJlc3MgRG9vciBOb1wiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5ic3RyZWV0ID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgQmlsbGluZyBBZGRyZXNzIFN0cmVldFwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5iYXJlYSA9PT0gXCJcIikge1xyXG4gICAgICBhbGVydChcIkVudGVyIEJpbGxpbmcgQWRkcmVzcyBBcmVhXCIpO1xyXG4gICAgICBlcnJvckZsYWcgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmJjaXR5ID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgQmlsbGluZyBBZGRyZXNzIENpdHlcIik7XHJcbiAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuYmRpc3RyaWN0ID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgQmlsbGluZyBBZGRyZXNzIERpc3RyaWN0XCIpO1xyXG4gICAgICBlcnJvckZsYWcgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmJzdGF0ZSA9PT0gXCJcIikge1xyXG4gICAgICBhbGVydChcIkVudGVyIEJpbGxpbmcgQWRkcmVzcyBTdGF0ZVwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5icGluID09PSBcIlwiKSB7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgQmlsbGluZyBBZGRyZXNzIFBpblwiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5icGluLmxlbmd0aCA8IDYgfHwgdGhpcy5icGluLmxlbmd0aCA+IDYpIHtcclxuICAgICAgYWxlcnQoXCJQaW4gTnVtYmVyIFNob3VsZCBiZSA2IGRpZ2l0c1wiKTtcclxuICAgICAgZXJyb3JGbGFnID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIGlmIChob21lbW9iaWxlbm8pIHtcclxuICAgICAgaWYgKGhvbWVtb2JpbGVuby5sZW5ndGggPCAxMCB8fCBob21lbW9iaWxlbm8ubGVuZ3RoID4gMTApIHtcclxuICAgICAgICBhbGVydChcIkhvbWUgTW9iaWxlIE51bWJlciBTaG91bGQgYmUgMTAgZGlnaXRzXCIpO1xyXG4gICAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmICh3b3JrbW9iaWxlbm8pIHtcclxuICAgICAgaWYgKHdvcmttb2JpbGVuby5sZW5ndGggPCAxMCB8fCB3b3JrbW9iaWxlbm8ubGVuZ3RoID4gMTApIHtcclxuICAgICAgICBhbGVydChcIldvcmsgTW9iaWxlIE51bWJlciBTaG91bGQgYmUgMTAgZGlnaXRzXCIpO1xyXG4gICAgICAgIGVycm9yRmxhZyA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC8qXHJcbiAgICBlbHNlIGlmKHRoaXMuc2Rvb3JubyA9PT0gXCJcIil7XHJcbiAgICAgIC8vYWxlcnQoXCJFbnRlciB0aGlzLmJkb29ybm9cIilcclxuICAgICAgdGhpcy5zZG9vcm5vID0gdGhpcy5iZG9vcm5vXHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKHRoaXMuc3N0cmVldCA9PT0gXCJcIil7XHJcbiAgICAgIC8vYWxlcnQoXCJFbnRlciBuYW1lXCIpXHJcbiAgICAgIHRoaXMuc3N0cmVldCA9IHRoaXMuYnN0cmVldFxyXG4gICAgfVxyXG4gICAgZWxzZSBpZih0aGlzLnNhcmVhID09PSBcIlwiKXtcclxuICAgICAgLy9hbGVydChcIkVudGVyIG5hbWVcIilcclxuICAgICAgdGhpcy5zYXJlYSA9IHRoaXMuYmFyZWFcclxuICAgIH1cclxuICAgIGVsc2UgaWYodGhpcy5zY2l0eSA9PT0gXCJcIil7XHJcbiAgICAgIC8vYWxlcnQoXCJFbnRlciBuYW1lXCIpXHJcbiAgICAgIHRoaXMuc2NpdHkgPSB0aGlzLmJjaXR5XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKHRoaXMuc2Rpc3RyaWN0ID09PSBcIlwiKXtcclxuICAgICAvLyBhbGVydChcIkVudGVyIG5hbWVcIilcclxuICAgICAgdGhpcy5zZGlzdHJpY3QgPSB0aGlzLmJkaXN0cmljdFxyXG4gICAgfVxyXG4gICAgZWxzZSBpZih0aGlzLnNzdGF0ZSA9PT0gXCJcIil7XHJcbiAgICAgLy9hbGVydChcIkVudGVyIG5hbWVcIilcclxuICAgICB0aGlzLnNzdGF0ZSA9IHRoaXMuYnN0YXRlXHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKHRoaXMuc3BpbiA9PT0gXCJcIil7XHJcbiAgICAgIGFsZXJ0KFwiRW50ZXIgbmFtZVwiKVxyXG4gICAgICB0aGlzLnNwaW4gPSB0aGlzLmJwaW5cclxuICAgIH1cclxuKi9cclxuICAgIGlmICghZXJyb3JGbGFnKSB7XHJcbiAgICAgIC8vY29kZSBmb3IgbG9hZGluZyBpbmRpY2F0b3Igc3RhcnQgaGVyZVxyXG4gICAgICBsZXQgb3B0aW9ucyA9IHtcclxuICAgICAgICBtZXNzYWdlOiAnTG9hZGluZy4uLicsXHJcbiAgICAgICAgcHJvZ3Jlc3M6IDAuNjUsXHJcbiAgICAgICAgYW5kcm9pZDoge1xyXG4gICAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcclxuICAgICAgICAgIGNhbmNlbGFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgbWF4OiAxMDAsXHJcbiAgICAgICAgICBwcm9ncmVzc051bWJlckZvcm1hdDogXCIlMWQvJTJkXCIsXHJcbiAgICAgICAgICBwcm9ncmVzc1BlcmNlbnRGb3JtYXQ6IDAuNTMsXHJcbiAgICAgICAgICBwcm9ncmVzc1N0eWxlOiAxLFxyXG4gICAgICAgICAgc2Vjb25kYXJ5UHJvZ3Jlc3M6IDFcclxuICAgICAgICB9LFxyXG4gICAgICAgIGlvczoge1xyXG4gICAgICAgICAgZGV0YWlsczogXCJBZGRpdGlvbmFsIGRldGFpbCBub3RlIVwiLFxyXG4gICAgICAgICAgc3F1YXJlOiBmYWxzZSxcclxuICAgICAgICAgIG1hcmdpbjogMTAsXHJcbiAgICAgICAgICBkaW1CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgICAgY29sb3I6IFwiIzRCOUVENlwiLFxyXG4gICAgICAgICAgbW9kZTogXCJcIi8vIHNlZSBpT1Mgc3BlY2lmaWMgb3B0aW9ucyBiZWxvd1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgIFxyXG4gICAgICB0aGlzLmxvYWRlci5zaG93KG9wdGlvbnMpO1xyXG4gICAgICAgLy9jb2RlIGZvciBsb2FkaW5nIGluZGljYXRvciBlbmQgaGVyZVxyXG5cclxuICAgICAgY29uc29sZS5sb2coXCJJbiBwb3N0XCIpO1xyXG5cclxuICAgICAgdGhpcy5kYXRhID0ge1xyXG4gICAgICAgIG5hbWU6IG5hbWUsXHJcbiAgICAgICAgZ3N0X25vOiBnc3QsXHJcbiAgICAgICAgZW1wbG95ZWVfaWQ6IHRoaXMudGVtcHVzZXIuaWQsXHJcbiAgICAgICAgc2hvcF9uYW1lOiBzaG9wbmFtZSxcclxuICAgICAgICByZWdpb246cmVnaW9uLFxyXG4gICAgICAgIHN0YXR1czogXCJwZW5kaW5nXCIsXHJcbiAgICAgICAgY29udGFjdF9kZXRhaWxzOiB7XHJcbiAgICAgICAgICB3b3JrX21vYmlsZV9ubzogd29ya21vYmlsZW5vLFxyXG4gICAgICAgICAgaG9tZV9tb2JpbGVfbm86IGhvbWVtb2JpbGVubyxcclxuICAgICAgICAgIGxhbmRsaW5lX25vOiBsYW5kbGluZW5vLFxyXG4gICAgICAgICAgZW1haWw6IGVtYWlsLFxyXG4gICAgICAgICAgY29udGFjdF9wZXJzb246IGNvbnRhY3RwZXJzb25cclxuICAgICAgICB9LFxyXG4gICAgICAgIHByaW1hcnlfbW9iaWxlX25vOiBwcmltYXJ5bW9iaWxlLFxyXG4gICAgICAgIGFkZHJlc3M6IFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgZG9vcl9ubzogdGhpcy5iZG9vcm5vLFxyXG4gICAgICAgICAgICBzdHJlZXRfbmFtZTogdGhpcy5ic3RyZWV0LFxyXG4gICAgICAgICAgICBhcmVhOiB0aGlzLmJhcmVhLFxyXG4gICAgICAgICAgICBjaXR5OiB0aGlzLmJjaXR5LFxyXG4gICAgICAgICAgICBkaXN0cmljdDogdGhpcy5iZGlzdHJpY3QsXHJcbiAgICAgICAgICAgIHN0YXRlOiB0aGlzLmJzdGF0ZSxcclxuICAgICAgICAgICAgc3RhdHVzOiBcImFjdGl2ZVwiLFxyXG4gICAgICAgICAgICBwaW46IHRoaXMuYnBpblxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgZG9vcl9ubzogdGhpcy5iZG9vcm5vLFxyXG4gICAgICAgICAgICBzdHJlZXRfbmFtZTogdGhpcy5ic3RyZWV0LFxyXG4gICAgICAgICAgICBhcmVhOiB0aGlzLmJhcmVhLFxyXG4gICAgICAgICAgICBjaXR5OiB0aGlzLmJjaXR5LFxyXG4gICAgICAgICAgICBkaXN0cmljdDogdGhpcy5iZGlzdHJpY3QsXHJcbiAgICAgICAgICAgIHN0YXRlOiB0aGlzLmJzdGF0ZSxcclxuICAgICAgICAgICAgc3RhdHVzOiBcImFjdGl2ZVwiLFxyXG4gICAgICAgICAgICBwaW46IHRoaXMuYnBpblxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgICAgfTtcclxuICAgICAgY29uc29sZS5sb2coXCJGdWxsIEN1c3RvbWVyIERhdGE6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5kYXRhKSk7XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5odHRwXHJcbiAgICAgICAgLnBvc3QoXHJcbiAgICAgICAgICBcImh0dHBzOi8vYXBwLmtyaXNobmFiaGF2YW5mb29kcy5pbi9hcGkvY3VzdG9tZXIvYWRkY3VzdG9tZXIvXCIsXHJcbiAgICAgICAgICB0aGlzLmRhdGEsXHJcbiAgICAgICAgICB7IGhlYWRlcnM6IHRoaXMuZ2V0Q29tbW9uSGVhZGVycygpIH1cclxuICAgICAgICApXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0cmVzdWx0KHJlc3VsdCk7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJDdXN0b21lciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0Lmpzb24pKVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJDdXN0b21lciBFcnJvcjogXCIgKyBlcnJvcik7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tZXJDcmVhdGVlcnJvcihlcnJvcik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldENvbW1vbkhlYWRlcnMoKSB7XHJcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKCk7XHJcbiAgICBoZWFkZXJzLnNldChcIkNvbnRlbnQtVHlwZVwiLCBcImFwcGxpY2F0aW9uL2pzb25cIik7XHJcbiAgICByZXR1cm4gaGVhZGVycztcclxuICB9XHJcbiAgZ2V0cmVzdWx0KHJlc3VsdCkge1xyXG4gICAgdGhpcy5sb2FkZXIuaGlkZSgpO1xyXG4gICAgdGhpcy5kaXNhYmxlQ3JlYXRlRkFCID0gZmFsc2U7XHJcbiAgICBkaWFsb2dzLmFsZXJ0KFwiQ3VzdG9tZXIgSXMgQ3JlYXRlZCBTdWNjZXNzZnVsbHlcIikudGhlbigoKSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiQ3VzdG9tZXIgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpO1xyXG4gICAgICBpZiAodGhpcy50ZW1wdXNlcltcInJvbGVcIl0gPT09IFwiY3VzdG9tZXJcIikge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvb3JkZXJsaXN0XCJdKTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLnRlbXB1c2VyW1wicm9sZVwiXSA9PT0gXCJyZXBcIikge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvb3JkZXJsaXN0XCJdKTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLnRlbXB1c2VyW1wicm9sZVwiXSA9PT0gXCJtYW5hZ2VyXCIpIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2FwcHJvdmVjdXN0b21lcmxpc3RcIl0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgY3VzdG9tZXJDcmVhdGVlcnJvcihlcnJvcikge1xyXG4gICAgZGlhbG9ncy5hbGVydChcIkN1c3RvbWVyIElzIE5vdCBDcmVhdGVkXCIpLnRoZW4oKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy50ZW1wdXNlcltcInJvbGVcIl0gPT09IFwiY3VzdG9tZXJcIikge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvb3JkZXJsaXN0XCJdKTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLnRlbXB1c2VyW1wicm9sZVwiXSA9PT0gXCJyZXBcIikge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvb3JkZXJsaXN0XCJdKTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLnRlbXB1c2VyW1wicm9sZVwiXSA9PT0gXCJtYW5hZ2VyXCIpIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2FwcHJvdmVjdXN0b21lcmxpc3RcIl0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19