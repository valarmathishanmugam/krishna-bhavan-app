"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_service_1 = require("../../Services/customer.service");
var customer_model_1 = require("../../Models/customer.model");
var saveorder_service_1 = require("../../Services/saveorder.service");
var login_service_1 = require("../../Services/login.service");
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var router_1 = require("nativescript-angular/router");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var PushNotifications = require("nativescript-push-notifications");
//import * as pushPlugin from "nativescript-push-notifications";
var localStorage = require("nativescript-localstorage");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var LoginComponent = (function () {
    // private pushSettings = {
    //   senderID: "472232624874", // Required: setting with the sender/project number
    //   notificationCallbackAndroid: (
    //     stringifiedData: String,
    //     fcmNotification: any
    //   ) => {
    //     const notificationBody = fcmNotification && fcmNotification.getBody();
    //     console.log(
    //       "Message received!\n" + notificationBody + "\n" + stringifiedData
    //     );
    //   }
    // };
    function LoginComponent(routerExtensions, page, loginservice, savecustomer, customerservice, savecustomerdetails, location, saveemployee, http) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.loginservice = loginservice;
        this.savecustomer = savecustomer;
        this.customerservice = customerservice;
        this.savecustomerdetails = savecustomerdetails;
        this.location = location;
        this.saveemployee = saveemployee;
        this.http = http;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.usernamevalid = "collapse";
        this.passwordvalid = "collapse";
        this.incorrectmsg = "collapse";
        this.servermsg = "";
        this.storeusername = "";
        this.storepassword = "";
        this.loader = new LoadingIndicator();
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            //this.exitApp(data.cancel);
        });
        var settings = {
            senderID: "472232624874",
            notificationCallbackAndroid: function (data, notification) {
                console.log("DATA: " + JSON.stringify(data));
                console.log("NOTIFICATION: " + JSON.stringify(notification));
                var notificationBody = notification && notification.getBody();
                console.log("notificationBody: " + notificationBody);
                alert("New Order: \n" + notificationBody);
                //this.updateMessage("Message received!\n" + notificationBody + "\n" + data);
            }
        };
        //   pushPlugin.register(this.pushSettings, (token: String) => {
        //     alert("Device registered. Access token: " + token);;
        // }, function() { });
        //PushNotifications.onTokenRefresh
        //Commented by Balaji
        PushNotifications.register(settings, function (data) {
            console.log("REGISTRATION ID: " + data);
            _this.regid = data;
            PushNotifications.onMessageReceived(settings.notificationCallbackAndroid);
        }, function (error) {
            console.log("Firebase Error: " + error);
        });
        //Commented by Balaji
        /*var currentdate = new Date();
          console.log("Date: " + currentdate.getDate())
          var date = currentdate.toISOString();
          console.log("ISO Date: " + date)
          */
        if (localStorage.getItem("local username") !== null) {
            //console.log("username not null")
            this.storeusername = localStorage.getItem("local username");
        }
        else {
            //console.log("username is null")
        }
        if (localStorage.getItem("local password") !== null) {
            //console.log("password not null")
            this.storepassword = localStorage.getItem("local password");
        }
        else {
            //console.log("password is null")
        }
        //console.log("username Storage: " + this.storeusername + " password storage: " + this.storepassword);
    };
    LoginComponent.prototype.submit = function (username, password, args) {
        //username="9500231761";
        //password = "dummypass";
        var _this = this;
        this.usernamevalid = "collapse";
        this.passwordvalid = "collapse";
        this.incorrectmsg = "collapse";
        //console.log("username: "+ username + "password: "+ password);
        this.checkvalue = this.FirstCheckBox.nativeElement.checked;
        console.log("Checked value: " + this.checkvalue);
        if (this.checkvalue == true) {
            //console.log("Its Truee...")
            localStorage.setItem("local username", username);
            localStorage.setItem("local password", password);
        }
        else if (this.checkvalue == false) {
            //console.log("Its False...")
        }
        if (username === "") {
            console.log("username is empty");
            this.usernamevalid = "visible";
        }
        else if (password === "") {
            console.log("password is empty");
            this.passwordvalid = "visible";
        }
        else {
            this.showLoadingIndicator();
            console.log("Call Login Service");
            this.loginservice.login(username, password).subscribe(function (data) {
                _this.loader.hide();
                console.log("Login Data: " + data.text());
                _this.incorrectmsg = "visible";
                _this.servermsg = data.text();
                _this.parseusers(username, password, data.json());
            }, function (err) {
                console.log("Login Error: " + err);
            });
        }
        //(error) => {
        // console.log("Login error: " + error)
        //}
        //this.routerExtensions.navigate(["/customerlist"]);
        //this.routerExtensions.navigate(["/productlist"]);
        //let view = <TextField.textfield>args.object;
        //const tip = new ToolTip(view,{text:"Some Text",backgroundColor:"pink",textColor:"black"});
        //tip.show();
    };
    LoginComponent.prototype.showLoadingIndicator = function () {
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
    };
    LoginComponent.prototype.parseusers = function (username, password, data) {
        var _this = this;
        console.log("loginId: " + data.id);
        var temprole = data.role;
        this.loginservice.setScope(data);
        if (temprole === "customer") {
            console.log("customer");
            this.routerExtensions.navigate(["/productlist"]);
            this.savecustomer.setScope(data.id);
            this.customerservice
                .getcustomerbyid(data.id)
                .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        }
        else if (temprole === "rep") {
            console.log("rep");
            this.saveemployee.setScope(data.id);
            this.routerExtensions.navigate(["/customerlist"]);
        }
        else if (temprole === "manager") {
            console.log("manager");
            this.saveemployee.setScope(data.id);
            this.routerExtensions.navigate(["/managerorderlist"]);
        }
        // else if (temprole === "admin") {
        //   console.log("admin");
        //   this.saveemployee.setScope(data.id);
        //   this.routerExtensions.navigate(["/productlist"]);
        // }
        this.data = {
            mobile_no: username,
            password: password,
            registration_id: this.regid
        };
        console.log("REG ID: " + this.regid);
        console.log("Reg data: " + JSON.stringify(this.data));
        this.http
            .post("https://app.krishnabhavanfoods.in/api/user/addregistrationid", this.data, { headers: this.getCommonHeaders() })
            .subscribe(function (result) {
            //this.customerreport(result.json())
            console.log("Reg Result: " + JSON.stringify(result.json()));
        }, function (error) {
            console.log("Reg Error: " + error);
        });
    };
    LoginComponent.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
        this.savecustomerdetails.setScope(this.customer);
        this.saveemployee.setScope(data.employee_id.$oid);
    };
    LoginComponent.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    return LoginComponent;
}());
__decorate([
    core_1.ViewChild("cb1"),
    __metadata("design:type", core_1.ElementRef)
], LoginComponent.prototype, "FirstCheckBox", void 0);
LoginComponent = __decorate([
    core_1.Component({
        selector: "login-comp",
        moduleId: module.id,
        templateUrl: "login.component.html",
        styleUrls: ["login.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        login_service_1.loginService,
        saveorder_service_1.SaveCustomer,
        customer_service_1.CustomerService,
        saveorder_service_1.SaveCustomerDetails,
        common_1.Location,
        saveorder_service_1.SaveEmployeeId,
        http_1.Http])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0VBQWtFO0FBQ2xFLDhEQUFnRTtBQUNoRSxzRUFJMEM7QUFDMUMsOERBQTREO0FBQzVELHNDQU11QjtBQUN2QixzREFBcUQ7QUFDckQsc0RBQStEO0FBTS9ELDJDQUdxQjtBQUNyQix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBQzNDLHNDQUF3RDtBQUN4RCxtRUFBcUU7QUFDckUsZ0VBQWdFO0FBQ2hFLHdEQUEwRDtBQUUxRCxJQUFJLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLGdCQUFnQixDQUFDO0FBT3RGLElBQWEsY0FBYztJQWlCekIsMkJBQTJCO0lBQzNCLGtGQUFrRjtJQUNsRixtQ0FBbUM7SUFDbkMsK0JBQStCO0lBQy9CLDJCQUEyQjtJQUMzQixXQUFXO0lBQ1gsNkVBQTZFO0lBQzdFLG1CQUFtQjtJQUNuQiwwRUFBMEU7SUFDMUUsU0FBUztJQUNULE1BQU07SUFDTixLQUFLO0lBRUwsd0JBQ1UsZ0JBQWtDLEVBQ2xDLElBQVUsRUFDVixZQUEwQixFQUMxQixZQUEwQixFQUMxQixlQUFnQyxFQUNoQyxtQkFBd0MsRUFDeEMsUUFBa0IsRUFDbEIsWUFBNEIsRUFDNUIsSUFBVTtRQVJWLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsaUJBQVksR0FBWixZQUFZLENBQWdCO1FBQzVCLFNBQUksR0FBSixJQUFJLENBQU07UUF0Q3BCLGFBQVEsR0FBRyxJQUFJLHlCQUFRLEVBQUUsQ0FBQztRQUMxQixZQUFPLEdBQUcsSUFBSSx3QkFBTyxFQUFFLENBQUM7UUFDeEIsa0JBQWEsR0FBRyxVQUFVLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxVQUFVLENBQUM7UUFDM0IsaUJBQVksR0FBRyxVQUFVLENBQUM7UUFDMUIsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQU1mLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBRW5CLFdBQU0sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7SUF5QjdCLENBQUM7SUFFSixpQ0FBUSxHQUFSO1FBQUEsaUJBZ0VDO1FBL0RDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FDcEIsZ0NBQWtCLENBQUMsd0JBQXdCLEVBQzNDLFVBQUMsSUFBeUM7WUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2Qiw0QkFBNEI7UUFDOUIsQ0FBQyxDQUNGLENBQUM7UUFFRixJQUFJLFFBQVEsR0FBRztZQUNiLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLDJCQUEyQixFQUFFLFVBQUMsSUFBSSxFQUFFLFlBQVk7Z0JBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQU0sZ0JBQWdCLEdBQUcsWUFBWSxJQUFJLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLENBQUMsZUFBZSxHQUFHLGdCQUFnQixDQUFDLENBQUM7Z0JBQzFDLDZFQUE2RTtZQUMvRSxDQUFDO1NBQ0YsQ0FBQztRQUVGLGdFQUFnRTtRQUNoRSwyREFBMkQ7UUFDM0Qsc0JBQXNCO1FBQ3RCLGtDQUFrQztRQUVsQyxxQkFBcUI7UUFDckIsaUJBQWlCLENBQUMsUUFBUSxDQUN4QixRQUFRLEVBQ1IsVUFBQSxJQUFJO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUN4QyxLQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FDakMsUUFBUSxDQUFDLDJCQUEyQixDQUNyQyxDQUFDO1FBQ0osQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUNGLENBQUM7UUFDRixxQkFBcUI7UUFDckI7Ozs7WUFJSTtRQUVKLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3BELGtDQUFrQztZQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixpQ0FBaUM7UUFDbkMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3BELGtDQUFrQztZQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixpQ0FBaUM7UUFDbkMsQ0FBQztRQUNELHNHQUFzRztJQUN4RyxDQUFDO0lBRUQsK0JBQU0sR0FBTixVQUFPLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSTtRQUM3Qix3QkFBd0I7UUFDeEIseUJBQXlCO1FBRjNCLGlCQW1EQztRQS9DQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQztRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQztRQUUvQiwrREFBK0Q7UUFFL0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUM7UUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFakQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzVCLDZCQUE2QjtZQUM3QixZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ2pELFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbkQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEMsNkJBQTZCO1FBQy9CLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7UUFDakMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7UUFDakMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQ25ELFVBQUEsSUFBSTtnQkFDRixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDMUMsS0FBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7Z0JBQzlCLEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM3QixLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7WUFDbkQsQ0FBQyxFQUNELFVBQUEsR0FBRztnQkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNyQyxDQUFDLENBQ0YsQ0FBQztRQUNKLENBQUM7UUFDRCxjQUFjO1FBQ2QsdUNBQXVDO1FBQ3ZDLEdBQUc7UUFDSCxvREFBb0Q7UUFDcEQsbURBQW1EO1FBQ25ELDhDQUE4QztRQUM5Qyw0RkFBNEY7UUFDNUYsYUFBYTtJQUNmLENBQUM7SUFDRCw2Q0FBb0IsR0FBcEI7UUFDQyxJQUFJLE9BQU8sR0FBRztZQUNYLE9BQU8sRUFBRSxZQUFZO1lBQ3JCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixVQUFVLEVBQUUsS0FBSztnQkFDakIsR0FBRyxFQUFFLEdBQUc7Z0JBQ1Isb0JBQW9CLEVBQUUsU0FBUztnQkFDL0IscUJBQXFCLEVBQUUsSUFBSTtnQkFDM0IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLGlCQUFpQixFQUFFLENBQUM7YUFDckI7WUFDRCxHQUFHLEVBQUU7Z0JBQ0gsT0FBTyxFQUFFLHlCQUF5QjtnQkFDbEMsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsYUFBYSxFQUFFLElBQUk7Z0JBQ25CLEtBQUssRUFBRSxTQUFTO2dCQUNoQixJQUFJLEVBQUMsRUFBRSxDQUFDLGlDQUFpQzthQUMxQztTQUNGLENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsbUNBQVUsR0FBVixVQUFXLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSTtRQUFuQyxpQkFrREM7UUFqREMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRW5DLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFakMsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLGVBQWU7aUJBQ2pCLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO2lCQUN4QixTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7UUFDekQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUNELG1DQUFtQztRQUNuQywwQkFBMEI7UUFDMUIseUNBQXlDO1FBQ3pDLHNEQUFzRDtRQUN0RCxJQUFJO1FBRUosSUFBSSxDQUFDLElBQUksR0FBRztZQUNWLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLGVBQWUsRUFBRSxJQUFJLENBQUMsS0FBSztTQUM1QixDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUk7YUFDTixJQUFJLENBQ0gsOERBQThELEVBQzlELElBQUksQ0FBQyxJQUFJLEVBQ1QsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FDckM7YUFDQSxTQUFTLENBQ1IsVUFBQSxNQUFNO1lBQ0osb0NBQW9DO1lBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQ0QsVUFBQSxLQUFLO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDckMsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDO0lBRUQsdUNBQWMsR0FBZCxVQUFlLElBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBRTNDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFckMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQseUNBQWdCLEdBQWhCO1FBQ0UsSUFBSSxPQUFPLEdBQUcsSUFBSSxjQUFPLEVBQUUsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUNILHFCQUFDO0FBQUQsQ0FBQyxBQXhRRCxJQXdRQztBQTlQbUI7SUFBakIsZ0JBQVMsQ0FBQyxLQUFLLENBQUM7OEJBQWdCLGlCQUFVO3FEQUFDO0FBVmpDLGNBQWM7SUFOMUIsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxZQUFZO1FBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsc0JBQXNCO1FBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO0tBQ25DLENBQUM7cUNBZ0M0Qix5QkFBZ0I7UUFDNUIsV0FBSTtRQUNJLDRCQUFZO1FBQ1osZ0NBQVk7UUFDVCxrQ0FBZTtRQUNYLHVDQUFtQjtRQUM5QixpQkFBUTtRQUNKLGtDQUFjO1FBQ3RCLFdBQUk7R0F2Q1QsY0FBYyxDQXdRMUI7QUF4UVksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDdXN0b21lclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vU2VydmljZXMvY3VzdG9tZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBBZGRyZXNzLCBDdXN0b21lciB9IGZyb20gXCIuLi8uLi9Nb2RlbHMvY3VzdG9tZXIubW9kZWxcIjtcclxuaW1wb3J0IHtcclxuICBTYXZlQ3VzdG9tZXIsXHJcbiAgU2F2ZUN1c3RvbWVyRGV0YWlscyxcclxuICBTYXZlRW1wbG95ZWVJZFxyXG59IGZyb20gXCIuLi8uLi9TZXJ2aWNlcy9zYXZlb3JkZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vU2VydmljZXMvbG9naW4uc2VydmljZVwiO1xyXG5pbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgQWZ0ZXJWaWV3SW5pdCxcclxuICBWaWV3Q2hpbGQsXHJcbiAgRWxlbWVudFJlZlxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdGVsZXJpay11aS9zaWRlZHJhd2VyXCI7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXRlbGVyaWstdWkvc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gXCJ1aS90ZXh0LWZpZWxkXCI7XHJcbmltcG9ydCB7IFRvb2xUaXAgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXRvb2x0aXBcIjtcclxuaW1wb3J0IHsgY29uZmlybSB9IGZyb20gXCJ1aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7XHJcbiAgQW5kcm9pZEFwcGxpY2F0aW9uLFxyXG4gIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhXHJcbn0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcclxuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xyXG5pbXBvcnQgKiBhcyBQdXNoTm90aWZpY2F0aW9ucyBmcm9tIFwibmF0aXZlc2NyaXB0LXB1c2gtbm90aWZpY2F0aW9uc1wiO1xyXG4vL2ltcG9ydCAqIGFzIHB1c2hQbHVnaW4gZnJvbSBcIm5hdGl2ZXNjcmlwdC1wdXNoLW5vdGlmaWNhdGlvbnNcIjtcclxuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gXCJuYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlXCI7XHJcbmltcG9ydCB7IEN1c3RvbWVyRGV0YWlsIH0gZnJvbSBcIi4uLy4uL0NvbXBvbmVudHMvU2FsZXNfcmVwL0N1c3RvbWVyRGV0YWlsL2N1c3RvbWVyZGV0YWlsLmNvbXBvbmVudFwiO1xyXG52YXIgTG9hZGluZ0luZGljYXRvciA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtbG9hZGluZy1pbmRpY2F0b3ItbmV3XCIpLkxvYWRpbmdJbmRpY2F0b3I7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImxvZ2luLWNvbXBcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcImxvZ2luLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJsb2dpbi5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgY3VzdG9tZXIgPSBuZXcgQ3VzdG9tZXIoKTtcclxuICBhZGRyZXNzID0gbmV3IEFkZHJlc3MoKTtcclxuICB1c2VybmFtZXZhbGlkID0gXCJjb2xsYXBzZVwiO1xyXG4gIHBhc3N3b3JkdmFsaWQgPSBcImNvbGxhcHNlXCI7XHJcbiAgaW5jb3JyZWN0bXNnID0gXCJjb2xsYXBzZVwiO1xyXG4gIHNlcnZlcm1zZyA9IFwiXCI7XHJcbiAgdGVtcGRhdGE6IGFueTtcclxuICBkYXRhO1xyXG4gIHJlZ2lkO1xyXG4gIEBWaWV3Q2hpbGQoXCJjYjFcIikgRmlyc3RDaGVja0JveDogRWxlbWVudFJlZjtcclxuICBjaGVja3ZhbHVlOiBib29sZWFuO1xyXG4gIHN0b3JldXNlcm5hbWUgPSBcIlwiO1xyXG4gIHN0b3JlcGFzc3dvcmQgPSBcIlwiO1xyXG4gIGN1c3RvbWVyZGV0YWlsYnlpZDogYW55O1xyXG4gIGxvYWRlciA9IG5ldyBMb2FkaW5nSW5kaWNhdG9yKCk7XHJcblxyXG4gIC8vIHByaXZhdGUgcHVzaFNldHRpbmdzID0ge1xyXG4gIC8vICAgc2VuZGVySUQ6IFwiNDcyMjMyNjI0ODc0XCIsIC8vIFJlcXVpcmVkOiBzZXR0aW5nIHdpdGggdGhlIHNlbmRlci9wcm9qZWN0IG51bWJlclxyXG4gIC8vICAgbm90aWZpY2F0aW9uQ2FsbGJhY2tBbmRyb2lkOiAoXHJcbiAgLy8gICAgIHN0cmluZ2lmaWVkRGF0YTogU3RyaW5nLFxyXG4gIC8vICAgICBmY21Ob3RpZmljYXRpb246IGFueVxyXG4gIC8vICAgKSA9PiB7XHJcbiAgLy8gICAgIGNvbnN0IG5vdGlmaWNhdGlvbkJvZHkgPSBmY21Ob3RpZmljYXRpb24gJiYgZmNtTm90aWZpY2F0aW9uLmdldEJvZHkoKTtcclxuICAvLyAgICAgY29uc29sZS5sb2coXHJcbiAgLy8gICAgICAgXCJNZXNzYWdlIHJlY2VpdmVkIVxcblwiICsgbm90aWZpY2F0aW9uQm9keSArIFwiXFxuXCIgKyBzdHJpbmdpZmllZERhdGFcclxuICAvLyAgICAgKTtcclxuICAvLyAgIH1cclxuICAvLyB9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgcGFnZTogUGFnZSxcclxuICAgIHByaXZhdGUgbG9naW5zZXJ2aWNlOiBsb2dpblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHNhdmVjdXN0b21lcjogU2F2ZUN1c3RvbWVyLFxyXG4gICAgcHJpdmF0ZSBjdXN0b21lcnNlcnZpY2U6IEN1c3RvbWVyU2VydmljZSxcclxuICAgIHByaXZhdGUgc2F2ZWN1c3RvbWVyZGV0YWlsczogU2F2ZUN1c3RvbWVyRGV0YWlscyxcclxuICAgIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uLFxyXG4gICAgcHJpdmF0ZSBzYXZlZW1wbG95ZWU6IFNhdmVFbXBsb3llZUlkLFxyXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwXHJcbiAgKSB7fVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xyXG4gICAgYXBwbGljYXRpb24uYW5kcm9pZC5vbihcclxuICAgICAgQW5kcm9pZEFwcGxpY2F0aW9uLmFjdGl2aXR5QmFja1ByZXNzZWRFdmVudCxcclxuICAgICAgKGRhdGE6IEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhKSA9PiB7XHJcbiAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAvL3RoaXMubG9jYXRpb24uYmFjaygpO1xyXG4gICAgICAgIC8vdGhpcy5leGl0QXBwKGRhdGEuY2FuY2VsKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuXHJcbiAgICBsZXQgc2V0dGluZ3MgPSB7XHJcbiAgICAgIHNlbmRlcklEOiBcIjQ3MjIzMjYyNDg3NFwiLFxyXG4gICAgICBub3RpZmljYXRpb25DYWxsYmFja0FuZHJvaWQ6IChkYXRhLCBub3RpZmljYXRpb24pID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkRBVEE6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiTk9USUZJQ0FUSU9OOiBcIiArIEpTT04uc3RyaW5naWZ5KG5vdGlmaWNhdGlvbikpO1xyXG4gICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbkJvZHkgPSBub3RpZmljYXRpb24gJiYgbm90aWZpY2F0aW9uLmdldEJvZHkoKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIm5vdGlmaWNhdGlvbkJvZHk6IFwiICsgbm90aWZpY2F0aW9uQm9keSk7XHJcbiAgICAgICAgYWxlcnQoXCJOZXcgT3JkZXI6IFxcblwiICsgbm90aWZpY2F0aW9uQm9keSk7XHJcbiAgICAgICAgLy90aGlzLnVwZGF0ZU1lc3NhZ2UoXCJNZXNzYWdlIHJlY2VpdmVkIVxcblwiICsgbm90aWZpY2F0aW9uQm9keSArIFwiXFxuXCIgKyBkYXRhKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICAvLyAgIHB1c2hQbHVnaW4ucmVnaXN0ZXIodGhpcy5wdXNoU2V0dGluZ3MsICh0b2tlbjogU3RyaW5nKSA9PiB7XHJcbiAgICAvLyAgICAgYWxlcnQoXCJEZXZpY2UgcmVnaXN0ZXJlZC4gQWNjZXNzIHRva2VuOiBcIiArIHRva2VuKTs7XHJcbiAgICAvLyB9LCBmdW5jdGlvbigpIHsgfSk7XHJcbiAgICAvL1B1c2hOb3RpZmljYXRpb25zLm9uVG9rZW5SZWZyZXNoXHJcblxyXG4gICAgLy9Db21tZW50ZWQgYnkgQmFsYWppXHJcbiAgICBQdXNoTm90aWZpY2F0aW9ucy5yZWdpc3RlcihcclxuICAgICAgc2V0dGluZ3MsXHJcbiAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiUkVHSVNUUkFUSU9OIElEOiBcIiArIGRhdGEpO1xyXG4gICAgICAgIHRoaXMucmVnaWQgPSBkYXRhO1xyXG4gICAgICAgIFB1c2hOb3RpZmljYXRpb25zLm9uTWVzc2FnZVJlY2VpdmVkKFxyXG4gICAgICAgICAgc2V0dGluZ3Mubm90aWZpY2F0aW9uQ2FsbGJhY2tBbmRyb2lkXHJcbiAgICAgICAgKTtcclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiRmlyZWJhc2UgRXJyb3I6IFwiICsgZXJyb3IpO1xyXG4gICAgICB9XHJcbiAgICApO1xyXG4gICAgLy9Db21tZW50ZWQgYnkgQmFsYWppXHJcbiAgICAvKnZhciBjdXJyZW50ZGF0ZSA9IG5ldyBEYXRlKCk7ICAgIFxyXG4gICAgICBjb25zb2xlLmxvZyhcIkRhdGU6IFwiICsgY3VycmVudGRhdGUuZ2V0RGF0ZSgpKVxyXG4gICAgICB2YXIgZGF0ZSA9IGN1cnJlbnRkYXRlLnRvSVNPU3RyaW5nKCk7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiSVNPIERhdGU6IFwiICsgZGF0ZSlcclxuICAgICAgKi9cclxuXHJcbiAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJsb2NhbCB1c2VybmFtZVwiKSAhPT0gbnVsbCkge1xyXG4gICAgICAvL2NvbnNvbGUubG9nKFwidXNlcm5hbWUgbm90IG51bGxcIilcclxuICAgICAgdGhpcy5zdG9yZXVzZXJuYW1lID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJsb2NhbCB1c2VybmFtZVwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vY29uc29sZS5sb2coXCJ1c2VybmFtZSBpcyBudWxsXCIpXHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibG9jYWwgcGFzc3dvcmRcIikgIT09IG51bGwpIHtcclxuICAgICAgLy9jb25zb2xlLmxvZyhcInBhc3N3b3JkIG5vdCBudWxsXCIpXHJcbiAgICAgIHRoaXMuc3RvcmVwYXNzd29yZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwibG9jYWwgcGFzc3dvcmRcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvL2NvbnNvbGUubG9nKFwicGFzc3dvcmQgaXMgbnVsbFwiKVxyXG4gICAgfVxyXG4gICAgLy9jb25zb2xlLmxvZyhcInVzZXJuYW1lIFN0b3JhZ2U6IFwiICsgdGhpcy5zdG9yZXVzZXJuYW1lICsgXCIgcGFzc3dvcmQgc3RvcmFnZTogXCIgKyB0aGlzLnN0b3JlcGFzc3dvcmQpO1xyXG4gIH1cclxuXHJcbiAgc3VibWl0KHVzZXJuYW1lLCBwYXNzd29yZCwgYXJncykge1xyXG4gICAgLy91c2VybmFtZT1cIjk1MDAyMzE3NjFcIjtcclxuICAgIC8vcGFzc3dvcmQgPSBcImR1bW15cGFzc1wiO1xyXG5cclxuICAgIHRoaXMudXNlcm5hbWV2YWxpZCA9IFwiY29sbGFwc2VcIjtcclxuICAgIHRoaXMucGFzc3dvcmR2YWxpZCA9IFwiY29sbGFwc2VcIjtcclxuICAgIHRoaXMuaW5jb3JyZWN0bXNnID0gXCJjb2xsYXBzZVwiO1xyXG5cclxuICAgIC8vY29uc29sZS5sb2coXCJ1c2VybmFtZTogXCIrIHVzZXJuYW1lICsgXCJwYXNzd29yZDogXCIrIHBhc3N3b3JkKTtcclxuXHJcbiAgICB0aGlzLmNoZWNrdmFsdWUgPSB0aGlzLkZpcnN0Q2hlY2tCb3gubmF0aXZlRWxlbWVudC5jaGVja2VkO1xyXG4gICAgY29uc29sZS5sb2coXCJDaGVja2VkIHZhbHVlOiBcIiArIHRoaXMuY2hlY2t2YWx1ZSk7XHJcblxyXG4gICAgaWYgKHRoaXMuY2hlY2t2YWx1ZSA9PSB0cnVlKSB7XHJcbiAgICAgIC8vY29uc29sZS5sb2coXCJJdHMgVHJ1ZWUuLi5cIilcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJsb2NhbCB1c2VybmFtZVwiLCB1c2VybmFtZSk7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibG9jYWwgcGFzc3dvcmRcIiwgcGFzc3dvcmQpO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmNoZWNrdmFsdWUgPT0gZmFsc2UpIHtcclxuICAgICAgLy9jb25zb2xlLmxvZyhcIkl0cyBGYWxzZS4uLlwiKVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh1c2VybmFtZSA9PT0gXCJcIikge1xyXG4gICAgICBjb25zb2xlLmxvZyhcInVzZXJuYW1lIGlzIGVtcHR5XCIpO1xyXG4gICAgICB0aGlzLnVzZXJuYW1ldmFsaWQgPSBcInZpc2libGVcIjtcclxuICAgIH0gZWxzZSBpZiAocGFzc3dvcmQgPT09IFwiXCIpIHtcclxuICAgICAgY29uc29sZS5sb2coXCJwYXNzd29yZCBpcyBlbXB0eVwiKTtcclxuICAgICAgdGhpcy5wYXNzd29yZHZhbGlkID0gXCJ2aXNpYmxlXCI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNob3dMb2FkaW5nSW5kaWNhdG9yKCk7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiQ2FsbCBMb2dpbiBTZXJ2aWNlXCIpO1xyXG4gICAgICB0aGlzLmxvZ2luc2VydmljZS5sb2dpbih1c2VybmFtZSwgcGFzc3dvcmQpLnN1YnNjcmliZShcclxuICAgICAgICBkYXRhID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyLmhpZGUoKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiTG9naW4gRGF0YTogXCIgKyBkYXRhLnRleHQoKSk7XHJcbiAgICAgICAgICB0aGlzLmluY29ycmVjdG1zZyA9IFwidmlzaWJsZVwiO1xyXG4gICAgICAgICAgdGhpcy5zZXJ2ZXJtc2cgPSBkYXRhLnRleHQoKTtcclxuICAgICAgICAgIHRoaXMucGFyc2V1c2Vycyh1c2VybmFtZSwgcGFzc3dvcmQsIGRhdGEuanNvbigpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkxvZ2luIEVycm9yOiBcIiArIGVycik7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gICAgLy8oZXJyb3IpID0+IHtcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiTG9naW4gZXJyb3I6IFwiICsgZXJyb3IpXHJcbiAgICAvL31cclxuICAgIC8vdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jdXN0b21lcmxpc3RcIl0pO1xyXG4gICAgLy90aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3Byb2R1Y3RsaXN0XCJdKTtcclxuICAgIC8vbGV0IHZpZXcgPSA8VGV4dEZpZWxkLnRleHRmaWVsZD5hcmdzLm9iamVjdDtcclxuICAgIC8vY29uc3QgdGlwID0gbmV3IFRvb2xUaXAodmlldyx7dGV4dDpcIlNvbWUgVGV4dFwiLGJhY2tncm91bmRDb2xvcjpcInBpbmtcIix0ZXh0Q29sb3I6XCJibGFja1wifSk7XHJcbiAgICAvL3RpcC5zaG93KCk7XHJcbiAgfVxyXG4gIHNob3dMb2FkaW5nSW5kaWNhdG9yKCl7XHJcbiAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICBtZXNzYWdlOiAnTG9hZGluZy4uLicsXHJcbiAgICAgIHByb2dyZXNzOiAwLjY1LFxyXG4gICAgICBhbmRyb2lkOiB7XHJcbiAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcclxuICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcclxuICAgICAgICBtYXg6IDEwMCxcclxuICAgICAgICBwcm9ncmVzc051bWJlckZvcm1hdDogXCIlMWQvJTJkXCIsXHJcbiAgICAgICAgcHJvZ3Jlc3NQZXJjZW50Rm9ybWF0OiAwLjUzLFxyXG4gICAgICAgIHByb2dyZXNzU3R5bGU6IDEsXHJcbiAgICAgICAgc2Vjb25kYXJ5UHJvZ3Jlc3M6IDFcclxuICAgICAgfSxcclxuICAgICAgaW9zOiB7XHJcbiAgICAgICAgZGV0YWlsczogXCJBZGRpdGlvbmFsIGRldGFpbCBub3RlIVwiLFxyXG4gICAgICAgIHNxdWFyZTogZmFsc2UsXHJcbiAgICAgICAgbWFyZ2luOiAxMCxcclxuICAgICAgICBkaW1CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgIGNvbG9yOiBcIiM0QjlFRDZcIixcclxuICAgICAgICBtb2RlOlwiXCIgLy8gc2VlIGlPUyBzcGVjaWZpYyBvcHRpb25zIGJlbG93XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICAgXHJcbiAgICB0aGlzLmxvYWRlci5zaG93KG9wdGlvbnMpO1xyXG4gIH1cclxuXHJcbiAgcGFyc2V1c2Vycyh1c2VybmFtZSwgcGFzc3dvcmQsIGRhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwibG9naW5JZDogXCIgKyBkYXRhLmlkKTtcclxuXHJcbiAgICB2YXIgdGVtcHJvbGUgPSBkYXRhLnJvbGU7XHJcbiAgICB0aGlzLmxvZ2luc2VydmljZS5zZXRTY29wZShkYXRhKTtcclxuXHJcbiAgICBpZiAodGVtcHJvbGUgPT09IFwiY3VzdG9tZXJcIikge1xyXG4gICAgICBjb25zb2xlLmxvZyhcImN1c3RvbWVyXCIpO1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3Byb2R1Y3RsaXN0XCJdKTtcclxuICAgICAgdGhpcy5zYXZlY3VzdG9tZXIuc2V0U2NvcGUoZGF0YS5pZCk7XHJcbiAgICAgIHRoaXMuY3VzdG9tZXJzZXJ2aWNlXHJcbiAgICAgICAgLmdldGN1c3RvbWVyYnlpZChkYXRhLmlkKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnBhcnNlQ3VzdG9tZXJzKGRhdGEuanNvbigpKSk7XHJcbiAgICB9IGVsc2UgaWYgKHRlbXByb2xlID09PSBcInJlcFwiKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwicmVwXCIpO1xyXG4gICAgICB0aGlzLnNhdmVlbXBsb3llZS5zZXRTY29wZShkYXRhLmlkKTtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jdXN0b21lcmxpc3RcIl0pO1xyXG4gICAgfSBlbHNlIGlmICh0ZW1wcm9sZSA9PT0gXCJtYW5hZ2VyXCIpIHtcclxuICAgICAgY29uc29sZS5sb2coXCJtYW5hZ2VyXCIpO1xyXG4gICAgICB0aGlzLnNhdmVlbXBsb3llZS5zZXRTY29wZShkYXRhLmlkKTtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9tYW5hZ2Vyb3JkZXJsaXN0XCJdKTtcclxuICAgIH0gXHJcbiAgICAvLyBlbHNlIGlmICh0ZW1wcm9sZSA9PT0gXCJhZG1pblwiKSB7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKFwiYWRtaW5cIik7XHJcbiAgICAvLyAgIHRoaXMuc2F2ZWVtcGxveWVlLnNldFNjb3BlKGRhdGEuaWQpO1xyXG4gICAgLy8gICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3Byb2R1Y3RsaXN0XCJdKTtcclxuICAgIC8vIH1cclxuXHJcbiAgICB0aGlzLmRhdGEgPSB7XHJcbiAgICAgIG1vYmlsZV9ubzogdXNlcm5hbWUsXHJcbiAgICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcclxuICAgICAgcmVnaXN0cmF0aW9uX2lkOiB0aGlzLnJlZ2lkXHJcbiAgICB9O1xyXG4gICAgY29uc29sZS5sb2coXCJSRUcgSUQ6IFwiICsgdGhpcy5yZWdpZCk7XHJcbiAgICBjb25zb2xlLmxvZyhcIlJlZyBkYXRhOiBcIiArIEpTT04uc3RyaW5naWZ5KHRoaXMuZGF0YSkpO1xyXG4gICAgdGhpcy5odHRwXHJcbiAgICAgIC5wb3N0KFxyXG4gICAgICAgIFwiaHR0cHM6Ly9hcHAua3Jpc2huYWJoYXZhbmZvb2RzLmluL2FwaS91c2VyL2FkZHJlZ2lzdHJhdGlvbmlkXCIsXHJcbiAgICAgICAgdGhpcy5kYXRhLFxyXG4gICAgICAgIHsgaGVhZGVyczogdGhpcy5nZXRDb21tb25IZWFkZXJzKCkgfVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgcmVzdWx0ID0+IHtcclxuICAgICAgICAgIC8vdGhpcy5jdXN0b21lcnJlcG9ydChyZXN1bHQuanNvbigpKVxyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJSZWcgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdC5qc29uKCkpKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVnIEVycm9yOiBcIiArIGVycm9yKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuICBwYXJzZUN1c3RvbWVycyhkYXRhOiBhbnkpIHtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEuc2hvcF9uYW1lKTtcclxuXHJcbiAgICB0aGlzLmN1c3RvbWVyLnNob3BuYW1lID0gZGF0YS5zaG9wX25hbWU7XHJcbiAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RuYW1lID0gZGF0YS5uYW1lO1xyXG4gICAgdGhpcy5jdXN0b21lci5jb250YWN0bnVtYmVyID0gZGF0YS5wcmltYXJ5X21vYmlsZV9ubztcclxuICAgIHRoaXMuY3VzdG9tZXIuZ3N0bm8gPSBkYXRhLmdzdF9ubztcclxuICAgIHRoaXMuYWRkcmVzcy5jaXR5ID0gZGF0YS5hZGRyZXNzWzBdLmNpdHk7XHJcbiAgICB0aGlzLmFkZHJlc3MuZG9vcl9ubyA9IGRhdGEuYWRkcmVzc1swXS5kb29yX25vO1xyXG4gICAgdGhpcy5hZGRyZXNzLmFyZWEgPSBkYXRhLmFkZHJlc3NbMF0uYXJlYTtcclxuICAgIHRoaXMuYWRkcmVzcy5waW4gPSBkYXRhLmFkZHJlc3NbMF0ucGluO1xyXG4gICAgdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lID0gZGF0YS5hZGRyZXNzWzBdLnN0cmVldF9uYW1lO1xyXG4gICAgdGhpcy5hZGRyZXNzLmRpc3RyaWN0ID0gZGF0YS5hZGRyZXNzWzBdLmRpc3RyaWN0O1xyXG4gICAgdGhpcy5hZGRyZXNzLnN0YXRlID0gZGF0YS5hZGRyZXNzWzBdLnN0YXRlO1xyXG5cclxuICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IHRoaXMuYWRkcmVzcztcclxuXHJcbiAgICB0aGlzLnNhdmVjdXN0b21lcmRldGFpbHMuc2V0U2NvcGUodGhpcy5jdXN0b21lcik7XHJcbiAgICB0aGlzLnNhdmVlbXBsb3llZS5zZXRTY29wZShkYXRhLmVtcGxveWVlX2lkLiRvaWQpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q29tbW9uSGVhZGVycygpIHtcclxuICAgIGxldCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoKTtcclxuICAgIGhlYWRlcnMuc2V0KFwiQ29udGVudC1UeXBlXCIsIFwiYXBwbGljYXRpb24vanNvblwiKTtcclxuICAgIHJldHVybiBoZWFkZXJzO1xyXG4gIH1cclxufVxyXG4iXX0=