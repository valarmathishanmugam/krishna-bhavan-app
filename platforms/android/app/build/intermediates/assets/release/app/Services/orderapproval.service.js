"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var OrderApproval = (function () {
    function OrderApproval(http) {
        this.http = http;
    }
    OrderApproval.prototype.approveorder = function (id, status, manager_id) {
        console.log("status of approve order" + status);
        var updatestatus = { "status": status, "sm_approved_by": manager_id };
        console.log("update status" + updatestatus);
        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/order_approval/" + id, updatestatus, { headers: this.getCommonHeaders() })
            .subscribe(function (result) {
            console.log("dataresult: " + result);
        }, function (error) {
            console.log("Order Approval Error: " + error);
            //this.orderplaceerror();
        });
    };
    OrderApproval.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    OrderApproval.prototype.updateorder = function () {
    };
    return OrderApproval;
}());
OrderApproval = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], OrderApproval);
exports.OrderApproval = OrderApproval;
