import { CustomerService } from "../../Services/customer.service";
import { Address, Customer } from "../../Models/customer.model";
import {
  SaveCustomer,
  SaveCustomerDetails,
  SaveEmployeeId
} from "../../Services/saveorder.service";
import { loginService } from "../../Services/login.service";
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-telerik-ui/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui/sidedrawer/angular";
import { TextField } from "ui/text-field";
import { ToolTip } from "nativescript-tooltip";
import { confirm } from "ui/dialogs";
import {
  AndroidApplication,
  AndroidActivityBackPressedEventData
} from "application";
import * as application from "application";
import { Location } from "@angular/common";
import { Http, Headers, Response } from "@angular/http";
import * as PushNotifications from "nativescript-push-notifications";
//import * as pushPlugin from "nativescript-push-notifications";
import * as localStorage from "nativescript-localstorage";
import { CustomerDetail } from "../../Components/Sales_rep/CustomerDetail/customerdetail.component";
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
@Component({
  selector: "login-comp",
  moduleId: module.id,
  templateUrl: "login.component.html",
  styleUrls: ["login.component.css"]
})
export class LoginComponent implements OnInit {
  customer = new Customer();
  address = new Address();
  usernamevalid = "collapse";
  passwordvalid = "collapse";
  incorrectmsg = "collapse";
  servermsg = "";
  tempdata: any;
  data;
  regid;
  @ViewChild("cb1") FirstCheckBox: ElementRef;
  checkvalue: boolean;
  storeusername = "";
  storepassword = "";
  customerdetailbyid: any;
  loader = new LoadingIndicator();

  // private pushSettings = {
  //   senderID: "472232624874", // Required: setting with the sender/project number
  //   notificationCallbackAndroid: (
  //     stringifiedData: String,
  //     fcmNotification: any
  //   ) => {
  //     const notificationBody = fcmNotification && fcmNotification.getBody();
  //     console.log(
  //       "Message received!\n" + notificationBody + "\n" + stringifiedData
  //     );
  //   }
  // };

  constructor(
    private routerExtensions: RouterExtensions,
    private page: Page,
    private loginservice: loginService,
    private savecustomer: SaveCustomer,
    private customerservice: CustomerService,
    private savecustomerdetails: SaveCustomerDetails,
    private location: Location,
    private saveemployee: SaveEmployeeId,
    private http: Http
  ) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
    application.android.on(
      AndroidApplication.activityBackPressedEvent,
      (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        //alert("Back Pressed")
        //this.location.back();
        //this.exitApp(data.cancel);
      }
    );

    let settings = {
      senderID: "472232624874",
      notificationCallbackAndroid: (data, notification) => {
        console.log("DATA: " + JSON.stringify(data));
        console.log("NOTIFICATION: " + JSON.stringify(notification));
        const notificationBody = notification && notification.getBody();
        console.log("notificationBody: " + notificationBody);
        alert("New Order: \n" + notificationBody);
        //this.updateMessage("Message received!\n" + notificationBody + "\n" + data);
      }
    };

    //   pushPlugin.register(this.pushSettings, (token: String) => {
    //     alert("Device registered. Access token: " + token);;
    // }, function() { });
    //PushNotifications.onTokenRefresh

    //Commented by Balaji
    PushNotifications.register(
      settings,
      data => {
        console.log("REGISTRATION ID: " + data);
        this.regid = data;
        PushNotifications.onMessageReceived(
          settings.notificationCallbackAndroid
        );
      },
      error => {
        console.log("Firebase Error: " + error);
      }
    );
    //Commented by Balaji
    /*var currentdate = new Date();    
      console.log("Date: " + currentdate.getDate())
      var date = currentdate.toISOString();
      console.log("ISO Date: " + date)
      */

    if (localStorage.getItem("local username") !== null) {
      //console.log("username not null")
      this.storeusername = localStorage.getItem("local username");
    } else {
      //console.log("username is null")
    }

    if (localStorage.getItem("local password") !== null) {
      //console.log("password not null")
      this.storepassword = localStorage.getItem("local password");
    } else {
      //console.log("password is null")
    }
    //console.log("username Storage: " + this.storeusername + " password storage: " + this.storepassword);
  }

  submit(username, password, args) {
    //username="9500231761";
    //password = "dummypass";

    this.usernamevalid = "collapse";
    this.passwordvalid = "collapse";
    this.incorrectmsg = "collapse";

    //console.log("username: "+ username + "password: "+ password);

    this.checkvalue = this.FirstCheckBox.nativeElement.checked;
    console.log("Checked value: " + this.checkvalue);

    if (this.checkvalue == true) {
      //console.log("Its Truee...")
      localStorage.setItem("local username", username);
      localStorage.setItem("local password", password);
    } else if (this.checkvalue == false) {
      //console.log("Its False...")
    }

    if (username === "") {
      console.log("username is empty");
      this.usernamevalid = "visible";
    } else if (password === "") {
      console.log("password is empty");
      this.passwordvalid = "visible";
    } else {
      this.showLoadingIndicator();
      console.log("Call Login Service");
      this.loginservice.login(username, password).subscribe(
        data => {
          this.loader.hide();
          console.log("Login Data: " + data.text());
          this.incorrectmsg = "visible";
          this.servermsg = data.text();
          this.parseusers(username, password, data.json());
        },
        err => {
          console.log("Login Error: " + err);
        }
      );
    }
    //(error) => {
    // console.log("Login error: " + error)
    //}
    //this.routerExtensions.navigate(["/customerlist"]);
    //this.routerExtensions.navigate(["/productlist"]);
    //let view = <TextField.textfield>args.object;
    //const tip = new ToolTip(view,{text:"Some Text",backgroundColor:"pink",textColor:"black"});
    //tip.show();
  }
  showLoadingIndicator(){
   let options = {
      message: 'Loading...',
      progress: 0.65,
      android: {
        indeterminate: true,
        cancelable: false,
        max: 100,
        progressNumberFormat: "%1d/%2d",
        progressPercentFormat: 0.53,
        progressStyle: 1,
        secondaryProgress: 1
      },
      ios: {
        details: "Additional detail note!",
        square: false,
        margin: 10,
        dimBackground: true,
        color: "#4B9ED6",
        mode:"" // see iOS specific options below
      }
    };
     
    this.loader.show(options);
  }

  parseusers(username, password, data) {
    console.log("loginId: " + data.id);

    var temprole = data.role;
    this.loginservice.setScope(data);

    if (temprole === "customer") {
      console.log("customer");
      this.routerExtensions.navigate(["/productlist"]);
      this.savecustomer.setScope(data.id);
      this.customerservice
        .getcustomerbyid(data.id)
        .subscribe(data => this.parseCustomers(data.json()));
    } else if (temprole === "rep") {
      console.log("rep");
      this.saveemployee.setScope(data.id);
      this.routerExtensions.navigate(["/customerlist"]);
    } else if (temprole === "manager") {
      console.log("manager");
      this.saveemployee.setScope(data.id);
      this.routerExtensions.navigate(["/managerorderlist"]);
    } 
    // else if (temprole === "admin") {
    //   console.log("admin");
    //   this.saveemployee.setScope(data.id);
    //   this.routerExtensions.navigate(["/productlist"]);
    // }

    this.data = {
      mobile_no: username,
      password: password,
      registration_id: this.regid
    };
    console.log("REG ID: " + this.regid);
    console.log("Reg data: " + JSON.stringify(this.data));
    this.http
      .post(
        "https://app.krishnabhavanfoods.in/api/user/addregistrationid",
        this.data,
        { headers: this.getCommonHeaders() }
      )
      .subscribe(
        result => {
          //this.customerreport(result.json())
          console.log("Reg Result: " + JSON.stringify(result.json()));
        },
        error => {
          console.log("Reg Error: " + error);
        }
      );
  }

  parseCustomers(data: any) {
    console.log(data.shop_name);

    this.customer.shopname = data.shop_name;
    this.customer.contactname = data.name;
    this.customer.contactnumber = data.primary_mobile_no;
    this.customer.gstno = data.gst_no;
    this.address.city = data.address[0].city;
    this.address.door_no = data.address[0].door_no;
    this.address.area = data.address[0].area;
    this.address.pin = data.address[0].pin;
    this.address.street_name = data.address[0].street_name;
    this.address.district = data.address[0].district;
    this.address.state = data.address[0].state;

    this.customer.address = this.address;

    this.savecustomerdetails.setScope(this.customer);
    this.saveemployee.setScope(data.employee_id.$oid);
  }

  getCommonHeaders() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    return headers;
  }
}
