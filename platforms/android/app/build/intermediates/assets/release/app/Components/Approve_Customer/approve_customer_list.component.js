"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_model_1 = require("../../Models/customer.model");
var customer_service_1 = require("../../Services/customer.service");
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ApproveCustomerList = (function () {
    function ApproveCustomerList(customerservice, page, location, routerExtensions) {
        this.customerservice = customerservice;
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.customers = new Array();
        this.loader = new LoadingIndicator();
    }
    ApproveCustomerList.prototype.ngOnInit = function () {
        //code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        //code for loading indicator end here
        this.getcustomer();
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });     
    };
    ApproveCustomerList.prototype.getcustomer = function () {
        var _this = this;
        this.customerservice.getunapprovedcustomer()
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
    };
    ApproveCustomerList.prototype.parseCustomers = function (data) {
        this.loader.hide();
        //console.log("data: " + JSON.stringify(data));
        console.log(data[0]._id.$oid);
        for (var i = 0; i < data.length; i++) {
            this.customer = new customer_model_1.Customer();
            this.customer.id = data[i]._id.$oid;
            this.customer.shopname = data[i].shop_name;
            this.customer.contactname = data[i].name;
            this.customers.push(this.customer);
        }
    };
    return ApproveCustomerList;
}());
ApproveCustomerList = __decorate([
    core_1.Component({
        selector: "approve-customer-list",
        moduleId: module.id,
        templateUrl: "approve_customer_list.component.html",
        styleUrls: ["approve_customer_list.component.css"]
    }),
    __metadata("design:paramtypes", [customer_service_1.CustomerService, page_1.Page, common_1.Location, router_1.RouterExtensions])
], ApproveCustomerList);
exports.ApproveCustomerList = ApproveCustomerList;
