"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../Services/login.service");
var dialog_component_1 = require("../Dialog/dialog.component");
var product_service_1 = require("../../Services/product.service");
var saveorder_service_1 = require("../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var product_model_1 = require("../../Models/product.model");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var platform_1 = require("platform");
var http_1 = require("@angular/http");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ProductList = (function () {
    // enableMessage = 'collapse';
    function ProductList(route, routerExtensions, http, page, saveorderservice, productservice, savecustomer, savetotal, saveproducts, modalService, viewContainerRef, loginservice, location) {
        this.route = route;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.page = page;
        this.saveorderservice = saveorderservice;
        this.productservice = productservice;
        this.savecustomer = savecustomer;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Product List";
        this.productDetails = [];
        this.productsOrdered = [];
        this.productid = [];
        this.orderquantity = [];
        this.rackquantity = [];
        this.productname = [];
        this.productprice = [];
        this.productdiscount = [];
        // commented by valar
        this.discount_product_id = [];
        this.discount_product_name = [];
        this.offer_quantity = [];
        this.base_quantity = [];
        this.discount_product_quantity = [];
        this.line_total = [];
        //qtyedit: string =""
        this.boolean = false;
        this.Productproperty = [];
        this.loader = new LoadingIndicator();
        this.products = new Array();
        this.original = new Array();
        this.offervisible = "collapse";
        this.customerselected = "collapse";
        this.totalvisible = "collapse";
        //let searchBar = <SearchBar>args.object;
        //searchBar.dismissSoftInput();
    }
    ProductList.prototype.ngOnInit = function () {
        var _this = this;
        // code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        // code for loading indicator end here
        this.page.actionBarHidden = false;
        this.ifcustomer = "visible";
        /*
        this.productDetails.push({id: 1, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 2, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 3, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 4, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 5, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        */
        //const id = this.route.snapshot.params["id"];
        //console.log(id);
        this.getproducts();
        this.tempuser = this.loginservice.getScope();
        this.checkcustomer(this.tempuser);
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
        this.getcustomer = this.savecustomer.getScope();
        if (this.getcustomer !== false && this.getcustomer.length != 0) {
            console.log("Customer Length: " + this.getcustomer.length);
            this.customerselected = "visible";
            if (this.tempuser.role !== 'customer') {
                this.totalvisible = "visible";
            }
        }
    };
    ProductList.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            return;
        }
    };
    ProductList.prototype.getproducts = function () {
        var _this = this;
        this.productservice.getproduct()
            .subscribe(function (data) { return _this.parseProducts(data.json()); });
    };
    ProductList.prototype.parseProducts = function (data) {
        this.loader.hide();
        console.log("data.length:" + data.length);
        for (var i = 0; i < data.length; i++) {
            this.offervisible = "collapse";
            var tempid = "";
            //this.addvisibility[i] = "visible";
            //this.editvisibility[i] = "collapse";
            if (data[i].discount) {
                for (var a = 0; a < 1; a++) {
                    console.log("data.length:" + data.length);
                    // created by valarmathi
                    this.offervisible = "visible";
                    console.log("Offer Inside Region: " + data[i].discount[a].region);
                    tempid = data[i].discount[a]._id.$oid;
                    if (data[i].discount) {
                        this.offers = new product_model_1.Offer();
                        this.offers.offervisible = "visible";
                        this.offers.base_quantity = data[i].discount[a].base_quantity;
                        this.offers.offer_quantity = data[i].discount[a].offer_quantity;
                    }
                    // commented by valarmathi
                    // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
                    //     var tempUserRegion = this.tempuser.region.find(item => item == data[i].discount[a].region);
                    // }
                    // else{
                    //     var tempUserRegion = this.tempuser.region;
                    // }
                    // console.log("USER REGION =====>"+tempUserRegion);
                    // if(data[i].discount[a].region === tempUserRegion){
                    //     this.offervisible = "visible";
                    //     console.log("Offer Inside Region: " + data[i].discount[a].region)
                    //     tempid = data[i].discount[a].discount_id.$oid;
                    //     // this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" +tempid)
                    //     // .subscribe(datas => {
                    //     //     this.offerDetails = datas;
                    //     //     console.log("product offer details"+this.offerDetails.json());
                    //     // });
                    //     //.subscribe(data =>this.saveoffers.setScope(data));
                    //     //this.tempoffer = this.saveoffers.getScope();
                    //     //console.log("TempOffer: " + JSON.stringify(this.tempoffer))     
                    //     //this.productIn.discount_id = tempid; 
                    //     if(data[i].discount_list){
                    //         this.offers = new Offer();
                    //         this.offers.offervisible = "visible"
                    //         this.offers.base_quantity = data[i].discount_list[0][0].base_quantity;
                    //         this.offers.offer_quantity = data[i].discount_list[0][0].offer_quantity;
                    //     }                 
                    //     //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    // }
                    // else{
                    //     //this.productIn.discount_id = ""; 
                    //     this.offers = new Offer();
                    //     this.offers.offervisible = "collapse"
                    // }
                }
            }
            else {
                //this.productIn.discount_id = ""; 
                this.offers = new product_model_1.Offer();
                this.offers.offervisible = "collapse";
            }
            this.productIn = new product_model_1.Product();
            this.productIn.product_id = data[i]._id.$oid;
            this.productIn.product_name = data[i].name;
            this.productIn.categoryName = data[i].category_name;
            this.productIn.case_quantity = data[i].case_quantity;
            //this.productIn.discount_id = "5a91165f9733343884af261d";
            this.productIn.discount_id = tempid;
            // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
            //   for(let j=0;j<this.tempuser.region.length;j++){
            //     for(let k=0;k<data[i].price.length;k++){
            //         if(this.tempuser.region[j] == data[i].price[k].name){
            //             this.productIn.product_price = data[i].price[k].price;
            //         }
            //     }
            //   }
            // }
            // else if(this.tempuser.role == "customer"){
            //     for(let m=0;m<data[i].price.length;m++){
            //         if(this.tempuser.region == data[i].price[m].name){
            //             this.productIn.product_price = data[i].price[m].price;
            //         }
            //     }
            // }
            this.productIn.product_price = data[i].price[0].price;
            if (data[i].discount) {
                this.productIn.discount = data[i].discount[0];
            }
            if (data[i].image_name) {
                //console.log(data[i].name + " : " + data[i].image_name)           
                this.productIn.image = "https://app.krishnabhavanfoods.in/" + data[i].image_name;
            }
            else {
                //console.log(data[i].name + " : " + "Noo")
                this.productIn.image = "~/Images/product_img.png";
            }
            this.productIn.offer = this.offers;
            this.products.push(this.productIn);
            this.original.push(this.productIn);
        }
        this.saveproducts.setScope(this.products);
        this.orderedproducts = this.saveorderservice.getScope();
        console.log("Ordered products: " + this.orderedproducts);
        if (this.orderedproducts !== false) {
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else {
            return;
        }
        // }
    };
    ProductList.prototype.addtocart = function (productid, index) {
        var _this = this;
        console.log("id: " + productid);
        this.boolean = false;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            if (this.productsOrdered[i]["product_id"] == productid) {
                console.log("already inserted");
                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: " + rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, productid); });
    };
    ProductList.prototype.setresult = function (quantity, productid) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            rack = String(rack);
            var order = quantity[1];
            order = String(order);
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            for (var i = 0; i < this.products.length; i++) {
                if (this.products[i]["product_id"] == productid) {
                    this.temp = order;
                    this.total = this.products[i]["product_price"];
                    this.total = this.total * this.temp;
                    //this.total = String(this.total);
                    //console.log("this.line_total: "+ this.total);
                    if (this.boolean === false) {
                        this.rackQty(productid, rack, order, this.total);
                    }
                    else {
                        this.rackQtyedit(rack, order, this.total, this.index, productid);
                    }
                    console.log("promptid: " + this.products[i]["product_id"]);
                    this.products[i]["rackquantity"] = rack;
                    this.products[i]["quantity"] = order;
                }
            }
        }
    };
    ProductList.prototype.rackQty = function (productid, rack, order, line_total) {
        console.log("Inside rack Qty");
        this.productid.push(productid);
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);
        for (var i = 0; i < this.products.length; i++) {
            if (this.products[i].product_id == productid) {
                this.productname.push(this.products[i].product_name);
                this.productprice.push(String(this.products[i].product_price));
                if (this.products[i].discount) {
                    var nodiscount = false;
                    console.log("GET Discount values ==>" + JSON.stringify(this.products[i].discount['_id'].$oid));
                    this.productdiscount.push(this.products[i].discount['_id'].$oid);
                    console.log("DISCOUNT LIST" + JSON.stringify(this.products[i].discount));
                    // commented by valar for hide discount - start
                    //  this.discount_product_id.push(this.products[i].discount['discount_product_id'].$oid);
                    //  this.base_quantity.push(this.products[i].discount['base_quantity'] +"");
                    //  this.offer_quantity.push(this.products[i].discount['offer_quantity']+"");
                    //  if(this.products[i].discount['discount_product_name']){
                    //  this.discount_product_name.push(this.products[i].discount['discount_product_name']);
                    //  }
                    //  else {
                    //     this.discount_product_name.push("");   
                    //  }
                    //  let baseQuantity = parseInt(this.products[i].discount['base_quantity']);
                    //  let offer_quantity = parseInt(this.products[i].discount['offer_quantity']);
                    //  if(order >= baseQuantity){
                    //      var remainder = order%baseQuantity;
                    //      var quotient = Math.floor(order/baseQuantity);
                    //      var offerQuantity = (quotient * offer_quantity);
                    //      this.discount_product_quantity.push(offerQuantity+"");                   
                    //  }
                    // commented by valar for hide discount - end                 
                }
                else {
                    var nodiscount = true;
                }
            }
        }
        //console.log("line_total: " + this.line_total);        
        var temp = (this.productid.length) - 1;
        console.log("temp value" + temp);
        if (nodiscount === false) {
            console.log("Inserting discount product");
            for (var d = 0; d < this.products.length; d++) {
                if (this.products[d].product_id == this.productid[temp]) {
                    var discount_id = this.products[d].discount_id;
                    var temp1 = this.productdiscount.indexOf(discount_id);
                    console.log("temp1 value" + temp1);
                }
            }
            this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
                quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
                line_total: this.line_total[temp], discount_id: this.productdiscount[temp1], product_price: this.productprice[temp]
                // commented by valar for hide discount - start
                //  ,discount_product_id:this.discount_product_id[temp1],discount_product_quantity:this.discount_product_quantity[temp1],discount_product_name:this.discount_product_name[temp1],offer_quantity:this.offer_quantity[temp1],base_quantity:this.base_quantity[temp1]
                // commented by valar for hide discount - end
            });
        }
        else if (nodiscount === true) {
            console.log("Inserting No Discount Product");
            this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
                quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
                line_total: this.line_total[temp], product_price: this.productprice[temp] });
        }
        this.gettotal();
    };
    ProductList.prototype.rackQtyedit = function (rack, order, line_total, index, productid) {
        console.log("Inside rack Qty edit");
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        // commented by valar for hide discount - start
        //  for(let i=0; i < this.products.length; i++) { 
        //      if(this.products[i].product_id == productid){
        //          if(this.products[i].discount){ 
        //             let baseQuantity = parseInt(this.products[i].discount['base_quantity']); 
        //             let offer_Quantity = parseInt(this.products[i].discount['offer_quantity']);
        //              if(order >= baseQuantity){
        //                  var remainder = order%baseQuantity;
        //                  var quotient = Math.floor(order/baseQuantity);
        //                  var offerQuantity = (quotient * offer_Quantity);
        //                  this.productsOrdered[index]["discount_product_quantity"] = offerQuantity+"";                        
        //              }    
        //       }
        //     }
        //  }
        // commented by valar for hide discount - end
        this.gettotal();
    };
    ProductList.prototype.gettotal = function () {
        this.totalAmount = 0;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal + this.totalAmount;
            console.log("totalamont: " + this.totalAmount);
            //this.totalAmount = String(this.totalAmount);
        }
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    };
    ProductList.prototype.productdetail = function () {
        //this.routerExtensions.navigate(["/productdetail"]);
    };
    ProductList.prototype.updateproducts = function (orderedproducts) {
        for (var a = 0; a < this.products.length; a++) {
            for (var i = 0; i < orderedproducts.length; i++) {
                if (this.products[a]["product_id"] == orderedproducts[i]["product_id"]) {
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    };
    ProductList.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductList.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
    };
    ProductList.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductList.prototype.searchBarLoaded = function (args) {
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return ProductList;
}());
ProductList = __decorate([
    core_1.Component({
        selector: "product-list",
        moduleId: module.id,
        templateUrl: "ProductList.component.html",
        styleUrls: ["ProductList.component.css"]
    }),
    __metadata("design:paramtypes", [router_2.ActivatedRoute, router_1.RouterExtensions, http_1.Http,
        page_1.Page, saveorder_service_1.SaveOrderService, product_service_1.ProductService,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef, login_service_1.loginService,
        common_1.Location])
], ProductList);
exports.ProductList = ProductList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJvZHVjdExpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUHJvZHVjdExpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsOERBQTREO0FBRTVELCtEQUFvRDtBQUNwRCxrRUFBOEU7QUFDOUUsc0VBQTZGO0FBQzdGLHNDQUF1RTtBQUV2RSxzREFBK0Q7QUFDL0Qsc0RBQXFEO0FBQ3JELDBDQUFpRDtBQUNqRCw0REFBNEQ7QUFFNUQsa0VBQTJGO0FBQzNGLHFDQUFtQztBQUNuQyxzQ0FBcUM7QUFDckMsMkNBQXNGO0FBQ3RGLHlDQUEyQztBQUMzQywwQ0FBMkM7QUFDM0MsSUFBSSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztBQVN0RixJQUFhLFdBQVc7SUFrRHBCLDhCQUE4QjtJQUU5QixxQkFBb0IsS0FBcUIsRUFBVSxnQkFBa0MsRUFBVSxJQUFVLEVBQzdGLElBQVUsRUFBVSxnQkFBa0MsRUFBVSxjQUE4QixFQUM5RixZQUEwQixFQUFVLFNBQW9CLEVBQVUsWUFBMEIsRUFDNUYsWUFBZ0MsRUFBVSxnQkFBa0MsRUFBVSxZQUEwQixFQUNoSCxRQUFrQjtRQUpWLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFDN0YsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUYsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDNUYsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQ2hILGFBQVEsR0FBUixRQUFRLENBQVU7UUF0RDlCLFVBQUssR0FBVyxjQUFjLENBQUM7UUFDL0IsbUJBQWMsR0FBa0IsRUFBRSxDQUFDO1FBQ25DLG9CQUFlLEdBQWtCLEVBQUUsQ0FBQztRQUdwQyxjQUFTLEdBQWtCLEVBQUUsQ0FBQztRQUM5QixrQkFBYSxHQUFrQixFQUFFLENBQUM7UUFDbEMsaUJBQVksR0FBa0IsRUFBRSxDQUFDO1FBQ2pDLGdCQUFXLEdBQWtCLEVBQUUsQ0FBQztRQUNoQyxpQkFBWSxHQUFrQixFQUFFLENBQUM7UUFDakMsb0JBQWUsR0FBa0IsRUFBRSxDQUFDO1FBRXBDLHFCQUFxQjtRQUNyQix3QkFBbUIsR0FBa0IsRUFBRSxDQUFDO1FBQ3hDLDBCQUFxQixHQUFrQixFQUFFLENBQUM7UUFDMUMsbUJBQWMsR0FBa0IsRUFBRSxDQUFDO1FBQ25DLGtCQUFhLEdBQWtCLEVBQUUsQ0FBQztRQUNsQyw4QkFBeUIsR0FBa0IsRUFBRSxDQUFDO1FBRzlDLGVBQVUsR0FBa0IsRUFBRSxDQUFDO1FBRy9CLHFCQUFxQjtRQUNyQixZQUFPLEdBQVcsS0FBSyxDQUFDO1FBRXhCLG9CQUFlLEdBQVUsRUFBRSxDQUFDO1FBRTVCLFdBQU0sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7UUFJekIsYUFBUSxHQUFjLElBQUksS0FBSyxFQUFFLENBQUM7UUFHbEMsYUFBUSxHQUFjLElBQUksS0FBSyxFQUFFLENBQUM7UUFTbEMsaUJBQVksR0FBRyxVQUFVLENBQUM7UUFDMUIscUJBQWdCLEdBQUcsVUFBVSxDQUFDO1FBQzlCLGlCQUFZLEdBQUcsVUFBVSxDQUFBO1FBVXhCLHlDQUF5QztRQUN6QywrQkFBK0I7SUFDbkMsQ0FBQztJQUVMLDhCQUFRLEdBQVI7UUFBQSxpQkF3REM7UUF2REcsd0NBQXdDO1FBQ3hDLElBQUksT0FBTyxHQUFHO1lBQ1YsT0FBTyxFQUFFLFlBQVk7WUFDckIsUUFBUSxFQUFFLElBQUk7WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsYUFBYSxFQUFFLElBQUk7Z0JBQ25CLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixHQUFHLEVBQUUsR0FBRztnQkFDUixvQkFBb0IsRUFBRSxTQUFTO2dCQUMvQixxQkFBcUIsRUFBRSxJQUFJO2dCQUMzQixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsaUJBQWlCLEVBQUUsQ0FBQzthQUNyQjtZQUNELEdBQUcsRUFBRTtnQkFDSCxPQUFPLEVBQUUseUJBQXlCO2dCQUNsQyxNQUFNLEVBQUUsS0FBSztnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBQyxFQUFFLENBQUMsaUNBQWlDO2FBQzFDO1NBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzFCLHNDQUFzQztRQUV4QyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUE7UUFDM0I7Ozs7OztVQU1FO1FBQ0YsOENBQThDO1FBQzlDLGtCQUFrQjtRQUNsQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDMUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUN4RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1lBQ2xDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFBLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQ2xDLENBQUM7UUFDSCxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2QsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO1FBQ2hDLENBQUM7UUFDRCxJQUFJLENBQUEsQ0FBQztZQUNELE1BQU0sQ0FBQztRQUNYLENBQUM7SUFDTCxDQUFDO0lBQ0QsaUNBQVcsR0FBWDtRQUFBLGlCQUdDO1FBRkcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUU7YUFDL0IsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBL0IsQ0FBK0IsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxtQ0FBYSxHQUFiLFVBQWMsSUFBUztRQUVuQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRW5CLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUxQyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUVoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQztZQUMvQixJQUFJLE1BQU0sR0FBRSxFQUFFLENBQUM7WUFDZixvQ0FBb0M7WUFDcEMsc0NBQXNDO1lBSXRDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFDO2dCQUVqQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO29CQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQzFDLHdCQUF3QjtvQkFDeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7b0JBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDakUsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztvQkFDdEMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUM7d0JBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxxQkFBSyxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQTt3QkFDcEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7d0JBQzlELElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDO29CQUNwRSxDQUFDO29CQUdMLDBCQUEwQjtvQkFDMUIsdUVBQXVFO29CQUN2RSxrR0FBa0c7b0JBQ2xHLElBQUk7b0JBQ0osUUFBUTtvQkFDUixpREFBaUQ7b0JBQ2pELElBQUk7b0JBR0osb0RBQW9EO29CQUNwRCxxREFBcUQ7b0JBQ3JELHFDQUFxQztvQkFDckMsd0VBQXdFO29CQUN4RSxxREFBcUQ7b0JBQ3JELGtHQUFrRztvQkFDbEcsK0JBQStCO29CQUMvQix3Q0FBd0M7b0JBQ3hDLDRFQUE0RTtvQkFDNUUsYUFBYTtvQkFDYiwyREFBMkQ7b0JBRTNELHFEQUFxRDtvQkFDckQseUVBQXlFO29CQUN6RSw4Q0FBOEM7b0JBQzlDLGlDQUFpQztvQkFDakMscUNBQXFDO29CQUNyQywrQ0FBK0M7b0JBQy9DLGlGQUFpRjtvQkFDakYsbUZBQW1GO29CQUNuRix5QkFBeUI7b0JBR3pCLHlHQUF5RztvQkFDekcsSUFBSTtvQkFFSixRQUFRO29CQUNSLDBDQUEwQztvQkFDMUMsaUNBQWlDO29CQUNqQyw0Q0FBNEM7b0JBQzVDLElBQUk7Z0JBSVIsQ0FBQztZQUNMLENBQUM7WUFDRCxJQUFJLENBQUEsQ0FBQztnQkFDRCxtQ0FBbUM7Z0JBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxxQkFBSyxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQTtZQUN6QyxDQUFDO1lBSUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHVCQUFPLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzNDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDcEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUNyRCwwREFBMEQ7WUFFMUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1lBRXBDLHVFQUF1RTtZQUN2RSxvREFBb0Q7WUFDcEQsK0NBQStDO1lBQy9DLGdFQUFnRTtZQUNoRSxxRUFBcUU7WUFDckUsWUFBWTtZQUNaLFFBQVE7WUFDUixNQUFNO1lBQ04sSUFBSTtZQUNKLDZDQUE2QztZQUM3QywrQ0FBK0M7WUFDL0MsNkRBQTZEO1lBQzdELHFFQUFxRTtZQUNyRSxZQUFZO1lBQ1osUUFBUTtZQUVSLElBQUk7WUFDSixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUV0RCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQztnQkFFakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxDQUFDO1lBSUQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFBLENBQUM7Z0JBQ25CLG1FQUFtRTtnQkFDbkUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsb0NBQW9DLEdBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztZQUNwRixDQUFDO1lBQ0QsSUFBSSxDQUFBLENBQUM7Z0JBQ0QsMkNBQTJDO2dCQUMzQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRywwQkFBMEIsQ0FBQTtZQUNyRCxDQUFDO1lBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZDLENBQUM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFFekQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxLQUFLLENBQUMsQ0FBQSxDQUFDO1lBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNoRCxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDRCxNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0wsSUFBSTtJQUNKLENBQUM7SUFFRCwrQkFBUyxHQUFULFVBQVUsU0FBUyxFQUFDLEtBQUs7UUFBekIsaUJBdUJDO1FBdEJHLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUcsQ0FBQyxFQUFFLEVBQUcsQ0FBQztZQUNuRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFBLENBQUM7Z0JBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFFaEMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUUsUUFBUSxHQUFHLFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxPQUFPLEdBQXVCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBQztZQUM1QyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCO1NBQzFDLENBQUM7UUFFRixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyx5QkFBTSxFQUFFLE9BQU8sQ0FBQzthQUMzQyxJQUFJLENBQUMsVUFBQyxZQUEyQixJQUFLLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUMsU0FBUyxDQUFDLEVBQXRDLENBQXNDLENBQUMsQ0FBQztJQUVuRixDQUFDO0lBRU0sK0JBQVMsR0FBaEIsVUFBaUIsUUFBUSxFQUFDLFNBQVM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUUsUUFBUSxDQUFDLENBQUM7UUFFbEMsRUFBRSxDQUFBLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxDQUFBLENBQUM7WUFDbkIsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQUksQ0FBQSxDQUFDO1lBQ0wsSUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsR0FBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUcxRCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDLENBQUM7Z0JBQzFDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUEsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7b0JBQ2xCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2xDLGtDQUFrQztvQkFDbEMsK0NBQStDO29CQUMvQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssQ0FBQyxDQUFBLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyRCxDQUFDO29CQUNELElBQUksQ0FBQSxDQUFDO3dCQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3BFLENBQUM7b0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUMxRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFDeEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQ3pDLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRyw2QkFBTyxHQUFQLFVBQVEsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsVUFBVTtRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFakMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBRXpDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxDQUFBLENBQUM7Z0JBRXpDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBSS9ELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQztvQkFDMUIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDN0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRWpFLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUM5RCwrQ0FBK0M7b0JBRXhELHlGQUF5RjtvQkFDekYsNEVBQTRFO29CQUM1RSw2RUFBNkU7b0JBQzdFLDJEQUEyRDtvQkFDM0Qsd0ZBQXdGO29CQUN4RixLQUFLO29CQUNMLFVBQVU7b0JBQ1YsOENBQThDO29CQUM5QyxLQUFLO29CQUNMLDRFQUE0RTtvQkFDNUUsK0VBQStFO29CQUMvRSw4QkFBOEI7b0JBQzlCLDJDQUEyQztvQkFDM0Msc0RBQXNEO29CQUN0RCx3REFBd0Q7b0JBQ3hELGlGQUFpRjtvQkFDakYsS0FBSztvQkFFSCw4REFBOEQ7Z0JBRXBFLENBQUM7Z0JBQ0QsSUFBSSxDQUFBLENBQUM7b0JBQ0QsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUMxQixDQUFDO1lBR0wsQ0FBQztRQUNMLENBQUM7UUFFRCx3REFBd0Q7UUFFeEQsSUFBSSxJQUFJLEdBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBQyxJQUFJLENBQUMsQ0FBQztRQUcvQixFQUFFLENBQUEsQ0FBQyxVQUFVLEtBQUssS0FBSyxDQUFDLENBQUEsQ0FBQztZQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDMUMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUNwQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDcEQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7b0JBQy9DLElBQUksS0FBSyxHQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN2RCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckMsQ0FBQztZQUVMLENBQUM7WUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDbkYsUUFBUSxFQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsYUFBYSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsWUFBWSxFQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO2dCQUM3RyxVQUFVLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxXQUFXLEVBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsRUFBRSxhQUFhLEVBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7Z0JBRS9HLCtDQUErQztnQkFFaEQsa1FBQWtRO2dCQUVsUSw2Q0FBNkM7YUFDaEQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUNELElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLENBQUEsQ0FBQztZQUd6QixPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7WUFFN0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQyxVQUFVLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxNQUFNLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25GLFFBQVEsRUFBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLGFBQWEsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLFlBQVksRUFBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQkFDN0csVUFBVSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsYUFBYSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBRWxGLENBQUM7UUFDRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELGlDQUFXLEdBQVgsVUFBWSxJQUFJLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBQyxLQUFLLEVBQUMsU0FBUztRQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFFcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3BELElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBRXRELCtDQUErQztRQUdoRCxrREFBa0Q7UUFDbEQscURBQXFEO1FBQ3JELDJDQUEyQztRQUMzQyx3RkFBd0Y7UUFDeEYsMEZBQTBGO1FBQzFGLDBDQUEwQztRQUMxQyx1REFBdUQ7UUFDdkQsa0VBQWtFO1FBQ2xFLG9FQUFvRTtRQUNwRSx3SEFBd0g7UUFDeEgscUJBQXFCO1FBRXJCLFVBQVU7UUFFVixRQUFRO1FBQ1IsS0FBSztRQUNKLDZDQUE2QztRQUM5QyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDhCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFHLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDbkQsdURBQXVEO1lBQ3ZELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDL0MsOENBQThDO1FBQ2xELENBQUM7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELG1DQUFhLEdBQWI7UUFDSSxxREFBcUQ7SUFDekQsQ0FBQztJQUVELG9DQUFjLEdBQWQsVUFBZSxlQUFlO1FBRTFCLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQztZQUN4QyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQztnQkFDMUMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUNuRSxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDdkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzlELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDakQsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVNLDhCQUFRLEdBQWYsVUFBZ0IsSUFBSTtRQUNoQixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdCLElBQUksV0FBVyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFBLE1BQU0sSUFBRyxPQUFBLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztZQUNyRyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFEVCxDQUNTLENBQUMsQ0FBQztRQUN6RCxFQUFFLENBQUEsQ0FBQyxvQkFBUyxDQUFDLENBQUEsQ0FBQztZQUNWLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbkMsQ0FBQztJQUNMLENBQUM7SUFFTSxtQ0FBYSxHQUFwQixVQUFxQixJQUFJO1FBQ3JCLElBQUksU0FBUyxHQUFjLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkMsSUFBSSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQyw2Q0FBNkM7UUFDN0MscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQSxNQUFNLElBQUcsT0FBQSxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDckcsTUFBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBRFQsQ0FDUyxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLDZCQUFPLEdBQWQsVUFBZSxJQUFJO1FBQ2YsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM3QixTQUFTLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNwQixFQUFFLENBQUEsQ0FBQyxvQkFBUyxDQUFDLENBQUEsQ0FBQztZQUNWLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbkMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQ0FBZSxHQUFmLFVBQWdCLElBQUk7UUFDaEIsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxFQUFFLENBQUEsQ0FBQyxvQkFBUyxDQUFDLENBQUEsQ0FBQztZQUNWLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbkMsQ0FBQztJQUNMLENBQUM7SUFFTCxrQkFBQztBQUFELENBQUMsQUF0aEJELElBc2hCQztBQXRoQlksV0FBVztJQVB2QixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFDLGNBQWM7UUFDdkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBQyw0QkFBNEI7UUFDeEMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7S0FDM0MsQ0FBQztxQ0FzRDZCLHVCQUFjLEVBQTRCLHlCQUFnQixFQUFnQixXQUFJO1FBQ3ZGLFdBQUksRUFBNEIsb0NBQWdCLEVBQTBCLGdDQUFjO1FBQ2hGLGdDQUFZLEVBQXFCLDZCQUFTLEVBQXdCLDhCQUFZO1FBQzlFLGlDQUFrQixFQUE0Qix1QkFBZ0IsRUFBd0IsNEJBQVk7UUFDdEcsaUJBQVE7R0F4RHJCLFdBQVcsQ0FzaEJ2QjtBQXRoQlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzdGFjayB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWUnO1xyXG5pbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tICcuLi8uLi9TZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZUFycmF5IH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUtYXJyYXknO1xyXG5pbXBvcnQgeyBEaWFsb2cgfSBmcm9tICcuLi9EaWFsb2cvZGlhbG9nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFByb2R1Y3RTZXJ2aWNlLCBTYXZlUHJvZHVjdHMgfSBmcm9tICcuLi8uLi9TZXJ2aWNlcy9wcm9kdWN0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTYXZlT3JkZXJTZXJ2aWNlLCBTYXZlQ3VzdG9tZXIsIFNhdmVUb3RhbCB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL3NhdmVvcmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q29udGFpbmVyUmVmLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgcHJvbXB0LCBpbnB1dFR5cGUgfSBmcm9tIFwidWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBQcm9kdWN0LCBPZmZlciB9IGZyb20gXCIuLi8uLi9Nb2RlbHMvcHJvZHVjdC5tb2RlbFwiO1xyXG5pbXBvcnQgeyBTZWFyY2hCYXIgfSBmcm9tIFwidWkvc2VhcmNoLWJhclwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UsIE1vZGFsRGlhbG9nT3B0aW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2dcIjtcclxuaW1wb3J0IHtpc0FuZHJvaWR9IGZyb20gXCJwbGF0Zm9ybVwiO1xyXG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IEFuZHJvaWRBcHBsaWNhdGlvbiwgQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEgfSBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0ICogYXMgYXBwbGljYXRpb24gZnJvbSAnYXBwbGljYXRpb24nO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbnZhciBMb2FkaW5nSW5kaWNhdG9yID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1sb2FkaW5nLWluZGljYXRvci1uZXdcIikuTG9hZGluZ0luZGljYXRvcjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6XCJwcm9kdWN0LWxpc3RcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDpcIlByb2R1Y3RMaXN0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcIlByb2R1Y3RMaXN0LmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9kdWN0TGlzdHtcclxuXHJcbiAgICB0aXRsZTogc3RyaW5nID0gXCJQcm9kdWN0IExpc3RcIjtcclxuICAgIHByb2R1Y3REZXRhaWxzOiBBcnJheTxPYmplY3Q+ID0gW107XHJcbiAgICBwcm9kdWN0c09yZGVyZWQ6IEFycmF5PE9iamVjdD4gPSBbXTtcclxuICAgIG9yZGVyZWRwcm9kdWN0czogYW55O1xyXG4gICAgY3VzdG9tZXJpZDogYW55O1xyXG4gICAgcHJvZHVjdGlkOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBvcmRlcnF1YW50aXR5OiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICByYWNrcXVhbnRpdHk6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuICAgIHByb2R1Y3RuYW1lOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBwcm9kdWN0cHJpY2U6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuICAgIHByb2R1Y3RkaXNjb3VudDogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG5cclxuICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhclxyXG4gICAgZGlzY291bnRfcHJvZHVjdF9pZDogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgZGlzY291bnRfcHJvZHVjdF9uYW1lOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBvZmZlcl9xdWFudGl0eTogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgYmFzZV9xdWFudGl0eTogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eTogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICBcclxuICAgIHRvdGFsOiBhbnk7XHJcbiAgICBsaW5lX3RvdGFsOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICB0ZW1wOiBhbnk7XHJcbiAgICB0b3RhbEFtb3VudDogYW55IHwgc3RyaW5nO1xyXG4gICAgLy9xdHllZGl0OiBzdHJpbmcgPVwiXCJcclxuICAgIGJvb2xlYW46Ym9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaW5kZXg6YW55O1xyXG4gICAgUHJvZHVjdHByb3BlcnR5OiBhbnlbXSA9IFtdO1xyXG5cclxuICAgIGxvYWRlciA9IG5ldyBMb2FkaW5nSW5kaWNhdG9yKCk7XHJcbiAgICBcclxuICAgIHB1YmxpYyBzZWFyY2hQaHJhc2U6IHN0cmluZztcclxuXHJcbiAgICBwdWJsaWMgcHJvZHVjdHM6IFByb2R1Y3RbXSA9IG5ldyBBcnJheSgpO1xyXG4gICAgcHVibGljIHByb2R1Y3RJbjogUHJvZHVjdDtcclxuXHJcbiAgICBwdWJsaWMgb3JpZ2luYWw6IFByb2R1Y3RbXSA9IG5ldyBBcnJheSgpO1xyXG5cclxuICAgIHB1YmxpYyBhZGR2aXNpYmlsaXR5O1xyXG4gICAgcHVibGljIGVkaXR2aXNpYmlsaXR5O1xyXG5cclxuICAgIHB1YmxpYyBpZmN1c3RvbWVyO1xyXG4gICAgcHVibGljIG9mZmVyczogT2ZmZXI7XHJcbiAgICBwdWJsaWMgdGVtcHVzZXI7XHJcbiAgICBwdWJsaWMgZ2V0Y3VzdG9tZXI7XHJcbiAgICBwdWJsaWMgb2ZmZXJ2aXNpYmxlID0gXCJjb2xsYXBzZVwiO1xyXG4gICAgcHVibGljIGN1c3RvbWVyc2VsZWN0ZWQgPSBcImNvbGxhcHNlXCI7XHJcbiAgICBwdWJsaWMgdG90YWx2aXNpYmxlID0gXCJjb2xsYXBzZVwiXHJcbiAgICBvZmZlckRldGFpbHM7XHJcbiAgICAvLyBlbmFibGVNZXNzYWdlID0gJ2NvbGxhcHNlJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIGh0dHA6IEh0dHAsXHJcbiAgICAgICAgcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIHNhdmVvcmRlcnNlcnZpY2U6IFNhdmVPcmRlclNlcnZpY2UsIHByaXZhdGUgcHJvZHVjdHNlcnZpY2U6IFByb2R1Y3RTZXJ2aWNlLCBcclxuICAgICAgICBwcml2YXRlIHNhdmVjdXN0b21lcjogU2F2ZUN1c3RvbWVyLCBwcml2YXRlIHNhdmV0b3RhbDogU2F2ZVRvdGFsLCBwcml2YXRlIHNhdmVwcm9kdWN0czogU2F2ZVByb2R1Y3RzLFxyXG4gICAgICAgIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UsIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZiwgcHJpdmF0ZSBsb2dpbnNlcnZpY2U6IGxvZ2luU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbil7XHJcblxyXG4gICAgICAgICAgICAvL2xldCBzZWFyY2hCYXIgPSA8U2VhcmNoQmFyPmFyZ3Mub2JqZWN0O1xyXG4gICAgICAgICAgICAvL3NlYXJjaEJhci5kaXNtaXNzU29mdElucHV0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgLy8gY29kZSBmb3IgbG9hZGluZyBpbmRpY2F0b3Igc3RhcnQgaGVyZVxyXG4gICAgICAgIHZhciBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICBtZXNzYWdlOiAnTG9hZGluZy4uLicsXHJcbiAgICAgICAgICAgIHByb2dyZXNzOiAwLjY1LFxyXG4gICAgICAgICAgICBhbmRyb2lkOiB7XHJcbiAgICAgICAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgICBtYXg6IDEwMCxcclxuICAgICAgICAgICAgICBwcm9ncmVzc051bWJlckZvcm1hdDogXCIlMWQvJTJkXCIsXHJcbiAgICAgICAgICAgICAgcHJvZ3Jlc3NQZXJjZW50Rm9ybWF0OiAwLjUzLFxyXG4gICAgICAgICAgICAgIHByb2dyZXNzU3R5bGU6IDEsXHJcbiAgICAgICAgICAgICAgc2Vjb25kYXJ5UHJvZ3Jlc3M6IDFcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaW9zOiB7XHJcbiAgICAgICAgICAgICAgZGV0YWlsczogXCJBZGRpdGlvbmFsIGRldGFpbCBub3RlIVwiLFxyXG4gICAgICAgICAgICAgIHNxdWFyZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgbWFyZ2luOiAxMCxcclxuICAgICAgICAgICAgICBkaW1CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiM0QjlFRDZcIixcclxuICAgICAgICAgICAgICBtb2RlOlwiXCIgLy8gc2VlIGlPUyBzcGVjaWZpYyBvcHRpb25zIGJlbG93XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmxvYWRlci5zaG93KG9wdGlvbnMpO1xyXG4gICAgICAgICAgLy8gY29kZSBmb3IgbG9hZGluZyBpbmRpY2F0b3IgZW5kIGhlcmVcclxuXHJcbiAgICAgICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaWZjdXN0b21lciA9IFwidmlzaWJsZVwiXHJcbiAgICAgICAgLypcclxuICAgICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe2lkOiAxLCBuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGNhdGVnb3J5OiBcIkRlc3NlcnRzXCIsIGltZzpcIn4vSW1hZ2VzL3Byb2R1Y3QucG5nXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCBvcmRlcnF0eTpcIlwifSlcclxuICAgICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe2lkOiAyLCBuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGNhdGVnb3J5OiBcIkRlc3NlcnRzXCIsIGltZzpcIn4vSW1hZ2VzL3Byb2R1Y3QucG5nXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCBvcmRlcnF0eTpcIlwifSlcclxuICAgICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe2lkOiAzLCBuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGNhdGVnb3J5OiBcIkRlc3NlcnRzXCIsIGltZzpcIn4vSW1hZ2VzL3Byb2R1Y3QucG5nXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCBvcmRlcnF0eTpcIlwifSlcclxuICAgICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe2lkOiA0LCBuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGNhdGVnb3J5OiBcIkRlc3NlcnRzXCIsIGltZzpcIn4vSW1hZ2VzL3Byb2R1Y3QucG5nXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCBvcmRlcnF0eTpcIlwifSlcclxuICAgICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe2lkOiA1LCBuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGNhdGVnb3J5OiBcIkRlc3NlcnRzXCIsIGltZzpcIn4vSW1hZ2VzL3Byb2R1Y3QucG5nXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCBvcmRlcnF0eTpcIlwifSlcclxuICAgICAgICAqL1xyXG4gICAgICAgIC8vY29uc3QgaWQgPSB0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coaWQpO1xyXG4gICAgICAgIHRoaXMuZ2V0cHJvZHVjdHMoKTtcclxuICAgICAgICB0aGlzLnRlbXB1c2VyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKSAgICAgICAgXHJcbiAgICAgICAgdGhpcy5jaGVja2N1c3RvbWVyKHRoaXMudGVtcHVzZXIpO1xyXG4gICAgICAgIGFwcGxpY2F0aW9uLmFuZHJvaWQub24oQW5kcm9pZEFwcGxpY2F0aW9uLmFjdGl2aXR5QmFja1ByZXNzZWRFdmVudCwgKGRhdGE6IEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGRhdGEuY2FuY2VsID0gdHJ1ZTtcclxuICAgICAgICAgICAgLy9hbGVydChcIkJhY2sgUHJlc3NlZFwiKVxyXG4gICAgICAgICAgICAvL3RoaXMubG9jYXRpb24uYmFjaygpO1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMuYmFja1RvUHJldmlvdXNQYWdlKCk7XHJcbiAgICAgICAgICB9KTsgICBcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5nZXRjdXN0b21lciA9IHRoaXMuc2F2ZWN1c3RvbWVyLmdldFNjb3BlKCk7XHJcbiAgICAgICAgICBpZih0aGlzLmdldGN1c3RvbWVyICE9PSBmYWxzZSAmJiB0aGlzLmdldGN1c3RvbWVyLmxlbmd0aCAhPSAwKXsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJDdXN0b21lciBMZW5ndGg6IFwiICsgdGhpcy5nZXRjdXN0b21lci5sZW5ndGgpXHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tZXJzZWxlY3RlZCA9IFwidmlzaWJsZVwiO1xyXG4gICAgICAgICAgICBpZih0aGlzLnRlbXB1c2VyLnJvbGUgIT09ICdjdXN0b21lcicpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3RhbHZpc2libGUgPSBcInZpc2libGVcIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrY3VzdG9tZXIoZGF0YSl7XHJcbiAgICAgICAgaWYoZGF0YS5yb2xlID09PSAnY3VzdG9tZXInKXtcclxuICAgICAgICAgICAgdGhpcy5pZmN1c3RvbWVyID0gXCJjb2xsYXBzZVwiXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBnZXRwcm9kdWN0cygpe1xyXG4gICAgICAgIHRoaXMucHJvZHVjdHNlcnZpY2UuZ2V0cHJvZHVjdCgpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMucGFyc2VQcm9kdWN0cyhkYXRhLmpzb24oKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHBhcnNlUHJvZHVjdHMoZGF0YTogYW55KSB7XHJcbiAgICAgICBcclxuICAgICAgICB0aGlzLmxvYWRlci5oaWRlKCk7ICAgICAgICAgICBcclxuICAgICAgICBcclxuICAgICAgICBjb25zb2xlLmxvZyhcImRhdGEubGVuZ3RoOlwiICsgZGF0YS5sZW5ndGgpO1xyXG4gICAgXHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm9mZmVydmlzaWJsZSA9IFwiY29sbGFwc2VcIjtcclxuICAgICAgICAgICAgdmFyIHRlbXBpZCA9XCJcIjtcclxuICAgICAgICAgICAgLy90aGlzLmFkZHZpc2liaWxpdHlbaV0gPSBcInZpc2libGVcIjtcclxuICAgICAgICAgICAgLy90aGlzLmVkaXR2aXNpYmlsaXR5W2ldID0gXCJjb2xsYXBzZVwiO1xyXG5cclxuICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgaWYoZGF0YVtpXS5kaXNjb3VudCl7ICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgIGZvcih2YXIgYT0wOyBhIDwgMTsgYSsrKXtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImRhdGEubGVuZ3RoOlwiICsgZGF0YS5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNyZWF0ZWQgYnkgdmFsYXJtYXRoaVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub2ZmZXJ2aXNpYmxlID0gXCJ2aXNpYmxlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT2ZmZXIgSW5zaWRlIFJlZ2lvbjogXCIgKyBkYXRhW2ldLmRpc2NvdW50W2FdLnJlZ2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGlkID0gZGF0YVtpXS5kaXNjb3VudFthXS5faWQuJG9pZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoZGF0YVtpXS5kaXNjb3VudCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9mZmVycyA9IG5ldyBPZmZlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vZmZlcnMub2ZmZXJ2aXNpYmxlID0gXCJ2aXNpYmxlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2ZmZXJzLmJhc2VfcXVhbnRpdHkgPSBkYXRhW2ldLmRpc2NvdW50W2FdLmJhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9mZmVycy5vZmZlcl9xdWFudGl0eSA9IGRhdGFbaV0uZGlzY291bnRbYV0ub2ZmZXJfcXVhbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gICAgXHJcbiAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhcm1hdGhpXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYodGhpcy50ZW1wdXNlci5yb2xlID09IFwicmVwXCIgfHwgdGhpcy50ZW1wdXNlci5yb2xlID09IFwibWFuYWdlclwiICl7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHZhciB0ZW1wVXNlclJlZ2lvbiA9IHRoaXMudGVtcHVzZXIucmVnaW9uLmZpbmQoaXRlbSA9PiBpdGVtID09IGRhdGFbaV0uZGlzY291bnRbYV0ucmVnaW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgdmFyIHRlbXBVc2VyUmVnaW9uID0gdGhpcy50ZW1wdXNlci5yZWdpb247XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJVU0VSIFJFR0lPTiA9PT09PT5cIit0ZW1wVXNlclJlZ2lvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYoZGF0YVtpXS5kaXNjb3VudFthXS5yZWdpb24gPT09IHRlbXBVc2VyUmVnaW9uKXtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy5vZmZlcnZpc2libGUgPSBcInZpc2libGVcIjtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgY29uc29sZS5sb2coXCJPZmZlciBJbnNpZGUgUmVnaW9uOiBcIiArIGRhdGFbaV0uZGlzY291bnRbYV0ucmVnaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICB0ZW1waWQgPSBkYXRhW2ldLmRpc2NvdW50W2FdLmRpc2NvdW50X2lkLiRvaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8vIHRoaXMuaHR0cC5nZXQoXCJodHRwczovL2FwcC5rcmlzaG5hYmhhdmFuZm9vZHMuaW4vYXBpL2Rpc2NvdW50L2dldGRpc2NvdW50cy9pZC9cIiArdGVtcGlkKVxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAvLyAuc3Vic2NyaWJlKGRhdGFzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgLy8gICAgIHRoaXMub2ZmZXJEZXRhaWxzID0gZGF0YXM7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8vICAgICBjb25zb2xlLmxvZyhcInByb2R1Y3Qgb2ZmZXIgZGV0YWlsc1wiK3RoaXMub2ZmZXJEZXRhaWxzLmpzb24oKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8vIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAvLy5zdWJzY3JpYmUoZGF0YSA9PnRoaXMuc2F2ZW9mZmVycy5zZXRTY29wZShkYXRhKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAvL3RoaXMudGVtcG9mZmVyID0gdGhpcy5zYXZlb2ZmZXJzLmdldFNjb3BlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8vY29uc29sZS5sb2coXCJUZW1wT2ZmZXI6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy50ZW1wb2ZmZXIpKSAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8vdGhpcy5wcm9kdWN0SW4uZGlzY291bnRfaWQgPSB0ZW1waWQ7IFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICBpZihkYXRhW2ldLmRpc2NvdW50X2xpc3Qpe1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgdGhpcy5vZmZlcnMgPSBuZXcgT2ZmZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIHRoaXMub2ZmZXJzLm9mZmVydmlzaWJsZSA9IFwidmlzaWJsZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICB0aGlzLm9mZmVycy5iYXNlX3F1YW50aXR5ID0gZGF0YVtpXS5kaXNjb3VudF9saXN0WzBdWzBdLmJhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICB0aGlzLm9mZmVycy5vZmZlcl9xdWFudGl0eSA9IGRhdGFbaV0uZGlzY291bnRfbGlzdFswXVswXS5vZmZlcl9xdWFudGl0eTtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgfSAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAvL2NvbnNvbGUubG9nKFwiKioqQnV5XCIgKyB0aGlzLnRlbXBvZmZlci5iYXNlX3F1YW50aXR5ICsgXCJHZXRcIiArIHRoaXMudGVtcG9mZmVyLm9mZmVyX3F1YW50aXR5KSAgICBcclxuICAgICAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgLy90aGlzLnByb2R1Y3RJbi5kaXNjb3VudF9pZCA9IFwiXCI7IFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLm9mZmVycyA9IG5ldyBPZmZlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLm9mZmVycy5vZmZlcnZpc2libGUgPSBcImNvbGxhcHNlXCJcclxuICAgICAgICAgICAgICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5wcm9kdWN0SW4uZGlzY291bnRfaWQgPSBcIlwiOyBcclxuICAgICAgICAgICAgICAgIHRoaXMub2ZmZXJzID0gbmV3IE9mZmVyKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9mZmVycy5vZmZlcnZpc2libGUgPSBcImNvbGxhcHNlXCJcclxuICAgICAgICAgICAgfSAgIFxyXG5cclxuICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0SW4gPSBuZXcgUHJvZHVjdCgpO1xyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5wcm9kdWN0X2lkID0gZGF0YVtpXS5faWQuJG9pZDtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0SW4ucHJvZHVjdF9uYW1lID0gZGF0YVtpXS5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5jYXRlZ29yeU5hbWUgPSBkYXRhW2ldLmNhdGVnb3J5X25hbWU7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLmNhc2VfcXVhbnRpdHkgPSBkYXRhW2ldLmNhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgICAgIC8vdGhpcy5wcm9kdWN0SW4uZGlzY291bnRfaWQgPSBcIjVhOTExNjVmOTczMzM0Mzg4NGFmMjYxZFwiO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0SW4uZGlzY291bnRfaWQgPSB0ZW1waWQ7IFxyXG5cclxuICAgICAgICAgICAgLy8gaWYodGhpcy50ZW1wdXNlci5yb2xlID09IFwicmVwXCIgfHwgdGhpcy50ZW1wdXNlci5yb2xlID09IFwibWFuYWdlclwiICl7XHJcbiAgICAgICAgICAgIC8vICAgZm9yKGxldCBqPTA7ajx0aGlzLnRlbXB1c2VyLnJlZ2lvbi5sZW5ndGg7aisrKXtcclxuICAgICAgICAgICAgLy8gICAgIGZvcihsZXQgaz0wO2s8ZGF0YVtpXS5wcmljZS5sZW5ndGg7aysrKXtcclxuICAgICAgICAgICAgLy8gICAgICAgICBpZih0aGlzLnRlbXB1c2VyLnJlZ2lvbltqXSA9PSBkYXRhW2ldLnByaWNlW2tdLm5hbWUpe1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5wcm9kdWN0X3ByaWNlID0gZGF0YVtpXS5wcmljZVtrXS5wcmljZTtcclxuICAgICAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgICAgIC8vICAgfVxyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYodGhpcy50ZW1wdXNlci5yb2xlID09IFwiY3VzdG9tZXJcIil7XHJcbiAgICAgICAgICAgIC8vICAgICBmb3IobGV0IG09MDttPGRhdGFbaV0ucHJpY2UubGVuZ3RoO20rKyl7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgaWYodGhpcy50ZW1wdXNlci5yZWdpb24gPT0gZGF0YVtpXS5wcmljZVttXS5uYW1lKXtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5wcm9kdWN0SW4ucHJvZHVjdF9wcmljZSA9IGRhdGFbaV0ucHJpY2VbbV0ucHJpY2U7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5wcm9kdWN0X3ByaWNlID0gZGF0YVtpXS5wcmljZVswXS5wcmljZTtcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihkYXRhW2ldLmRpc2NvdW50KXtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5kaXNjb3VudCA9IGRhdGFbaV0uZGlzY291bnRbMF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihkYXRhW2ldLmltYWdlX25hbWUpeyAgICAgXHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGFbaV0ubmFtZSArIFwiIDogXCIgKyBkYXRhW2ldLmltYWdlX25hbWUpICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLmltYWdlID0gXCJodHRwczovL2FwcC5rcmlzaG5hYmhhdmFuZm9vZHMuaW4vXCIrIGRhdGFbaV0uaW1hZ2VfbmFtZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhkYXRhW2ldLm5hbWUgKyBcIiA6IFwiICsgXCJOb29cIilcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLmltYWdlID0gXCJ+L0ltYWdlcy9wcm9kdWN0X2ltZy5wbmdcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLm9mZmVyID0gdGhpcy5vZmZlcnM7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdHMucHVzaCh0aGlzLnByb2R1Y3RJbik7XHJcbiAgICAgICAgICAgIHRoaXMub3JpZ2luYWwucHVzaCh0aGlzLnByb2R1Y3RJbik7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH0gIFxyXG4gICAgICAgIHRoaXMuc2F2ZXByb2R1Y3RzLnNldFNjb3BlKHRoaXMucHJvZHVjdHMpO1xyXG5cclxuICAgICAgICB0aGlzLm9yZGVyZWRwcm9kdWN0cyA9IHRoaXMuc2F2ZW9yZGVyc2VydmljZS5nZXRTY29wZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiT3JkZXJlZCBwcm9kdWN0czogXCIgKyB0aGlzLm9yZGVyZWRwcm9kdWN0cyk7XHJcblxyXG4gICAgICAgIGlmKHRoaXMub3JkZXJlZHByb2R1Y3RzICE9PSBmYWxzZSl7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlcHJvZHVjdHModGhpcy5vcmRlcmVkcHJvZHVjdHMpO1xyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZCA9IHRoaXMub3JkZXJlZHByb2R1Y3RzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIGFkZHRvY2FydChwcm9kdWN0aWQsaW5kZXgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiaWQ6IFwiKyBwcm9kdWN0aWQpOyAgICAgICAgXHJcbiAgICAgICAgdGhpcy5ib29sZWFuID0gZmFsc2U7XHJcbiAgICAgICAgZm9yKHZhciBpPTAgOyBpIDwgdGhpcy5wcm9kdWN0c09yZGVyZWQubGVuZ3RoIDsgaSsrICkge1xyXG4gICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcInByb2R1Y3RfaWRcIl0gPT0gcHJvZHVjdGlkKXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWxyZWFkeSBpbnNlcnRlZFwiKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgcmFja2VkaXQgPSB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcInJhY2tfcXVhbnRpdHlcIl07XHJcbiAgICAgICAgICAgICAgICB2YXIgb3JkZXJlZGl0ID0gdGhpcy5wcm9kdWN0c09yZGVyZWRbaV1bXCJxdWFudGl0eVwiXTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmFjazogXCIrIHJhY2tlZGl0ICsgXCJvcmRlcjogXCIgKyBvcmRlcmVkaXQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sZWFuID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXggPSBpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBjb250ZXh0OiB7IHJhY2s6IHJhY2tlZGl0LCBvcmRlcjogb3JkZXJlZGl0fSxcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52aWV3Q29udGFpbmVyUmVmXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKERpYWxvZywgb3B0aW9ucylcclxuICAgICAgICAudGhlbigoZGlhbG9nUmVzdWx0OiBBcnJheTxzdHJpbmc+KSA9PiB0aGlzLnNldHJlc3VsdChkaWFsb2dSZXN1bHQscHJvZHVjdGlkKSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRyZXN1bHQocXVhbnRpdHkscHJvZHVjdGlkKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJlc3VsdDogXCIrIHF1YW50aXR5KTtcclxuXHJcbiAgICAgICAgaWYocXVhbnRpdHkgPT0gXCJudWxsXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgdmFyIHJhY2sgPSBxdWFudGl0eVswXTtcclxuICAgICAgICByYWNrID0gU3RyaW5nKHJhY2spO1xyXG4gICAgICAgIHZhciBvcmRlciA9IHF1YW50aXR5WzFdO1xyXG4gICAgICAgIG9yZGVyID0gU3RyaW5nKG9yZGVyKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJlczE6IFwiK3F1YW50aXR5WzBdICsgXCJyZXMyOiBcIisgcXVhbnRpdHlbMV0pO1xyXG5cclxuXHJcbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IHRoaXMucHJvZHVjdHMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzW2ldW1wicHJvZHVjdF9pZFwiXSA9PSBwcm9kdWN0aWQpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy50ZW1wID0gb3JkZXI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvdGFsID0gdGhpcy5wcm9kdWN0c1tpXVtcInByb2R1Y3RfcHJpY2VcIl07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvdGFsID0gdGhpcy50b3RhbCp0aGlzLnRlbXA7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMudG90YWwgPSBTdHJpbmcodGhpcy50b3RhbCk7XHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwidGhpcy5saW5lX3RvdGFsOiBcIisgdGhpcy50b3RhbCk7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmJvb2xlYW4gPT09IGZhbHNlKXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJhY2tRdHkocHJvZHVjdGlkLCByYWNrLCBvcmRlciwgdGhpcy50b3RhbCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmFja1F0eWVkaXQocmFjaywgb3JkZXIsIHRoaXMudG90YWwsIHRoaXMuaW5kZXgscHJvZHVjdGlkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicHJvbXB0aWQ6IFwiKyB0aGlzLnByb2R1Y3RzW2ldW1wicHJvZHVjdF9pZFwiXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RzW2ldW1wicmFja3F1YW50aXR5XCJdID0gcmFjaztcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbaV1bXCJxdWFudGl0eVwiXSA9IG9yZGVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4gICAgcmFja1F0eShwcm9kdWN0aWQsIHJhY2ssIG9yZGVyLCBsaW5lX3RvdGFsKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkluc2lkZSByYWNrIFF0eVwiKTtcclxuICAgICAgICB0aGlzLnByb2R1Y3RpZC5wdXNoKHByb2R1Y3RpZCk7ICAgICAgICBcclxuICAgICAgICB0aGlzLm9yZGVycXVhbnRpdHkucHVzaChvcmRlcik7XHJcbiAgICAgICAgdGhpcy5yYWNrcXVhbnRpdHkucHVzaChyYWNrKTtcclxuICAgICAgICB0aGlzLmxpbmVfdG90YWwucHVzaChsaW5lX3RvdGFsKTtcclxuXHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGkgPCB0aGlzLnByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XHJcbiBcclxuICAgICAgICAgICAgaWYodGhpcy5wcm9kdWN0c1tpXS5wcm9kdWN0X2lkID09IHByb2R1Y3RpZCl7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0bmFtZS5wdXNoKHRoaXMucHJvZHVjdHNbaV0ucHJvZHVjdF9uYW1lKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHByaWNlLnB1c2goU3RyaW5nKHRoaXMucHJvZHVjdHNbaV0ucHJvZHVjdF9wcmljZSkpO1xyXG5cclxuICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudCl7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG5vZGlzY291bnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkdFVCBEaXNjb3VudCB2YWx1ZXMgPT0+XCIrSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudFsnX2lkJ10uJG9pZCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdGRpc2NvdW50LnB1c2godGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudFsnX2lkJ10uJG9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJESVNDT1VOVCBMSVNUXCIrSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIHN0YXJ0XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vICB0aGlzLmRpc2NvdW50X3Byb2R1Y3RfaWQucHVzaCh0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50WydkaXNjb3VudF9wcm9kdWN0X2lkJ10uJG9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMuYmFzZV9xdWFudGl0eS5wdXNoKHRoaXMucHJvZHVjdHNbaV0uZGlzY291bnRbJ2Jhc2VfcXVhbnRpdHknXSArXCJcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMub2ZmZXJfcXVhbnRpdHkucHVzaCh0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50WydvZmZlcl9xdWFudGl0eSddK1wiXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICBpZih0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50WydkaXNjb3VudF9wcm9kdWN0X25hbWUnXSl7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMuZGlzY291bnRfcHJvZHVjdF9uYW1lLnB1c2godGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudFsnZGlzY291bnRfcHJvZHVjdF9uYW1lJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLmRpc2NvdW50X3Byb2R1Y3RfbmFtZS5wdXNoKFwiXCIpOyAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIGxldCBiYXNlUXVhbnRpdHkgPSBwYXJzZUludCh0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50WydiYXNlX3F1YW50aXR5J10pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICBsZXQgb2ZmZXJfcXVhbnRpdHkgPSBwYXJzZUludCh0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50WydvZmZlcl9xdWFudGl0eSddKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgaWYob3JkZXIgPj0gYmFzZVF1YW50aXR5KXtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgIHZhciByZW1haW5kZXIgPSBvcmRlciViYXNlUXVhbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICB2YXIgcXVvdGllbnQgPSBNYXRoLmZsb29yKG9yZGVyL2Jhc2VRdWFudGl0eSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICB2YXIgb2ZmZXJRdWFudGl0eSA9IChxdW90aWVudCAqIG9mZmVyX3F1YW50aXR5KTtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgIHRoaXMuZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eS5wdXNoKG9mZmVyUXVhbnRpdHkrXCJcIik7ICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gZW5kICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG5vZGlzY291bnQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwibGluZV90b3RhbDogXCIgKyB0aGlzLmxpbmVfdG90YWwpOyAgICAgICAgXHJcbiAgICAgICBcclxuICAgICAgICB2YXIgdGVtcCA9ICAodGhpcy5wcm9kdWN0aWQubGVuZ3RoKSAtIDE7IFxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwidGVtcCB2YWx1ZVwiK3RlbXApOyBcclxuICAgICAgICBcclxuICAgICAgICBcclxuICAgICAgICBpZihub2Rpc2NvdW50ID09PSBmYWxzZSl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiSW5zZXJ0aW5nIGRpc2NvdW50IHByb2R1Y3RcIik7XHJcbiAgICAgICAgICAgIGZvcih2YXIgZD0wO2Q8dGhpcy5wcm9kdWN0cy5sZW5ndGg7ZCsrKXsgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMucHJvZHVjdHNbZF0ucHJvZHVjdF9pZCA9PSB0aGlzLnByb2R1Y3RpZFt0ZW1wXSl7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGRpc2NvdW50X2lkID0gdGhpcy5wcm9kdWN0c1tkXS5kaXNjb3VudF9pZDtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgdGVtcDEgPSAgdGhpcy5wcm9kdWN0ZGlzY291bnQuaW5kZXhPZihkaXNjb3VudF9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0ZW1wMSB2YWx1ZVwiK3RlbXAxKTsgXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZC5wdXNoKHtwcm9kdWN0X2lkOnRoaXMucHJvZHVjdGlkW3RlbXBdLCBza3VfaWQ6dGhpcy5wcm9kdWN0aWRbdGVtcF0sIFxyXG4gICAgICAgICAgICAgICAgcXVhbnRpdHk6dGhpcy5vcmRlcnF1YW50aXR5W3RlbXBdLCByYWNrX3F1YW50aXR5OnRoaXMucmFja3F1YW50aXR5W3RlbXBdLCBwcm9kdWN0X25hbWU6dGhpcy5wcm9kdWN0bmFtZVt0ZW1wXSwgXHJcbiAgICAgICAgICAgICAgICBsaW5lX3RvdGFsOnRoaXMubGluZV90b3RhbFt0ZW1wXSwgZGlzY291bnRfaWQ6dGhpcy5wcm9kdWN0ZGlzY291bnRbdGVtcDFdLCBwcm9kdWN0X3ByaWNlOnRoaXMucHJvZHVjdHByaWNlW3RlbXBdXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG5cclxuICAgICAgICAgICAgICAgIC8vICAsZGlzY291bnRfcHJvZHVjdF9pZDp0aGlzLmRpc2NvdW50X3Byb2R1Y3RfaWRbdGVtcDFdLGRpc2NvdW50X3Byb2R1Y3RfcXVhbnRpdHk6dGhpcy5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5W3RlbXAxXSxkaXNjb3VudF9wcm9kdWN0X25hbWU6dGhpcy5kaXNjb3VudF9wcm9kdWN0X25hbWVbdGVtcDFdLG9mZmVyX3F1YW50aXR5OnRoaXMub2ZmZXJfcXVhbnRpdHlbdGVtcDFdLGJhc2VfcXVhbnRpdHk6dGhpcy5iYXNlX3F1YW50aXR5W3RlbXAxXVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIGVuZFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZihub2Rpc2NvdW50ID09PSB0cnVlKXtcclxuICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkluc2VydGluZyBObyBEaXNjb3VudCBQcm9kdWN0XCIpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0c09yZGVyZWQucHVzaCh7cHJvZHVjdF9pZDp0aGlzLnByb2R1Y3RpZFt0ZW1wXSwgc2t1X2lkOnRoaXMucHJvZHVjdGlkW3RlbXBdLCBcclxuICAgICAgICAgICAgICAgIHF1YW50aXR5OnRoaXMub3JkZXJxdWFudGl0eVt0ZW1wXSwgcmFja19xdWFudGl0eTp0aGlzLnJhY2txdWFudGl0eVt0ZW1wXSwgcHJvZHVjdF9uYW1lOnRoaXMucHJvZHVjdG5hbWVbdGVtcF0sIFxyXG4gICAgICAgICAgICAgICAgbGluZV90b3RhbDp0aGlzLmxpbmVfdG90YWxbdGVtcF0sIHByb2R1Y3RfcHJpY2U6dGhpcy5wcm9kdWN0cHJpY2VbdGVtcF19KTtcclxuICAgICAgICBcclxuICAgICAgICB9ICBcclxuICAgICAgICB0aGlzLmdldHRvdGFsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmFja1F0eWVkaXQocmFjaywgb3JkZXIsIGxpbmVfdG90YWwsaW5kZXgscHJvZHVjdGlkKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkluc2lkZSByYWNrIFF0eSBlZGl0XCIpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiYWxyZWFkeSBpbnNlcnRlZFwiKTtcclxuICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpbmRleF1bXCJyYWNrX3F1YW50aXR5XCJdID0gcmFjaztcclxuICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpbmRleF1bXCJxdWFudGl0eVwiXSA9IG9yZGVyO1xyXG4gICAgICAgIHRoaXMucHJvZHVjdHNPcmRlcmVkW2luZGV4XVtcImxpbmVfdG90YWxcIl0gPSBsaW5lX3RvdGFsO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG5cclxuXHJcbiAgICAgICAgLy8gIGZvcihsZXQgaT0wOyBpIDwgdGhpcy5wcm9kdWN0cy5sZW5ndGg7IGkrKykgeyBcclxuICAgICAgICAvLyAgICAgIGlmKHRoaXMucHJvZHVjdHNbaV0ucHJvZHVjdF9pZCA9PSBwcm9kdWN0aWQpe1xyXG4gICAgICAgIC8vICAgICAgICAgIGlmKHRoaXMucHJvZHVjdHNbaV0uZGlzY291bnQpeyBcclxuICAgICAgICAvLyAgICAgICAgICAgICBsZXQgYmFzZVF1YW50aXR5ID0gcGFyc2VJbnQodGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudFsnYmFzZV9xdWFudGl0eSddKTsgXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgbGV0IG9mZmVyX1F1YW50aXR5ID0gcGFyc2VJbnQodGhpcy5wcm9kdWN0c1tpXS5kaXNjb3VudFsnb2ZmZXJfcXVhbnRpdHknXSk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgIGlmKG9yZGVyID49IGJhc2VRdWFudGl0eSl7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICB2YXIgcmVtYWluZGVyID0gb3JkZXIlYmFzZVF1YW50aXR5O1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgdmFyIHF1b3RpZW50ID0gTWF0aC5mbG9vcihvcmRlci9iYXNlUXVhbnRpdHkpO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgdmFyIG9mZmVyUXVhbnRpdHkgPSAocXVvdGllbnQgKiBvZmZlcl9RdWFudGl0eSk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpbmRleF1bXCJkaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5XCJdID0gb2ZmZXJRdWFudGl0eStcIlwiOyAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICB9ICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIC8vICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgfVxyXG4gICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBlbmRcclxuICAgICAgICB0aGlzLmdldHRvdGFsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0dG90YWwoKXtcclxuICAgICAgICB0aGlzLnRvdGFsQW1vdW50ID0gMDtcclxuICAgICAgICBmb3IodmFyIGk9MCA7IGkgPCB0aGlzLnByb2R1Y3RzT3JkZXJlZC5sZW5ndGggOyBpKysgKSB7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coaSArXCI6IFwiICsgdGhpcy5wcm9kdWN0c09yZGVyZWRbaV1bXCJpZFwiXSk7XHJcbiAgICAgICAgICAgIHZhciB0ZW1wdG90YWwgPSB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcImxpbmVfdG90YWxcIl07XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxBbW91bnQgPSB0ZW1wdG90YWwgKyB0aGlzLnRvdGFsQW1vdW50OyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInRvdGFsYW1vbnQ6IFwiICsgdGhpcy50b3RhbEFtb3VudCk7XHJcbiAgICAgICAgICAgIC8vdGhpcy50b3RhbEFtb3VudCA9IFN0cmluZyh0aGlzLnRvdGFsQW1vdW50KTtcclxuICAgICAgICB9XHJcbiAgICAgICBcclxuICAgICAgICB0aGlzLnNhdmVvcmRlcnNlcnZpY2Uuc2V0U2NvcGUodGhpcy5wcm9kdWN0c09yZGVyZWQpO1xyXG4gICAgICAgIHRoaXMuc2F2ZXRvdGFsLnNldFNjb3BlKHRoaXMudG90YWxBbW91bnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb2R1Y3RkZXRhaWwoKXtcclxuICAgICAgICAvL3RoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJvZHVjdGRldGFpbFwiXSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlcHJvZHVjdHMob3JkZXJlZHByb2R1Y3RzKXtcclxuICAgICAgICBcclxuICAgICAgICBmb3IodmFyIGE9MDsgYSA8IHRoaXMucHJvZHVjdHMubGVuZ3RoOyBhKyspe1xyXG4gICAgICAgICAgICBmb3IodmFyIGk9MDsgaSA8IG9yZGVyZWRwcm9kdWN0cy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzW2FdW1wicHJvZHVjdF9pZFwiXSA9PSBvcmRlcmVkcHJvZHVjdHNbaV1bXCJwcm9kdWN0X2lkXCJdKXtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk9yZGVyZWQgcHJvZHVjdHMgbmFtZTogXCIgKyBvcmRlcmVkcHJvZHVjdHNbaV1bXCJwcm9kdWN0X25hbWVcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbYV1bXCJyYWNrcXVhbnRpdHlcIl0gPSBvcmRlcmVkcHJvZHVjdHNbaV1bXCJyYWNrX3F1YW50aXR5XCJdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbYV1bXCJxdWFudGl0eVwiXSA9IG9yZGVyZWRwcm9kdWN0c1tpXVtcInF1YW50aXR5XCJdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG90YWxBbW91bnQgPSB0aGlzLnNhdmV0b3RhbC5nZXRTY29wZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwdWJsaWMgb25TdWJtaXQoYXJncykgeyAgICAgICAgXHJcbiAgICAgICAgbGV0IHNlYXJjaEJhciA9IDxTZWFyY2hCYXI+YXJncy5vYmplY3Q7XHJcbiAgICAgICAgc2VhcmNoQmFyLmRpc21pc3NTb2Z0SW5wdXQoKTtcclxuICAgICAgICBsZXQgc2VhcmNoVmFsdWUgPSBzZWFyY2hCYXIudGV4dC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIHRoaXMucHJvZHVjdHMgPSB0aGlzLm9yaWdpbmFsLmZpbHRlcihyZXN1bHQ9PiByZXN1bHQucHJvZHVjdF9uYW1lLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoVmFsdWUpICB8fCBcclxuICAgICAgICByZXN1bHQuY2F0ZWdvcnlOYW1lLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoVmFsdWUpKTtcclxuICAgICAgICBpZihpc0FuZHJvaWQpeyAgICAgICAgXHJcbiAgICAgICAgICAgIHNlYXJjaEJhci5hbmRyb2lkLmNsZWFyRm9jdXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyBvblRleHRDaGFuZ2VkKGFyZ3MpIHtcclxuICAgICAgICBsZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgICAgICBsZXQgc2VhcmNoVmFsdWUgPSBzZWFyY2hCYXIudGV4dC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJzZWFyY2hWYWx1ZTogXCIgKyBzZWFyY2hWYWx1ZSk7XHJcbiAgICAgICAgLy9hbGVydChzZWFyY2hWYWx1ZSk7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0cyA9IHRoaXMub3JpZ2luYWwuZmlsdGVyKHJlc3VsdD0+IHJlc3VsdC5wcm9kdWN0X25hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hWYWx1ZSkgfHwgXHJcbiAgICAgICAgcmVzdWx0LmNhdGVnb3J5TmFtZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKHNlYXJjaFZhbHVlKSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyBvbkNsZWFyKGFyZ3MpIHtcclxuICAgICAgICBsZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgICAgICBzZWFyY2hCYXIuZGlzbWlzc1NvZnRJbnB1dCgpO1xyXG4gICAgICAgIHNlYXJjaEJhci50ZXh0ID0gXCJcIjtcclxuICAgICAgICBpZihpc0FuZHJvaWQpeyAgICAgICAgXHJcbiAgICAgICAgICAgIHNlYXJjaEJhci5hbmRyb2lkLmNsZWFyRm9jdXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoQmFyTG9hZGVkKGFyZ3Mpe1xyXG4gICAgICAgIGxldCBzZWFyY2hCYXIgPSA8U2VhcmNoQmFyPmFyZ3Mub2JqZWN0O1xyXG4gICAgICAgIGlmKGlzQW5kcm9pZCl7ICAgICAgICBcclxuICAgICAgICAgICAgc2VhcmNoQmFyLmFuZHJvaWQuY2xlYXJGb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn0iXX0=