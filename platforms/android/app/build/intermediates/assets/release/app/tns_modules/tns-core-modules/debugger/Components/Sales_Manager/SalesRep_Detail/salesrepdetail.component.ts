import { Component } from "@angular/core";
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import { RouterExtensions } from "nativescript-angular/router";
@Component({
    selector: "salesrep-detail",
    moduleId: module.id,
    templateUrl: "salesrepdetail.component.html",
    styleUrls: ["salesrepdetail.component.css"]
})

export class SalesRepDetail{

    title: string = "Sales Rep Detail";
    name: string = "Ramasamy";
    region:string = "Thiruvallur Region"
    mobile:any = 91+ 901236547;

    constructor(private page: Page, private location: Location, private routerExtensions: RouterExtensions){

    }

    ngOnInit(){
        application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            this.routerExtensions.backToPreviousPage();
          });
    }

}