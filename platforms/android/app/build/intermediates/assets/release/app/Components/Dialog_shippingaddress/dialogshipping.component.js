"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var DialogShipping = (function () {
    function DialogShipping(params) {
        this.params = params;
        this.address = [];
        this.doorno = params.context.doorno;
        this.streetname = params.context.streetname;
        this.area = params.context.area;
        this.city = params.context.city;
        this.district = params.context.district;
        this.state = params.context.state;
        this.pin = params.context.pin;
        console.log("Address in Dialog: " + this.doorno + this.streetname + this.area + this.city);
    }
    DialogShipping.prototype.ok = function (doorno, streetname, area, city, district, state, pin) {
        console.log("VAl1: " + doorno + "VAL2: " + streetname);
        if (doorno == "" || streetname == "" || area == "" || city == "" || district == "" || state == "" ||
            pin == "") {
            alert("Enter Value!!!");
        }
        else {
            console.log("Res1: " + this.doorno + "Res2: " + this.streetname);
            this.address.push(doorno);
            this.address.push(streetname);
            this.address.push(area);
            this.address.push(city);
            this.address.push(district);
            this.address.push(state);
            this.address.push(pin);
            this.params.closeCallback(this.address);
        }
    };
    DialogShipping.prototype.close = function (result) {
        console.log("Cancelled");
        console.log("Res: " + result);
        this.params.closeCallback(result);
    };
    return DialogShipping;
}());
DialogShipping = __decorate([
    core_1.Component({
        selector: "dialog-shipping",
        moduleId: module.id,
        templateUrl: "dialogshipping.component.html",
        styleUrls: ["dialogshipping.component.css"]
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams])
], DialogShipping);
exports.DialogShipping = DialogShipping;
