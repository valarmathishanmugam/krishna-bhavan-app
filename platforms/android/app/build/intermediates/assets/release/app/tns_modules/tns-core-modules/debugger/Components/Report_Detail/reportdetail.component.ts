import { Component } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import { RouterExtensions } from 'nativescript-angular/router';
import { DatePicker } from "ui/date-picker";
import { Http, Headers, Response } from '@angular/http';
import { Report } from '../../Models/order.model';
import { loginService } from '../../Services/login.service';
import { ReportService } from '../../Services/reports.service';


@Component({
    selector: "report-detail",
    moduleId: module.id,
    templateUrl: "reportdetail.component.html",
    styleUrls: ["reportdetail.component.css"]
  })
  export class ReportDetail{
    date: Date;
    fromdate: Date;
    todate: Date;
    isofromdate;
    isotodate;
    getdate: boolean | any;
    setfromdate;
    settodate;
    data;
    tempuser;
    big_total_count= "";
    big_total_amount ="";
    reports: Report[] = new Array();
    reportdetail: Report; 
    detailvisible = "visible"
    bigvisible = "collapse"
  
    constructor(private page: Page, private location: Location, private routerExtensions: RouterExtensions,
      private loginservice: loginService, private http: Http, private reportservice: ReportService){}
  
    ngOnInit() {
      application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        //alert("Back Pressed")
        //this.location.back();
        this.routerExtensions.backToPreviousPage();
      });
      this.tempuser = this.loginservice.getScope();
      this.getuserreport();

      this.getdate = this.reportservice.getdate();
      this.setfromdate = this.getdate["from"]
      this.settodate = this.getdate["to"]
      console.log("Tempfromdate: " + this.setfromdate)
    }


    getuserreport(){
        if(this.tempuser.role === "manager"){
            //console.log("Manager Id: " + this.tempuser.id)
            //alert("Manager Id: " + this.tempuser.id)

            this.reportservice.managerReport(this.tempuser.id)
            .subscribe((result) => {
                this.getmanagerreport(result.json())
                //console.log("Customer Result: " + JSON.stringify(result))
              }, (error) => {
                console.log("Manager report Error: " + error)
              });
            
              }
        
              else if(this.tempuser.role === "rep"){
                //console.log("Rep Id: " + this.tempuser.id)
                //alert("Rep Id: " + this.tempuser.id)

                this.reportservice.repReport(this.tempuser.id)
                .subscribe((result) => {
                    this.salesrepreport(result.json())
                    //console.log("Customer Result: " + JSON.stringify(result))
                  }, (error) => {
                    console.log("Rep report Error: " + error)
                  });
                
                  }
              else if(this.tempuser.role === "customer"){
                //console.log("Customer Id: " + this.tempuser.id)
                //alert("Customer Id: " + this.tempuser.id)
                
                this.reportservice.customerReport(this.tempuser.id)
                .subscribe((result) => {
                    this.customerreport(result.json())
                    //console.log("Customer Result: " + JSON.stringify(result))
                  }, (error) => {
                    console.log("Customer Error: " + error)
                  });
        
                  }
    }

    getmanagerreport(result){
        console.log("Manager Result: " + JSON.stringify(result));
        console.log("Dummy Result: " + result);
        console.log("Manager Report Details: " + result.salesrep_orders)
        //alert("Manager Result: " + JSON.stringify(result))
        this.bigvisible = "visible";
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for(var i=0;i<result.salesrep_orders.length; i++){
         //console.log("Manager Report Details: " + result.salesrep_orders.length)
          //alert("Manager Report Details: " + result.salesrep_orders.length)
          this.reportdetail = new Report();
          this.reportdetail.emp_id = result.salesrep_orders[i].salesrep_id;
          this.reportdetail.order_count = result.salesrep_orders[i].order_count;
          this.reportdetail.order_total = result.salesrep_orders[i].order_total;
      
          this.reports.push(this.reportdetail);
        }
      }
      
      salesrepreport(result){
        //console.log("rep Result: " + JSON.stringify(result));
        //alert("rep Result: " + JSON.stringify(result))
        this.bigvisible = "visible"
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for(var i=0;i<result.customer_orders_details.length; i++){
          console.log("Rep Report Details: " + result.customer_orders_details.length)
          //alert("Manager Report Details: " + result.customer_orders_details.length)
          this.reportdetail = new Report();
          this.reportdetail.emp_id = result.customer_orders_details[i].customer_id;
          this.reportdetail.order_count = result.customer_orders_details[i].order_count;
          this.reportdetail.order_total = result.customer_orders_details[i].order_total;
      
          this.reports.push(this.reportdetail);
        }
      }
      customerreport(result){
        console.log("customer Result: " + JSON.stringify(result));
        //alert("customer Result: " + JSON.stringify(result))+++++++++
        this.bigvisible = "visible"
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
      }

      goback(){
        //alert("Back Back...")
        this.routerExtensions.navigate(["/salesrepreport"]);
      }
}