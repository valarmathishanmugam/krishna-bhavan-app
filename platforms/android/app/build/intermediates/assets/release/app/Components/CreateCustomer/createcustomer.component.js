"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var http_1 = require("@angular/http");
var login_service_1 = require("../../Services/login.service");
var EmailValidator = require("email-validator");
var dialogs = require("ui/dialogs");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var CreateCustomer = (function () {
    function CreateCustomer(routerExtensions, location, http, loginservice) {
        this.routerExtensions = routerExtensions;
        this.location = location;
        this.http = http;
        this.loginservice = loginservice;
        this.disableCreateFAB = true;
        this.loader = new LoadingIndicator();
    }
    CreateCustomer.prototype.ngOnInit = function () {
        console.log(" In Add Customer 1");
        // application.android.on(
        //   AndroidApplication.activityBackPressedEvent,
        //   (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   }
        // );
        this.tempuser = this.loginservice.getScope();
    };
    CreateCustomer.prototype.createcustomer = function (name, shopname, primarymobile, region, contactperson, landlineno, homemobileno, workmobileno, email, gst) {
        var _this = this;
        if (gst) {
            gst = String(gst).toUpperCase();
        }
        var tf1 = this.tempbdoor.nativeElement;
        this.bdoorno = tf1.text;
        var tf2 = this.tempbstreet.nativeElement;
        this.bstreet = tf2.text;
        var tf3 = this.tempbarea.nativeElement;
        this.barea = tf3.text;
        var tf4 = this.tempbcity.nativeElement;
        this.bcity = tf4.text;
        var tf5 = this.tempbdistrict.nativeElement;
        this.bdistrict = tf5.text;
        var tf6 = this.tempbstate.nativeElement;
        this.bstate = tf6.text;
        var tf7 = this.tempbpin.nativeElement;
        this.bpin = tf7.text;
        var tf8 = this.tempsdoor.nativeElement;
        this.sdoorno = tf8.text;
        var tf9 = this.tempsstreet.nativeElement;
        this.sstreet = tf9.text;
        var tf10 = this.tempsarea.nativeElement;
        this.sarea = tf10.text;
        var tf11 = this.tempscity.nativeElement;
        this.scity = tf11.text;
        var tf12 = this.tempsdistrict.nativeElement;
        this.sdistrict = tf12.text;
        var tf13 = this.tempsstate.nativeElement;
        this.sstate = tf13.text;
        var tf14 = this.tempspin.nativeElement;
        this.spin = tf14.text;
        var emailIsValid = EmailValidator.validate(email);
        var errorFlag = false;
        console.log("EMAIL VALIDATION" + emailIsValid);
        if (name === "") {
            alert("Enter Name");
            errorFlag = true;
        }
        else if (shopname === "") {
            alert("Enter Shop Name");
            errorFlag = true;
        }
        else if (primarymobile === "") {
            alert("Enter Primary Mobile Number");
            errorFlag = true;
        }
        else if (primarymobile.length < 10 || primarymobile.length > 10) {
            alert("Primary Mobile Number Should be 10 digits");
            errorFlag = true;
        }
        else if (region === "") {
            alert("Enter Region");
            errorFlag = true;
        }
        else if (contactperson === "") {
            alert("Enter Contact Person");
            errorFlag = true;
        }
        else if (email === "") {
            alert("Enter Email");
            errorFlag = true;
        }
        else if (emailIsValid == false) {
            alert("Enter Valid Email Address");
            errorFlag = true;
        }
        else if (gst === "") {
            alert("Enter GST");
            errorFlag = true;
        }
        else if (gst.length < 15 || gst.length > 15) {
            alert("GST Number Length Should be 15");
            errorFlag = true;
        }
        else if (this.bdoorno === "") {
            alert("Enter Billing Address Door No");
            errorFlag = true;
        }
        else if (this.bstreet === "") {
            alert("Enter Billing Address Street");
            errorFlag = true;
        }
        else if (this.barea === "") {
            alert("Enter Billing Address Area");
            errorFlag = true;
        }
        else if (this.bcity === "") {
            alert("Enter Billing Address City");
            errorFlag = true;
        }
        else if (this.bdistrict === "") {
            alert("Enter Billing Address District");
            errorFlag = true;
        }
        else if (this.bstate === "") {
            alert("Enter Billing Address State");
            errorFlag = true;
        }
        else if (this.bpin === "") {
            alert("Enter Billing Address Pin");
            errorFlag = true;
        }
        else if (this.bpin.length < 6 || this.bpin.length > 6) {
            alert("Pin Number Should be 6 digits");
            errorFlag = true;
        }
        if (homemobileno) {
            if (homemobileno.length < 10 || homemobileno.length > 10) {
                alert("Home Mobile Number Should be 10 digits");
                errorFlag = true;
            }
        }
        if (workmobileno) {
            if (workmobileno.length < 10 || workmobileno.length > 10) {
                alert("Work Mobile Number Should be 10 digits");
                errorFlag = true;
            }
        }
        /*
        else if(this.sdoorno === ""){
          //alert("Enter this.bdoorno")
          this.sdoorno = this.bdoorno
        }
        else if(this.sstreet === ""){
          //alert("Enter name")
          this.sstreet = this.bstreet
        }
        else if(this.sarea === ""){
          //alert("Enter name")
          this.sarea = this.barea
        }
        else if(this.scity === ""){
          //alert("Enter name")
          this.scity = this.bcity
        }
        else if(this.sdistrict === ""){
         // alert("Enter name")
          this.sdistrict = this.bdistrict
        }
        else if(this.sstate === ""){
         //alert("Enter name")
         this.sstate = this.bstate
        }
        else if(this.spin === ""){
          alert("Enter name")
          this.spin = this.bpin
        }
    */
        if (!errorFlag) {
            //code for loading indicator start here
            var options = {
                message: 'Loading...',
                progress: 0.65,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    details: "Additional detail note!",
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#4B9ED6",
                    mode: "" // see iOS specific options below
                }
            };
            this.loader.show(options);
            //code for loading indicator end here
            console.log("In post");
            this.data = {
                name: name,
                gst_no: gst,
                employee_id: this.tempuser.id,
                shop_name: shopname,
                region: region,
                status: "pending",
                contact_details: {
                    work_mobile_no: workmobileno,
                    home_mobile_no: homemobileno,
                    landline_no: landlineno,
                    email: email,
                    contact_person: contactperson
                },
                primary_mobile_no: primarymobile,
                address: [
                    {
                        door_no: this.bdoorno,
                        street_name: this.bstreet,
                        area: this.barea,
                        city: this.bcity,
                        district: this.bdistrict,
                        state: this.bstate,
                        status: "active",
                        pin: this.bpin
                    },
                    {
                        door_no: this.bdoorno,
                        street_name: this.bstreet,
                        area: this.barea,
                        city: this.bcity,
                        district: this.bdistrict,
                        state: this.bstate,
                        status: "active",
                        pin: this.bpin
                    }
                ]
            };
            console.log("Full Customer Data: " + JSON.stringify(this.data));
            return this.http
                .post("https://app.krishnabhavanfoods.in/api/customer/addcustomer/", this.data, { headers: this.getCommonHeaders() })
                .subscribe(function (result) {
                _this.getresult(result);
                //console.log("Customer Result: " + JSON.stringify(result.json))
            }, function (error) {
                console.log("Customer Error: " + error);
                _this.customerCreateerror(error);
            });
        }
    };
    CreateCustomer.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    CreateCustomer.prototype.getresult = function (result) {
        var _this = this;
        this.loader.hide();
        this.disableCreateFAB = false;
        dialogs.alert("Customer Is Created Successfully").then(function () {
            console.log("Customer Result: " + JSON.stringify(result));
            if (_this.tempuser["role"] === "customer") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "rep") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "manager") {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
            }
        });
    };
    CreateCustomer.prototype.customerCreateerror = function (error) {
        var _this = this;
        dialogs.alert("Customer Is Not Created").then(function () {
            if (_this.tempuser["role"] === "customer") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "rep") {
                _this.routerExtensions.navigate(["/orderlist"]);
            }
            else if (_this.tempuser["role"] === "manager") {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
            }
        });
    };
    return CreateCustomer;
}());
__decorate([
    core_1.ViewChild("bdoorno"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbdoor", void 0);
__decorate([
    core_1.ViewChild("bstreet"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbstreet", void 0);
__decorate([
    core_1.ViewChild("barea"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbarea", void 0);
__decorate([
    core_1.ViewChild("bcity"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbcity", void 0);
__decorate([
    core_1.ViewChild("bdistrict"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbdistrict", void 0);
__decorate([
    core_1.ViewChild("bstate"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbstate", void 0);
__decorate([
    core_1.ViewChild("bpin"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempbpin", void 0);
__decorate([
    core_1.ViewChild("sdoorno"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsdoor", void 0);
__decorate([
    core_1.ViewChild("sstreet"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsstreet", void 0);
__decorate([
    core_1.ViewChild("sarea"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsarea", void 0);
__decorate([
    core_1.ViewChild("scity"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempscity", void 0);
__decorate([
    core_1.ViewChild("sdistrict"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsdistrict", void 0);
__decorate([
    core_1.ViewChild("sstate"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempsstate", void 0);
__decorate([
    core_1.ViewChild("spin"),
    __metadata("design:type", core_1.ElementRef)
], CreateCustomer.prototype, "tempspin", void 0);
CreateCustomer = __decorate([
    core_1.Component({
        selector: "create-customer",
        moduleId: module.id,
        templateUrl: "createcustomer.component.html",
        styleUrls: ["createcustomer.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        common_1.Location,
        http_1.Http,
        login_service_1.loginService])
], CreateCustomer);
exports.CreateCustomer = CreateCustomer;
