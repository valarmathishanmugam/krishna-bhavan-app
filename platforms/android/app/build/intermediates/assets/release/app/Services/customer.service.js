"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var login_service_1 = require("./login.service");
var CustomerService = (function () {
    function CustomerService(http, loginservice) {
        this.http = http;
        this.loginservice = loginservice;
        this.getcustomersUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers/active";
    }
    CustomerService.prototype.getcustomer = function () {
        this.tempuser = this.loginservice.getScope();
        var tempid = this.tempuser.id;
        if (this.tempuser["role"] === "manager") {
            //alert("Manager: " + tempid);
            // commented for get all active customers list by valarmathi
            //return this.http.get("https://app.krishnabhavanfoods.in/api/employee/customer/getmanagerreportees/id/"+tempid);
            return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/active");
        }
        else if (this.tempuser["role"] === "rep") {
            //alert("Rep: " + tempid);
            // commented for get all active customers list by valarmathi
            //return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getrepreportees/id/"+tempid);
            return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/active");
        }
        //return this.http.get(this.getcustomersUrl);
    };
    CustomerService.prototype.getcustomerbyid = function (id) {
        this.getcustomersbyidUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/" + id;
        console.log(this.getcustomersbyidUrl);
        return this.http.get(this.getcustomersbyidUrl);
    };
    CustomerService.prototype.getunapprovedcustomer = function () {
        console.log("aprroval changed");
        this.tempuser = this.loginservice.getScope();
        // commented by valarmathi
        // return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getcustomers/toapprove/manager/id/"+this.tempuser.id);
        return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/toapprove");
    };
    return CustomerService;
}());
CustomerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, login_service_1.loginService])
], CustomerService);
exports.CustomerService = CustomerService;
