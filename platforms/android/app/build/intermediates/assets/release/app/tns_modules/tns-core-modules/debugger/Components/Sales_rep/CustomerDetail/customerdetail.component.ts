import { loginService } from '../../../Services/login.service';
import { SaveCustomer, SaveCustomerDetails, SaveOrderService } from '../../../Services/saveorder.service';
import { CustomerService } from '../../../Services/customer.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer, Address} from "../../../Models/customer.model";
import { RouterExtensions } from 'nativescript-angular/router';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import { Page } from 'tns-core-modules/ui/page/page';

@Component({
    selector: "customer-detail",
    moduleId: module.id,
    templateUrl: "customerdetail.component.html",
    styleUrls: ["customerdetail.component.css"]
})

export class CustomerDetail implements OnInit{

    customerid: any;

    title: string = "Customer Detail";
    //contactname: string = "PunniyaMoorthy";
    //shopname: string = "Kanniga Parameshwari Store";
    //contactnumber = "9123654789";
    //gstno = data.gst_no;
    //city = "Chennai";
    //door_no = "No.123";
    //area = "Amman Nagar";
    //pin = "600019";
    //street_name = "2nd Street";
    customer = new Customer();
    address = new Address();
    addordervisible = "visible"
    //address: Address;

    constructor(private route: ActivatedRoute, private customerservice: CustomerService, private routerExtensions: RouterExtensions,
         private savecustomer: SaveCustomer, private savecustomerdetails: SaveCustomerDetails, private loginservice: loginService,
         private saveorderservice: SaveOrderService, private location: Location, private page: Page) { }

    ngOnInit(){
        // defalut datas 
        this.customer.shopname = "Abc Enterprices";
        this.customer.contactname = "Logesh";
        this.customer.contactnumber = 0987654321;
    
        this.address.city = "chennai";
        this.address.door_no = "4";
        this.address.area = "main area";
        this.address.pin = 639008;
        this.address.street_name = "main street";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";

        this.customer.address = this.address;
        //this.address = ({doorno:"No.123", street_name:"",area: ", opp. Eldam's Road, AnnaSalai", city:"Chennai", pin:"611111", contactnumber:"9874563210"})
        
        const id = this.route.snapshot.params["id"];

        //const id = "abcd";
        this.customerid = id;
        this.savecustomer.setScope(this.customerid);

        this.customerservice.getcustomerbyid(id)
        .subscribe(data => this.parseCustomers(data.json()));

        console.log(id);
        
        this.savecustomer.setScope(id);

        application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            this.routerExtensions.backToPreviousPage();
          });
    }

    parseCustomers(data: any) {

        console.log(data.shop_name);

            this.customer.shopname = data.shop_name;
            this.customer.contactname = data.name;
            this.customer.contactnumber = data.primary_mobile_no;
            this.customer.gstno = data.gst_no;
            this.address.city = data.address[0].city;
            this.address.door_no = data.address[0].door_no;
            this.address.area = data.address[0].area;
            this.address.pin = data.address[0].pin;
            this.address.street_name = data.address[0].street_name;
            this.address.district = data.address[0].district;
            this.address.state = data.address[0].state;

            this.customer.address = this.address;

            this.savecustomerdetails.setScope(this.customer);

        }  

        gotoshop(){
            this.routerExtensions.navigate(["/productlist"]);  
            var a = false;
            
            this.saveorderservice.setScope(a);           
        }


}