"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var SalesRepDetail = (function () {
    function SalesRepDetail(page, location, routerExtensions) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.title = "Sales Rep Detail";
        this.name = "Ramasamy";
        this.region = "Thiruvallur Region";
        this.mobile = 91 + 901236547;
    }
    SalesRepDetail.prototype.ngOnInit = function () {
        var _this = this;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    return SalesRepDetail;
}());
SalesRepDetail = __decorate([
    core_1.Component({
        selector: "salesrep-detail",
        moduleId: module.id,
        templateUrl: "salesrepdetail.component.html",
        styleUrls: ["salesrepdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions])
], SalesRepDetail);
exports.SalesRepDetail = SalesRepDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNyZXBkZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2FsZXNyZXBkZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLHNEQUFxRDtBQUNyRCwyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQUMzQyxzREFBK0Q7QUFRL0QsSUFBYSxjQUFjO0lBT3ZCLHdCQUFvQixJQUFVLEVBQVUsUUFBa0IsRUFBVSxnQkFBa0M7UUFBbEYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBTHRHLFVBQUssR0FBVyxrQkFBa0IsQ0FBQztRQUNuQyxTQUFJLEdBQVcsVUFBVSxDQUFDO1FBQzFCLFdBQU0sR0FBVSxvQkFBb0IsQ0FBQTtRQUNwQyxXQUFNLEdBQU8sRUFBRSxHQUFFLFNBQVMsQ0FBQztJQUkzQixDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUFBLGlCQU9DO1FBTkcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsZ0NBQWtCLENBQUMsd0JBQXdCLEVBQUUsVUFBQyxJQUF5QztZQUMxRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQix1QkFBdUI7WUFDdkIsdUJBQXVCO1lBQ3ZCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzdDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVMLHFCQUFDO0FBQUQsQ0FBQyxBQXBCRCxJQW9CQztBQXBCWSxjQUFjO0lBUDFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsK0JBQStCO1FBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO0tBQzlDLENBQUM7cUNBUzRCLFdBQUksRUFBb0IsaUJBQVEsRUFBNEIseUJBQWdCO0dBUDdGLGNBQWMsQ0FvQjFCO0FBcEJZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcInNhbGVzcmVwLWRldGFpbFwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcInNhbGVzcmVwZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcInNhbGVzcmVwZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTYWxlc1JlcERldGFpbHtcclxuXHJcbiAgICB0aXRsZTogc3RyaW5nID0gXCJTYWxlcyBSZXAgRGV0YWlsXCI7XHJcbiAgICBuYW1lOiBzdHJpbmcgPSBcIlJhbWFzYW15XCI7XHJcbiAgICByZWdpb246c3RyaW5nID0gXCJUaGlydXZhbGx1ciBSZWdpb25cIlxyXG4gICAgbW9iaWxlOmFueSA9IDkxKyA5MDEyMzY1NDc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKXtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59Il19