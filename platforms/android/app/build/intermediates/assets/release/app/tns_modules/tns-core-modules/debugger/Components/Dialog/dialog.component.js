"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var Dialog = (function () {
    function Dialog(params) {
        this.params = params;
        this.quantity = [];
        this.rack = params.context.rack;
        this.order = params.context.order;
    }
    Dialog.prototype.ok = function (result1, result2) {
        /* Commented BY Balaji
        if(result1 === "" || result2 === ""){
            alert("Enter Value!!!");
        }
        else if(result1 === "0" || result2 === "0"){
            alert("Enter Valid Value!!!");
        }

        else if(isNaN(Number(result1)) == true || isNaN(Number(result2)) == true){
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else{
            console.log("Res1: "+result1+"Res2: "+result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);
        }
        */
        if (result1 === "") {
            result1 = "0";
        }
        if (result2 === "" || result2 === "0") {
            alert("Enter Value!!!");
        }
        else if (isNaN(Number(result1)) == true || isNaN(Number(result2)) == true) {
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else {
            console.log("Res1: " + result1 + "Res2: " + result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);
        }
    };
    Dialog.prototype.close = function (result) {
        console.log("Cancelled");
        console.log("Res: " + result);
        this.params.closeCallback(result);
    };
    return Dialog;
}());
Dialog = __decorate([
    core_1.Component({
        selector: "dialog",
        moduleId: module.id,
        templateUrl: "dialog.component.html",
        styleUrls: ["dialog.component.css"]
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams])
], Dialog);
exports.Dialog = Dialog;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRpYWxvZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsa0VBQXNFO0FBU3RFLElBQWEsTUFBTTtJQUtmLGdCQUFvQixNQUF5QjtRQUF6QixXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQUY3QyxhQUFRLEdBQWtCLEVBQUUsQ0FBQztRQUd6QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFFdEMsQ0FBQztJQUVNLG1CQUFFLEdBQVQsVUFBVSxPQUFlLEVBQUUsT0FBZTtRQUN0Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBa0JFO1FBRUYsRUFBRSxDQUFBLENBQUMsT0FBTyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEIsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNsQixDQUFDO1FBRUQsRUFBRSxDQUFBLENBQUMsT0FBTyxLQUFLLEVBQUUsSUFBSSxPQUFPLEtBQUssR0FBRyxDQUFDLENBQUEsQ0FBQztZQUNsQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM1QixDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUM1QixLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBQyxPQUFPLEdBQUMsUUFBUSxHQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QyxDQUFDO0lBR1AsQ0FBQztJQUVNLHNCQUFLLEdBQVosVUFBYSxNQUFjO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUUsTUFBTSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUNILGFBQUM7QUFBRCxDQUFDLEFBMURELElBMERDO0FBMURZLE1BQU07SUFQbEIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsdUJBQXVCO1FBQ3BDLFNBQVMsRUFBRSxDQUFDLHNCQUFzQixDQUFDO0tBQ3RDLENBQUM7cUNBTzhCLGdDQUFpQjtHQUxwQyxNQUFNLENBMERsQjtBQTFEWSx3QkFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nUGFyYW1zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJkaWFsb2dcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJkaWFsb2cuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wiZGlhbG9nLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEaWFsb2cge1xyXG4gICAgcmFjazogc3RyaW5nO1xyXG4gICAgb3JkZXI6c3RyaW5nO1xyXG4gICAgcXVhbnRpdHk6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhcmFtczogTW9kYWxEaWFsb2dQYXJhbXMpIHtcclxuICAgICAgICB0aGlzLnJhY2sgPSBwYXJhbXMuY29udGV4dC5yYWNrO1xyXG4gICAgICAgIHRoaXMub3JkZXIgPSBwYXJhbXMuY29udGV4dC5vcmRlcjtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9rKHJlc3VsdDE6IHN0cmluZywgcmVzdWx0Mjogc3RyaW5nKSB7XHJcbiAgICAgICAgLyogQ29tbWVudGVkIEJZIEJhbGFqaVxyXG4gICAgICAgIGlmKHJlc3VsdDEgPT09IFwiXCIgfHwgcmVzdWx0MiA9PT0gXCJcIil7XHJcbiAgICAgICAgICAgIGFsZXJ0KFwiRW50ZXIgVmFsdWUhISFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYocmVzdWx0MSA9PT0gXCIwXCIgfHwgcmVzdWx0MiA9PT0gXCIwXCIpe1xyXG4gICAgICAgICAgICBhbGVydChcIkVudGVyIFZhbGlkIFZhbHVlISEhXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZWxzZSBpZihpc05hTihOdW1iZXIocmVzdWx0MSkpID09IHRydWUgfHwgaXNOYU4oTnVtYmVyKHJlc3VsdDIpKSA9PSB0cnVlKXtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJOb3QgYSBOdW1iZXJcIik7XHJcbiAgICAgICAgICAgIGFsZXJ0KFwiRW50ZXIgVmFsaWQgVmFsdWUhISFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVzMTogXCIrcmVzdWx0MStcIlJlczI6IFwiK3Jlc3VsdDIpO1xyXG4gICAgICAgICAgICB0aGlzLnF1YW50aXR5LnB1c2gocmVzdWx0MSk7XHJcbiAgICAgICAgICAgIHRoaXMucXVhbnRpdHkucHVzaChyZXN1bHQyKTtcclxuICAgICAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjayh0aGlzLnF1YW50aXR5KTsgICAgIFxyXG4gICAgICAgIH0gICBcclxuICAgICAgICAqL1xyXG5cclxuICAgICAgICBpZihyZXN1bHQxID09PSBcIlwiKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdDEgPSBcIjBcIjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHJlc3VsdDIgPT09IFwiXCIgfHwgcmVzdWx0MiA9PT0gXCIwXCIpe1xyXG4gICAgICAgICAgICBhbGVydChcIkVudGVyIFZhbHVlISEhXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKGlzTmFOKE51bWJlcihyZXN1bHQxKSkgPT0gdHJ1ZSB8fCBpc05hTihOdW1iZXIocmVzdWx0MikpID09IHRydWUpe1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5vdCBhIE51bWJlclwiKTtcclxuICAgICAgICAgICAgYWxlcnQoXCJFbnRlciBWYWxpZCBWYWx1ZSEhIVwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJSZXMxOiBcIityZXN1bHQxK1wiUmVzMjogXCIrcmVzdWx0Mik7XHJcbiAgICAgICAgICAgIHRoaXMucXVhbnRpdHkucHVzaChyZXN1bHQxKTtcclxuICAgICAgICAgICAgdGhpcy5xdWFudGl0eS5wdXNoKHJlc3VsdDIpO1xyXG4gICAgICAgICAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKHRoaXMucXVhbnRpdHkpOyAgICAgXHJcbiAgICAgICAgfSAgIFxyXG4gICAgICAgIFxyXG5cclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZShyZXN1bHQ6IHN0cmluZykge1xyXG4gICAgY29uc29sZS5sb2coXCJDYW5jZWxsZWRcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIlJlczogXCIrIHJlc3VsdCk7XHJcbiAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKHJlc3VsdCk7XHJcbiAgfVxyXG59Il19