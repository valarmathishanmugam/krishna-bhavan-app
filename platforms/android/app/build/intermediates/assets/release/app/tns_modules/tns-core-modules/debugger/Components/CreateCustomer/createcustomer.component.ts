import { Component, ViewChild, ElementRef } from "@angular/core";
import { TextField } from "ui/text-field";
import { Location } from "@angular/common";
import { RouterExtensions } from "nativescript-angular/router";
import {
  AndroidApplication,
  AndroidActivityBackPressedEventData
} from "application";
import * as application from "application";
import { Http, Headers, Response } from "@angular/http";
import { loginService } from "../../Services/login.service";
import * as EmailValidator from "email-validator";
import * as dialogs from "ui/dialogs";
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;

@Component({
  selector: "create-customer",
  moduleId: module.id,
  templateUrl: "createcustomer.component.html",
  styleUrls: ["createcustomer.component.css"]
})
export class CreateCustomer {
  name: any;
  shopname;
  primarymobile;
  region;
  contactperson;
  landlineno;
  homemobileno;
  workmobileno;
  email;
  gst;
  bdoorno;
  bstreet;
  barea;
  bcity;
  bdistrict;
  bstate;
  bpin;
  sdoorno;
  sstreet;
  sarea;
  scity;
  sdistrict;
  sstate;
  spin;
  cusname;
  tempgst;
  data: any;
  tempuser;
  disableCreateFAB :boolean = true;
  loader = new LoadingIndicator();

  @ViewChild("bdoorno") tempbdoor: ElementRef;
  @ViewChild("bstreet") tempbstreet: ElementRef;
  @ViewChild("barea") tempbarea: ElementRef;
  @ViewChild("bcity") tempbcity: ElementRef;
  @ViewChild("bdistrict") tempbdistrict: ElementRef;
  @ViewChild("bstate") tempbstate: ElementRef;
  @ViewChild("bpin") tempbpin: ElementRef;
  @ViewChild("sdoorno") tempsdoor: ElementRef;
  @ViewChild("sstreet") tempsstreet: ElementRef;
  @ViewChild("sarea") tempsarea: ElementRef;
  @ViewChild("scity") tempscity: ElementRef;
  @ViewChild("sdistrict") tempsdistrict: ElementRef;
  @ViewChild("sstate") tempsstate: ElementRef;
  @ViewChild("spin") tempspin: ElementRef;

  constructor(
    private routerExtensions: RouterExtensions,
    private location: Location,
    private http: Http,
    private loginservice: loginService
  ) {}

  ngOnInit() {
    console.log(" In Add Customer 1");
    application.android.on(
      AndroidApplication.activityBackPressedEvent,
      (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        //alert("Back Pressed")
        //this.location.back();
        this.routerExtensions.backToPreviousPage();
      }
    );
    this.tempuser = this.loginservice.getScope();
  }

  createcustomer(
    name,
    shopname,
    primarymobile,
    region,
    contactperson,
    landlineno,
    homemobileno,
    workmobileno,
    email,
    gst
  ) {
    if(gst) {
      gst = String(gst).toUpperCase();
    }
    let tf1 = <TextField>this.tempbdoor.nativeElement;
    this.bdoorno = tf1.text;
    let tf2 = <TextField>this.tempbstreet.nativeElement;
    this.bstreet = tf2.text;
    let tf3 = <TextField>this.tempbarea.nativeElement;
    this.barea = tf3.text;
    let tf4 = <TextField>this.tempbcity.nativeElement;
    this.bcity = tf4.text;
    let tf5 = <TextField>this.tempbdistrict.nativeElement;
    this.bdistrict = tf5.text;
    let tf6 = <TextField>this.tempbstate.nativeElement;
    this.bstate = tf6.text;
    let tf7 = <TextField>this.tempbpin.nativeElement;
    this.bpin = tf7.text;
    let tf8 = <TextField>this.tempsdoor.nativeElement;
    this.sdoorno = tf8.text;
    let tf9 = <TextField>this.tempsstreet.nativeElement;
    this.sstreet = tf9.text;
    let tf10 = <TextField>this.tempsarea.nativeElement;
    this.sarea = tf10.text;
    let tf11 = <TextField>this.tempscity.nativeElement;
    this.scity = tf11.text;
    let tf12 = <TextField>this.tempsdistrict.nativeElement;
    this.sdistrict = tf12.text;
    let tf13 = <TextField>this.tempsstate.nativeElement;
    this.sstate = tf13.text;
    let tf14 = <TextField>this.tempspin.nativeElement;
    this.spin = tf14.text;
    let emailIsValid = EmailValidator.validate(email);

    let errorFlag = false;

    console.log("EMAIL VALIDATION" + emailIsValid);
    if (name === "") {
      alert("Enter Name");
      errorFlag = true;
    } else if (shopname === "") {
      alert("Enter Shop Name");
      errorFlag = true;
    } else if (primarymobile === "") {
      alert("Enter Primary Mobile Number");
      errorFlag = true;
    } else if (primarymobile.length < 10 || primarymobile.length > 10) {
      alert("Primary Mobile Number Should be 10 digits");
      errorFlag = true;
    } else if (region === "") {
      alert("Enter Region");
      errorFlag = true;
    } else if (contactperson === "") {
      alert("Enter Contact Person");
      errorFlag = true;
    } else if (email === "") {
      alert("Enter Email");
      errorFlag = true;
    } else if (emailIsValid == false) {
      alert("Enter Valid Email Address");
      errorFlag = true;
    } else if (gst === "") {
      alert("Enter GST");
      errorFlag = true;
    } else if (gst.length < 15 || gst.length > 15) {
      alert("GST Number Length Should be 15");
      errorFlag = true;
    } else if (this.bdoorno === "") {
      alert("Enter Billing Address Door No");
      errorFlag = true;
    } else if (this.bstreet === "") {
      alert("Enter Billing Address Street");
      errorFlag = true;
    } else if (this.barea === "") {
      alert("Enter Billing Address Area");
      errorFlag = true;
    } else if (this.bcity === "") {
      alert("Enter Billing Address City");
      errorFlag = true;
    } else if (this.bdistrict === "") {
      alert("Enter Billing Address District");
      errorFlag = true;
    } else if (this.bstate === "") {
      alert("Enter Billing Address State");
      errorFlag = true;
    } else if (this.bpin === "") {
      alert("Enter Billing Address Pin");
      errorFlag = true;
    } else if (this.bpin.length < 6 || this.bpin.length > 6) {
      alert("Pin Number Should be 6 digits");
      errorFlag = true;
    }
    if (homemobileno) {
      if (homemobileno.length < 10 || homemobileno.length > 10) {
        alert("Home Mobile Number Should be 10 digits");
        errorFlag = true;
      }
    }
    if (workmobileno) {
      if (workmobileno.length < 10 || workmobileno.length > 10) {
        alert("Work Mobile Number Should be 10 digits");
        errorFlag = true;
      }
    }
    /*
    else if(this.sdoorno === ""){
      //alert("Enter this.bdoorno")
      this.sdoorno = this.bdoorno
    }
    else if(this.sstreet === ""){
      //alert("Enter name")
      this.sstreet = this.bstreet
    }
    else if(this.sarea === ""){
      //alert("Enter name")
      this.sarea = this.barea
    }
    else if(this.scity === ""){
      //alert("Enter name")
      this.scity = this.bcity
    }
    else if(this.sdistrict === ""){
     // alert("Enter name")
      this.sdistrict = this.bdistrict
    }
    else if(this.sstate === ""){
     //alert("Enter name")
     this.sstate = this.bstate
    }
    else if(this.spin === ""){
      alert("Enter name")
      this.spin = this.bpin
    }
*/
    if (!errorFlag) {
      //code for loading indicator start here
      let options = {
        message: 'Loading...',
        progress: 0.65,
        android: {
          indeterminate: true,
          cancelable: false,
          max: 100,
          progressNumberFormat: "%1d/%2d",
          progressPercentFormat: 0.53,
          progressStyle: 1,
          secondaryProgress: 1
        },
        ios: {
          details: "Additional detail note!",
          square: false,
          margin: 10,
          dimBackground: true,
          color: "#4B9ED6",
          mode: ""// see iOS specific options below
        }
      };
       
      this.loader.show(options);
       //code for loading indicator end here

      console.log("In post");

      this.data = {
        name: name,
        gst_no: gst,
        employee_id: this.tempuser.id,
        shop_name: shopname,
        region:region,
        status: "pending",
        contact_details: {
          work_mobile_no: workmobileno,
          home_mobile_no: homemobileno,
          landline_no: landlineno,
          email: email,
          contact_person: contactperson
        },
        primary_mobile_no: primarymobile,
        address: [
          {
            door_no: this.bdoorno,
            street_name: this.bstreet,
            area: this.barea,
            city: this.bcity,
            district: this.bdistrict,
            state: this.bstate,
            status: "active",
            pin: this.bpin
          },
          {
            door_no: this.bdoorno,
            street_name: this.bstreet,
            area: this.barea,
            city: this.bcity,
            district: this.bdistrict,
            state: this.bstate,
            status: "active",
            pin: this.bpin
          }
        ]
      };
      console.log("Full Customer Data: " + JSON.stringify(this.data));

      return this.http
        .post(
          "https://app.krishnabhavanfoods.in/api/customer/addcustomer/",
          this.data,
          { headers: this.getCommonHeaders() }
        )
        .subscribe(
          result => {
            this.getresult(result);
            //console.log("Customer Result: " + JSON.stringify(result.json))
          },
          error => {
            console.log("Customer Error: " + error);
            this.customerCreateerror(error);
          }
        );
    }
  }

  getCommonHeaders() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    return headers;
  }
  getresult(result) {
    this.loader.hide();
    this.disableCreateFAB = false;
    dialogs.alert("Customer Is Created Successfully").then(() => {
      console.log("Customer Result: " + JSON.stringify(result));
      if (this.tempuser["role"] === "customer") {
        this.routerExtensions.navigate(["/orderlist"]);
      } else if (this.tempuser["role"] === "rep") {
        this.routerExtensions.navigate(["/orderlist"]);
      } else if (this.tempuser["role"] === "manager") {
        this.routerExtensions.navigate(["/approvecustomerlist"]);
      }
    });
  }
  customerCreateerror(error) {
    dialogs.alert("Customer Is Not Created").then(() => {
      if (this.tempuser["role"] === "customer") {
        this.routerExtensions.navigate(["/orderlist"]);
      } else if (this.tempuser["role"] === "rep") {
        this.routerExtensions.navigate(["/orderlist"]);
      } else if (this.tempuser["role"] === "manager") {
        this.routerExtensions.navigate(["/approvecustomerlist"]);
      }
    });
  }
}
