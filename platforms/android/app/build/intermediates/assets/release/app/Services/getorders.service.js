"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var GetOrderService = (function () {
    function GetOrderService(http) {
        this.http = http;
        this.scope = false;
        this.getcustomersUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers";
    }
    GetOrderService.prototype.getorders = function () {
        return this.http.get(this.getcustomersUrl);
    };
    GetOrderService.prototype.getScope = function () {
        return this.scope;
    };
    GetOrderService.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return GetOrderService;
}());
GetOrderService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], GetOrderService);
exports.GetOrderService = GetOrderService;
