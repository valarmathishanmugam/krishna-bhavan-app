import { Component } from "@angular/core";
import { prompt, inputType } from "ui/dialogs";
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';

@Component({
    selector:"product-detail",
    moduleId: module.id,
    templateUrl:"productdetail.component.html",
    styleUrls: ["productdetail.component.css"]
})

export class ProductDetail{

    title: string = "Product Detail";
    productDetails: Object;
    description: string = `Ready to fry Items, very crispy & tasty,
     traditionally made. Easy to fry on a daily basis Can be fried in Micro oven.`;

     constructor(private page: Page, private location: Location, private routerExtensions: RouterExtensions){}

    ngOnInit(){
        this.productDetails = ({name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free"})
        application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            this.routerExtensions.backToPreviousPage();
          });  
    }

}