"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var getorder_service_1 = require("../../Services/getorder.service");
var login_service_1 = require("../../Services/login.service");
var application_1 = require("application");
var application = require("application");
var OrderNotes = (function () {
    function OrderNotes(routerExtensions, page, getorderservice, route, loginservice) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.getorderservice = getorderservice;
        this.route = route;
        this.loginservice = loginservice;
        console.log("Notes Data1: " + this.orderid);
        this.getorderservice.getorderbyid(this.orderid)
            .subscribe(function (data) { return _this.getorderdetails(data.json()); });
    }
    OrderNotes.prototype.ngOnInit = function () {
        //var tempcustomer = this.loginservice.getScope();
        var _this = this;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    OrderNotes.prototype.getorderdetails = function (data) {
        console.log("Notes Data");
        if (data.notes) {
            console.log("Notes Length: " + data.length);
        }
    };
    return OrderNotes;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], OrderNotes.prototype, "orderid", void 0);
OrderNotes = __decorate([
    core_1.Component({
        selector: "order-notes",
        moduleId: module.id,
        templateUrl: "ordernotes.component.html",
        styleUrls: ["ordernotes.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        login_service_1.loginService])
], OrderNotes);
exports.OrderNotes = OrderNotes;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXJub3Rlcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmRlcm5vdGVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFpRDtBQUNqRCxzREFBK0Q7QUFDL0Qsc0RBQXFEO0FBQ3JELDBDQUFpRDtBQUNqRCxvRUFBMkQ7QUFDM0QsOERBQTREO0FBQzVELDJDQUFzRjtBQUN0Rix5Q0FBMkM7QUFTM0MsSUFBYSxVQUFVO0lBR25CLG9CQUNZLGdCQUFrQyxFQUNsQyxJQUFVLEVBQ1YsZUFBeUIsRUFDekIsS0FBcUIsRUFDckIsWUFBMEI7UUFMdEMsaUJBVUc7UUFUUyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixvQkFBZSxHQUFmLGVBQWUsQ0FBVTtRQUN6QixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUVsQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDMUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUM5QyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFqQyxDQUFpQyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELDZCQUFRLEdBQVI7UUFHSSxrREFBa0Q7UUFIdEQsaUJBV0M7UUFORyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxnQ0FBa0IsQ0FBQyx3QkFBd0IsRUFBRSxVQUFDLElBQXlDO1lBQzVHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0NBQWUsR0FBZixVQUFnQixJQUFJO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDekIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUM7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUMvQyxDQUFDO0lBQ0gsQ0FBQztJQUNQLGlCQUFDO0FBQUQsQ0FBQyxBQWxDRCxJQWtDQztBQWpDWTtJQUFSLFlBQUssRUFBRTs7MkNBQVM7QUFEUixVQUFVO0lBUHRCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsYUFBYTtRQUN2QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLDJCQUEyQjtRQUN4QyxTQUFTLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztLQUMxQyxDQUFDO3FDQU1nQyx5QkFBZ0I7UUFDNUIsV0FBSTtRQUNPLDJCQUFRO1FBQ2xCLHVCQUFjO1FBQ1AsNEJBQVk7R0FSN0IsVUFBVSxDQWtDdEI7QUFsQ1ksZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEdldE9yZGVyIH0gZnJvbSAnLi4vLi4vU2VydmljZXMvZ2V0b3JkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IGxvZ2luU2VydmljZSB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL2xvZ2luLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbmRyb2lkQXBwbGljYXRpb24sIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhIH0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJvcmRlci1ub3Rlc1wiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcIm9yZGVybm90ZXMuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wib3JkZXJub3Rlcy5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgT3JkZXJOb3Rlc3tcclxuICAgIEBJbnB1dCgpIG9yZGVyaWQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZSxcclxuICAgICAgICBwcml2YXRlIGdldG9yZGVyc2VydmljZTogR2V0T3JkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2dpbnNlcnZpY2U6IGxvZ2luU2VydmljZVxyXG4gICAgICApIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIk5vdGVzIERhdGExOiBcIiArdGhpcy5vcmRlcmlkKVxyXG4gICAgICAgIHRoaXMuZ2V0b3JkZXJzZXJ2aWNlLmdldG9yZGVyYnlpZCh0aGlzLm9yZGVyaWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMuZ2V0b3JkZXJkZXRhaWxzKGRhdGEuanNvbigpKSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIG5nT25Jbml0KCkge1xyXG5cclxuICAgICAgICBcclxuICAgICAgICAgIC8vdmFyIHRlbXBjdXN0b21lciA9IHRoaXMubG9naW5zZXJ2aWNlLmdldFNjb3BlKCk7XHJcbiAgXHJcbiAgICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGdldG9yZGVyZGV0YWlscyhkYXRhKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJOb3RlcyBEYXRhXCIpXHJcbiAgICAgICAgaWYoZGF0YS5ub3Rlcyl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTm90ZXMgTGVuZ3RoOiBcIiArIGRhdGEubGVuZ3RoKVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG59Il19