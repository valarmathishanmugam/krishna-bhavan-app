import { loginService } from '../../../Services/login.service';
import { GetOrder } from '../../../Services/getorder.service';
import { OrderApproval } from '../../../Services/orderapproval.service';
import { Customer } from '../../../Models/customer.model';
import { Order } from '../../../Models/order.model';
import { Component, Input } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'ui/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import { SelectedIndexChangedEventData,ValueList } from 'nativescript-drop-down';

@Component({
    selector: "order-list",
    moduleId: module.id,
    templateUrl: "reporderlist.component.html",
    styleUrls: ["reporderlist.component.css"]
})

export class RepOrderList{

    title: string = "Order List";
    orderDetails: Array<Object> = [];
    public allorder: Order[] = new Array();
    public original: Order[] = new Array();
    public order: Order
    public customer:Customer
    public approvein;
    public statusin;
    public ifcustomer;    
    public selectedIndex: number = null;
    public items: ValueList<string>;
    tempdate:Date;
    @Input() id;
    
    constructor(private routerExtensions: RouterExtensions, private getorder: GetOrder, private page: Page,
         private loginservice: loginService, private location: Location){

    }

        ngOnInit(){
            this.page.actionBarHidden = false;

            console.log("In Order list customer id: " + this.id)
            //this.orderDetails.push({rep:"Ramasamy",id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.getorder
            .getmanagerorders(this.id)
            .subscribe(data => this.fetchallorders(data.json()));
            var tempcustomer = this.loginservice.getScope()
            this.checkcustomer(tempcustomer);

            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                //alert("Back Pressed")
                //this.location.back();
                this.routerExtensions.backToPreviousPage();
              });

              this.items = new ValueList<string>();
              this.items.push({
                    value:   "all",
                    display: "All Orders"
                },
                {
                    value:   "not_reviewed",
                    display: "Waiting For Approval"
                },
                {
                    value:   "hold",
                    display: "Hold Orders"
                },
                {
                    value:   "approved",
                    display: "Pending File"
                },
                {
                    value:   "completed",
                    display: "Dispatched Orders"
                }
            );
        }
    
        checkcustomer(data){
            if(data.role === 'customer'){
                this.ifcustomer = "collapse"
            }
            else{
                this.ifcustomer = "visible"
            }
        }
        fetchallorders(data){
            console.log("All manager orders" + data.length)
            for (let i = 0; i < data.length; i++) {
                this.order = new Order();
                this.order._id = data[i].id.$oid;
                this.order.order_id = data[i].order_id;
                //this.tempdate = data[i].order_date;
                //this.order.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
                this.order.order_date = data[i].date;
                
                this.order.total_amount = data[i].total_amount;
                this.customer = new Customer();
                this.customer.shopname = data[i].shop_name;
                this.order.customer = this.customer;
                
                var tempstatus = data[i].status;

                if(tempstatus.toLowerCase() === "hold"){//onHold
                    console.log("status: Hold");
                    this.order.status = "~/Images/hold.png"
                    this.order.approve = "visible"
                  }
                  else if(tempstatus.toLowerCase() === "pending"){//not_reviewed by sales_manager
                    console.log("status: not_reviewed");
                    this.order.status = "~/Images/not_reviewed.png"
                    this.order.approve = "visible"
                  }
                  else if(tempstatus.toLowerCase() === "processed"){//Completed
                    console.log("status: completed");
                    this.order.status = "~/Images/completed.png"
                    this.order.approve = "hidden"
                  }
                  else if(tempstatus.toLowerCase() === "approved"){//Approved & Processing
                    console.log("status: approved");
                    this.order.status = "~/Images/approved.png"
                    this.order.approve = "hidden"
                  }   
                
                this.allorder.push(this.order);
                console.log("all order lists"+this.allorder[0].order_id);
                this.original.push(this.order);
                this.allorder.reverse();
              }
        }

        onchange(args){
            console.log("Changed");
            console.log("Changed Value: " + this.items.getValue(args.newIndex));
            if(this.items.getValue(args.newIndex) === "all"){
                this.allorder = this.original;
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "not_reviewed"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/not_reviewed.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "hold"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/hold.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "approved"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/approved.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "completed"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/completed.png"));
                this.allorder.reverse();
            }
        }

        onopen(){
            console.log("Opened");
        }

        onclose(){
            console.log("Closed");
        }

}