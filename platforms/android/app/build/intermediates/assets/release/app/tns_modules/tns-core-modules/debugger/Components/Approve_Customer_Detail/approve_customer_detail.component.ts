import { Address, Customer } from '../../Models/customer.model';
import { CustomerService } from '../../Services/customer.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { confirm } from "ui/dialogs";
import * as dialogs from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';

@Component({
    selector: "approve-customer-detail",
    moduleId: module.id,
    templateUrl: "approve_customer_detail.component.html",
    styleUrls: ["approve_customer_detail.component.css"]
})

export class ApproveCustomerDetail{
    
    customerid: "";
    customer = new Customer();    
    address = new Address();
    disableApproveFAB: boolean = true;

    constructor( private customerservice: CustomerService,private route: ActivatedRoute, private http: Http,
        private routerExtensions: RouterExtensions, private page: Page, private location: Location){
        
    }
    
      ngOnInit() {
        //    datas for initial value start here
        this.customer.shopname = "ABC Departmental store";
        this.customer.contactname = "name";
        this.address.door_no = "7";
        this.address.street_name = "some street";
        this.address.area ="some area";
        this.address.city = "chennai";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.address.pin = 658990;
        this.customer.contactnumber = 0987654321;
        //    datas for initial value end here


        const id = this.route.snapshot.params["id"];
        this.customerid = id;

        this.customerservice.getcustomerbyid(id)
        .subscribe(data => this.parseCustomers(data.json()));

        application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            this.routerExtensions.backToPreviousPage();
          });  
      }

      parseCustomers(data: any) {
        console.log(data.shop_name);
        
                    this.customer.shopname = data.shop_name;
                    this.customer.contactname = data.name;
                    this.customer.contactnumber = data.primary_mobile_no;
                    this.customer.gstno = data.gst_no;
                    this.address.city = data.address[0].city;
                    this.address.door_no = data.address[0].door_no;
                    this.address.area = data.address[0].area;
                    this.address.pin = data.address[0].pin;
                    this.address.street_name = data.address[0].street_name;
                    this.address.district = data.address[0].district;
                    this.address.state = data.address[0].state;
        
                    this.customer.address = this.address;
      }

      gotoshop(){

        let options = {
            title: "Approve Customer",
            //message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        
        confirm(options).then((result: boolean) => {
            if(result == true){
                this.routerExtensions.navigate(["/approvecustomerlist"]);
                var data = {"status":"active"};
                return this.http.post(
                    
                  "https://app.krishnabhavanfoods.in/api/customer/customer_approval/"+this.customerid,
                    data, 
                    { headers: this.getCommonHeaders() }
                    )    
                      .subscribe((result) => {
                          this.disableApproveFAB = false;
                          console.log("Approve Customer Service Result: " + result)
                    }, (error) => {
                        console.log("Approve Customer Service Error: " + error)
                    });
            }
            else{
                return;
            }
        });
          
    }

    getCommonHeaders() {
        let headers = new Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    }
}