"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var sidedrawer_1 = require("nativescript-telerik-ui/sidedrawer");
var page_1 = require("tns-core-modules/ui/page");
var ActionBar = (function () {
    function ActionBar(_page, _router, routerExtensions, _changeDetectionRef) {
        this._page = _page;
        this._router = _router;
        this.routerExtensions = routerExtensions;
        this._changeDetectionRef = _changeDetectionRef;
        _page.on("loaded", this.onLoaded, this);
    }
    ActionBar.prototype.orderdetails = function () {
        this.routerExtensions.navigate(["/managerorderlist"]);
    };
    ActionBar.prototype.ngAfterViewInit = function () {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    };
    ActionBar.prototype.ngOnInit = function () {
        var _this = this;
        this.mainContentText = "SideDrawer for NativeScript can be easily setup in the HTML definition of your page by defining tkDrawerContent and tkMainContent. The component has a default transition and position and also exposes notifications related to changes in its state. Swipe from left to open side drawer.";
        this._router.events.subscribe(function (e) {
            if (e instanceof router_2.NavigationEnd) {
                _this.drawer.closeDrawer();
            }
        });
    };
    Object.defineProperty(ActionBar.prototype, "mainContentText", {
        get: function () {
            return this._mainContentText;
        },
        set: function (value) {
            this._mainContentText = value;
        },
        enumerable: true,
        configurable: true
    });
    ActionBar.prototype.openDrawer = function () {
        this.drawer.showDrawer();
    };
    ActionBar.prototype.onCloseDrawerTap = function () {
        this.drawer.closeDrawer();
    };
    ActionBar.prototype.onLoaded = function (args) {
        this._sideDrawerTransition = new sidedrawer_1.SlideInOnTopTransition();
    };
    return ActionBar;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ActionBar.prototype, "actionbartitle", void 0);
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], ActionBar.prototype, "drawerComponent", void 0);
ActionBar = __decorate([
    core_1.Component({
        selector: "action-bar",
        moduleId: module.id,
        templateUrl: "actionbar.component.html",
        styles: ["\n    ActionBar {\n        color: #fff;\n    }\n    "]
    }),
    __param(0, core_1.Inject(page_1.Page)),
    __metadata("design:paramtypes", [page_1.Page, router_2.Router, router_1.RouterExtensions, core_1.ChangeDetectorRef])
], ActionBar);
exports.ActionBar = ActionBar;
