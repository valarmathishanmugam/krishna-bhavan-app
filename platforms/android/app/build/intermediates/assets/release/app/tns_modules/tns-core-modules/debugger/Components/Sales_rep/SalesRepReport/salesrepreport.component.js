"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var login_service_1 = require("../../../Services/login.service");
var http_1 = require("@angular/http");
var reports_service_1 = require("../../../Services/reports.service");
var SalesRepReport = (function () {
    function SalesRepReport(page, location, routerExtensions, loginservice, http, reportservice) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.loginservice = loginservice;
        this.http = http;
        this.reportservice = reportservice;
        this.big_total_count = "";
        this.big_total_amount = "";
        this.reports = new Array();
        this.detailvisible = "visible";
        this.bigvisible = "collapse";
    }
    SalesRepReport.prototype.ngOnInit = function () {
        var _this = this;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
        this.tempuser = this.loginservice.getScope();
    };
    SalesRepReport.prototype.onPickerLoaded = function (args) {
        var datePicker = args.object;
        this.date = new Date();
        datePicker.minDate = new Date(1989, 0, 29);
        datePicker.maxDate = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
        //datePicker.maxDate = new Date(2048, 3, 24);
        datePicker.year = this.date.getFullYear();
        datePicker.month = this.date.getMonth() + 1;
        datePicker.day = this.date.getDate();
    };
    SalesRepReport.prototype.fromdatefunc = function (args) {
        //console.log("Date changed");
        //console.log("New value: " + args.value);
        this.fromdate = args.value;
        this.setfromdate = this.fromdate.getDate() + "/" + (this.fromdate.getMonth() + 1) + "/" + this.fromdate.getFullYear();
        //console.log("fromstring: " + this.fromdate.getDate() +"/" + (this.fromdate.getMonth()+1)+ "/" + this.fromdate.getFullYear());
        this.isofromdate = this.fromdate.toISOString();
    };
    SalesRepReport.prototype.todatefunc = function (args) {
        //console.log("Date changed");
        //console.log("New value: " + args.value);  
        this.todate = args.value;
        this.settodate = this.todate.getDate() + "/" + (this.todate.getMonth() + 1) + "/" + this.todate.getFullYear();
        //console.log("tostring: " + this.todate.getDate() +"/" + (this.todate.getMonth()+1)+ "/" + this.todate.getFullYear());
        //console.log("To Iso value: " + this.todate.toISOString());
        this.isotodate = this.todate.toISOString();
    };
    SalesRepReport.prototype.submit = function () {
        this.getdate = { "start_date": this.isofromdate, "end_date": this.isotodate };
        console.log("start date and end date for report" + this.getdate);
        this.tempset = { "from": this.setfromdate, "to": this.settodate };
        this.reportservice.setScope(this.getdate);
        this.reportservice.setdate(this.tempset);
        this.routerExtensions.navigate(["/reportdetail"]);
        //this.data = {"start_date": this.isofromdate, "end_date": this.isotodate}
        this.data = { "start_date": "2018-02-01T10:35:48.343", "end_date": "2018-05-07T10:35:48.000" };
        /*
          if(this.tempuser.role === "manager"){
            //console.log("Manager Id: " + this.tempuser.id)
            //alert("Manager Id: " + this.tempuser.id)
            this.http.post("https://app.krishnabhavanfoods.in/api/orders/getmanagerreports/id/"+this.tempuser.id,
                this.data,
                { headers: this.getCommonHeaders() }
                ).subscribe((result) => {
                  this.getmanagerreport(result.json())
                  //console.log("Customer Result: " + JSON.stringify(result))
                }, (error) => {
                  console.log("Manager report Error: " + error)
                });
              }
        
              else if(this.tempuser.role === "rep"){
                //console.log("Rep Id: " + this.tempuser.id)
                //alert("Rep Id: " + this.tempuser.id)
                this.http.post("https://app.krishnabhavanfoods.in/api/orders/getsalesrepreports/id/"+this.tempuser.id,
                    this.data,
                    { headers: this.getCommonHeaders() }
                    ).subscribe((result) => {
                      this.salesrepreport(result.json())
                      //console.log("Customer Result: " + JSON.stringify(result))
                    }, (error) => {
                      console.log("Rep report Error: " + error)
                    });
                  }
              else if(this.tempuser.role === "customer"){
                //console.log("Customer Id: " + this.tempuser.id)
                //alert("Customer Id: " + this.tempuser.id)
                this.http.post("https://app.krishnabhavanfoods.in/api/orders/getcustomerreports/id/"+this.tempuser.id,
                    this.data,
                    { headers: this.getCommonHeaders() }
                    ).subscribe((result) => {
                      this.customerreport(result.json())
                      //console.log("Customer Result: " + JSON.stringify(result))
                    }, (error) => {
                      console.log("Customer Error: " + error)
                    });
        
                  }
        
                  */
    };
    return SalesRepReport;
}());
SalesRepReport = __decorate([
    core_1.Component({
        selector: "rep-report",
        moduleId: module.id,
        templateUrl: "salesrepreport.component.html",
        styleUrls: ["salesrepreport.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions,
        login_service_1.loginService, http_1.Http, reports_service_1.ReportService])
], SalesRepReport);
exports.SalesRepReport = SalesRepReport;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNyZXByZXBvcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2FsZXNyZXByZXBvcnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0Esc0NBQTBDO0FBQzFDLHNEQUFxRDtBQUNyRCwyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQUMzQyxzREFBK0Q7QUFFL0QsaUVBQStEO0FBQy9ELHNDQUF3RDtBQUV4RCxxRUFBa0U7QUFRbEUsSUFBYSxjQUFjO0lBbUJ6Qix3QkFBb0IsSUFBVSxFQUFVLFFBQWtCLEVBQVUsZ0JBQWtDLEVBQzVGLFlBQTBCLEVBQVUsSUFBVSxFQUFVLGFBQTRCO1FBRDFFLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUM1RixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQVY5RixvQkFBZSxHQUFFLEVBQUUsQ0FBQztRQUNwQixxQkFBZ0IsR0FBRSxFQUFFLENBQUM7UUFDckIsWUFBTyxHQUFhLElBQUksS0FBSyxFQUFFLENBQUM7UUFFaEMsa0JBQWEsR0FBRyxTQUFTLENBQUE7UUFDekIsZUFBVSxHQUFHLFVBQVUsQ0FBQTtJQUt5RSxDQUFDO0lBRWpHLGlDQUFRLEdBQVI7UUFBQSxpQkFTQztRQVJDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDNUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBRUQsdUNBQWMsR0FBZCxVQUFlLElBQUk7UUFDakIsSUFBSSxVQUFVLEdBQWUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN6QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDdkIsVUFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNDLFVBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUNsRyw2Q0FBNkM7UUFFN0MsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzFDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBQyxDQUFDLENBQUM7UUFDMUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFQyxxQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLDhCQUE4QjtRQUM5QiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFDLENBQUMsQ0FBQyxHQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xILCtIQUErSDtRQUMvSCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUE7SUFFbEQsQ0FBQztJQUVELG1DQUFVLEdBQVYsVUFBVyxJQUFJO1FBQ2IsOEJBQThCO1FBQzlCLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxHQUFFLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUMsQ0FBQyxDQUFDLEdBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDMUcsdUhBQXVIO1FBQ3ZILDREQUE0RDtRQUM1RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUE7SUFDNUMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFFRSxJQUFJLENBQUMsT0FBTyxHQUFFLEVBQUMsWUFBWSxFQUFDLElBQUksQ0FBQyxXQUFXLEVBQUMsVUFBVSxFQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsQ0FBQTtRQUN2RSxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUMsQ0FBQTtRQUU5RCxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXpDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBRWxELDBFQUEwRTtRQUM1RSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUMsWUFBWSxFQUFFLHlCQUF5QixFQUFFLFVBQVUsRUFBRSx5QkFBeUIsRUFBQyxDQUFBO1FBQzVGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O29CQTJDWTtJQUNaLENBQUM7SUFDRCxxQkFBQztBQUFELENBQUMsQUEzSEQsSUEySEM7QUEzSFksY0FBYztJQU4xQixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFlBQVk7UUFDdEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSwrQkFBK0I7UUFDNUMsU0FBUyxFQUFFLENBQUMsOEJBQThCLENBQUM7S0FDNUMsQ0FBQztxQ0FvQjBCLFdBQUksRUFBb0IsaUJBQVEsRUFBNEIseUJBQWdCO1FBQzlFLDRCQUFZLEVBQWdCLFdBQUksRUFBeUIsK0JBQWE7R0FwQm5GLGNBQWMsQ0EySDFCO0FBM0hZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xyXG5pbXBvcnQgeyBBbmRyb2lkQXBwbGljYXRpb24sIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhIH0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBEYXRlUGlja2VyIH0gZnJvbSBcInVpL2RhdGUtcGlja2VyXCI7XHJcbmltcG9ydCB7IGxvZ2luU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL1NlcnZpY2VzL2xvZ2luLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgeyBSZXBvcnQgfSBmcm9tICcuLi8uLi8uLi9Nb2RlbHMvb3JkZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBSZXBvcnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvcmVwb3J0cy5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInJlcC1yZXBvcnRcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcInNhbGVzcmVwcmVwb3J0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJzYWxlc3JlcHJlcG9ydC5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTYWxlc1JlcFJlcG9ydHtcclxuICBkYXRlOiBEYXRlO1xyXG4gIGZyb21kYXRlOiBEYXRlO1xyXG4gIHRvZGF0ZTogRGF0ZTtcclxuICBpc29mcm9tZGF0ZTtcclxuICBpc290b2RhdGU7XHJcbiAgc2V0ZnJvbWRhdGU7XHJcbiAgc2V0dG9kYXRlO1xyXG4gIGRhdGE7XHJcbiAgdGVtcHVzZXI7XHJcbiAgYmlnX3RvdGFsX2NvdW50PSBcIlwiO1xyXG4gIGJpZ190b3RhbF9hbW91bnQgPVwiXCI7XHJcbiAgcmVwb3J0czogUmVwb3J0W10gPSBuZXcgQXJyYXkoKTtcclxuICByZXBvcnRkZXRhaWw6IFJlcG9ydDsgXHJcbiAgZGV0YWlsdmlzaWJsZSA9IFwidmlzaWJsZVwiXHJcbiAgYmlndmlzaWJsZSA9IFwiY29sbGFwc2VcIlxyXG4gIGdldGRhdGU7XHJcbiAgdGVtcHNldDtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBsb2dpbnNlcnZpY2U6IGxvZ2luU2VydmljZSwgcHJpdmF0ZSBodHRwOiBIdHRwLCBwcml2YXRlIHJlcG9ydHNlcnZpY2U6IFJlcG9ydFNlcnZpY2Upe31cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy50ZW1wdXNlciA9IHRoaXMubG9naW5zZXJ2aWNlLmdldFNjb3BlKCk7XHJcbiAgfVxyXG5cclxuICBvblBpY2tlckxvYWRlZChhcmdzKSB7XHJcbiAgICBsZXQgZGF0ZVBpY2tlciA9IDxEYXRlUGlja2VyPmFyZ3Mub2JqZWN0O1xyXG4gICAgdGhpcy5kYXRlID0gbmV3IERhdGUoKTtcclxuICAgIGRhdGVQaWNrZXIubWluRGF0ZSA9IG5ldyBEYXRlKDE5ODksIDAsIDI5KTtcclxuICAgIGRhdGVQaWNrZXIubWF4RGF0ZSA9IG5ldyBEYXRlKHRoaXMuZGF0ZS5nZXRGdWxsWWVhcigpLCB0aGlzLmRhdGUuZ2V0TW9udGgoKSwgdGhpcy5kYXRlLmdldERhdGUoKSk7XHJcbiAgICAvL2RhdGVQaWNrZXIubWF4RGF0ZSA9IG5ldyBEYXRlKDIwNDgsIDMsIDI0KTtcclxuICAgIFxyXG4gICAgZGF0ZVBpY2tlci55ZWFyID0gdGhpcy5kYXRlLmdldEZ1bGxZZWFyKCk7XHJcbiAgICBkYXRlUGlja2VyLm1vbnRoID0gdGhpcy5kYXRlLmdldE1vbnRoKCkrMTtcclxuICAgIGRhdGVQaWNrZXIuZGF5ID0gdGhpcy5kYXRlLmdldERhdGUoKTtcclxufVxyXG5cclxuICBmcm9tZGF0ZWZ1bmMoYXJncykge1xyXG4gICAgLy9jb25zb2xlLmxvZyhcIkRhdGUgY2hhbmdlZFwiKTtcclxuICAgIC8vY29uc29sZS5sb2coXCJOZXcgdmFsdWU6IFwiICsgYXJncy52YWx1ZSk7XHJcbiAgICB0aGlzLmZyb21kYXRlID0gYXJncy52YWx1ZTtcclxuICAgIHRoaXMuc2V0ZnJvbWRhdGUgPSB0aGlzLmZyb21kYXRlLmdldERhdGUoKSArXCIvXCIgKyAodGhpcy5mcm9tZGF0ZS5nZXRNb250aCgpKzEpKyBcIi9cIiArIHRoaXMuZnJvbWRhdGUuZ2V0RnVsbFllYXIoKTtcclxuICAgIC8vY29uc29sZS5sb2coXCJmcm9tc3RyaW5nOiBcIiArIHRoaXMuZnJvbWRhdGUuZ2V0RGF0ZSgpICtcIi9cIiArICh0aGlzLmZyb21kYXRlLmdldE1vbnRoKCkrMSkrIFwiL1wiICsgdGhpcy5mcm9tZGF0ZS5nZXRGdWxsWWVhcigpKTtcclxuICAgIHRoaXMuaXNvZnJvbWRhdGUgPSB0aGlzLmZyb21kYXRlLnRvSVNPU3RyaW5nKClcclxuICAgIFxyXG59XHJcblxyXG50b2RhdGVmdW5jKGFyZ3MpIHtcclxuICAvL2NvbnNvbGUubG9nKFwiRGF0ZSBjaGFuZ2VkXCIpO1xyXG4gIC8vY29uc29sZS5sb2coXCJOZXcgdmFsdWU6IFwiICsgYXJncy52YWx1ZSk7ICBcclxuICB0aGlzLnRvZGF0ZSA9IGFyZ3MudmFsdWU7XHJcbiAgdGhpcy5zZXR0b2RhdGUgPSB0aGlzLnRvZGF0ZS5nZXREYXRlKCkgK1wiL1wiICsgKHRoaXMudG9kYXRlLmdldE1vbnRoKCkrMSkrIFwiL1wiICsgdGhpcy50b2RhdGUuZ2V0RnVsbFllYXIoKTtcclxuICAvL2NvbnNvbGUubG9nKFwidG9zdHJpbmc6IFwiICsgdGhpcy50b2RhdGUuZ2V0RGF0ZSgpICtcIi9cIiArICh0aGlzLnRvZGF0ZS5nZXRNb250aCgpKzEpKyBcIi9cIiArIHRoaXMudG9kYXRlLmdldEZ1bGxZZWFyKCkpO1xyXG4gIC8vY29uc29sZS5sb2coXCJUbyBJc28gdmFsdWU6IFwiICsgdGhpcy50b2RhdGUudG9JU09TdHJpbmcoKSk7XHJcbiAgdGhpcy5pc290b2RhdGUgPSB0aGlzLnRvZGF0ZS50b0lTT1N0cmluZygpXHJcbn1cclxuXHJcbnN1Ym1pdCgpe1xyXG5cclxuICB0aGlzLmdldGRhdGU9IHtcInN0YXJ0X2RhdGVcIjp0aGlzLmlzb2Zyb21kYXRlLFwiZW5kX2RhdGVcIjp0aGlzLmlzb3RvZGF0ZX1cclxuICBjb25zb2xlLmxvZyhcInN0YXJ0IGRhdGUgYW5kIGVuZCBkYXRlIGZvciByZXBvcnRcIit0aGlzLmdldGRhdGUpO1xyXG4gIHRoaXMudGVtcHNldCA9IHtcImZyb21cIjogdGhpcy5zZXRmcm9tZGF0ZSxcInRvXCI6IHRoaXMuc2V0dG9kYXRlfVxyXG5cclxuICB0aGlzLnJlcG9ydHNlcnZpY2Uuc2V0U2NvcGUodGhpcy5nZXRkYXRlKTtcclxuICB0aGlzLnJlcG9ydHNlcnZpY2Uuc2V0ZGF0ZSh0aGlzLnRlbXBzZXQpO1xyXG4gIFxyXG4gIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcmVwb3J0ZGV0YWlsXCJdKTtcclxuXHJcbiAgLy90aGlzLmRhdGEgPSB7XCJzdGFydF9kYXRlXCI6IHRoaXMuaXNvZnJvbWRhdGUsIFwiZW5kX2RhdGVcIjogdGhpcy5pc290b2RhdGV9XHJcbnRoaXMuZGF0YSA9IHtcInN0YXJ0X2RhdGVcIjogXCIyMDE4LTAyLTAxVDEwOjM1OjQ4LjM0M1wiLCBcImVuZF9kYXRlXCI6IFwiMjAxOC0wNS0wN1QxMDozNTo0OC4wMDBcIn1cclxuLypcclxuICBpZih0aGlzLnRlbXB1c2VyLnJvbGUgPT09IFwibWFuYWdlclwiKXtcclxuICAgIC8vY29uc29sZS5sb2coXCJNYW5hZ2VyIElkOiBcIiArIHRoaXMudGVtcHVzZXIuaWQpXHJcbiAgICAvL2FsZXJ0KFwiTWFuYWdlciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgdGhpcy5odHRwLnBvc3QoXCJodHRwczovL2FwcC5rcmlzaG5hYmhhdmFuZm9vZHMuaW4vYXBpL29yZGVycy9nZXRtYW5hZ2VycmVwb3J0cy9pZC9cIit0aGlzLnRlbXB1c2VyLmlkLFxyXG4gICAgICAgIHRoaXMuZGF0YSwgXHJcbiAgICAgICAgeyBoZWFkZXJzOiB0aGlzLmdldENvbW1vbkhlYWRlcnMoKSB9XHJcbiAgICAgICAgKS5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5nZXRtYW5hZ2VycmVwb3J0KHJlc3VsdC5qc29uKCkpXHJcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiQ3VzdG9tZXIgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpXHJcbiAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIk1hbmFnZXIgcmVwb3J0IEVycm9yOiBcIiArIGVycm9yKVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBlbHNlIGlmKHRoaXMudGVtcHVzZXIucm9sZSA9PT0gXCJyZXBcIil7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIlJlcCBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgIC8vYWxlcnQoXCJSZXAgSWQ6IFwiICsgdGhpcy50ZW1wdXNlci5pZClcclxuICAgICAgICB0aGlzLmh0dHAucG9zdChcImh0dHBzOi8vYXBwLmtyaXNobmFiaGF2YW5mb29kcy5pbi9hcGkvb3JkZXJzL2dldHNhbGVzcmVwcmVwb3J0cy9pZC9cIit0aGlzLnRlbXB1c2VyLmlkLFxyXG4gICAgICAgICAgICB0aGlzLmRhdGEsIFxyXG4gICAgICAgICAgICB7IGhlYWRlcnM6IHRoaXMuZ2V0Q29tbW9uSGVhZGVycygpIH1cclxuICAgICAgICAgICAgKS5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuc2FsZXNyZXByZXBvcnQocmVzdWx0Lmpzb24oKSlcclxuICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiQ3VzdG9tZXIgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpXHJcbiAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVwIHJlcG9ydCBFcnJvcjogXCIgKyBlcnJvcilcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgIGVsc2UgaWYodGhpcy50ZW1wdXNlci5yb2xlID09PSBcImN1c3RvbWVyXCIpe1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJDdXN0b21lciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgIC8vYWxlcnQoXCJDdXN0b21lciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgIHRoaXMuaHR0cC5wb3N0KFwiaHR0cHM6Ly9hcHAua3Jpc2huYWJoYXZhbmZvb2RzLmluL2FwaS9vcmRlcnMvZ2V0Y3VzdG9tZXJyZXBvcnRzL2lkL1wiK3RoaXMudGVtcHVzZXIuaWQsXHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSwgXHJcbiAgICAgICAgICAgIHsgaGVhZGVyczogdGhpcy5nZXRDb21tb25IZWFkZXJzKCkgfVxyXG4gICAgICAgICAgICApLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21lcnJlcG9ydChyZXN1bHQuanNvbigpKVxyXG4gICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJDdXN0b21lciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSlcclxuICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJDdXN0b21lciBFcnJvcjogXCIgKyBlcnJvcilcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICovXHJcbn1cclxufSJdfQ==