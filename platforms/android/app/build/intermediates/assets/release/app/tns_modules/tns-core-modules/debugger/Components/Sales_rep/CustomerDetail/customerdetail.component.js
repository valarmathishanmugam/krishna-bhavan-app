"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var customer_service_1 = require("../../../Services/customer.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var customer_model_1 = require("../../../Models/customer.model");
var router_2 = require("nativescript-angular/router");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var page_1 = require("tns-core-modules/ui/page/page");
var CustomerDetail = (function () {
    //address: Address;
    function CustomerDetail(route, customerservice, routerExtensions, savecustomer, savecustomerdetails, loginservice, saveorderservice, location, page) {
        this.route = route;
        this.customerservice = customerservice;
        this.routerExtensions = routerExtensions;
        this.savecustomer = savecustomer;
        this.savecustomerdetails = savecustomerdetails;
        this.loginservice = loginservice;
        this.saveorderservice = saveorderservice;
        this.location = location;
        this.page = page;
        this.title = "Customer Detail";
        //contactname: string = "PunniyaMoorthy";
        //shopname: string = "Kanniga Parameshwari Store";
        //contactnumber = "9123654789";
        //gstno = data.gst_no;
        //city = "Chennai";
        //door_no = "No.123";
        //area = "Amman Nagar";
        //pin = "600019";
        //street_name = "2nd Street";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.addordervisible = "visible";
    }
    CustomerDetail.prototype.ngOnInit = function () {
        var _this = this;
        // defalut datas 
        this.customer.shopname = "Abc Enterprices";
        this.customer.contactname = "Logesh";
        this.customer.contactnumber = 0987654321;
        this.address.city = "chennai";
        this.address.door_no = "4";
        this.address.area = "main area";
        this.address.pin = 639008;
        this.address.street_name = "main street";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.customer.address = this.address;
        //this.address = ({doorno:"No.123", street_name:"",area: ", opp. Eldam's Road, AnnaSalai", city:"Chennai", pin:"611111", contactnumber:"9874563210"})
        var id = this.route.snapshot.params["id"];
        //const id = "abcd";
        this.customerid = id;
        this.savecustomer.setScope(this.customerid);
        this.customerservice.getcustomerbyid(id)
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        console.log(id);
        this.savecustomer.setScope(id);
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    CustomerDetail.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
        this.savecustomerdetails.setScope(this.customer);
    };
    CustomerDetail.prototype.gotoshop = function () {
        this.routerExtensions.navigate(["/productlist"]);
        var a = false;
        this.saveorderservice.setScope(a);
    };
    return CustomerDetail;
}());
CustomerDetail = __decorate([
    core_1.Component({
        selector: "customer-detail",
        moduleId: module.id,
        templateUrl: "customerdetail.component.html",
        styleUrls: ["customerdetail.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, customer_service_1.CustomerService, router_2.RouterExtensions,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveCustomerDetails, login_service_1.loginService,
        saveorder_service_1.SaveOrderService, common_1.Location, page_1.Page])
], CustomerDetail);
exports.CustomerDetail = CustomerDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZXJkZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY3VzdG9tZXJkZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUVBQStEO0FBQy9ELHlFQUEwRztBQUMxRyx1RUFBcUU7QUFDckUsc0NBQWtEO0FBQ2xELDBDQUFpRDtBQUNqRCxpRUFBa0U7QUFDbEUsc0RBQStEO0FBQy9ELDJDQUFzRjtBQUN0Rix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBQzNDLHNEQUFxRDtBQVNyRCxJQUFhLGNBQWM7SUFpQnZCLG1CQUFtQjtJQUVuQix3QkFBb0IsS0FBcUIsRUFBVSxlQUFnQyxFQUFVLGdCQUFrQyxFQUNsSCxZQUEwQixFQUFVLG1CQUF3QyxFQUFVLFlBQTBCLEVBQ2hILGdCQUFrQyxFQUFVLFFBQWtCLEVBQVUsSUFBVTtRQUYzRSxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUFVLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEgsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDaEgscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBakIvRixVQUFLLEdBQVcsaUJBQWlCLENBQUM7UUFDbEMseUNBQXlDO1FBQ3pDLGtEQUFrRDtRQUNsRCwrQkFBK0I7UUFDL0Isc0JBQXNCO1FBQ3RCLG1CQUFtQjtRQUNuQixxQkFBcUI7UUFDckIsdUJBQXVCO1FBQ3ZCLGlCQUFpQjtRQUNqQiw2QkFBNkI7UUFDN0IsYUFBUSxHQUFHLElBQUkseUJBQVEsRUFBRSxDQUFDO1FBQzFCLFlBQU8sR0FBRyxJQUFJLHdCQUFPLEVBQUUsQ0FBQztRQUN4QixvQkFBZSxHQUFHLFNBQVMsQ0FBQTtJQUt3RSxDQUFDO0lBRXBHLGlDQUFRLEdBQVI7UUFBQSxpQkFvQ0M7UUFuQ0csaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO1FBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7UUFFekMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLGFBQWEsQ0FBQztRQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDO1FBRWxDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDckMscUpBQXFKO1FBRXJKLElBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU1QyxvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRTVDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQzthQUN2QyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUM7UUFFckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVoQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUvQixXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxnQ0FBa0IsQ0FBQyx3QkFBd0IsRUFBRSxVQUFDLElBQXlDO1lBQzFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsdUNBQWMsR0FBZCxVQUFlLElBQVM7UUFFcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBRTNDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFckMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFckQsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUM7UUFFZCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFHVCxxQkFBQztBQUFELENBQUMsQUEzRkQsSUEyRkM7QUEzRlksY0FBYztJQVAxQixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGlCQUFpQjtRQUMzQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLCtCQUErQjtRQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztLQUM5QyxDQUFDO3FDQXFCNkIsdUJBQWMsRUFBMkIsa0NBQWUsRUFBNEIseUJBQWdCO1FBQ3BHLGdDQUFZLEVBQStCLHVDQUFtQixFQUF3Qiw0QkFBWTtRQUM5RixvQ0FBZ0IsRUFBb0IsaUJBQVEsRUFBZ0IsV0FBSTtHQXJCdEYsY0FBYyxDQTJGMUI7QUEzRlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9TZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2F2ZUN1c3RvbWVyLCBTYXZlQ3VzdG9tZXJEZXRhaWxzLCBTYXZlT3JkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvc2F2ZW9yZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDdXN0b21lclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9TZXJ2aWNlcy9jdXN0b21lci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBDdXN0b21lciwgQWRkcmVzc30gZnJvbSBcIi4uLy4uLy4uL01vZGVscy9jdXN0b21lci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJjdXN0b21lci1kZXRhaWxcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJjdXN0b21lcmRldGFpbC5jb21wb25lbnQuaHRtbFwiLFxyXG4gICAgc3R5bGVVcmxzOiBbXCJjdXN0b21lcmRldGFpbC5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tZXJEZXRhaWwgaW1wbGVtZW50cyBPbkluaXR7XHJcblxyXG4gICAgY3VzdG9tZXJpZDogYW55O1xyXG5cclxuICAgIHRpdGxlOiBzdHJpbmcgPSBcIkN1c3RvbWVyIERldGFpbFwiO1xyXG4gICAgLy9jb250YWN0bmFtZTogc3RyaW5nID0gXCJQdW5uaXlhTW9vcnRoeVwiO1xyXG4gICAgLy9zaG9wbmFtZTogc3RyaW5nID0gXCJLYW5uaWdhIFBhcmFtZXNod2FyaSBTdG9yZVwiO1xyXG4gICAgLy9jb250YWN0bnVtYmVyID0gXCI5MTIzNjU0Nzg5XCI7XHJcbiAgICAvL2dzdG5vID0gZGF0YS5nc3Rfbm87XHJcbiAgICAvL2NpdHkgPSBcIkNoZW5uYWlcIjtcclxuICAgIC8vZG9vcl9ubyA9IFwiTm8uMTIzXCI7XHJcbiAgICAvL2FyZWEgPSBcIkFtbWFuIE5hZ2FyXCI7XHJcbiAgICAvL3BpbiA9IFwiNjAwMDE5XCI7XHJcbiAgICAvL3N0cmVldF9uYW1lID0gXCIybmQgU3RyZWV0XCI7XHJcbiAgICBjdXN0b21lciA9IG5ldyBDdXN0b21lcigpO1xyXG4gICAgYWRkcmVzcyA9IG5ldyBBZGRyZXNzKCk7XHJcbiAgICBhZGRvcmRlcnZpc2libGUgPSBcInZpc2libGVcIlxyXG4gICAgLy9hZGRyZXNzOiBBZGRyZXNzO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLCBwcml2YXRlIGN1c3RvbWVyc2VydmljZTogQ3VzdG9tZXJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICAgICAgIHByaXZhdGUgc2F2ZWN1c3RvbWVyOiBTYXZlQ3VzdG9tZXIsIHByaXZhdGUgc2F2ZWN1c3RvbWVyZGV0YWlsczogU2F2ZUN1c3RvbWVyRGV0YWlscywgcHJpdmF0ZSBsb2dpbnNlcnZpY2U6IGxvZ2luU2VydmljZSxcclxuICAgICAgICAgcHJpdmF0ZSBzYXZlb3JkZXJzZXJ2aWNlOiBTYXZlT3JkZXJTZXJ2aWNlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpe1xyXG4gICAgICAgIC8vIGRlZmFsdXQgZGF0YXMgXHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5zaG9wbmFtZSA9IFwiQWJjIEVudGVycHJpY2VzXCI7XHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IFwiTG9nZXNoXCI7XHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bnVtYmVyID0gMDk4NzY1NDMyMTtcclxuICAgIFxyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5jaXR5ID0gXCJjaGVubmFpXCI7XHJcbiAgICAgICAgdGhpcy5hZGRyZXNzLmRvb3Jfbm8gPSBcIjRcIjtcclxuICAgICAgICB0aGlzLmFkZHJlc3MuYXJlYSA9IFwibWFpbiBhcmVhXCI7XHJcbiAgICAgICAgdGhpcy5hZGRyZXNzLnBpbiA9IDYzOTAwODtcclxuICAgICAgICB0aGlzLmFkZHJlc3Muc3RyZWV0X25hbWUgPSBcIm1haW4gc3RyZWV0XCI7XHJcbiAgICAgICAgdGhpcy5hZGRyZXNzLmRpc3RyaWN0ID0gXCJjaGVubmFpXCI7XHJcbiAgICAgICAgdGhpcy5hZGRyZXNzLnN0YXRlID0gXCJUYW1pbCBOYWR1XCI7XHJcblxyXG4gICAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IHRoaXMuYWRkcmVzcztcclxuICAgICAgICAvL3RoaXMuYWRkcmVzcyA9ICh7ZG9vcm5vOlwiTm8uMTIzXCIsIHN0cmVldF9uYW1lOlwiXCIsYXJlYTogXCIsIG9wcC4gRWxkYW0ncyBSb2FkLCBBbm5hU2FsYWlcIiwgY2l0eTpcIkNoZW5uYWlcIiwgcGluOlwiNjExMTExXCIsIGNvbnRhY3RudW1iZXI6XCI5ODc0NTYzMjEwXCJ9KVxyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnN0IGlkID0gdGhpcy5yb3V0ZS5zbmFwc2hvdC5wYXJhbXNbXCJpZFwiXTtcclxuXHJcbiAgICAgICAgLy9jb25zdCBpZCA9IFwiYWJjZFwiO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tZXJpZCA9IGlkO1xyXG4gICAgICAgIHRoaXMuc2F2ZWN1c3RvbWVyLnNldFNjb3BlKHRoaXMuY3VzdG9tZXJpZCk7XHJcblxyXG4gICAgICAgIHRoaXMuY3VzdG9tZXJzZXJ2aWNlLmdldGN1c3RvbWVyYnlpZChpZClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5wYXJzZUN1c3RvbWVycyhkYXRhLmpzb24oKSkpO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhpZCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5zYXZlY3VzdG9tZXIuc2V0U2NvcGUoaWQpO1xyXG5cclxuICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGFyc2VDdXN0b21lcnMoZGF0YTogYW55KSB7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEuc2hvcF9uYW1lKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUgPSBkYXRhLnNob3BfbmFtZTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IGRhdGEubmFtZTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bnVtYmVyID0gZGF0YS5wcmltYXJ5X21vYmlsZV9ubztcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lci5nc3RubyA9IGRhdGEuZ3N0X25vO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MuY2l0eSA9IGRhdGEuYWRkcmVzc1swXS5jaXR5O1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MuZG9vcl9ubyA9IGRhdGEuYWRkcmVzc1swXS5kb29yX25vO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MuYXJlYSA9IGRhdGEuYWRkcmVzc1swXS5hcmVhO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MucGluID0gZGF0YS5hZGRyZXNzWzBdLnBpbjtcclxuICAgICAgICAgICAgdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lID0gZGF0YS5hZGRyZXNzWzBdLnN0cmVldF9uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MuZGlzdHJpY3QgPSBkYXRhLmFkZHJlc3NbMF0uZGlzdHJpY3Q7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGRhdGEuYWRkcmVzc1swXS5zdGF0ZTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IHRoaXMuYWRkcmVzcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWN1c3RvbWVyZGV0YWlscy5zZXRTY29wZSh0aGlzLmN1c3RvbWVyKTtcclxuXHJcbiAgICAgICAgfSAgXHJcblxyXG4gICAgICAgIGdvdG9zaG9wKCl7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJvZHVjdGxpc3RcIl0pOyAgXHJcbiAgICAgICAgICAgIHZhciBhID0gZmFsc2U7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnNhdmVvcmRlcnNlcnZpY2Uuc2V0U2NvcGUoYSk7ICAgICAgICAgICBcclxuICAgICAgICB9XHJcblxyXG5cclxufSJdfQ==