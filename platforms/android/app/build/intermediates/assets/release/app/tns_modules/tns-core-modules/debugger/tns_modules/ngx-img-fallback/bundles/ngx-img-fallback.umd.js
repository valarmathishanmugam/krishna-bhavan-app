(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core'], factory) :
	(factory((global['ngx-img-fallback'] = {}),global.ng.core));
}(this, (function (exports,core) { 'use strict';

var ImgFallbackDirective = /** @class */ (function () {
    function ImgFallbackDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.loaded = new core.EventEmitter();
        this.isApplied = false;
        this.ERROR_EVENT_TYPE = 'error';
        this.LOAD_EVENT_TYPE = 'load';
        this.nativeElement = el.nativeElement;
        this.onError = this.onError.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.addEvents();
    }
    ImgFallbackDirective.prototype.ngOnDestroy = function () {
        this.removeErrorEvent();
        this.removeOnLoadEvent();
    };
    ImgFallbackDirective.prototype.onError = function () {
        if (this.nativeElement.getAttribute('src') !== this.imgSrc) {
            this.isApplied = true;
            this.renderer.setAttribute(this.nativeElement, 'src', this.imgSrc);
        }
        else {
            this.removeOnLoadEvent();
        }
    };
    ImgFallbackDirective.prototype.onLoad = function () {
        this.loaded.emit(this.isApplied);
    };
    ImgFallbackDirective.prototype.removeErrorEvent = function () {
        if (this.cancelOnError) {
            this.cancelOnError();
        }
    };
    ImgFallbackDirective.prototype.removeOnLoadEvent = function () {
        if (this.cancelOnLoad) {
            this.cancelOnLoad();
        }
    };
    ImgFallbackDirective.prototype.addEvents = function () {
        this.cancelOnError = this.renderer.listen(this.nativeElement, this.ERROR_EVENT_TYPE, this.onError);
        this.cancelOnLoad = this.renderer.listen(this.nativeElement, this.LOAD_EVENT_TYPE, this.onLoad);
    };
    return ImgFallbackDirective;
}());
ImgFallbackDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[src-fallback]'
            },] },
];
ImgFallbackDirective.ctorParameters = function () { return [
    { type: core.ElementRef, },
    { type: core.Renderer2, },
]; };
ImgFallbackDirective.propDecorators = {
    "imgSrc": [{ type: core.Input, args: ['src-fallback',] },],
    "loaded": [{ type: core.Output, args: ['loaded',] },],
};
var ImgFallbackModule = /** @class */ (function () {
    function ImgFallbackModule() {
    }
    return ImgFallbackModule;
}());
ImgFallbackModule.decorators = [
    { type: core.NgModule, args: [{
                declarations: [ImgFallbackDirective],
                exports: [ImgFallbackDirective]
            },] },
];
ImgFallbackModule.ctorParameters = function () { return []; };

exports.ImgFallbackModule = ImgFallbackModule;
exports.ImgFallbackDirective = ImgFallbackDirective;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ngx-img-fallback.umd.js.map
