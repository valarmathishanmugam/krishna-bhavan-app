"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../Services/login.service");
var dialog_component_1 = require("../Dialog/dialog.component");
var product_service_1 = require("../../Services/product.service");
var saveorder_service_1 = require("../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var product_model_1 = require("../../Models/product.model");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var http_1 = require("@angular/http");
var platform_1 = require("platform");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ProductOffers = (function () {
    // enableMessage = 'collapse';
    function ProductOffers(route, routerExtensions, page, saveorderservice, productservice, savecustomer, savetotal, saveproducts, modalService, viewContainerRef, loginservice, http, saveoffers, location) {
        this.route = route;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.saveorderservice = saveorderservice;
        this.productservice = productservice;
        this.savecustomer = savecustomer;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.loginservice = loginservice;
        this.http = http;
        this.saveoffers = saveoffers;
        this.location = location;
        this.title = "Product List";
        this.productDetails = [];
        this.productsOrdered = [];
        this.productid = [];
        this.orderquantity = [];
        this.rackquantity = [];
        this.productname = [];
        this.productprice = [];
        this.productdiscount = [];
        this.line_total = [];
        //qtyedit: string =""
        this.boolean = false;
        this.loader = new LoadingIndicator();
        this.products = new Array();
        this.original = new Array();
        //let searchBar = <SearchBar>args.object;
        //searchBar.dismissSoftInput();
    }
    ProductOffers.prototype.ngOnInit = function () {
        var _this = this;
        // code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        // code for loading indicator end here
        //application.android.off(AndroidApplication.activityBackPressedEvent);
        this.page.actionBarHidden = false;
        this.ifcustomer = "visible";
        //this.getproducts();
        this.productservice.getDiscounts()
            .subscribe(function (data) { _this.loadDiscounts(data.json()); });
        this.tempuser = this.loginservice.getScope();
        this.checkcustomer(this.tempuser);
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            // if(this.tempuser.role == 'rep'){
            //     this.routerExtensions.navigate(["/customerlist"]);
            // }
            _this.routerExtensions.backToPreviousPage();
        });
    };
    ProductOffers.prototype.loadDiscounts = function (data) {
        // if (typeof data === 'string') {
        //     this.loader.hide();
        //     this.enableMessage = 'visible';
        // }
        // else {   
        this.loader.hide();
        // this.enableMessage = 'collapse';
        for (var i = 0; i < data.length; i++) {
            console.log("discount" + i);
            if (data[i].image_name != "") {
                data[i].image_name = "https://app.krishnabhavanfoods.in/" + data[i].image_name;
            }
            else {
                data[i].image_name = "~/Images/product_img.png";
            }
            data[i].valid_from = data[i].valid_from.slice(0, 11);
            data[i].valid_to = data[i].valid_to.slice(0, 11);
            this.products.push(data[i]);
            console.log("data inside the product array" + JSON.stringify(this.products));
        }
        // }
    };
    ProductOffers.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            return;
        }
    };
    ProductOffers.prototype.getproducts = function () {
        var _this = this;
        this.productservice.getproduct()
            .subscribe(function (data) { return _this.parseProducts(data.json()); });
    };
    ProductOffers.prototype.parseProducts = function (data) {
        var _this = this;
        console.log("data.length:" + data.length);
        var tempdiscounttotal = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].discount) {
                //console.log("Offer Region: " + data[i].discount["0"].region)
                //console.log("User Region: " + this.tempuser.region)
                for (var a = 0; a < 1; a++) {
                    console.log("data.length:" + data.length);
                    if (data[i].discount[a].region === this.tempuser.region) {
                        console.log("Offer Inside Region: " + data[i].discount[a].region);
                        var tempproducts = data[i];
                        //console.log("Discount Product Details: " + JSON.stringify(data[i])) 
                        var tempid = data[i].discount[a].discount_id.$oid;
                        this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" + tempid)
                            .subscribe(function (data) { return _this.saveoffers.setScope(data); });
                        this.tempoffer = this.saveoffers.getScope();
                        console.log("TempOffer: " + JSON.stringify(this.tempoffer));
                        this.productIn = new product_model_1.Product();
                        this.productIn.product_id = data[i]._id.$oid;
                        this.productIn.product_name = data[i].name;
                        this.productIn.categoryName = data[i].category_name;
                        this.productIn.product_price = data[i].price[0].price;
                        this.offers = new product_model_1.Offer();
                        this.offers.base_quantity = "2";
                        this.offers.offer_quantity = "1";
                        this.productIn.offer = this.offers;
                        this.products.push(this.productIn);
                        tempdiscounttotal = tempdiscounttotal + 1;
                        //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    }
                    else { }
                }
                //this.original.push(this.productIn);
            }
            else { }
        }
        console.log("tempdiscounttotal: " + tempdiscounttotal);
        this.saveproducts.setScope(this.products);
        this.orderedproducts = this.saveorderservice.getScope();
        console.log("Ordered products: " + this.orderedproducts);
        if (this.orderedproducts !== false) {
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else {
            return;
        }
    };
    ProductOffers.prototype.getdiscountdetails = function (productdetails, data) {
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity);
        this.tempoffer.base_quantity = data.base_quantity;
        this.tempoffer.offer_quantity = data.offer_quantity;
        /*
        console.log("Discount Product Details: " + JSON.stringify(productdetails))
        
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity)

        this.productIn = new Product();
        this.productIn.product_id = productdetails._id.$oid;
        this.productIn.product_name = productdetails.name;
        this.productIn.categoryName = productdetails.category_name;
        this.productIn.product_price = productdetails.price[0].price;
        
        console.log("Discount Product Name: " + productdetails.name)

        this.offers = new Offer();
        this.offers.base_quantity = data.base_quantity;
        this.offers.offer_quantity = data.offer_quantity;
        this.productIn.offer = this.offers;

        this.products.push(this.productIn);
        */
    };
    ProductOffers.prototype.addtocart = function (productid, index) {
        var _this = this;
        console.log("id: " + productid);
        this.boolean = false;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            if (this.productsOrdered[i]["product_id"] == productid) {
                console.log("already inserted");
                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: " + rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, productid); });
    };
    ProductOffers.prototype.setresult = function (quantity, productid) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            for (var i = 0; i < this.products.length; i++) {
                if (this.products[i]["product_id"] == productid) {
                    this.temp = order;
                    this.total = this.products[i]["product_price"];
                    this.total = this.total * this.temp;
                    //console.log("this.line_total: "+ this.total);
                    if (this.boolean === false) {
                        this.rackQty(productid, rack, order, this.total);
                    }
                    else {
                        this.rackQtyedit(rack, order, this.total, this.index);
                    }
                    console.log("promptid: " + this.products[i]["product_id"]);
                    this.products[i]["rackquantity"] = rack;
                    this.products[i]["quantity"] = order;
                }
            }
        }
    };
    ProductOffers.prototype.rackQty = function (productid, rack, order, line_total) {
        console.log("Inside rack Qty");
        this.productid.push(productid);
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);
        for (var i = 0; i < this.products.length; i++) {
            if (this.products[i].product_id == productid) {
                this.productname.push(this.products[i].product_name);
                this.productprice.push(this.products[i].product_price);
                this.productdiscount.push(this.products[i].discount_id);
            }
        }
        //console.log("line_total: " + this.line_total);        
        console.log("ProductId: " + this.productid + "orderquantity: " + this.orderquantity + "rackquantity: " + this.rackquantity + "line_total: " + this.line_total);
        var temp = (this.productid.length) - 1;
        this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
            quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
            line_total: this.line_total[temp], discount_id: this.productdiscount[temp], product_price: this.productprice[temp] });
        this.gettotal();
    };
    ProductOffers.prototype.rackQtyedit = function (rack, order, line_total, index) {
        console.log("Inside rack Qty edit");
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        this.gettotal();
    };
    ProductOffers.prototype.gettotal = function () {
        this.totalAmount = 0;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal + this.totalAmount;
            //console.log("totalamount: " + this.totalAmount);
        }
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    };
    ProductOffers.prototype.productdetail = function () {
        //this.routerExtensions.navigate(["/productdetail"]);
    };
    ProductOffers.prototype.updateproducts = function (orderedproducts) {
        for (var a = 0; a < this.products.length; a++) {
            for (var i = 0; i < orderedproducts.length; i++) {
                if (this.products[a]["product_id"] == orderedproducts[i]["product_id"]) {
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    };
    ProductOffers.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductOffers.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
    };
    ProductOffers.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductOffers.prototype.searchBarLoaded = function (args) {
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return ProductOffers;
}());
ProductOffers = __decorate([
    core_1.Component({
        selector: "product-offers",
        moduleId: module.id,
        templateUrl: "productoffers.component.html",
        styleUrls: ["productoffers.component.css"]
    }),
    __metadata("design:paramtypes", [router_2.ActivatedRoute, router_1.RouterExtensions,
        page_1.Page, saveorder_service_1.SaveOrderService, product_service_1.ProductService,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef,
        login_service_1.loginService, http_1.Http, product_service_1.SaveOffers, common_1.Location])
], ProductOffers);
exports.ProductOffers = ProductOffers;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdG9mZmVycy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9kdWN0b2ZmZXJzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDhEQUE0RDtBQUU1RCwrREFBb0Q7QUFDcEQsa0VBQTBGO0FBQzFGLHNFQUE2RjtBQUM3RixzQ0FBdUU7QUFFdkUsc0RBQStEO0FBQy9ELHNEQUFxRDtBQUNyRCwwQ0FBaUQ7QUFDakQsNERBQTREO0FBRTVELGtFQUEyRjtBQUMzRixzQ0FBcUM7QUFDckMscUNBQW1DO0FBQ25DLDJDQUFzRjtBQUN0Rix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBQzNDLElBQUksZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7QUFhdEYsSUFBYSxhQUFhO0lBb0N0Qiw4QkFBOEI7SUFFOUIsdUJBQW9CLEtBQXFCLEVBQVUsZ0JBQWtDLEVBQ3pFLElBQVUsRUFBVSxnQkFBa0MsRUFBVSxjQUE4QixFQUM5RixZQUEwQixFQUFVLFNBQW9CLEVBQVUsWUFBMEIsRUFDNUYsWUFBZ0MsRUFBVSxnQkFBa0MsRUFDNUUsWUFBMEIsRUFBVSxJQUFVLEVBQVUsVUFBc0IsRUFBVSxRQUFrQjtRQUpsRyxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDekUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUYsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDNUYsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUM1RSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQXhDdEgsVUFBSyxHQUFXLGNBQWMsQ0FBQztRQUMvQixtQkFBYyxHQUFrQixFQUFFLENBQUM7UUFDbkMsb0JBQWUsR0FBa0IsRUFBRSxDQUFDO1FBR3BDLGNBQVMsR0FBa0IsRUFBRSxDQUFDO1FBQzlCLGtCQUFhLEdBQWtCLEVBQUUsQ0FBQztRQUNsQyxpQkFBWSxHQUFrQixFQUFFLENBQUM7UUFDakMsZ0JBQVcsR0FBa0IsRUFBRSxDQUFDO1FBQ2hDLGlCQUFZLEdBQWtCLEVBQUUsQ0FBQztRQUNqQyxvQkFBZSxHQUFrQixFQUFFLENBQUM7UUFFcEMsZUFBVSxHQUFrQixFQUFFLENBQUM7UUFHL0IscUJBQXFCO1FBQ3JCLFlBQU8sR0FBVyxLQUFLLENBQUM7UUFFeEIsV0FBTSxHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQztRQUl6QixhQUFRLEdBQWMsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUlsQyxhQUFRLEdBQWMsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQWlCakMseUNBQXlDO1FBQ3pDLCtCQUErQjtJQUNuQyxDQUFDO0lBRUwsZ0NBQVEsR0FBUjtRQUFBLGlCQStDQztRQTVDRyx3Q0FBd0M7UUFDekMsSUFBSSxPQUFPLEdBQUc7WUFDVCxPQUFPLEVBQUUsWUFBWTtZQUNyQixRQUFRLEVBQUUsSUFBSTtZQUNkLE9BQU8sRUFBRTtnQkFDUCxhQUFhLEVBQUUsSUFBSTtnQkFDbkIsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLEdBQUcsRUFBRSxHQUFHO2dCQUNSLG9CQUFvQixFQUFFLFNBQVM7Z0JBQy9CLHFCQUFxQixFQUFFLElBQUk7Z0JBQzNCLGFBQWEsRUFBRSxDQUFDO2dCQUNoQixpQkFBaUIsRUFBRSxDQUFDO2FBQ3JCO1lBQ0QsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSx5QkFBeUI7Z0JBQ2xDLE1BQU0sRUFBRSxLQUFLO2dCQUNiLE1BQU0sRUFBRSxFQUFFO2dCQUNWLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixLQUFLLEVBQUUsU0FBUztnQkFDaEIsSUFBSSxFQUFFLEVBQUUsQ0FBQSxpQ0FBaUM7YUFDMUM7U0FDRixDQUFDO1FBRUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsc0NBQXNDO1FBRXhDLHVFQUF1RTtRQUN2RSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUE7UUFDM0IscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFO2FBQ2pDLFNBQVMsQ0FBQyxVQUFDLElBQUksSUFBTSxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRWxDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDMUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixtQ0FBbUM7WUFDbkMseURBQXlEO1lBQ3pELElBQUk7WUFDSixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFDRCxxQ0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLGtDQUFrQztRQUNsQywwQkFBMEI7UUFDMUIsc0NBQXNDO1FBQ3RDLElBQUk7UUFDSixZQUFZO1FBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNuQixtQ0FBbUM7UUFDbkMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUMsQ0FBQSxDQUFDO2dCQUN6QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLG9DQUFvQyxHQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFDbEYsQ0FBQztZQUNELElBQUksQ0FBQSxDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsMEJBQTBCLENBQUE7WUFDbkQsQ0FBQztZQUNELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRWpELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEdBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUM1RSxDQUFDO1FBQ0wsSUFBSTtJQUVSLENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDLENBQUEsQ0FBQztZQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQTtRQUNoQyxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDRCxNQUFNLENBQUM7UUFDWCxDQUFDO0lBQ0wsQ0FBQztJQUNELG1DQUFXLEdBQVg7UUFBQSxpQkFHQztRQUZHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFO2FBQy9CLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQS9CLENBQStCLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBSUQscUNBQWEsR0FBYixVQUFjLElBQVM7UUFBdkIsaUJBa0VDO1FBakVHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUxQyxJQUFJLGlCQUFpQixHQUFHLENBQUMsQ0FBQztRQUMxQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUVoQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQztnQkFFakIsOERBQThEO2dCQUM5RCxxREFBcUQ7Z0JBRXJELEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFDLENBQUM7b0JBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFFMUMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQSxDQUFDO3dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ2pFLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDM0Isc0VBQXNFO3dCQUN0RSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7d0JBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGlFQUFpRSxHQUFFLE1BQU0sQ0FBQzs2QkFFdkYsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFHLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQTlCLENBQThCLENBQUMsQ0FBQzt3QkFFbEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUU1QyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBO3dCQUMzRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksdUJBQU8sRUFBRSxDQUFDO3dCQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzt3QkFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQzt3QkFDcEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBRXRELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxxQkFBSyxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQzt3QkFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDO3dCQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUVuQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBRW5DLGlCQUFpQixHQUFHLGlCQUFpQixHQUFHLENBQUMsQ0FBQzt3QkFDMUMsa0dBQWtHO29CQUN0RyxDQUFDO29CQUVELElBQUksQ0FBQSxDQUFDLENBQUEsQ0FBQztnQkFDVixDQUFDO2dCQUNELHFDQUFxQztZQUN6QyxDQUFDO1lBQ0QsSUFBSSxDQUFBLENBQUMsQ0FBQSxDQUFDO1FBR1YsQ0FBQztRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsaUJBQWlCLENBQUMsQ0FBQTtRQUN0RCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFFekQsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxLQUFLLENBQUMsQ0FBQSxDQUFDO1lBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNoRCxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDRCxNQUFNLENBQUM7UUFDWCxDQUFDO0lBQ0wsQ0FBQztJQUVELDBDQUFrQixHQUFsQixVQUFtQixjQUFjLEVBQUMsSUFBSTtRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFekYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ3BEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBbUJFO0lBQ04sQ0FBQztJQUVELGlDQUFTLEdBQVQsVUFBVSxTQUFTLEVBQUMsS0FBSztRQUF6QixpQkF1QkM7UUF0QkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUUsU0FBUyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRyxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQ25ELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUEsQ0FBQztnQkFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUVoQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRSxRQUFRLEdBQUcsU0FBUyxHQUFHLFNBQVMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDbkIsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLE9BQU8sR0FBdUI7WUFDOUIsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFDO1lBQzVDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDMUMsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLHlCQUFNLEVBQUUsT0FBTyxDQUFDO2FBQzNDLElBQUksQ0FBQyxVQUFDLFlBQTJCLElBQUssT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBQyxTQUFTLENBQUMsRUFBdEMsQ0FBc0MsQ0FBQyxDQUFDO0lBRW5GLENBQUM7SUFFTSxpQ0FBUyxHQUFoQixVQUFpQixRQUFRLEVBQUMsU0FBUztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRSxRQUFRLENBQUMsQ0FBQztRQUVsQyxFQUFFLENBQUEsQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLENBQUEsQ0FBQztZQUNuQixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDTCxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLEdBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFHMUQsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUMxQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFBLENBQUM7b0JBQzVDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO29CQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQy9DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNsQywrQ0FBK0M7b0JBQy9DLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssS0FBSyxDQUFDLENBQUEsQ0FBQzt3QkFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JELENBQUM7b0JBQ0QsSUFBSSxDQUFBLENBQUM7d0JBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUMxRCxDQUFDO29CQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDMUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUM7b0JBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUN6QyxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUcsK0JBQU8sR0FBUCxVQUFRLFNBQVMsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLFVBQVU7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRWpDLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUV6QyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUV6QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN2RCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzVELENBQUM7UUFDTCxDQUFDO1FBRUQsd0RBQXdEO1FBQ3hELE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsaUJBQWlCLEdBQUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxnQkFBZ0IsR0FBRSxJQUFJLENBQUMsWUFBWSxHQUFFLGNBQWMsR0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFMUosSUFBSSxJQUFJLEdBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztZQUNuRixRQUFRLEVBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxhQUFhLEVBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxZQUFZLEVBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7WUFDN0csVUFBVSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsV0FBVyxFQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsYUFBYSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBRXRILElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsbUNBQVcsR0FBWCxVQUFZLElBQUksRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFDLEtBQUs7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRXBDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUNoRCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUV2RCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFHLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDbkQsdURBQXVEO1lBQ3ZELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLEdBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUM5QyxrREFBa0Q7UUFDdEQsQ0FBQztRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQscUNBQWEsR0FBYjtRQUNJLHFEQUFxRDtJQUN6RCxDQUFDO0lBRUQsc0NBQWMsR0FBZCxVQUFlLGVBQWU7UUFFMUIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO1lBQ3hDLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUMxQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQzVFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUN2RSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNqRCxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0NBQVEsR0FBZixVQUFnQixJQUFJO1FBQ2hCLElBQUksU0FBUyxHQUFjLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDN0IsSUFBSSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTSxJQUFHLE9BQUEsTUFBTSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ3JHLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQURULENBQ1MsQ0FBQyxDQUFDO1FBQ3pELEVBQUUsQ0FBQSxDQUFDLG9CQUFTLENBQUMsQ0FBQSxDQUFDO1lBQ1YsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNuQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLHFDQUFhLEdBQXBCLFVBQXFCLElBQUk7UUFDckIsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxJQUFJLFdBQVcsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9DLDZDQUE2QztRQUM3QyxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFBLE1BQU0sSUFBRyxPQUFBLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztZQUNyRyxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFEVCxDQUNTLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRU0sK0JBQU8sR0FBZCxVQUFlLElBQUk7UUFDZixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdCLFNBQVMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQSxDQUFDLG9CQUFTLENBQUMsQ0FBQSxDQUFDO1lBQ1YsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNuQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHVDQUFlLEdBQWYsVUFBZ0IsSUFBSTtRQUNoQixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLEVBQUUsQ0FBQSxDQUFDLG9CQUFTLENBQUMsQ0FBQSxDQUFDO1lBQ1YsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNuQyxDQUFDO0lBQ0wsQ0FBQztJQUVMLG9CQUFDO0FBQUQsQ0FBQyxBQTVZRCxJQTRZQztBQTVZWSxhQUFhO0lBUHpCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUMsZ0JBQWdCO1FBQ3pCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUMsOEJBQThCO1FBQzFDLFNBQVMsRUFBRSxDQUFDLDZCQUE2QixDQUFDO0tBQzdDLENBQUM7cUNBd0M2Qix1QkFBYyxFQUE0Qix5QkFBZ0I7UUFDbkUsV0FBSSxFQUE0QixvQ0FBZ0IsRUFBMEIsZ0NBQWM7UUFDaEYsZ0NBQVksRUFBcUIsNkJBQVMsRUFBd0IsOEJBQVk7UUFDOUUsaUNBQWtCLEVBQTRCLHVCQUFnQjtRQUM5RCw0QkFBWSxFQUFnQixXQUFJLEVBQXNCLDRCQUFVLEVBQW9CLGlCQUFRO0dBMUM3RyxhQUFhLENBNFl6QjtBQTVZWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGxvZ2luU2VydmljZSB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL2xvZ2luLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlQXJyYXkgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZS1hcnJheSc7XHJcbmltcG9ydCB7IERpYWxvZyB9IGZyb20gJy4uL0RpYWxvZy9kaWFsb2cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUHJvZHVjdFNlcnZpY2UsIFNhdmVQcm9kdWN0cywgU2F2ZU9mZmVycyB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL3Byb2R1Y3Quc2VydmljZSc7XHJcbmltcG9ydCB7IFNhdmVPcmRlclNlcnZpY2UsIFNhdmVDdXN0b21lciwgU2F2ZVRvdGFsIH0gZnJvbSAnLi4vLi4vU2VydmljZXMvc2F2ZW9yZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDb250YWluZXJSZWYsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBwcm9tcHQsIGlucHV0VHlwZSB9IGZyb20gXCJ1aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFByb2R1Y3QsIE9mZmVyIH0gZnJvbSBcIi4uLy4uL01vZGVscy9wcm9kdWN0Lm1vZGVsXCI7XHJcbmltcG9ydCB7IFNlYXJjaEJhciB9IGZyb20gXCJ1aS9zZWFyY2gtYmFyXCI7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSwgTW9kYWxEaWFsb2dPcHRpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7aXNBbmRyb2lkfSBmcm9tIFwicGxhdGZvcm1cIjtcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG52YXIgTG9hZGluZ0luZGljYXRvciA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtbG9hZGluZy1pbmRpY2F0b3ItbmV3XCIpLkxvYWRpbmdJbmRpY2F0b3I7XHJcblxyXG5cclxuXHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjpcInByb2R1Y3Qtb2ZmZXJzXCIsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6XCJwcm9kdWN0b2ZmZXJzLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcInByb2R1Y3RvZmZlcnMuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFByb2R1Y3RPZmZlcnN7XHJcblxyXG4gICAgdGl0bGU6IHN0cmluZyA9IFwiUHJvZHVjdCBMaXN0XCI7XHJcbiAgICBwcm9kdWN0RGV0YWlsczogQXJyYXk8T2JqZWN0PiA9IFtdO1xyXG4gICAgcHJvZHVjdHNPcmRlcmVkOiBBcnJheTxPYmplY3Q+ID0gW107XHJcbiAgICBvcmRlcmVkcHJvZHVjdHM6IGFueTtcclxuICAgIGN1c3RvbWVyaWQ6IGFueTtcclxuICAgIHByb2R1Y3RpZDogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgb3JkZXJxdWFudGl0eTogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgcmFja3F1YW50aXR5OiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBwcm9kdWN0bmFtZTogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG4gICAgcHJvZHVjdHByaWNlOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBwcm9kdWN0ZGlzY291bnQ6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuICAgIHRvdGFsOiBhbnk7XHJcbiAgICBsaW5lX3RvdGFsOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICB0ZW1wOiBhbnk7XHJcbiAgICB0b3RhbEFtb3VudDogYW55O1xyXG4gICAgLy9xdHllZGl0OiBzdHJpbmcgPVwiXCJcclxuICAgIGJvb2xlYW46Ym9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaW5kZXg6YW55O1xyXG4gICAgbG9hZGVyID0gbmV3IExvYWRpbmdJbmRpY2F0b3IoKTtcclxuICAgIFxyXG4gICAgcHVibGljIHNlYXJjaFBocmFzZTogc3RyaW5nO1xyXG5cclxuICAgIHB1YmxpYyBwcm9kdWN0czogUHJvZHVjdFtdID0gbmV3IEFycmF5KCk7XHJcbiAgICBwdWJsaWMgcHJvZHVjdEluOiBQcm9kdWN0O1xyXG4gICAgcHVibGljIG9mZmVyczogT2ZmZXI7XHJcblxyXG4gICAgcHVibGljIG9yaWdpbmFsOiBQcm9kdWN0W10gPSBuZXcgQXJyYXkoKTtcclxuXHJcbiAgICBwdWJsaWMgYWRkdmlzaWJpbGl0eTtcclxuICAgIHB1YmxpYyBlZGl0dmlzaWJpbGl0eTtcclxuXHJcbiAgICBwdWJsaWMgaWZjdXN0b21lcjtcclxuICAgIHB1YmxpYyB0ZW1wdXNlcjtcclxuICAgIHB1YmxpYyB0ZW1wb2ZmZXI7XHJcbiAgICAvLyBlbmFibGVNZXNzYWdlID0gJ2NvbGxhcHNlJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBcclxuICAgICAgICBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgc2F2ZW9yZGVyc2VydmljZTogU2F2ZU9yZGVyU2VydmljZSwgcHJpdmF0ZSBwcm9kdWN0c2VydmljZTogUHJvZHVjdFNlcnZpY2UsIFxyXG4gICAgICAgIHByaXZhdGUgc2F2ZWN1c3RvbWVyOiBTYXZlQ3VzdG9tZXIsIHByaXZhdGUgc2F2ZXRvdGFsOiBTYXZlVG90YWwsIHByaXZhdGUgc2F2ZXByb2R1Y3RzOiBTYXZlUHJvZHVjdHMsXHJcbiAgICAgICAgcHJpdmF0ZSBtb2RhbFNlcnZpY2U6IE1vZGFsRGlhbG9nU2VydmljZSwgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxyXG4gICAgICAgIHByaXZhdGUgbG9naW5zZXJ2aWNlOiBsb2dpblNlcnZpY2UsIHByaXZhdGUgaHR0cDogSHR0cCwgcHJpdmF0ZSBzYXZlb2ZmZXJzOiBTYXZlT2ZmZXJzLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbil7XHJcblxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgLy9sZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgICAgICAgICAgLy9zZWFyY2hCYXIuZGlzbWlzc1NvZnRJbnB1dCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpe1xyXG5cclxuICAgICAgIFxyXG4gICAgICAgIC8vIGNvZGUgZm9yIGxvYWRpbmcgaW5kaWNhdG9yIHN0YXJ0IGhlcmVcclxuICAgICAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICBtZXNzYWdlOiAnTG9hZGluZy4uLicsXHJcbiAgICAgICAgICAgIHByb2dyZXNzOiAwLjY1LFxyXG4gICAgICAgICAgICBhbmRyb2lkOiB7XHJcbiAgICAgICAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgICBtYXg6IDEwMCxcclxuICAgICAgICAgICAgICBwcm9ncmVzc051bWJlckZvcm1hdDogXCIlMWQvJTJkXCIsXHJcbiAgICAgICAgICAgICAgcHJvZ3Jlc3NQZXJjZW50Rm9ybWF0OiAwLjUzLFxyXG4gICAgICAgICAgICAgIHByb2dyZXNzU3R5bGU6IDEsXHJcbiAgICAgICAgICAgICAgc2Vjb25kYXJ5UHJvZ3Jlc3M6IDFcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaW9zOiB7XHJcbiAgICAgICAgICAgICAgZGV0YWlsczogXCJBZGRpdGlvbmFsIGRldGFpbCBub3RlIVwiLFxyXG4gICAgICAgICAgICAgIHNxdWFyZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgbWFyZ2luOiAxMCxcclxuICAgICAgICAgICAgICBkaW1CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiM0QjlFRDZcIixcclxuICAgICAgICAgICAgICBtb2RlOiBcIlwiLy8gc2VlIGlPUyBzcGVjaWZpYyBvcHRpb25zIGJlbG93XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmxvYWRlci5zaG93KG9wdGlvbnMpO1xyXG4gICAgICAgICAgLy8gY29kZSBmb3IgbG9hZGluZyBpbmRpY2F0b3IgZW5kIGhlcmVcclxuXHJcbiAgICAgICAgLy9hcHBsaWNhdGlvbi5hbmRyb2lkLm9mZihBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50KTtcclxuICAgICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5pZmN1c3RvbWVyID0gXCJ2aXNpYmxlXCJcclxuICAgICAgICAvL3RoaXMuZ2V0cHJvZHVjdHMoKTtcclxuICAgICAgICB0aGlzLnByb2R1Y3RzZXJ2aWNlLmdldERpc2NvdW50cygpXHJcbiAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge3RoaXMubG9hZERpc2NvdW50cyhkYXRhLmpzb24oKSl9KTtcclxuICAgICAgICB0aGlzLnRlbXB1c2VyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKVxyXG4gICAgICAgIHRoaXMuY2hlY2tjdXN0b21lcih0aGlzLnRlbXB1c2VyKTtcclxuICAgICAgICBcclxuICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgICAgLy8gaWYodGhpcy50ZW1wdXNlci5yb2xlID09ICdyZXAnKXtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvY3VzdG9tZXJsaXN0XCJdKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMuYmFja1RvUHJldmlvdXNQYWdlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGxvYWREaXNjb3VudHMoZGF0YSl7ICAgICAgICAgICBcclxuICAgICAgICAvLyBpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubG9hZGVyLmhpZGUoKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5lbmFibGVNZXNzYWdlID0gJ3Zpc2libGUnO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAvLyBlbHNlIHsgICBcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXIuaGlkZSgpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmVuYWJsZU1lc3NhZ2UgPSAnY29sbGFwc2UnO1xyXG4gICAgICAgICAgICBmb3IobGV0IGk9MDtpPGRhdGEubGVuZ3RoO2krKyl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGlzY291bnRcIitpKTtcclxuICAgICAgICAgICAgaWYoZGF0YVtpXS5pbWFnZV9uYW1lICE9IFwiXCIpeyAgICAgXHJcbiAgICAgICAgICAgICAgICBkYXRhW2ldLmltYWdlX25hbWUgPSBcImh0dHBzOi8vYXBwLmtyaXNobmFiaGF2YW5mb29kcy5pbi9cIisgZGF0YVtpXS5pbWFnZV9uYW1lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICBkYXRhW2ldLmltYWdlX25hbWUgPSBcIn4vSW1hZ2VzL3Byb2R1Y3RfaW1nLnBuZ1wiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZGF0YVtpXS52YWxpZF9mcm9tID0gZGF0YVtpXS52YWxpZF9mcm9tLnNsaWNlKDAsIDExKTtcclxuICAgICAgICAgICAgZGF0YVtpXS52YWxpZF90byA9IGRhdGFbaV0udmFsaWRfdG8uc2xpY2UoMCwgMTEpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0cy5wdXNoKGRhdGFbaV0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImRhdGEgaW5zaWRlIHRoZSBwcm9kdWN0IGFycmF5XCIrIEpTT04uc3RyaW5naWZ5KHRoaXMucHJvZHVjdHMpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBjaGVja2N1c3RvbWVyKGRhdGEpe1xyXG4gICAgICAgIGlmKGRhdGEucm9sZSA9PT0gJ2N1c3RvbWVyJyl7XHJcbiAgICAgICAgICAgIHRoaXMuaWZjdXN0b21lciA9IFwiY29sbGFwc2VcIlxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZ2V0cHJvZHVjdHMoKXtcclxuICAgICAgICB0aGlzLnByb2R1Y3RzZXJ2aWNlLmdldHByb2R1Y3QoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnBhcnNlUHJvZHVjdHMoZGF0YS5qc29uKCkpKTtcclxuICAgIH1cclxuICAgXHJcbiAgICBcclxuXHJcbiAgICBwYXJzZVByb2R1Y3RzKGRhdGE6IGFueSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YS5sZW5ndGg6XCIgKyBkYXRhLmxlbmd0aCk7XHJcbiAgICBcclxuICAgICAgICB2YXIgdGVtcGRpc2NvdW50dG90YWwgPSAwO1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykgeyAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmKGRhdGFbaV0uZGlzY291bnQpeyAgICAgICBcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIk9mZmVyIFJlZ2lvbjogXCIgKyBkYXRhW2ldLmRpc2NvdW50W1wiMFwiXS5yZWdpb24pXHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiVXNlciBSZWdpb246IFwiICsgdGhpcy50ZW1wdXNlci5yZWdpb24pXHJcblxyXG4gICAgICAgICAgICAgICAgZm9yKHZhciBhPTA7IGEgPCAxOyBhKyspe1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YS5sZW5ndGg6XCIgKyBkYXRhLmxlbmd0aCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRhdGFbaV0uZGlzY291bnRbYV0ucmVnaW9uID09PSB0aGlzLnRlbXB1c2VyLnJlZ2lvbil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT2ZmZXIgSW5zaWRlIFJlZ2lvbjogXCIgKyBkYXRhW2ldLmRpc2NvdW50W2FdLnJlZ2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRlbXBwcm9kdWN0cyA9IGRhdGFbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJEaXNjb3VudCBQcm9kdWN0IERldGFpbHM6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YVtpXSkpIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGVtcGlkID0gZGF0YVtpXS5kaXNjb3VudFthXS5kaXNjb3VudF9pZC4kb2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmh0dHAuZ2V0KFwiaHR0cHM6Ly9hcHAua3Jpc2huYWJoYXZhbmZvb2RzLmluL2FwaS9kaXNjb3VudC9nZXRkaXNjb3VudHMvaWQvXCIgK3RlbXBpZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8uc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5nZXRkaXNjb3VudGRldGFpbHModGVtcHByb2R1Y3RzLGRhdGEuanNvbigpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PnRoaXMuc2F2ZW9mZmVycy5zZXRTY29wZShkYXRhKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlbXBvZmZlciA9IHRoaXMuc2F2ZW9mZmVycy5nZXRTY29wZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJUZW1wT2ZmZXI6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy50ZW1wb2ZmZXIpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RJbiA9IG5ldyBQcm9kdWN0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLnByb2R1Y3RfaWQgPSBkYXRhW2ldLl9pZC4kb2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5wcm9kdWN0X25hbWUgPSBkYXRhW2ldLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdEluLmNhdGVnb3J5TmFtZSA9IGRhdGFbaV0uY2F0ZWdvcnlfbmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0SW4ucHJvZHVjdF9wcmljZSA9IGRhdGFbaV0ucHJpY2VbMF0ucHJpY2U7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vZmZlcnMgPSBuZXcgT2ZmZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vZmZlcnMuYmFzZV9xdWFudGl0eSA9IFwiMlwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9mZmVycy5vZmZlcl9xdWFudGl0eSA9IFwiMVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RJbi5vZmZlciA9IHRoaXMub2ZmZXJzOyBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHMucHVzaCh0aGlzLnByb2R1Y3RJbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wZGlzY291bnR0b3RhbCA9IHRlbXBkaXNjb3VudHRvdGFsICsgMTsgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIioqKkJ1eVwiICsgdGhpcy50ZW1wb2ZmZXIuYmFzZV9xdWFudGl0eSArIFwiR2V0XCIgKyB0aGlzLnRlbXBvZmZlci5vZmZlcl9xdWFudGl0eSkgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2V7fVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy90aGlzLm9yaWdpbmFsLnB1c2godGhpcy5wcm9kdWN0SW4pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2V7fSAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9ICBcclxuICAgICAgICBcclxuICAgICAgICBjb25zb2xlLmxvZyhcInRlbXBkaXNjb3VudHRvdGFsOiBcIiArIHRlbXBkaXNjb3VudHRvdGFsKVxyXG4gICAgICAgIHRoaXMuc2F2ZXByb2R1Y3RzLnNldFNjb3BlKHRoaXMucHJvZHVjdHMpO1xyXG5cclxuICAgICAgICB0aGlzLm9yZGVyZWRwcm9kdWN0cyA9IHRoaXMuc2F2ZW9yZGVyc2VydmljZS5nZXRTY29wZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiT3JkZXJlZCBwcm9kdWN0czogXCIgKyB0aGlzLm9yZGVyZWRwcm9kdWN0cyk7XHJcblxyXG4gICAgICAgIGlmKHRoaXMub3JkZXJlZHByb2R1Y3RzICE9PSBmYWxzZSl7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlcHJvZHVjdHModGhpcy5vcmRlcmVkcHJvZHVjdHMpO1xyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZCA9IHRoaXMub3JkZXJlZHByb2R1Y3RzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldGRpc2NvdW50ZGV0YWlscyhwcm9kdWN0ZGV0YWlscyxkYXRhKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLl9pZC4kb2lkICsgXCIgQnV5IFwiICsgZGF0YS5iYXNlX3F1YW50aXR5ICsgXCIgR2V0IFwiICsgZGF0YS5vZmZlcl9xdWFudGl0eSlcclxuXHJcbiAgICAgICAgdGhpcy50ZW1wb2ZmZXIuYmFzZV9xdWFudGl0eSA9IGRhdGEuYmFzZV9xdWFudGl0eTtcclxuICAgICAgICB0aGlzLnRlbXBvZmZlci5vZmZlcl9xdWFudGl0eSA9IGRhdGEub2ZmZXJfcXVhbnRpdHk7XHJcbiAgICAgICAgLypcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkRpc2NvdW50IFByb2R1Y3QgRGV0YWlsczogXCIgKyBKU09OLnN0cmluZ2lmeShwcm9kdWN0ZGV0YWlscykpXHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YS5faWQuJG9pZCArIFwiIEJ1eSBcIiArIGRhdGEuYmFzZV9xdWFudGl0eSArIFwiIEdldCBcIiArIGRhdGEub2ZmZXJfcXVhbnRpdHkpXHJcblxyXG4gICAgICAgIHRoaXMucHJvZHVjdEluID0gbmV3IFByb2R1Y3QoKTsgICAgICAgIFxyXG4gICAgICAgIHRoaXMucHJvZHVjdEluLnByb2R1Y3RfaWQgPSBwcm9kdWN0ZGV0YWlscy5faWQuJG9pZDtcclxuICAgICAgICB0aGlzLnByb2R1Y3RJbi5wcm9kdWN0X25hbWUgPSBwcm9kdWN0ZGV0YWlscy5uYW1lO1xyXG4gICAgICAgIHRoaXMucHJvZHVjdEluLmNhdGVnb3J5TmFtZSA9IHByb2R1Y3RkZXRhaWxzLmNhdGVnb3J5X25hbWU7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0SW4ucHJvZHVjdF9wcmljZSA9IHByb2R1Y3RkZXRhaWxzLnByaWNlWzBdLnByaWNlOyBcclxuICAgICAgICBcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkRpc2NvdW50IFByb2R1Y3QgTmFtZTogXCIgKyBwcm9kdWN0ZGV0YWlscy5uYW1lKVxyXG5cclxuICAgICAgICB0aGlzLm9mZmVycyA9IG5ldyBPZmZlcigpO1xyXG4gICAgICAgIHRoaXMub2ZmZXJzLmJhc2VfcXVhbnRpdHkgPSBkYXRhLmJhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgdGhpcy5vZmZlcnMub2ZmZXJfcXVhbnRpdHkgPSBkYXRhLm9mZmVyX3F1YW50aXR5O1xyXG4gICAgICAgIHRoaXMucHJvZHVjdEluLm9mZmVyID0gdGhpcy5vZmZlcnM7IFxyXG5cclxuICAgICAgICB0aGlzLnByb2R1Y3RzLnB1c2godGhpcy5wcm9kdWN0SW4pO1xyXG4gICAgICAgICovXHJcbiAgICB9XHJcblxyXG4gICAgYWRkdG9jYXJ0KHByb2R1Y3RpZCxpbmRleCl7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpZDogXCIrIHByb2R1Y3RpZCk7XHJcbiAgICAgICAgdGhpcy5ib29sZWFuID0gZmFsc2U7XHJcbiAgICAgICAgZm9yKHZhciBpPTAgOyBpIDwgdGhpcy5wcm9kdWN0c09yZGVyZWQubGVuZ3RoIDsgaSsrICkge1xyXG4gICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcInByb2R1Y3RfaWRcIl0gPT0gcHJvZHVjdGlkKXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWxyZWFkeSBpbnNlcnRlZFwiKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgcmFja2VkaXQgPSB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcInJhY2tfcXVhbnRpdHlcIl07XHJcbiAgICAgICAgICAgICAgICB2YXIgb3JkZXJlZGl0ID0gdGhpcy5wcm9kdWN0c09yZGVyZWRbaV1bXCJxdWFudGl0eVwiXTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmFjazogXCIrIHJhY2tlZGl0ICsgXCJvcmRlcjogXCIgKyBvcmRlcmVkaXQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sZWFuID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXggPSBpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBjb250ZXh0OiB7IHJhY2s6IHJhY2tlZGl0LCBvcmRlcjogb3JkZXJlZGl0fSxcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52aWV3Q29udGFpbmVyUmVmXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKERpYWxvZywgb3B0aW9ucylcclxuICAgICAgICAudGhlbigoZGlhbG9nUmVzdWx0OiBBcnJheTxzdHJpbmc+KSA9PiB0aGlzLnNldHJlc3VsdChkaWFsb2dSZXN1bHQscHJvZHVjdGlkKSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRyZXN1bHQocXVhbnRpdHkscHJvZHVjdGlkKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJlc3VsdDogXCIrIHF1YW50aXR5KTtcclxuXHJcbiAgICAgICAgaWYocXVhbnRpdHkgPT0gXCJudWxsXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgdmFyIHJhY2sgPSBxdWFudGl0eVswXTtcclxuICAgICAgICB2YXIgb3JkZXIgPSBxdWFudGl0eVsxXTtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJyZXMxOiBcIitxdWFudGl0eVswXSArIFwicmVzMjogXCIrIHF1YW50aXR5WzFdKTtcclxuXHJcblxyXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLnByb2R1Y3RzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgaWYodGhpcy5wcm9kdWN0c1tpXVtcInByb2R1Y3RfaWRcIl0gPT0gcHJvZHVjdGlkKXtcclxuICAgICAgICAgICAgICAgIHRoaXMudGVtcCA9IG9yZGVyO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3RhbCA9IHRoaXMucHJvZHVjdHNbaV1bXCJwcm9kdWN0X3ByaWNlXCJdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3RhbCA9IHRoaXMudG90YWwqdGhpcy50ZW1wO1xyXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcInRoaXMubGluZV90b3RhbDogXCIrIHRoaXMudG90YWwpO1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5ib29sZWFuID09PSBmYWxzZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yYWNrUXR5KHByb2R1Y3RpZCwgcmFjaywgb3JkZXIsIHRoaXMudG90YWwpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJhY2tRdHllZGl0KHJhY2ssIG9yZGVyLCB0aGlzLnRvdGFsLCB0aGlzLmluZGV4KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicHJvbXB0aWQ6IFwiKyB0aGlzLnByb2R1Y3RzW2ldW1wicHJvZHVjdF9pZFwiXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RzW2ldW1wicmFja3F1YW50aXR5XCJdID0gcmFjaztcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbaV1bXCJxdWFudGl0eVwiXSA9IG9yZGVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4gICAgcmFja1F0eShwcm9kdWN0aWQsIHJhY2ssIG9yZGVyLCBsaW5lX3RvdGFsKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkluc2lkZSByYWNrIFF0eVwiKTtcclxuICAgICAgICB0aGlzLnByb2R1Y3RpZC5wdXNoKHByb2R1Y3RpZCk7ICAgICAgICBcclxuICAgICAgICB0aGlzLm9yZGVycXVhbnRpdHkucHVzaChvcmRlcik7XHJcbiAgICAgICAgdGhpcy5yYWNrcXVhbnRpdHkucHVzaChyYWNrKTtcclxuICAgICAgICB0aGlzLmxpbmVfdG90YWwucHVzaChsaW5lX3RvdGFsKTtcclxuXHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGkgPCB0aGlzLnByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzW2ldLnByb2R1Y3RfaWQgPT0gcHJvZHVjdGlkKXtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2R1Y3RuYW1lLnB1c2godGhpcy5wcm9kdWN0c1tpXS5wcm9kdWN0X25hbWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0cHJpY2UucHVzaCh0aGlzLnByb2R1Y3RzW2ldLnByb2R1Y3RfcHJpY2UpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0ZGlzY291bnQucHVzaCh0aGlzLnByb2R1Y3RzW2ldLmRpc2NvdW50X2lkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcImxpbmVfdG90YWw6IFwiICsgdGhpcy5saW5lX3RvdGFsKTsgICAgICAgIFxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiUHJvZHVjdElkOiBcIisgdGhpcy5wcm9kdWN0aWQgKyBcIm9yZGVycXVhbnRpdHk6IFwiKyB0aGlzLm9yZGVycXVhbnRpdHkgKyBcInJhY2txdWFudGl0eTogXCIrIHRoaXMucmFja3F1YW50aXR5KyBcImxpbmVfdG90YWw6IFwiKyB0aGlzLmxpbmVfdG90YWwpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciB0ZW1wID0gICh0aGlzLnByb2R1Y3RpZC5sZW5ndGgpIC0gMTsgICAgIFxyXG4gICAgICAgIHRoaXMucHJvZHVjdHNPcmRlcmVkLnB1c2goe3Byb2R1Y3RfaWQ6dGhpcy5wcm9kdWN0aWRbdGVtcF0sIHNrdV9pZDp0aGlzLnByb2R1Y3RpZFt0ZW1wXSwgXHJcbiAgICAgICAgICAgIHF1YW50aXR5OnRoaXMub3JkZXJxdWFudGl0eVt0ZW1wXSwgcmFja19xdWFudGl0eTp0aGlzLnJhY2txdWFudGl0eVt0ZW1wXSwgcHJvZHVjdF9uYW1lOnRoaXMucHJvZHVjdG5hbWVbdGVtcF0sIFxyXG4gICAgICAgICAgICBsaW5lX3RvdGFsOnRoaXMubGluZV90b3RhbFt0ZW1wXSwgZGlzY291bnRfaWQ6dGhpcy5wcm9kdWN0ZGlzY291bnRbdGVtcF0sIHByb2R1Y3RfcHJpY2U6dGhpcy5wcm9kdWN0cHJpY2VbdGVtcF19KTtcclxuICAgIFxyXG4gICAgICAgIHRoaXMuZ2V0dG90YWwoKTtcclxuICAgIH1cclxuXHJcbiAgICByYWNrUXR5ZWRpdChyYWNrLCBvcmRlciwgbGluZV90b3RhbCxpbmRleCl7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJJbnNpZGUgcmFjayBRdHkgZWRpdFwiKTtcclxuICAgICAgICBcclxuICAgICAgICBjb25zb2xlLmxvZyhcImFscmVhZHkgaW5zZXJ0ZWRcIik7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0c09yZGVyZWRbaW5kZXhdW1wicmFja19xdWFudGl0eVwiXSA9IHJhY2s7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0c09yZGVyZWRbaW5kZXhdW1wicXVhbnRpdHlcIl0gPSBvcmRlcjtcclxuICAgICAgICB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpbmRleF1bXCJsaW5lX3RvdGFsXCJdID0gbGluZV90b3RhbDtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmdldHRvdGFsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0dG90YWwoKXtcclxuICAgICAgICB0aGlzLnRvdGFsQW1vdW50ID0gMDtcclxuICAgICAgICBmb3IodmFyIGk9MCA7IGkgPCB0aGlzLnByb2R1Y3RzT3JkZXJlZC5sZW5ndGggOyBpKysgKSB7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coaSArXCI6IFwiICsgdGhpcy5wcm9kdWN0c09yZGVyZWRbaV1bXCJpZFwiXSk7XHJcbiAgICAgICAgICAgIHZhciB0ZW1wdG90YWwgPSB0aGlzLnByb2R1Y3RzT3JkZXJlZFtpXVtcImxpbmVfdG90YWxcIl07XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxBbW91bnQgPSB0ZW1wdG90YWwrdGhpcy50b3RhbEFtb3VudDtcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcInRvdGFsYW1vdW50OiBcIiArIHRoaXMudG90YWxBbW91bnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNhdmVvcmRlcnNlcnZpY2Uuc2V0U2NvcGUodGhpcy5wcm9kdWN0c09yZGVyZWQpO1xyXG4gICAgICAgIHRoaXMuc2F2ZXRvdGFsLnNldFNjb3BlKHRoaXMudG90YWxBbW91bnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb2R1Y3RkZXRhaWwoKXtcclxuICAgICAgICAvL3RoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJvZHVjdGRldGFpbFwiXSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlcHJvZHVjdHMob3JkZXJlZHByb2R1Y3RzKXtcclxuICAgICAgICBcclxuICAgICAgICBmb3IodmFyIGE9MDsgYSA8IHRoaXMucHJvZHVjdHMubGVuZ3RoOyBhKyspe1xyXG4gICAgICAgICAgICBmb3IodmFyIGk9MDsgaSA8IG9yZGVyZWRwcm9kdWN0cy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLnByb2R1Y3RzW2FdW1wicHJvZHVjdF9pZFwiXSA9PSBvcmRlcmVkcHJvZHVjdHNbaV1bXCJwcm9kdWN0X2lkXCJdKXtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk9yZGVyZWQgcHJvZHVjdHMgbmFtZTogXCIgKyBvcmRlcmVkcHJvZHVjdHNbaV1bXCJwcm9kdWN0X25hbWVcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbYV1bXCJyYWNrcXVhbnRpdHlcIl0gPSBvcmRlcmVkcHJvZHVjdHNbaV1bXCJyYWNrX3F1YW50aXR5XCJdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZHVjdHNbYV1bXCJxdWFudGl0eVwiXSA9IG9yZGVyZWRwcm9kdWN0c1tpXVtcInF1YW50aXR5XCJdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG90YWxBbW91bnQgPSB0aGlzLnNhdmV0b3RhbC5nZXRTY29wZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwdWJsaWMgb25TdWJtaXQoYXJncykgeyAgICAgICAgXHJcbiAgICAgICAgbGV0IHNlYXJjaEJhciA9IDxTZWFyY2hCYXI+YXJncy5vYmplY3Q7XHJcbiAgICAgICAgc2VhcmNoQmFyLmRpc21pc3NTb2Z0SW5wdXQoKTtcclxuICAgICAgICBsZXQgc2VhcmNoVmFsdWUgPSBzZWFyY2hCYXIudGV4dC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIHRoaXMucHJvZHVjdHMgPSB0aGlzLm9yaWdpbmFsLmZpbHRlcihyZXN1bHQ9PiByZXN1bHQucHJvZHVjdF9uYW1lLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoVmFsdWUpICB8fCBcclxuICAgICAgICByZXN1bHQuY2F0ZWdvcnlOYW1lLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoVmFsdWUpKTtcclxuICAgICAgICBpZihpc0FuZHJvaWQpeyAgICAgICAgXHJcbiAgICAgICAgICAgIHNlYXJjaEJhci5hbmRyb2lkLmNsZWFyRm9jdXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyBvblRleHRDaGFuZ2VkKGFyZ3MpIHtcclxuICAgICAgICBsZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgICAgICBsZXQgc2VhcmNoVmFsdWUgPSBzZWFyY2hCYXIudGV4dC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJzZWFyY2hWYWx1ZTogXCIgKyBzZWFyY2hWYWx1ZSk7XHJcbiAgICAgICAgLy9hbGVydChzZWFyY2hWYWx1ZSk7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0cyA9IHRoaXMub3JpZ2luYWwuZmlsdGVyKHJlc3VsdD0+IHJlc3VsdC5wcm9kdWN0X25hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hWYWx1ZSkgfHwgXHJcbiAgICAgICAgcmVzdWx0LmNhdGVnb3J5TmFtZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKHNlYXJjaFZhbHVlKSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyBvbkNsZWFyKGFyZ3MpIHtcclxuICAgICAgICBsZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgICAgICBzZWFyY2hCYXIuZGlzbWlzc1NvZnRJbnB1dCgpO1xyXG4gICAgICAgIHNlYXJjaEJhci50ZXh0ID0gXCJcIjtcclxuICAgICAgICBpZihpc0FuZHJvaWQpeyAgICAgICAgXHJcbiAgICAgICAgICAgIHNlYXJjaEJhci5hbmRyb2lkLmNsZWFyRm9jdXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoQmFyTG9hZGVkKGFyZ3Mpe1xyXG4gICAgICAgIGxldCBzZWFyY2hCYXIgPSA8U2VhcmNoQmFyPmFyZ3Mub2JqZWN0O1xyXG4gICAgICAgIGlmKGlzQW5kcm9pZCl7ICAgICAgICBcclxuICAgICAgICAgICAgc2VhcmNoQmFyLmFuZHJvaWQuY2xlYXJGb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn0iXX0=