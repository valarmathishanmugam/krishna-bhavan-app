"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var ProductDetail = (function () {
    function ProductDetail(page, location, routerExtensions) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.title = "Product Detail";
        this.description = "Ready to fry Items, very crispy & tasty,\n     traditionally made. Easy to fry on a daily basis Can be fried in Micro oven.";
    }
    ProductDetail.prototype.ngOnInit = function () {
        var _this = this;
        this.productDetails = ({ name: "Krishna's Custard Powder", category: "Desserts", img: "~/Images/product.png", offer: "Buy 1 Get 2 Free" });
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    return ProductDetail;
}());
ProductDetail = __decorate([
    core_1.Component({
        selector: "product-detail",
        moduleId: module.id,
        templateUrl: "productdetail.component.html",
        styleUrls: ["productdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions])
], ProductDetail);
exports.ProductDetail = ProductDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdGRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9kdWN0ZGV0YWlsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUUxQyxzREFBK0Q7QUFDL0Qsc0RBQXFEO0FBQ3JELDJDQUFzRjtBQUN0Rix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBUzNDLElBQWEsYUFBYTtJQU9yQix1QkFBb0IsSUFBVSxFQUFVLFFBQWtCLEVBQVUsZ0JBQWtDO1FBQWxGLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUx2RyxVQUFLLEdBQVcsZ0JBQWdCLENBQUM7UUFFakMsZ0JBQVcsR0FBVyw2SEFDd0QsQ0FBQztJQUUwQixDQUFDO0lBRTFHLGdDQUFRLEdBQVI7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFDLElBQUksRUFBQywwQkFBMEIsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBQyxzQkFBc0IsRUFBRSxLQUFLLEVBQUMsa0JBQWtCLEVBQUMsQ0FBQyxDQUFBO1FBQ3JJLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDMUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFTCxvQkFBQztBQUFELENBQUMsQUFuQkQsSUFtQkM7QUFuQlksYUFBYTtJQVB6QixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFDLGdCQUFnQjtRQUN6QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFDLDhCQUE4QjtRQUMxQyxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztLQUM3QyxDQUFDO3FDQVM2QixXQUFJLEVBQW9CLGlCQUFRLEVBQTRCLHlCQUFnQjtHQVA5RixhQUFhLENBbUJ6QjtBQW5CWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IHByb21wdCwgaW5wdXRUeXBlIH0gZnJvbSBcInVpL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZSc7XHJcbmltcG9ydCB7IEFuZHJvaWRBcHBsaWNhdGlvbiwgQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEgfSBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0ICogYXMgYXBwbGljYXRpb24gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6XCJwcm9kdWN0LWRldGFpbFwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOlwicHJvZHVjdGRldGFpbC5jb21wb25lbnQuaHRtbFwiLFxyXG4gICAgc3R5bGVVcmxzOiBbXCJwcm9kdWN0ZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9kdWN0RGV0YWlse1xyXG5cclxuICAgIHRpdGxlOiBzdHJpbmcgPSBcIlByb2R1Y3QgRGV0YWlsXCI7XHJcbiAgICBwcm9kdWN0RGV0YWlsczogT2JqZWN0O1xyXG4gICAgZGVzY3JpcHRpb246IHN0cmluZyA9IGBSZWFkeSB0byBmcnkgSXRlbXMsIHZlcnkgY3Jpc3B5ICYgdGFzdHksXHJcbiAgICAgdHJhZGl0aW9uYWxseSBtYWRlLiBFYXN5IHRvIGZyeSBvbiBhIGRhaWx5IGJhc2lzIENhbiBiZSBmcmllZCBpbiBNaWNybyBvdmVuLmA7XHJcblxyXG4gICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyl7fVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0RGV0YWlscyA9ICh7bmFtZTpcIktyaXNobmEncyBDdXN0YXJkIFBvd2RlclwiLCBjYXRlZ29yeTogXCJEZXNzZXJ0c1wiLCBpbWc6XCJ+L0ltYWdlcy9wcm9kdWN0LnBuZ1wiLCBvZmZlcjpcIkJ1eSAxIEdldCAyIEZyZWVcIn0pXHJcbiAgICAgICAgYXBwbGljYXRpb24uYW5kcm9pZC5vbihBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50LCAoZGF0YTogQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEpID0+IHtcclxuICAgICAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgICAgICAgIC8vdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgICAgICAgIH0pOyAgXHJcbiAgICB9XHJcblxyXG59Il19