import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    selector: "dialog",
    moduleId: module.id,
    templateUrl: "dialog.component.html",
    styleUrls: ["dialog.component.css"]
})

export class Dialog {
    rack: string;
    order:string;
    quantity: Array<string> = [];

    constructor(private params: ModalDialogParams) {
        this.rack = params.context.rack;
        this.order = params.context.order;

    }

    public ok(result1: string, result2: string) {
        /* Commented BY Balaji
        if(result1 === "" || result2 === ""){
            alert("Enter Value!!!");
        }
        else if(result1 === "0" || result2 === "0"){
            alert("Enter Valid Value!!!");
        }

        else if(isNaN(Number(result1)) == true || isNaN(Number(result2)) == true){
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else{
            console.log("Res1: "+result1+"Res2: "+result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);     
        }   
        */

        if(result1 === "") {
            result1 = "0";
        }

        if(result2 === "" || result2 === "0"){
            alert("Enter Value!!!");
        }
        else if(isNaN(Number(result1)) == true || isNaN(Number(result2)) == true){
            console.log("Not a Number");
            alert("Enter Valid Value!!!");
        }
        else{
            console.log("Res1: "+result1+"Res2: "+result2);
            this.quantity.push(result1);
            this.quantity.push(result2);
            this.params.closeCallback(this.quantity);     
        }   
        

  }

  public close(result: string) {
    console.log("Cancelled");
    console.log("Res: "+ result);
    this.params.closeCallback(result);
  }
}