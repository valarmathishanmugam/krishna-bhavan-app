"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var getorder_service_1 = require("../../Services/getorder.service");
var login_service_1 = require("../../Services/login.service");
var OrderNotes = (function () {
    function OrderNotes(routerExtensions, page, getorderservice, route, loginservice) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.getorderservice = getorderservice;
        this.route = route;
        this.loginservice = loginservice;
        console.log("Notes Data1: " + this.orderid);
        this.getorderservice.getorderbyid(this.orderid)
            .subscribe(function (data) { return _this.getorderdetails(data.json()); });
    }
    OrderNotes.prototype.ngOnInit = function () {
        //var tempcustomer = this.loginservice.getScope();
        //   application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });
    };
    OrderNotes.prototype.getorderdetails = function (data) {
        console.log("Notes Data");
        if (data.notes) {
            console.log("Notes Length: " + data.length);
        }
    };
    return OrderNotes;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], OrderNotes.prototype, "orderid", void 0);
OrderNotes = __decorate([
    core_1.Component({
        selector: "order-notes",
        moduleId: module.id,
        templateUrl: "ordernotes.component.html",
        styleUrls: ["ordernotes.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        login_service_1.loginService])
], OrderNotes);
exports.OrderNotes = OrderNotes;
