"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var dialogshipping_component_1 = require("../../Dialog_shippingaddress/dialogshipping.component");
var dialog_component_1 = require("../../Dialog/dialog.component");
var product_service_1 = require("../../../Services/product.service");
var sendorder_service_1 = require("../../../Services/sendorder.service");
var product_model_1 = require("../../../Models/product.model");
var customer_model_1 = require("../../../Models/customer.model");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var dialogs_1 = require("ui/dialogs");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var dialogs = require("ui/dialogs");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
// import { Product } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';
var RepOrderReview = (function () {
    function RepOrderReview(routerExtensions, saveorderservice, page, savecustomer, sendorder, savetotal, saveproducts, modalService, viewContainerRef, savecustomerdetails, saveshipping, savenotes, loginservice, location) {
        this.routerExtensions = routerExtensions;
        this.saveorderservice = saveorderservice;
        this.page = page;
        this.savecustomer = savecustomer;
        this.sendorder = sendorder;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.savecustomerdetails = savecustomerdetails;
        this.saveshipping = saveshipping;
        this.savenotes = savenotes;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Order Review";
        //orderDetails: object;
        //address: object;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.editShipping = false;
        this.disableFAB = true;
        this.formattedData = [];
    }
    RepOrderReview.prototype.ngOnInit = function () {
        var _this = this;
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        this.products = this.saveorderservice.getScope();
        this.getproducts = this.saveproducts.getScope();
        //console.log(this.productId);
        console.log("Id: " + this.products[0]["product_id"] + "name: " + this.products[0]["product_name"] + "orderqty: " + this.products[0]["quantity"] + this.products[0]["line_total"]);
        this.customerId = this.savecustomer.getScope();
        this.customerdetails = this.savecustomerdetails.getScope();
        this.total_amount = this.savetotal.getScope();
        console.log("Customer Id: " + this.customerId);
        this.getcustomerdetails(this.customerdetails);
        this.getorder(this.products);
        var tempcustomer = this.loginservice.getScope();
        this.checkcustomer(tempcustomer);
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    RepOrderReview.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            this.ifcustomer = "visible";
        }
    };
    RepOrderReview.prototype.gotoshop = function (note, ship) {
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Is Your Shipping Address Correct?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.disableFAB = false;
                console.log("notes: " + note);
                _this.savenotes.setScope(note);
                _this.saveshipping.setScope(ship);
                _this.savecustomerdetails.setScope(_this.customer);
                _this.sendorder.placeorder();
            }
            else {
                return;
            }
        });
    };
    RepOrderReview.prototype.getcustomerdetails = function (customerdetails) {
        this.customer.shopname = customerdetails.shopname;
        this.customer.contactname = customerdetails.contactname;
        this.customer.contactnumber = customerdetails.contactnumber;
        this.address.area = customerdetails.address.area;
        this.address.city = customerdetails.address.city;
        this.address.door_no = customerdetails.address.door_no;
        this.address.pin = customerdetails.address.pin;
        this.address.street_name = customerdetails.address.street_name;
        this.address.district = customerdetails.address.district;
        this.address.state = customerdetails.address.state;
        this.customer.address = this.address;
    };
    RepOrderReview.prototype.addorder = function (customerid, productid) {
        console.log("product id in order review:" + productid);
        //this.productDetails.push({name:"Krishna's Custard Powder", id: productid, img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
    };
    RepOrderReview.prototype.getorder = function (products) {
        console.log("products.length: " + products.length);
        console.log(products);
        // commented by valar for hide discount - start
        // if(products[0].offer){
        //     products[0]["offer_quantity"] = products[0].offer.offer_quantity;
        //     products[0]["base_quantity"] = products[0].offer.base_quantity;
        //     products[0]["discount_product_name"]=products[0].offer.discount_product_name;
        // }
        // commented by valar for hide discount - end
        for (var i = 0; i < products.length; i++) {
            this.product = new product_model_1.Product();
            // this.product.offer = new Offer();  // commented by valar for hide discount
            this.product.product_id = products[i]["product_id"];
            this.product.sku_id = products[i]["sku_id"];
            this.product.product_name = products[i]["product_name"];
            this.product.line_total = products[i]["line_total"];
            this.product.rack_quantity = products[i]["rack_quantity"];
            this.product.quantity = products[i]["quantity"];
            // commented by valar for hide discount - start
            if (products[i]["discount_id"]) {
                this.product.discount_id = products[i]["discount_id"];
                //     this.product.offer.offer_quantity = products[i]["offer_quantity"]+"";
                //     this.product.offer.base_quantity = products[i]["base_quantity"];
                //     this.product.discount_product_id = products[i]["discount_product_id"];
                //     this.product.discount_product_quantity = products[i]["discount_product_quantity"];
                //     this.product.offer.discount_product_name = products[i]["discount_product_name"];
                //     console.log( this.product.discount_product_quantity);
            }
            // commented by valar for hide discount - start
            this.product.product_price = products[i]["product_price"];
            console.log("product property" + JSON.stringify(this.product));
            this.allproducts.push(this.product);
        }
    };
    RepOrderReview.prototype.editqty = function (id, index) {
        var _this = this;
        console.log("id: " + id + "index: " + index);
        console.log("id: " + JSON.stringify(this.allproducts));
        console.log("orderqty in editqty: " + this.allproducts[index]["quantity"]);
        var rackedit = this.allproducts[index]["rack_quantity"];
        var orderedit = this.allproducts[index]["quantity"];
        console.log("orderedit: " + orderedit);
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, id, index); });
    };
    RepOrderReview.prototype.setresult = function (quantity, productid, index) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            console.log("New Value");
            this.allproducts[index]["rack_quantity"] = rack;
            this.allproducts[index]["quantity"] = order;
            // commented by valar for hide discount - start
            // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
            // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
            // if(parseInt(order) >= baseQuantity ){
            //     var remainder = parseInt(order)%baseQuantity;
            //     var quotient = Math.floor(parseInt(order)/baseQuantity);
            //     var offerQuantity = (quotient * offer_Quantity);
            //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+"";       
            // }
            // commented by valar for hide discount - end
            this.changelinetotal(productid, index, order);
            this.changetotal();
        }
    };
    RepOrderReview.prototype.removeorder = function (id, index) {
        //this.allproducts.splice(index,1);
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            console.log("removeorder: " + result);
            if (result == true) {
                _this.allproducts.splice(index, 1);
                console.log("Order Length: " + _this.allproducts.length);
                _this.changetotal();
                if (_this.allproducts.length == 0) {
                    console.log("Array null");
                    _this.routerExtensions.navigate(["/productlist"], { clearHistory: true });
                    dialogs.alert({
                        title: "Empty",
                        message: "Shop Products",
                        okButtonText: "Ok"
                    }).then(function () {
                        console.log("Products");
                        //this.routerExtensions.navigate(["/productlist"]);
                    });
                }
                else {
                    return;
                }
            }
            else {
                return;
            }
        });
    };
    RepOrderReview.prototype.changelinetotal = function (productid, index, order) {
        for (var i = 0; i < this.getproducts.length; i++) {
            if (this.getproducts[i]["product_id"] == productid) {
                var tempprice = this.getproducts[i]["product_price"];
                console.log("tempprice: " + tempprice);
            }
        }
        this.templinetotal = tempprice * order;
        console.log("Line total in change total: " + this.allproducts[index]["line_total"]);
        this.allproducts[index]["line_total"] = this.templinetotal;
    };
    RepOrderReview.prototype.changetotal = function () {
        this.total_amount = 0;
        for (var i = 0; i < this.allproducts.length; i++) {
            console.log(i + ": " + this.allproducts[i]["product_id"]);
            var temptotal = this.allproducts[i]["line_total"];
            this.total_amount = temptotal + this.total_amount;
            console.log("totalamount: " + this.total_amount);
        }
        // commented by valar for hide discount - start
        // this.formattedData = [];
        // for(let j=0;j<this.allproducts.length;j++){
        //     if(this.allproducts[j].discount_id){
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price,
        //             'discount_id': this.allproducts[j].discount_id,
        //             'discount_product_quantity': this.allproducts[j].discount_product_quantity
        //         }); 
        //     }
        //     else {
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price
        //         });   
        //     }     
        // }
        // console.log("all product datas=====>"+JSON.stringify(this.formattedData));
        // commented by valar for hide discount - end
        this.saveorderservice.setScope(this.allproducts);
        this.savetotal.setScope(this.total_amount);
    };
    RepOrderReview.prototype.editshippingaddress = function () {
        var _this = this;
        var doornoedit = this.address.door_no;
        var areaedit = this.address.area;
        var cityedit = this.address.city;
        var pinedit = this.address.pin;
        var streetnameedit = this.address.street_name;
        var districtedit = this.address.district;
        var stateedit = this.address.state;
        console.log("state: " + stateedit);
        var options = {
            context: { doorno: doornoedit, streetname: streetnameedit, area: areaedit, city: cityedit, district: districtedit,
                state: stateedit, pin: pinedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialogshipping_component_1.DialogShipping, options)
            .then(function (dialogResult) { return _this.editaddress(dialogResult); });
    };
    RepOrderReview.prototype.editaddress = function (address) {
        if (address == "null") {
            return;
        }
        else {
            this.address.door_no = address[0];
            this.address.street_name = address[1];
            this.address.area = address[2];
            this.address.city = address[3];
            this.address.district = address[4];
            this.address.state = address[5];
            this.address.pin = address[6];
            this.customer.address = this.address;
            this.savecustomerdetails.setScope(this.customer);
        }
    };
    return RepOrderReview;
}());
RepOrderReview = __decorate([
    core_1.Component({
        selector: "order-review",
        moduleId: module.id,
        templateUrl: "reporderreview.component.html",
        styleUrls: ["reporderreview.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, saveorder_service_1.SaveOrderService,
        page_1.Page, saveorder_service_1.SaveCustomer, sendorder_service_1.SendOrder,
        saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef,
        saveorder_service_1.SaveCustomerDetails, saveorder_service_1.SaveShipping,
        saveorder_service_1.SaveNotes, login_service_1.loginService, common_1.Location])
], RepOrderReview);
exports.RepOrderReview = RepOrderReview;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwb3JkZXJyZXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVwb3JkZXJyZXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUVBQStEO0FBQy9ELGtHQUF1RjtBQUN2RixrRUFBdUQ7QUFDdkQscUVBQWlFO0FBQ2pFLHlFQUFnRTtBQUNoRSwrREFBK0Q7QUFDL0QsaUVBQW1FO0FBQ25FLHlFQVE2QztBQUM3QyxzQ0FBbUY7QUFDbkYsc0RBQStEO0FBQy9ELHNEQUFxRDtBQUNyRCxzQ0FBd0Q7QUFHeEQsa0VBQTJGO0FBQzNGLG9DQUFzQztBQUN0QywyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQUMzQyx3R0FBd0c7QUFTeEcsSUFBYSxjQUFjO0lBd0J2Qix3QkFBb0IsZ0JBQWtDLEVBQVUsZ0JBQWtDLEVBQ3RGLElBQVUsRUFBVSxZQUEwQixFQUFVLFNBQW9CLEVBQzVFLFNBQW9CLEVBQVUsWUFBMEIsRUFDeEQsWUFBZ0MsRUFBVSxnQkFBa0MsRUFDNUUsbUJBQXdDLEVBQVUsWUFBMEIsRUFDNUUsU0FBb0IsRUFBVSxZQUEwQixFQUFVLFFBQWtCO1FBTDVFLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ3RGLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDNUUsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQ3hELGlCQUFZLEdBQVosWUFBWSxDQUFvQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDNUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzVFLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUEzQmhHLFVBQUssR0FBVyxjQUFjLENBQUM7UUFDL0IsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixhQUFRLEdBQUcsSUFBSSx5QkFBUSxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFHLElBQUksd0JBQU8sRUFBRSxDQUFDO1FBQ3hCLG1CQUFjLEdBQWtCLEVBQUUsQ0FBQztRQU81QixnQkFBVyxHQUFjLElBQUksS0FBSyxFQUFFLENBQUM7UUFFckMsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFJNUIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUVsQixrQkFBYSxHQUFVLEVBQUUsQ0FBQztJQU93RSxDQUFDO0lBRW5HLGlDQUFRLEdBQVI7UUFBQSxpQkEyQks7UUExQkQsNEpBQTRKO1FBQzVKLHdIQUF3SDtRQUN4SCxtSkFBbUo7UUFDbkosbUpBQW1KO1FBQ25KLG1KQUFtSjtRQUNuSixtSkFBbUo7UUFDbkosbUpBQW1KO1FBQ25KLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2pELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoRCw4QkFBOEI7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxRQUFRLEdBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsR0FBRyxZQUFZLEdBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDOUssSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBQy9DLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFakMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsZ0NBQWtCLENBQUMsd0JBQXdCLEVBQUUsVUFBQyxJQUF5QztZQUMxRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQix1QkFBdUI7WUFDdkIsdUJBQXVCO1lBQ3ZCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzdDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2QsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO1FBQ2hDLENBQUM7UUFDRCxJQUFJLENBQUEsQ0FBQztZQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFBO1FBQy9CLENBQUM7SUFDTCxDQUFDO0lBQ0wsaUNBQVEsR0FBUixVQUFTLElBQUksRUFBQyxJQUFJO1FBQWxCLGlCQW9CQztRQW5CRyxJQUFJLE9BQU8sR0FBRztZQUNWLEtBQUssRUFBRSxjQUFjO1lBQ3JCLE9BQU8sRUFBRSxtQ0FBbUM7WUFDNUMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSTtTQUN6QixDQUFDO1FBQ0YsaUJBQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFlO1lBQ2xDLEVBQUUsQ0FBQSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQSxDQUFDO2dCQUNmLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO2dCQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDOUIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlCLEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtnQkFDaEQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxDQUFDO1lBQ0QsSUFBSSxDQUFBLENBQUM7Z0JBQ0QsTUFBTSxDQUFDO1lBQ1gsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFrQixHQUFsQixVQUFtQixlQUFlO1FBRTlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUM7UUFDbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQztRQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsR0FBRyxlQUFlLENBQUMsYUFBYSxDQUFDO1FBQzVELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1FBQy9ELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ3pELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQ25ELElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7SUFFekMsQ0FBQztJQUVELGlDQUFRLEdBQVIsVUFBUyxVQUFVLEVBQUUsU0FBUztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixHQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3RELDhJQUE4STtJQUNsSixDQUFDO0lBRUQsaUNBQVEsR0FBUixVQUFTLFFBQVE7UUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3JCLCtDQUErQztRQUVoRCx5QkFBeUI7UUFDekIsd0VBQXdFO1FBQ3hFLHNFQUFzRTtRQUN0RSxvRkFBb0Y7UUFDcEYsSUFBSTtRQUVILDZDQUE2QztRQUM5QyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUVwQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksdUJBQU8sRUFBRSxDQUFDO1lBQzdCLDZFQUE2RTtZQUU3RSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMvQywrQ0FBK0M7WUFFL0MsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUMxRCw0RUFBNEU7Z0JBQzVFLHVFQUF1RTtnQkFDdkUsNkVBQTZFO2dCQUM3RSx5RkFBeUY7Z0JBQ3pGLHVGQUF1RjtnQkFFdkYsNERBQTREO1lBQzNELENBQUM7WUFFRCwrQ0FBK0M7WUFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBRTFELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQ0FBTyxHQUFQLFVBQVEsRUFBRSxFQUFDLEtBQUs7UUFBaEIsaUJBaUJDO1FBZkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUUsRUFBRSxHQUFHLFNBQVMsR0FBRSxLQUFLLENBQUUsQ0FBQztRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzFFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDeEQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUUsQ0FBQztRQUV4QyxJQUFJLE9BQU8sR0FBdUI7WUFDOUIsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFDO1lBQzVDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDMUMsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLHlCQUFNLEVBQUUsT0FBTyxDQUFDO2FBQzNDLElBQUksQ0FBQyxVQUFDLFlBQTJCLElBQUssT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsS0FBSyxDQUFDLEVBQXJDLENBQXFDLENBQUMsQ0FBQztJQUVsRixDQUFDO0lBRU0sa0NBQVMsR0FBaEIsVUFBaUIsUUFBUSxFQUFDLFNBQVMsRUFBQyxLQUFLO1FBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRWxDLEVBQUUsQ0FBQSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQ25CLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUEsQ0FBQztZQUNMLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsR0FBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUUxRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXpCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzNDLCtDQUErQztZQUVoRCw0RUFBNEU7WUFDNUUsK0VBQStFO1lBQy9FLHdDQUF3QztZQUN4QyxvREFBb0Q7WUFDcEQsK0RBQStEO1lBQy9ELHVEQUF1RDtZQUN2RCxzRkFBc0Y7WUFDdEYsSUFBSTtZQUVILDZDQUE2QztZQUM5QyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBQyxLQUFLLEVBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXZCLENBQUM7SUFDTCxDQUFDO0lBRUcsb0NBQVcsR0FBWCxVQUFZLEVBQUUsRUFBQyxLQUFLO1FBQ2hCLG1DQUFtQztRQUR2QyxpQkFxQ0M7UUFsQ0csSUFBSSxPQUFPLEdBQUc7WUFDVixLQUFLLEVBQUUsY0FBYztZQUNyQixPQUFPLEVBQUUsdUJBQXVCO1lBQ2hDLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDekIsQ0FBQztRQUVGLGlCQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBZTtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsQ0FBQztZQUN0QyxFQUFFLENBQUEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDZixLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEQsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNuQixFQUFFLENBQUEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO29CQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUMxQixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFFekUsT0FBTyxDQUFDLEtBQUssQ0FBQzt3QkFDVixLQUFLLEVBQUUsT0FBTzt3QkFDZCxPQUFPLEVBQUUsZUFBZTt3QkFDeEIsWUFBWSxFQUFFLElBQUk7cUJBQ3JCLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDeEIsbURBQW1EO29CQUN2RCxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQUNELElBQUksQ0FBQSxDQUFDO29CQUNELE1BQU0sQ0FBQztnQkFDWCxDQUFDO1lBQ0wsQ0FBQztZQUNELElBQUksQ0FBQSxDQUFDO2dCQUNELE1BQU0sQ0FBQztZQUNYLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFJRCx3Q0FBZSxHQUFmLFVBQWdCLFNBQVMsRUFBQyxLQUFLLEVBQUMsS0FBSztRQUVqQyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFHLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDL0MsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQSxDQUFDO2dCQUMvQyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxDQUFDO1FBQ0wsQ0FBQztRQUVELElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQTtRQUNuRixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7SUFFL0QsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztRQUN0QixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFHLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUN6RCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3JELENBQUM7UUFDQSwrQ0FBK0M7UUFFaEQsMkJBQTJCO1FBQzNCLDhDQUE4QztRQUM5QywyQ0FBMkM7UUFDM0Msb0NBQW9DO1FBQ3BDLDJEQUEyRDtRQUMzRCxvREFBb0Q7UUFDcEQsZ0VBQWdFO1FBQ2hFLDREQUE0RDtRQUM1RCxrRUFBa0U7UUFDbEUsd0RBQXdEO1FBQ3hELGtFQUFrRTtRQUNsRSw4REFBOEQ7UUFDOUQseUZBQXlGO1FBQ3pGLGVBQWU7UUFDZixRQUFRO1FBQ1IsYUFBYTtRQUNiLG9DQUFvQztRQUNwQywyREFBMkQ7UUFDM0Qsb0RBQW9EO1FBQ3BELGdFQUFnRTtRQUNoRSw0REFBNEQ7UUFDNUQsa0VBQWtFO1FBQ2xFLHdEQUF3RDtRQUN4RCxpRUFBaUU7UUFFakUsaUJBQWlCO1FBRWpCLGFBQWE7UUFDYixJQUFJO1FBQ0osNkVBQTZFO1FBRTVFLDZDQUE2QztRQUU5QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDRDQUFtQixHQUFuQjtRQUFBLGlCQW1CQztRQWpCRyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUN0QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUNqQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUNqQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQztRQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUM5QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUN6QyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUUsQ0FBQztRQUVwQyxJQUFJLE9BQU8sR0FBdUI7WUFDOUIsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsY0FBYyxFQUFFLElBQUksRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUMsWUFBWTtnQkFDeEcsS0FBSyxFQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUMsT0FBTyxFQUFFO1lBQ25DLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDMUMsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLHlDQUFjLEVBQUUsT0FBTyxDQUFDO2FBQ25ELElBQUksQ0FBQyxVQUFDLFlBQTJCLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUE5QixDQUE4QixDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxPQUFPO1FBRWYsRUFBRSxDQUFBLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxDQUFBLENBQUM7WUFDbEIsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQUksQ0FBQSxDQUFDO1lBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDckMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckQsQ0FBQztJQUVMLENBQUM7SUFDTCxxQkFBQztBQUFELENBQUMsQUE5VkQsSUE4VkM7QUE5VlksY0FBYztJQVAxQixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGNBQWM7UUFDeEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSwrQkFBK0I7UUFDNUMsU0FBUyxFQUFFLENBQUMsOEJBQThCLENBQUM7S0FDOUMsQ0FBQztxQ0EwQndDLHlCQUFnQixFQUE0QixvQ0FBZ0I7UUFDaEYsV0FBSSxFQUF3QixnQ0FBWSxFQUFxQiw2QkFBUztRQUNqRSw2QkFBUyxFQUF3Qiw4QkFBWTtRQUMxQyxpQ0FBa0IsRUFBNEIsdUJBQWdCO1FBQ3ZELHVDQUFtQixFQUF3QixnQ0FBWTtRQUNqRSw2QkFBUyxFQUF3Qiw0QkFBWSxFQUFvQixpQkFBUTtHQTdCdkYsY0FBYyxDQThWMUI7QUE5Vlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9TZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGlhbG9nU2hpcHBpbmcgfSBmcm9tICcuLi8uLi9EaWFsb2dfc2hpcHBpbmdhZGRyZXNzL2RpYWxvZ3NoaXBwaW5nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERpYWxvZyB9IGZyb20gJy4uLy4uL0RpYWxvZy9kaWFsb2cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2F2ZVByb2R1Y3RzIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvcHJvZHVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VuZE9yZGVyIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvc2VuZG9yZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPZmZlciwgUHJvZHVjdCB9IGZyb20gJy4uLy4uLy4uL01vZGVscy9wcm9kdWN0Lm1vZGVsJztcclxuaW1wb3J0IHsgQ3VzdG9tZXIsIEFkZHJlc3MgfSBmcm9tICcuLi8uLi8uLi9Nb2RlbHMvY3VzdG9tZXIubW9kZWwnO1xyXG5pbXBvcnQge1xyXG4gICAgU2F2ZUN1c3RvbWVyLFxyXG4gICAgU2F2ZUN1c3RvbWVyRGV0YWlscyxcclxuICAgIFNhdmVOb3RlcyxcclxuICAgIFNhdmVPcmRlclNlcnZpY2UsXHJcbiAgICBTYXZlU2hpcHBpbmcsXHJcbiAgICBTYXZlVG90YWxcclxuICAgIFxyXG59IGZyb20gJy4uLy4uLy4uL1NlcnZpY2VzL3NhdmVvcmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBBZnRlclZpZXdJbml0LCBWaWV3Q29udGFpbmVyUmVmLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZSc7XHJcbmltcG9ydCB7IHByb21wdCwgaW5wdXRUeXBlLCBjb25maXJtIH0gZnJvbSBcInVpL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgUGxhY2Vob2xkZXIgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL3VpL3BsYWNlaG9sZGVyJztcclxuaW1wb3J0IHsgT3JkZXIgfSBmcm9tICcuLi8uLi8uLi9Nb2RlbHMvb3JkZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UsIE1vZGFsRGlhbG9nT3B0aW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2dcIjtcclxuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tICd1aS9kaWFsb2dzJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tICdhcHBsaWNhdGlvbic7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuLy8gaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJy4uLy4uLy4uLy4uL3BsYXRmb3Jtcy9hbmRyb2lkL2FwcC9zcmMvbWFpbi9hc3NldHMvYXBwL01vZGVscy9wcm9kdWN0Lm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwib3JkZXItcmV2aWV3XCIsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6IFwicmVwb3JkZXJyZXZpZXcuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wicmVwb3JkZXJyZXZpZXcuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFJlcE9yZGVyUmV2aWV3IGltcGxlbWVudHMgT25Jbml0e1xyXG5cclxuICAgIHRpdGxlOiBzdHJpbmcgPSBcIk9yZGVyIFJldmlld1wiO1xyXG4gICAgLy9vcmRlckRldGFpbHM6IG9iamVjdDtcclxuICAgIC8vYWRkcmVzczogb2JqZWN0O1xyXG4gICAgY3VzdG9tZXIgPSBuZXcgQ3VzdG9tZXIoKTtcclxuICAgIGFkZHJlc3MgPSBuZXcgQWRkcmVzcygpO1xyXG4gICAgcHJvZHVjdERldGFpbHM6IEFycmF5PE9iamVjdD4gPSBbXTtcclxuICAgIGN1c3RvbWVySWQ6IGFueTtcclxuICAgIGN1c3RvbWVyZGV0YWlsczogYW55O1xyXG4gICAgcHJvZHVjdHM6IGFueTtcclxuICAgIGdldHByb2R1Y3RzOiBhbnk7XHJcbiAgICBwdWJsaWMgcHJvZHVjdDogUHJvZHVjdDtcclxuICAgIHB1YmxpYyBvcmRlcjogT3JkZXI7XHJcbiAgICBwdWJsaWMgYWxscHJvZHVjdHM6IFByb2R1Y3RbXSA9IG5ldyBBcnJheSgpO1xyXG4gICAgb3JkZXJyZW1vdmVkOiBhbnk7XHJcbiAgICBwdWJsaWMgZWRpdFNoaXBwaW5nID0gZmFsc2U7XHJcbiAgICB0b3RhbF9hbW91bnQ6IGFueTtcclxuICAgIHRlbXBsaW5ldG90YWw6IGFueTtcclxuICAgIHNoaXBwaW5nbW9kZTogYW55O1xyXG4gICAgZGlzYWJsZUZBQiA9IHRydWU7XHJcbiAgICBwdWJsaWMgaWZjdXN0b21lcjtcclxuICAgIGZvcm1hdHRlZERhdGE6YW55IFtdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHNhdmVvcmRlcnNlcnZpY2U6IFNhdmVPcmRlclNlcnZpY2UsIFxyXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBzYXZlY3VzdG9tZXI6IFNhdmVDdXN0b21lciwgcHJpdmF0ZSBzZW5kb3JkZXI6IFNlbmRPcmRlciwgXHJcbiAgICAgICAgcHJpdmF0ZSBzYXZldG90YWw6IFNhdmVUb3RhbCwgcHJpdmF0ZSBzYXZlcHJvZHVjdHM6IFNhdmVQcm9kdWN0cyxcclxuICAgICAgICBwcml2YXRlIG1vZGFsU2VydmljZTogTW9kYWxEaWFsb2dTZXJ2aWNlLCBwcml2YXRlIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSBzYXZlY3VzdG9tZXJkZXRhaWxzOiBTYXZlQ3VzdG9tZXJEZXRhaWxzLCBwcml2YXRlIHNhdmVzaGlwcGluZzogU2F2ZVNoaXBwaW5nLCBcclxuICAgICAgICBwcml2YXRlIHNhdmVub3RlczogU2F2ZU5vdGVzLCBwcml2YXRlIGxvZ2luc2VydmljZTogbG9naW5TZXJ2aWNlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbil7fVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgLy90aGlzLm9yZGVyRGV0YWlscyA9ICh7aWQ6XCJhc2gzNDI4Mzk4MDkzOFwiLCBzaG9wOlwiS2FubmlnYSBQYXJhbWVzaHdhcmkgU3RvcmVzXCIsIGltZzpcIn4vSW1hZ2VzL2dyZWVuX2RvdC5wbmdcIiwgZGF0ZTpcIjgvMi8xOFwiLCBzaGlwcGluZ21vZGU6XCJLLlAuTiBUcmF2ZWxzXCJ9KVxyXG4gICAgICAgIC8vdGhpcy5hZGRyZXNzID0gKHthcmVhOiBcIk5vLjEyMywgb3BwLiBFbGRhbSdzIFJvYWQsIEFubmFTYWxhaVwiLCBjaXR5OlwiQ2hlbm5haVwiLCBwaW5jb2RlOlwiNjExMTExXCIsIG1vYmlsZTpcIjk4NzQ1NjMyMTBcIn0pXHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgICAgdGhpcy5wcm9kdWN0cyA9IHRoaXMuc2F2ZW9yZGVyc2VydmljZS5nZXRTY29wZSgpO1xyXG4gICAgICAgIHRoaXMuZ2V0cHJvZHVjdHMgPSB0aGlzLnNhdmVwcm9kdWN0cy5nZXRTY29wZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2codGhpcy5wcm9kdWN0SWQpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIklkOiBcIisgdGhpcy5wcm9kdWN0c1swXVtcInByb2R1Y3RfaWRcIl0gKyBcIm5hbWU6IFwiKyB0aGlzLnByb2R1Y3RzWzBdW1wicHJvZHVjdF9uYW1lXCJdICsgXCJvcmRlcnF0eTogXCIrIHRoaXMucHJvZHVjdHNbMF1bXCJxdWFudGl0eVwiXSsgdGhpcy5wcm9kdWN0c1swXVtcImxpbmVfdG90YWxcIl0pO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbWVySWQgPSB0aGlzLnNhdmVjdXN0b21lci5nZXRTY29wZSgpO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbWVyZGV0YWlscyA9IHRoaXMuc2F2ZWN1c3RvbWVyZGV0YWlscy5nZXRTY29wZSgpO1xyXG4gICAgICAgICAgICB0aGlzLnRvdGFsX2Ftb3VudCA9IHRoaXMuc2F2ZXRvdGFsLmdldFNjb3BlKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ3VzdG9tZXIgSWQ6IFwiICsgdGhpcy5jdXN0b21lcklkKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRjdXN0b21lcmRldGFpbHModGhpcy5jdXN0b21lcmRldGFpbHMpO1xyXG4gICAgICAgICAgICB0aGlzLmdldG9yZGVyKHRoaXMucHJvZHVjdHMpO1xyXG4gICAgICAgICAgICB2YXIgdGVtcGN1c3RvbWVyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKVxyXG4gICAgICAgICAgICB0aGlzLmNoZWNrY3VzdG9tZXIodGVtcGN1c3RvbWVyKTtcclxuXHJcbiAgICAgICAgICAgIGFwcGxpY2F0aW9uLmFuZHJvaWQub24oQW5kcm9pZEFwcGxpY2F0aW9uLmFjdGl2aXR5QmFja1ByZXNzZWRFdmVudCwgKGRhdGE6IEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgICAgICAgICAgICAvL3RoaXMubG9jYXRpb24uYmFjaygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgIGNoZWNrY3VzdG9tZXIoZGF0YSl7XHJcbiAgICAgICAgICAgIGlmKGRhdGEucm9sZSA9PT0gJ2N1c3RvbWVyJyl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlmY3VzdG9tZXIgPSBcImNvbGxhcHNlXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pZmN1c3RvbWVyID0gXCJ2aXNpYmxlXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIGdvdG9zaG9wKG5vdGUsc2hpcCl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkFyZSBZb3UgU3VyZVwiLFxyXG4gICAgICAgICAgICBtZXNzYWdlOiBcIklzIFlvdXIgU2hpcHBpbmcgQWRkcmVzcyBDb3JyZWN0P1wiLFxyXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiWWVzXCIsXHJcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiTm9cIlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uZmlybShvcHRpb25zKS50aGVuKChyZXN1bHQ6IGJvb2xlYW4pID0+IHsgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihyZXN1bHQgPT0gdHJ1ZSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVGQUIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwibm90ZXM6IFwiICsgbm90ZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNhdmVub3Rlcy5zZXRTY29wZShub3RlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2F2ZXNoaXBwaW5nLnNldFNjb3BlKHNoaXApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zYXZlY3VzdG9tZXJkZXRhaWxzLnNldFNjb3BlKHRoaXMuY3VzdG9tZXIpXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbmRvcmRlci5wbGFjZW9yZGVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pOyAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Y3VzdG9tZXJkZXRhaWxzKGN1c3RvbWVyZGV0YWlscyl7XHJcblxyXG4gICAgICAgIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUgPSBjdXN0b21lcmRldGFpbHMuc2hvcG5hbWU7XHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IGN1c3RvbWVyZGV0YWlscy5jb250YWN0bmFtZTtcclxuICAgICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RudW1iZXIgPSBjdXN0b21lcmRldGFpbHMuY29udGFjdG51bWJlcjtcclxuICAgICAgICB0aGlzLmFkZHJlc3MuYXJlYSA9IGN1c3RvbWVyZGV0YWlscy5hZGRyZXNzLmFyZWE7XHJcbiAgICAgICAgdGhpcy5hZGRyZXNzLmNpdHkgPSBjdXN0b21lcmRldGFpbHMuYWRkcmVzcy5jaXR5O1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5kb29yX25vID0gY3VzdG9tZXJkZXRhaWxzLmFkZHJlc3MuZG9vcl9ubztcclxuICAgICAgICB0aGlzLmFkZHJlc3MucGluID0gY3VzdG9tZXJkZXRhaWxzLmFkZHJlc3MucGluO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5zdHJlZXRfbmFtZSA9IGN1c3RvbWVyZGV0YWlscy5hZGRyZXNzLnN0cmVldF9uYW1lO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IGN1c3RvbWVyZGV0YWlscy5hZGRyZXNzLmRpc3RyaWN0O1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGN1c3RvbWVyZGV0YWlscy5hZGRyZXNzLnN0YXRlO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IHRoaXMuYWRkcmVzczsgICAgICAgIFxyXG5cclxuICAgIH1cclxuXHJcbiAgICBhZGRvcmRlcihjdXN0b21lcmlkLCBwcm9kdWN0aWQpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwicHJvZHVjdCBpZCBpbiBvcmRlciByZXZpZXc6XCIrIHByb2R1Y3RpZCk7XHJcbiAgICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IHByb2R1Y3RpZCwgaW1nOlwicmVzOi8vc3RvcmVcIiwgb2ZmZXI6XCJCdXkgMSBHZXQgMiBGcmVlXCIsIHJhY2s6XCI1XCIsIG9yZGVyOlwiNVwifSlcclxuICAgIH1cclxuXHJcbiAgICBnZXRvcmRlcihwcm9kdWN0cyl7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJwcm9kdWN0cy5sZW5ndGg6IFwiKyBwcm9kdWN0cy5sZW5ndGgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHByb2R1Y3RzKTtcclxuICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gc3RhcnRcclxuXHJcbiAgICAgICAgLy8gaWYocHJvZHVjdHNbMF0ub2ZmZXIpe1xyXG4gICAgICAgIC8vICAgICBwcm9kdWN0c1swXVtcIm9mZmVyX3F1YW50aXR5XCJdID0gcHJvZHVjdHNbMF0ub2ZmZXIub2ZmZXJfcXVhbnRpdHk7XHJcbiAgICAgICAgLy8gICAgIHByb2R1Y3RzWzBdW1wiYmFzZV9xdWFudGl0eVwiXSA9IHByb2R1Y3RzWzBdLm9mZmVyLmJhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgLy8gICAgIHByb2R1Y3RzWzBdW1wiZGlzY291bnRfcHJvZHVjdF9uYW1lXCJdPXByb2R1Y3RzWzBdLm9mZmVyLmRpc2NvdW50X3Byb2R1Y3RfbmFtZTtcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBlbmRcclxuICAgICAgICBmb3IobGV0IGk9MDsgaSA8IHByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3QgPSBuZXcgUHJvZHVjdCgpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnByb2R1Y3Qub2ZmZXIgPSBuZXcgT2ZmZXIoKTsgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudFxyXG5cclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0LnByb2R1Y3RfaWQgPSBwcm9kdWN0c1tpXVtcInByb2R1Y3RfaWRcIl07XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdC5za3VfaWQgPSBwcm9kdWN0c1tpXVtcInNrdV9pZFwiXTtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0LnByb2R1Y3RfbmFtZSA9IHByb2R1Y3RzW2ldW1wicHJvZHVjdF9uYW1lXCJdOyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3QubGluZV90b3RhbCA9IHByb2R1Y3RzW2ldW1wibGluZV90b3RhbFwiXTtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0LnJhY2tfcXVhbnRpdHkgPSBwcm9kdWN0c1tpXVtcInJhY2tfcXVhbnRpdHlcIl07XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdC5xdWFudGl0eSA9IHByb2R1Y3RzW2ldW1wicXVhbnRpdHlcIl07XHJcbiAgICAgICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG5cclxuICAgICAgICAgICAgIGlmKHByb2R1Y3RzW2ldW1wiZGlzY291bnRfaWRcIl0pe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9kdWN0LmRpc2NvdW50X2lkID0gcHJvZHVjdHNbaV1bXCJkaXNjb3VudF9pZFwiXTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMucHJvZHVjdC5vZmZlci5vZmZlcl9xdWFudGl0eSA9IHByb2R1Y3RzW2ldW1wib2ZmZXJfcXVhbnRpdHlcIl0rXCJcIjtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMucHJvZHVjdC5vZmZlci5iYXNlX3F1YW50aXR5ID0gcHJvZHVjdHNbaV1bXCJiYXNlX3F1YW50aXR5XCJdO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5wcm9kdWN0LmRpc2NvdW50X3Byb2R1Y3RfaWQgPSBwcm9kdWN0c1tpXVtcImRpc2NvdW50X3Byb2R1Y3RfaWRcIl07XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLnByb2R1Y3QuZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eSA9IHByb2R1Y3RzW2ldW1wiZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eVwiXTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMucHJvZHVjdC5vZmZlci5kaXNjb3VudF9wcm9kdWN0X25hbWUgPSBwcm9kdWN0c1tpXVtcImRpc2NvdW50X3Byb2R1Y3RfbmFtZVwiXTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvLyAgICAgY29uc29sZS5sb2coIHRoaXMucHJvZHVjdC5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5KTtcclxuICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3QucHJvZHVjdF9wcmljZSA9IHByb2R1Y3RzW2ldW1wicHJvZHVjdF9wcmljZVwiXTtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJwcm9kdWN0IHByb3BlcnR5XCIrSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9kdWN0KSk7XHJcbiAgICAgICAgICAgIHRoaXMuYWxscHJvZHVjdHMucHVzaCh0aGlzLnByb2R1Y3QpO1xyXG4gICAgICAgIH0gXHJcbiAgICB9XHJcblxyXG4gICAgZWRpdHF0eShpZCxpbmRleCl7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiaWQ6IFwiICtpZCArIFwiaW5kZXg6IFwiICtpbmRleCApO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiaWQ6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5hbGxwcm9kdWN0cykpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwib3JkZXJxdHkgaW4gZWRpdHF0eTogXCIgK3RoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wicXVhbnRpdHlcIl0pO1xyXG4gICAgICAgIHZhciByYWNrZWRpdCA9IHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wicmFja19xdWFudGl0eVwiXTtcclxuICAgICAgICB2YXIgb3JkZXJlZGl0ID0gdGhpcy5hbGxwcm9kdWN0c1tpbmRleF1bXCJxdWFudGl0eVwiXTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIm9yZGVyZWRpdDogXCIgKyBvcmRlcmVkaXQgKTtcclxuXHJcbiAgICAgICAgbGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgY29udGV4dDogeyByYWNrOiByYWNrZWRpdCwgb3JkZXI6IG9yZGVyZWRpdH0sXHJcbiAgICAgICAgICAgIHZpZXdDb250YWluZXJSZWY6IHRoaXMudmlld0NvbnRhaW5lclJlZlxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMubW9kYWxTZXJ2aWNlLnNob3dNb2RhbChEaWFsb2csIG9wdGlvbnMpXHJcbiAgICAgICAgLnRoZW4oKGRpYWxvZ1Jlc3VsdDogQXJyYXk8c3RyaW5nPikgPT4gdGhpcy5zZXRyZXN1bHQoZGlhbG9nUmVzdWx0LGlkLGluZGV4KSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRyZXN1bHQocXVhbnRpdHkscHJvZHVjdGlkLGluZGV4KXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJlc3VsdDogXCIrIHF1YW50aXR5KTtcclxuXHJcbiAgICAgICAgaWYocXVhbnRpdHkgPT0gXCJudWxsXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgdmFyIHJhY2sgPSBxdWFudGl0eVswXTtcclxuICAgICAgICB2YXIgb3JkZXIgPSBxdWFudGl0eVsxXTtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJyZXMxOiBcIitxdWFudGl0eVswXSArIFwicmVzMjogXCIrIHF1YW50aXR5WzFdKTtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJOZXcgVmFsdWVcIik7ICAgICAgXHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5hbGxwcm9kdWN0c1tpbmRleF1bXCJyYWNrX3F1YW50aXR5XCJdID0gcmFjaztcclxuICAgICAgICB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcInF1YW50aXR5XCJdID0gb3JkZXI7XHJcbiAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIHN0YXJ0XHJcblxyXG4gICAgICAgIC8vIGxldCBiYXNlUXVhbnRpdHkgPSBwYXJzZUludCh0aGlzLmFsbHByb2R1Y3RzW2luZGV4XS5vZmZlci5iYXNlX3F1YW50aXR5KTtcclxuICAgICAgICAvLyBsZXQgb2ZmZXJfUXVhbnRpdHkgPSBwYXJzZUludCh0aGlzLmFsbHByb2R1Y3RzW2luZGV4XS5vZmZlci5vZmZlcl9xdWFudGl0eSk7XHJcbiAgICAgICAgLy8gaWYocGFyc2VJbnQob3JkZXIpID49IGJhc2VRdWFudGl0eSApe1xyXG4gICAgICAgIC8vICAgICB2YXIgcmVtYWluZGVyID0gcGFyc2VJbnQob3JkZXIpJWJhc2VRdWFudGl0eTtcclxuICAgICAgICAvLyAgICAgdmFyIHF1b3RpZW50ID0gTWF0aC5mbG9vcihwYXJzZUludChvcmRlcikvYmFzZVF1YW50aXR5KTtcclxuICAgICAgICAvLyAgICAgdmFyIG9mZmVyUXVhbnRpdHkgPSAocXVvdGllbnQgKiBvZmZlcl9RdWFudGl0eSk7XHJcbiAgICAgICAgLy8gICAgIHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wiZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eVwiXSA9IG9mZmVyUXVhbnRpdHkrXCJcIjsgICAgICAgXHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gZW5kXHJcbiAgICAgICAgdGhpcy5jaGFuZ2VsaW5ldG90YWwocHJvZHVjdGlkLGluZGV4LG9yZGVyKTtcclxuICAgICAgICB0aGlzLmNoYW5nZXRvdGFsKCk7XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG4gICAgcmVtb3Zlb3JkZXIoaWQsaW5kZXgpe1xyXG4gICAgICAgIC8vdGhpcy5hbGxwcm9kdWN0cy5zcGxpY2UoaW5kZXgsMSk7XHJcblxyXG4gICAgICAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJBcmUgWW91IFN1cmVcIixcclxuICAgICAgICAgICAgbWVzc2FnZTogXCJXYW50IHRvIHJlbW92ZSBvcmRlcj9cIixcclxuICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIlllc1wiLFxyXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5vXCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbmZpcm0ob3B0aW9ucykudGhlbigocmVzdWx0OiBib29sZWFuKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmVtb3Zlb3JkZXI6IFwiICsgcmVzdWx0KTtcclxuICAgICAgICAgICAgaWYocmVzdWx0ID09IHRydWUpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbGxwcm9kdWN0cy5zcGxpY2UoaW5kZXgsMSk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk9yZGVyIExlbmd0aDogXCIgKyB0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYW5nZXRvdGFsKCk7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aCA9PSAwKXtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkFycmF5IG51bGxcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcm9kdWN0bGlzdFwiXSwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgZGlhbG9ncy5hbGVydCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkVtcHR5XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiU2hvcCBQcm9kdWN0c1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT2tcIlxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlByb2R1Y3RzXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL3RoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJvZHVjdGxpc3RcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG5cclxuICAgIGNoYW5nZWxpbmV0b3RhbChwcm9kdWN0aWQsaW5kZXgsb3JkZXIpe1xyXG5cclxuICAgICAgICBmb3IodmFyIGk9MCA7IGkgPCB0aGlzLmdldHByb2R1Y3RzLmxlbmd0aCA7IGkrKyApIHtcclxuICAgICAgICAgICAgaWYodGhpcy5nZXRwcm9kdWN0c1tpXVtcInByb2R1Y3RfaWRcIl0gPT0gcHJvZHVjdGlkKXtcclxuICAgICAgICAgICAgICAgIHZhciB0ZW1wcHJpY2UgPSB0aGlzLmdldHByb2R1Y3RzW2ldW1wicHJvZHVjdF9wcmljZVwiXTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidGVtcHByaWNlOiBcIit0ZW1wcHJpY2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnRlbXBsaW5ldG90YWwgPSB0ZW1wcHJpY2UgKiBvcmRlcjtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxpbmUgdG90YWwgaW4gY2hhbmdlIHRvdGFsOiBcIiArIHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wibGluZV90b3RhbFwiXSlcclxuICAgICAgICB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcImxpbmVfdG90YWxcIl0gPSB0aGlzLnRlbXBsaW5ldG90YWw7ICAgICAgICBcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2V0b3RhbCgpe1xyXG4gICAgICAgIHRoaXMudG90YWxfYW1vdW50ID0gMDtcclxuICAgICAgICBmb3IodmFyIGk9MCA7IGkgPCB0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aCA7IGkrKyApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coaSArXCI6IFwiICsgdGhpcy5hbGxwcm9kdWN0c1tpXVtcInByb2R1Y3RfaWRcIl0pO1xyXG4gICAgICAgICAgICB2YXIgdGVtcHRvdGFsID0gdGhpcy5hbGxwcm9kdWN0c1tpXVtcImxpbmVfdG90YWxcIl07XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxfYW1vdW50ID0gdGVtcHRvdGFsICsgdGhpcy50b3RhbF9hbW91bnQ7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidG90YWxhbW91bnQ6IFwiICsgdGhpcy50b3RhbF9hbW91bnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gc3RhcnRcclxuXHJcbiAgICAgICAgLy8gdGhpcy5mb3JtYXR0ZWREYXRhID0gW107XHJcbiAgICAgICAgLy8gZm9yKGxldCBqPTA7ajx0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aDtqKyspe1xyXG4gICAgICAgIC8vICAgICBpZih0aGlzLmFsbHByb2R1Y3RzW2pdLmRpc2NvdW50X2lkKXtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuZm9ybWF0dGVkRGF0YS5wdXNoKHtcclxuICAgICAgICAvLyAgICAgICAgICAgICAncHJvZHVjdF9pZCc6dGhpcy5hbGxwcm9kdWN0c1tqXS5wcm9kdWN0X2lkLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICdza3VfaWQnOiB0aGlzLmFsbHByb2R1Y3RzW2pdLnNrdV9pZCxcclxuICAgICAgICAvLyAgICAgICAgICAgICAncHJvZHVjdF9uYW1lJzogdGhpcy5hbGxwcm9kdWN0c1tqXS5wcm9kdWN0X25hbWUsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ2xpbmVfdG90YWwnOiB0aGlzLmFsbHByb2R1Y3RzW2pdLmxpbmVfdG90YWwsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ3JhY2tfcXVhbnRpdHknOiB0aGlzLmFsbHByb2R1Y3RzW2pdLnJhY2tfcXVhbnRpdHksXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ3F1YW50aXR5JzogdGhpcy5hbGxwcm9kdWN0c1tqXS5xdWFudGl0eSxcclxuICAgICAgICAvLyAgICAgICAgICAgICAncHJvZHVjdF9wcmljZSc6IHRoaXMuYWxscHJvZHVjdHNbal0ucHJvZHVjdF9wcmljZSxcclxuICAgICAgICAvLyAgICAgICAgICAgICAnZGlzY291bnRfaWQnOiB0aGlzLmFsbHByb2R1Y3RzW2pdLmRpc2NvdW50X2lkLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICdkaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5JzogdGhpcy5hbGxwcm9kdWN0c1tqXS5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5XHJcbiAgICAgICAgLy8gICAgICAgICB9KTsgXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgZWxzZSB7XHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLmZvcm1hdHRlZERhdGEucHVzaCh7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ3Byb2R1Y3RfaWQnOnRoaXMuYWxscHJvZHVjdHNbal0ucHJvZHVjdF9pZCxcclxuICAgICAgICAvLyAgICAgICAgICAgICAnc2t1X2lkJzogdGhpcy5hbGxwcm9kdWN0c1tqXS5za3VfaWQsXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ3Byb2R1Y3RfbmFtZSc6IHRoaXMuYWxscHJvZHVjdHNbal0ucHJvZHVjdF9uYW1lLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICdsaW5lX3RvdGFsJzogdGhpcy5hbGxwcm9kdWN0c1tqXS5saW5lX3RvdGFsLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICdyYWNrX3F1YW50aXR5JzogdGhpcy5hbGxwcm9kdWN0c1tqXS5yYWNrX3F1YW50aXR5LFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICdxdWFudGl0eSc6IHRoaXMuYWxscHJvZHVjdHNbal0ucXVhbnRpdHksXHJcbiAgICAgICAgLy8gICAgICAgICAgICAgJ3Byb2R1Y3RfcHJpY2UnOiB0aGlzLmFsbHByb2R1Y3RzW2pdLnByb2R1Y3RfcHJpY2VcclxuICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIC8vICAgICAgICAgfSk7ICAgXHJcblxyXG4gICAgICAgIC8vICAgICB9ICAgICBcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJhbGwgcHJvZHVjdCBkYXRhcz09PT09PlwiK0pTT04uc3RyaW5naWZ5KHRoaXMuZm9ybWF0dGVkRGF0YSkpO1xyXG5cclxuICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gZW5kXHJcblxyXG4gICAgICAgIHRoaXMuc2F2ZW9yZGVyc2VydmljZS5zZXRTY29wZSh0aGlzLmFsbHByb2R1Y3RzKTtcclxuICAgICAgICB0aGlzLnNhdmV0b3RhbC5zZXRTY29wZSh0aGlzLnRvdGFsX2Ftb3VudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdHNoaXBwaW5nYWRkcmVzcygpeyAgICAgICAgXHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgdmFyIGRvb3Jub2VkaXQgPSB0aGlzLmFkZHJlc3MuZG9vcl9ubztcclxuICAgICAgICB2YXIgYXJlYWVkaXQgPSB0aGlzLmFkZHJlc3MuYXJlYTtcclxuICAgICAgICB2YXIgY2l0eWVkaXQgPSB0aGlzLmFkZHJlc3MuY2l0eTtcclxuICAgICAgICB2YXIgcGluZWRpdCA9IHRoaXMuYWRkcmVzcy5waW47XHJcbiAgICAgICAgdmFyIHN0cmVldG5hbWVlZGl0ID0gdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lO1xyXG4gICAgICAgIHZhciBkaXN0cmljdGVkaXQgPSB0aGlzLmFkZHJlc3MuZGlzdHJpY3Q7XHJcbiAgICAgICAgdmFyIHN0YXRlZWRpdCA9IHRoaXMuYWRkcmVzcy5zdGF0ZTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInN0YXRlOiBcIiArIHN0YXRlZWRpdCApO1xyXG5cclxuICAgICAgICBsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBjb250ZXh0OiB7IGRvb3JubzogZG9vcm5vZWRpdCwgc3RyZWV0bmFtZTpzdHJlZXRuYW1lZWRpdCwgYXJlYTphcmVhZWRpdCwgY2l0eTpjaXR5ZWRpdCwgZGlzdHJpY3Q6ZGlzdHJpY3RlZGl0LFxyXG4gICAgICAgICAgICAgICAgIHN0YXRlOnN0YXRlZWRpdCwgcGluOnBpbmVkaXQgfSxcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52aWV3Q29udGFpbmVyUmVmXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKERpYWxvZ1NoaXBwaW5nLCBvcHRpb25zKVxyXG4gICAgICAgIC50aGVuKChkaWFsb2dSZXN1bHQ6IEFycmF5PHN0cmluZz4pID0+IHRoaXMuZWRpdGFkZHJlc3MoZGlhbG9nUmVzdWx0KSk7XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdGFkZHJlc3MoYWRkcmVzcyl7XHJcblxyXG4gICAgICAgIGlmKGFkZHJlc3MgPT0gXCJudWxsXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5kb29yX25vID0gYWRkcmVzc1swXTtcclxuICAgICAgICAgICAgdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lID0gYWRkcmVzc1sxXTtcclxuICAgICAgICAgICAgdGhpcy5hZGRyZXNzLmFyZWEgPSBhZGRyZXNzWzJdO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MuY2l0eSA9IGFkZHJlc3NbM107XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IGFkZHJlc3NbNF07XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGFkZHJlc3NbNV07XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5waW4gPSBhZGRyZXNzWzZdOyBcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lci5hZGRyZXNzID0gdGhpcy5hZGRyZXNzO1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVjdXN0b21lcmRldGFpbHMuc2V0U2NvcGUodGhpcy5jdXN0b21lcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSJdfQ==