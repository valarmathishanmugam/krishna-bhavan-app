"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CustomerInfo = (function () {
    function CustomerInfo() {
    }
    CustomerInfo.prototype.ngOnInit = function () {
        console.log("InfoID" + this.customerid);
        this.address = ({ shop: "Kanniga Parameshwari Store", gst: "ABCD12345698", area: "No.123, opp. Eldam's Road, AnnaSalai", city: "Chennai", pincode: "611111", mobile: "9874563210" });
    };
    return CustomerInfo;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], CustomerInfo.prototype, "customerid", void 0);
CustomerInfo = __decorate([
    core_1.Component({
        selector: "customer-info",
        moduleId: module.id,
        templateUrl: "customerinfo.component.html",
        styleUrls: ["customerinfo.component.css"]
    })
], CustomerInfo);
exports.CustomerInfo = CustomerInfo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZXJpbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImN1c3RvbWVyaW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBaUQ7QUFRakQsSUFBYSxZQUFZO0lBQXpCO0lBWUEsQ0FBQztJQU5HLCtCQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUMsSUFBSSxFQUFDLDRCQUE0QixFQUFFLEdBQUcsRUFBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLHNDQUFzQyxFQUFFLElBQUksRUFBQyxTQUFTLEVBQUUsT0FBTyxFQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUMsWUFBWSxFQUFDLENBQUMsQ0FBQTtJQUVqTCxDQUFDO0lBRUwsbUJBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQztBQVRZO0lBQVIsWUFBSyxFQUFFOztnREFBWTtBQUhYLFlBQVk7SUFQeEIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsNkJBQTZCO1FBQzFDLFNBQVMsRUFBRSxDQUFDLDRCQUE0QixDQUFDO0tBQzVDLENBQUM7R0FFVyxZQUFZLENBWXhCO0FBWlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiY3VzdG9tZXItaW5mb1wiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcImN1c3RvbWVyaW5mby5jb21wb25lbnQuaHRtbFwiLFxyXG4gICAgc3R5bGVVcmxzOiBbXCJjdXN0b21lcmluZm8uY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbWVySW5mb3tcclxuXHJcbiAgICBhZGRyZXNzOiBvYmplY3Q7XHJcbiAgICBASW5wdXQoKSBjdXN0b21lcmlkO1xyXG4gICAgXHJcblxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkluZm9JRFwiKyB0aGlzLmN1c3RvbWVyaWQpO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcyA9ICh7c2hvcDpcIkthbm5pZ2EgUGFyYW1lc2h3YXJpIFN0b3JlXCIsIGdzdDpcIkFCQ0QxMjM0NTY5OFwiLCBhcmVhOiBcIk5vLjEyMywgb3BwLiBFbGRhbSdzIFJvYWQsIEFubmFTYWxhaVwiLCBjaXR5OlwiQ2hlbm5haVwiLCBwaW5jb2RlOlwiNjExMTExXCIsIG1vYmlsZTpcIjk4NzQ1NjMyMTBcIn0pXHJcbiAgICAgIFxyXG4gICAgfVxyXG5cclxufSJdfQ==