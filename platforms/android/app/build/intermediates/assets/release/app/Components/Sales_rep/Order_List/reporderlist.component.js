"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var getorder_service_1 = require("../../../Services/getorder.service");
var customer_model_1 = require("../../../Models/customer.model");
var order_model_1 = require("../../../Models/order.model");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var common_1 = require("@angular/common");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var RepOrderList = (function () {
    function RepOrderList(routerExtensions, getorder, page, loginservice, location) {
        this.routerExtensions = routerExtensions;
        this.getorder = getorder;
        this.page = page;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Order List";
        this.orderDetails = [];
        this.allorder = new Array();
        this.original = new Array();
        this.selectedIndex = null;
    }
    RepOrderList.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = false;
        console.log("In Order list customer id: " + this.id);
        //this.orderDetails.push({rep:"Ramasamy",id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
        this.getorder
            .getmanagerorders(this.id)
            .subscribe(function (data) { return _this.fetchallorders(data.json()); });
        var tempcustomer = this.loginservice.getScope();
        this.checkcustomer(tempcustomer);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });
        this.items = new nativescript_drop_down_1.ValueList();
        this.items.push({
            value: "all",
            display: "All Orders"
        }, {
            value: "not_reviewed",
            display: "Waiting For Approval"
        }, {
            value: "hold",
            display: "Hold Orders"
        }, {
            value: "approved",
            display: "Pending File"
        }, {
            value: "completed",
            display: "Dispatched Orders"
        });
    };
    RepOrderList.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            this.ifcustomer = "visible";
        }
    };
    RepOrderList.prototype.fetchallorders = function (data) {
        console.log("All manager orders" + data.length);
        for (var i = 0; i < data.length; i++) {
            this.order = new order_model_1.Order();
            this.order._id = data[i].id.$oid;
            this.order.order_id = data[i].order_id;
            //this.tempdate = data[i].order_date;
            //this.order.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
            this.order.order_date = data[i].date;
            this.order.total_amount = data[i].total_amount;
            this.customer = new customer_model_1.Customer();
            this.customer.shopname = data[i].shop_name;
            this.order.customer = this.customer;
            var tempstatus = data[i].status;
            if (tempstatus.toLowerCase() === "hold") {
                console.log("status: Hold");
                this.order.status = "~/Images/hold.png";
                this.order.approve = "visible";
            }
            else if (tempstatus.toLowerCase() === "pending") {
                console.log("status: not_reviewed");
                this.order.status = "~/Images/not_reviewed.png";
                this.order.approve = "visible";
            }
            else if (tempstatus.toLowerCase() === "processed") {
                console.log("status: completed");
                this.order.status = "~/Images/completed.png";
                this.order.approve = "hidden";
            }
            else if (tempstatus.toLowerCase() === "approved") {
                console.log("status: approved");
                this.order.status = "~/Images/approved.png";
                this.order.approve = "hidden";
            }
            this.allorder.push(this.order);
            console.log("all order lists" + this.allorder[0].order_id);
            this.original.push(this.order);
            this.allorder.reverse();
        }
    };
    RepOrderList.prototype.onchange = function (args) {
        console.log("Changed");
        console.log("Changed Value: " + this.items.getValue(args.newIndex));
        if (this.items.getValue(args.newIndex) === "all") {
            this.allorder = this.original;
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "not_reviewed") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/not_reviewed.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "hold") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/hold.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "approved") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/approved.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "completed") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/completed.png"); });
            this.allorder.reverse();
        }
    };
    RepOrderList.prototype.onopen = function () {
        console.log("Opened");
    };
    RepOrderList.prototype.onclose = function () {
        console.log("Closed");
    };
    return RepOrderList;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], RepOrderList.prototype, "id", void 0);
RepOrderList = __decorate([
    core_1.Component({
        selector: "order-list",
        moduleId: module.id,
        templateUrl: "reporderlist.component.html",
        styleUrls: ["reporderlist.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, getorder_service_1.GetOrder, page_1.Page,
        login_service_1.loginService, common_1.Location])
], RepOrderList);
exports.RepOrderList = RepOrderList;
