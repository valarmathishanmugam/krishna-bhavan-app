"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {
//     Response,
// } from '../../../../platforms/android/app/build/intermediates/assets/debug/app/tns_modules/@angular/http/src/static_response';
var orderapproval_service_1 = require("../../../Services/orderapproval.service");
var getorder_service_1 = require("../../../Services/getorder.service");
var dialogshipping_component_1 = require("../../Dialog_shippingaddress/dialogshipping.component");
var dialog_component_1 = require("../../Dialog/dialog.component");
var product_service_1 = require("../../../Services/product.service");
var sendorder_service_1 = require("../../../Services/sendorder.service");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var order_model_1 = require("../../../Models/order.model");
var product_model_1 = require("../../../Models/product.model");
var customer_model_1 = require("../../../Models/customer.model");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var dialogs_1 = require("ui/dialogs");
var dialogs = require("ui/dialogs");
var animation_1 = require("ui/animation");
var router_2 = require("@angular/router");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var saveorder_service_2 = require("../../../Services/saveorder.service");
var http_1 = require("@angular/http");
// import { Offer } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';
var ManagerOrderReview = (function () {
    function ManagerOrderReview(routerExtensions, saveorderservice, page, savecustomer, sendorder, savetotal, saveproducts, modalService, viewContainerRef, savecustomerdetails, saveshipping, savenotes, getorderservice, route, orderapproval, location, saveupdateorderid, saveupdateorder_id, http) {
        this.routerExtensions = routerExtensions;
        this.saveorderservice = saveorderservice;
        this.page = page;
        this.savecustomer = savecustomer;
        this.sendorder = sendorder;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.savecustomerdetails = savecustomerdetails;
        this.saveshipping = saveshipping;
        this.savenotes = savenotes;
        this.getorderservice = getorderservice;
        this.route = route;
        this.orderapproval = orderapproval;
        this.location = location;
        this.saveupdateorderid = saveupdateorderid;
        this.saveupdateorder_id = saveupdateorder_id;
        this.http = http;
        this.title = "Order Review";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.editShipping = false;
        this.isFabOpen = false;
        this.orderupdate_customerdetails = [];
        this.allnotes = new Array();
        this.disableHoldFab = true;
        this.disableApproveFab = true;
    }
    ManagerOrderReview.prototype.ngOnInit = function () {
        var _this = this;
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        this.customer.shopname = "ABC Departmental store";
        this.customer.contactname = "name";
        this.address.door_no = "7";
        this.address.street_name = "some street";
        this.address.area = "some area";
        this.address.city = "chennai";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.address.pin = 658990;
        var id = this.route.snapshot.params["id"];
        this.getorderservice
            .getorderbyid(id)
            .subscribe(function (data) { return _this.getallorders(data.json()); }, function (err) { return console.log("Get order by id error: " + err); });
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    ManagerOrderReview.prototype.getallorders = function (data) {
        console.log("All Data: " + JSON.stringify(data));
        console.log("products.length: " + data[0].line_items.length);
        //data = data._body;
        this.order_id = data[0]._id.$oid;
        this.formattedOrder_id = data[0].order_id;
        console.log("formatted order id" + this.formattedOrder_id);
        this.saveupdateorderid.setScope(this.order_id);
        this.saveupdateorder_id.setScope(data[0].order_id);
        this.order_date = data[0].order_date;
        //this.tempdate = data[0].order_date;
        //this.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
        var tempstatus = data[0].status;
        this.customer.shopname = data[0].shop_name;
        console.log("shopname: " + this.customer.shopname);
        this.customer.contactname = data[0].customer_name;
        this.address.shippingmode = data[0].preferred_shipping;
        this.address.area = data[0].shipping_address.area;
        this.address.city = data[0].shipping_address.city;
        this.address.door_no = data[0].shipping_address.door_no;
        this.address.pin = data[0].shipping_address.pin;
        this.address.street_name = data[0].shipping_address.street_name;
        this.address.district = data[0].shipping_address.district;
        this.address.state = data[0].shipping_address.state;
        this.customer.address = this.address;
        this.total_amount = Number(data[0].total_amount);
        console.log("CUSTOMER =====> " + JSON.stringify(this.customer));
        console.log("ADDRESS =====> " + JSON.stringify(this.address));
        if (data[0].notes) {
            //console.log("My Notes")
            //console.log(data[0].customer_name + " : " + data[0].notes.length)
            for (var a = 0; a < data[0].notes.length; a++) {
                this.notes = new order_model_1.Note();
                //console.log("My Notes: " +data[0].notes[a].note)
                this.notes.created_date = data[0].notes[a].created_date;
                this.notes.note = data[0].notes[a].note;
                this.notes.created_by = data[0].notes[a].created_by;
                this.allnotes.push(this.notes);
            }
            if (data[0].notes.length == 0) {
                if (data[0].current_note) {
                    this.notes = new order_model_1.Note();
                    console.log("Current Notes: " + data[0].current_note.note);
                    this.notes.created_date = data[0].current_note.created_date;
                    this.notes.note = data[0].current_note.note;
                    this.notes.created_by = data[0].current_note.created_by;
                    this.allnotes.push(this.notes);
                }
                else { }
            }
        }
        // else{}
        this.orderupdate_customerdetails.push({ orderdate: this.order_date, employeeid: data[0].employee_id.$oid,
            customerid: data[0].customer_id.$oid, customername: data[0].customer_name, shopname: data[0].shop_name,
            shippingaddress: data[0].shipping_address, billingaddress: data[0].billing_address, preferredshipping: data[0].preferred_shipping,
            totalamount: data[0].total_amount });
        console.log(JSON.stringify(this.orderupdate_customerdetails));
        for (var i = 0; i < data[0].line_items.length; i++) {
            this.product = new product_model_1.Product();
            // commented by valar for hide discount - start
            // this.product.offer = new Offer();
            // commented by valar for hide discount - end
            this.product.product_id = data[0].line_items[i].product_id.$oid;
            this.product.sku_id = data[0].line_items[i].sku_id.$oid;
            this.product.product_name = data[0].line_items[i].product_name;
            this.product.line_total = Number(data[0].line_items[i].line_total);
            this.product.rack_quantity = Number(data[0].line_items[i].rack_quantity);
            this.product.quantity = Number(data[0].line_items[i].quantity);
            console.log("PRODUCT: " + JSON.stringify(this.product));
            // commented by valar for hide discount - start
            // if(data[0].line_items[i].discount_id){            
            //   console.log("Has Discount")   
            //   var nodiscount = false;
            //   this.product.discount_id = data[0].line_items[i].discount_id.$oid;
            //   this.product.discount_product_quantity =  data[0].line_items[i].discount_product_quantity;
            //   this.product.offer.base_quantity = data[0].line_items[i].discount_details[0].base_quantity;
            //   this.product.offer.offer_quantity = data[0].line_items[i].discount_details[0].offer_quantity;
            //   this.product.offer.discount_product_name = data[0].line_items[i].discount_details[1].discount_product_name;
            // }
            // else{            
            //   console.log("Has No Discount")
            //   this.product.discount_id = "";
            //   var nodiscount = true;
            // }
            // commented by valar for hide discount - end
            this.product.product_price = Number(data[0].line_items[i].product_price);
            this.allproducts.push(this.product);
            // commented by valar for hide discount - start
            //   if(nodiscount === false){
            //     console.log("Inserting discount product..");
            //     this.productDetails.push({product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid, 
            //       quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name, 
            //       line_total: data[0].line_items[i].line_total, discount_id: data[0].line_items[i].discount_id.$oid, product_price: data[0].line_items[i].product_price,
            //     discount_product_quantity:data[0].line_items[i].discount_product_quantity});
            //   }
            // else if(nodiscount === true){
            // commented by valar for hide discount - end
            console.log("Inserting No Discount Product...");
            this.productDetails.push({ product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid,
                quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name,
                line_total: data[0].line_items[i].line_total, product_price: data[0].line_items[i].product_price });
            // } // commented by valar for hide discount
        }
        console.log("All Product details: " + JSON.stringify(this.productDetails));
        if (tempstatus.toLowerCase() === "hold") {
            console.log("status: Hold");
            this.order_status = "~/Images/hold.png";
        }
        else if (tempstatus.toLowerCase() === "pending") {
            console.log("status: not_reviewed");
            this.order_status = "~/Images/not_reviewed.png";
        }
        else if (tempstatus.toLowerCase() === "processed") {
            console.log("status: completed");
            this.order_status = "~/Images/completed.png";
        }
        else if (tempstatus.toLowerCase() === "approved") {
            console.log("status: approved");
            this.order_status = "~/Images/approved.png";
        }
    };
    ManagerOrderReview.prototype.editqty = function (id, index) {
        var _this = this;
        console.log("id: " + id + "index: " + index);
        console.log("id: " + JSON.stringify(this.allproducts));
        console.log("orderqty in editqty: " + this.allproducts[index]["quantity"]);
        var rackedit = this.allproducts[index]["rack_quantity"];
        var orderedit = this.allproducts[index]["quantity"];
        console.log("orderedit: " + orderedit);
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService
            .showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) {
            return _this.setresult(dialogResult, id, index);
        });
    };
    ManagerOrderReview.prototype.setresult = function (quantity, productid, index) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            console.log("New Value");
            this.changelinetotal(productid, index, order);
            //this.changetotal();
            this.allproducts[index]["rack_quantity"] = rack;
            this.allproducts[index]["quantity"] = order;
            this.productDetails[index]["rack_quantity"] = rack;
            this.productDetails[index]["quantity"] = order;
            // commented by valar for hide discount - start
            // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
            // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
            // if(parseInt(order) >= baseQuantity ){
            //     var remainder = parseInt(order)%baseQuantity;
            //     var quotient = Math.floor(parseInt(order)/baseQuantity);
            //     var offerQuantity = (quotient * offer_Quantity);
            //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+""; 
            //     this.productDetails[index]["discount_product_quantity"] = offerQuantity+""   
            // }
            // commented by valar for hide discount - end
        }
    };
    ManagerOrderReview.prototype.removeorder = function (id, index) {
        //this.allproducts.splice(index,1);
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            console.log("removeorder: " + result);
            if (result == true) {
                if (_this.allproducts.length > 1) {
                    _this.allproducts.splice(index, 1);
                    _this.productDetails.splice(index, 1);
                    console.log("Order Length: " + _this.allproducts.length);
                    _this.changetotal();
                }
                else {
                    console.log("products cannot be empty");
                    //this.routerExtensions.navigate(["/productlist"]);
                    dialogs
                        .alert({
                        title: "Deleting All!",
                        message: "Products Cannot be Empty",
                        okButtonText: "Ok"
                    });
                }
            }
            else {
                return;
            }
        });
    };
    ManagerOrderReview.prototype.changelinetotal = function (productid, index, order) {
        for (var i = 0; i < this.allproducts.length; i++) {
            if (this.allproducts[i]["product_id"] == productid) {
                var tempprice = Number(this.allproducts[i]["product_price"]);
                console.log("tempprice: " + tempprice);
            }
        }
        this.templinetotal = tempprice * order;
        console.log("Line total in change total: " + this.allproducts[index]["line_total"]);
        this.allproducts[index]["line_total"] = this.templinetotal;
        this.productDetails[index]["line_total"] = this.templinetotal;
        this.changetotal();
    };
    ManagerOrderReview.prototype.changetotal = function () {
        this.total_amount = 0;
        for (var i = 0; i < this.allproducts.length; i++) {
            console.log(i + ": " + this.allproducts[i]["product_id"]);
            var temptotal = this.allproducts[i]["line_total"];
            this.total_amount = temptotal + this.total_amount;
            console.log("totalamount: " + this.total_amount);
        }
        this.orderupdate_customerdetails[0]["totalamount"] = this.total_amount;
    };
    ManagerOrderReview.prototype.editshippingaddress = function () {
        var _this = this;
        var doornoedit = this.address.door_no;
        var areaedit = this.address.area;
        var cityedit = this.address.city;
        var pinedit = this.address.pin;
        var streetnameedit = this.address.street_name;
        var districtedit = this.address.district;
        var stateedit = this.address.state;
        console.log("state: " + stateedit);
        var options = {
            context: {
                doorno: doornoedit,
                streetname: streetnameedit,
                area: areaedit,
                city: cityedit,
                district: districtedit,
                state: stateedit,
                pin: pinedit
            },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService
            .showModal(dialogshipping_component_1.DialogShipping, options)
            .then(function (dialogResult) { return _this.editaddress(dialogResult); });
    };
    ManagerOrderReview.prototype.editaddress = function (address) {
        if (address == "null") {
            return;
        }
        else {
            this.address.door_no = address[0];
            this.orderupdate_customerdetails[0]["shippingaddress"]["door_no"] = address[0];
            this.address.street_name = address[1];
            this.orderupdate_customerdetails[0]["shippingaddress"]["street_name"] = address[1];
            this.address.area = address[2];
            this.orderupdate_customerdetails[0]["shippingaddress"]["area"] = address[2];
            this.address.city = address[3];
            this.orderupdate_customerdetails[0]["shippingaddress"]["city"] = address[3];
            this.address.district = address[4];
            this.orderupdate_customerdetails[0]["shippingaddress"]["district"] = address[4];
            this.address.state = address[5];
            this.orderupdate_customerdetails[0]["shippingaddress"]["state"] = address[5];
            this.address.pin = address[6];
            this.orderupdate_customerdetails[0]["shippingaddress"]["pin"] = address[6];
            this.customer.address = this.address;
        }
    };
    ManagerOrderReview.prototype.fabTap = function (args) {
        var _this = this;
        var fab = args.object;
        var definitions = new Array();
        var button = this.page.getViewById("dial1");
        var button2 = this.page.getViewById("dial2");
        if (this.isFabOpen == true) {
            var a1 = { target: fab, rotate: 0, duration: 400, delay: 0 };
            definitions.push(a1);
            var a2 = {
                target: button,
                translate: { x: 0, y: 0 },
                opacity: 0,
                duration: 400,
                delay: 0
            };
            definitions.push(a2);
            var a3 = {
                target: button2,
                translate: { x: 0, y: 0 },
                opacity: 0,
                duration: 440,
                delay: 0
            };
            definitions.push(a3);
            var animationSet = new animation_1.Animation(definitions);
            animationSet
                .play()
                .then(function () {
                //console.log("Animations finished");
                _this.isFabOpen = false;
            })
                .catch(function (e) {
                console.log(e.message);
            });
        }
        else {
            var a4 = { target: fab, rotate: 45, duration: 400, delay: 0 };
            definitions.push(a4);
            var a5 = {
                target: button,
                translate: { x: 0, y: -54 },
                opacity: 1,
                duration: 400,
                delay: 0
            };
            definitions.push(a5);
            var a6 = {
                target: button2,
                translate: { x: 0, y: -100 },
                opacity: 1,
                duration: 440,
                delay: 0
            };
            definitions.push(a6);
            var animationSet = new animation_1.Animation(definitions);
            animationSet
                .play()
                .then(function () {
                //console.log("Animations finished");
                _this.isFabOpen = true;
            })
                .catch(function (e) {
                console.log(e.message);
            });
        }
    };
    ManagerOrderReview.prototype.approveorder = function (shipmode) {
        var _this = this;
        var options = {
            title: "Approve Order",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.disableApproveFab = false;
                _this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
                _this.savecustomerdetails.setScope(_this.orderupdate_customerdetails);
                _this.saveorderservice.setScope(_this.productDetails);
                var newnote = _this.newnote.nativeElement;
                newnote = newnote.text;
                console.log("New note: " + newnote);
                _this.savenotes.setScope(newnote);
                console.log("Approve Order");
                _this.sendorder.updateorder();
                var approval = "approved";
                _this.orderapproval.approveorder(_this.order_id, approval, _this.route.snapshot.params["id"]);
                _this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
            }
            else {
                return;
            }
        });
    };
    ManagerOrderReview.prototype.holdorder = function (shipmode) {
        var _this = this;
        var options = {
            title: "Hold Order",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                console.log("Shipping Mode: " + shipmode);
                _this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
                _this.savecustomerdetails.setScope(_this.orderupdate_customerdetails);
                _this.saveorderservice.setScope(_this.productDetails);
                var newnote = _this.newnote.nativeElement;
                newnote = newnote.text;
                console.log("New note: " + newnote);
                if (newnote !== "") {
                    _this.savenotes.setScope(newnote);
                    console.log("Hold Order");
                    _this.sendorder.updateorder();
                    var approval = "hold";
                    _this.disableHoldFab = false;
                    _this.orderapproval.approveorder(_this.order_id, approval, _this.route.snapshot.params["id"]);
                    _this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
                }
                else {
                    alert("Enter notes");
                }
            }
            else {
                return;
            }
        });
    };
    return ManagerOrderReview;
}());
__decorate([
    core_1.ViewChild("newnote"),
    __metadata("design:type", core_1.ElementRef)
], ManagerOrderReview.prototype, "newnote", void 0);
ManagerOrderReview = __decorate([
    core_1.Component({
        selector: "manager-order-review",
        moduleId: module.id,
        templateUrl: "managerorderreview.component.html",
        styleUrls: ["managerorderreview.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        saveorder_service_1.SaveOrderService,
        page_1.Page,
        saveorder_service_1.SaveCustomer,
        sendorder_service_1.SendOrder,
        saveorder_service_1.SaveTotal,
        product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef,
        saveorder_service_1.SaveCustomerDetails,
        saveorder_service_1.SaveShipping,
        saveorder_service_1.SaveNotes,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        orderapproval_service_1.OrderApproval,
        common_1.Location,
        saveorder_service_2.SaveUpdateOrderId,
        saveorder_service_2.SaveUpdateOrder_Id,
        http_1.Http])
], ManagerOrderReview);
exports.ManagerOrderReview = ManagerOrderReview;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlcm9yZGVycmV2aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1hbmFnZXJvcmRlcnJldmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxXQUFXO0FBQ1gsZ0JBQWdCO0FBQ2hCLGlJQUFpSTtBQUNqSSxpRkFBd0U7QUFDeEUsdUVBQThEO0FBQzlELGtHQUF1RjtBQUN2RixrRUFBdUQ7QUFDdkQscUVBQWlFO0FBQ2pFLHlFQUFnRTtBQUNoRSx5RUFPNkM7QUFDN0MsMkRBQTBEO0FBQzFELCtEQUE4RDtBQUM5RCxpRUFBbUU7QUFDbkUsc0NBQW1GO0FBQ25GLHNEQUErRDtBQUMvRCxnQ0FBK0I7QUFDL0Isa0VBQTJGO0FBQzNGLHNDQUF3RDtBQUN4RCxvQ0FBc0M7QUFDdEMsMENBQThEO0FBQzlELDBDQUFpRDtBQUdqRCwyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQUMzQyx5RUFBNEY7QUFHNUYsc0NBQXFDO0FBQ3JDLHNHQUFzRztBQVN0RyxJQUFhLGtCQUFrQjtJQThCN0IsNEJBQ1UsZ0JBQWtDLEVBQ2xDLGdCQUFrQyxFQUNsQyxJQUFVLEVBQ1YsWUFBMEIsRUFDMUIsU0FBb0IsRUFDcEIsU0FBb0IsRUFDcEIsWUFBMEIsRUFDMUIsWUFBZ0MsRUFDaEMsZ0JBQWtDLEVBQ2xDLG1CQUF3QyxFQUN4QyxZQUEwQixFQUMxQixTQUFvQixFQUNwQixlQUF5QixFQUN6QixLQUFxQixFQUNyQixhQUE0QixFQUM1QixRQUFrQixFQUNsQixpQkFBb0MsRUFDcEMsa0JBQXNDLEVBQ3RDLElBQVU7UUFsQlYscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3BCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQ2hDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3BCLG9CQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLFNBQUksR0FBSixJQUFJLENBQU07UUFoRHBCLFVBQUssR0FBVyxjQUFjLENBQUM7UUFDL0IsYUFBUSxHQUFHLElBQUkseUJBQVEsRUFBRSxDQUFDO1FBQzFCLFlBQU8sR0FBRyxJQUFJLHdCQUFPLEVBQUUsQ0FBQztRQUN4QixtQkFBYyxHQUFrQixFQUFFLENBQUM7UUFPNUIsZ0JBQVcsR0FBYyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBRXJDLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSXJCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFLekIsZ0NBQTJCLEdBQWtCLEVBQUUsQ0FBQztRQUV6QyxhQUFRLEdBQVcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUV0QyxtQkFBYyxHQUFZLElBQUksQ0FBQztRQUMvQixzQkFBaUIsR0FBWSxJQUFJLENBQUM7SUF1Qi9CLENBQUM7SUFFSixxQ0FBUSxHQUFSO1FBQUEsaUJBMEJDO1FBekJDLDRKQUE0SjtRQUM1Six3SEFBd0g7UUFDeEgsbUpBQW1KO1FBQ25KLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLHdCQUF3QixDQUFDO1FBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFFLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFFMUIsSUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxlQUFlO2FBQ2pCLFlBQVksQ0FBQyxFQUFFLENBQUM7YUFDaEIsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBOUIsQ0FBOEIsRUFDckMsVUFBQSxHQUFHLElBQUUsT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLEdBQUcsQ0FBQyxFQUE1QyxDQUE0QyxDQUFDLENBQUM7UUFFL0QsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsZ0NBQWtCLENBQUMsd0JBQXdCLEVBQUUsVUFBQyxJQUF5QztZQUM1RyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQix1QkFBdUI7WUFDdkIsdUJBQXVCO1lBQ3ZCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzdDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFZLEdBQVosVUFBYSxJQUFJO1FBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3RCxvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUN0QyxxQ0FBcUM7UUFDckMseUVBQXlFO1FBQ3pFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7UUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztRQUNsRCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO1FBQ3hELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUM7UUFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztRQUNoRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1FBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7UUFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyQyxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUk1RCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQztZQUNoQix5QkFBeUI7WUFDekIsbUVBQW1FO1lBQ25FLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGtCQUFJLEVBQUUsQ0FBQztnQkFDeEIsa0RBQWtEO2dCQUNsRCxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFDeEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO2dCQUVwRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakMsQ0FBQztZQUVELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQSxDQUFDO29CQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksa0JBQUksRUFBRSxDQUFDO29CQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUE7b0JBQ3pELElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDO29CQUM1RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztvQkFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7b0JBRXhELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsQ0FBQztnQkFDRCxJQUFJLENBQUEsQ0FBQyxDQUFBLENBQUM7WUFDTixDQUFDO1FBQ0wsQ0FBQztRQUNELFNBQVM7UUFHVCxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEVBQUMsU0FBUyxFQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSTtZQUMzRixVQUFVLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEVBQUUsUUFBUSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTO1lBQ25HLGVBQWUsRUFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsY0FBYyxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQjtZQUMvSCxXQUFXLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUE7UUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7UUFFMUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ25ELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSx1QkFBTyxFQUFFLENBQUM7WUFDN0IsK0NBQStDO1lBRS9DLG9DQUFvQztZQUVwQyw2Q0FBNkM7WUFFN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2hFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUN4RCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztZQUMvRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN6RSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUUvRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBSXZELCtDQUErQztZQUUvQyxxREFBcUQ7WUFDckQsbUNBQW1DO1lBQ25DLDRCQUE0QjtZQUM1Qix1RUFBdUU7WUFDdkUsK0ZBQStGO1lBQy9GLGdHQUFnRztZQUNoRyxrR0FBa0c7WUFDbEcsZ0hBQWdIO1lBQ2hILElBQUk7WUFDSixvQkFBb0I7WUFDcEIsbUNBQW1DO1lBQ25DLG1DQUFtQztZQUNuQywyQkFBMkI7WUFDM0IsSUFBSTtZQUVKLDZDQUE2QztZQUU3QyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUd6RSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFdEMsK0NBQStDO1lBRS9DLDhCQUE4QjtZQUM5QixtREFBbUQ7WUFDbkQsK0hBQStIO1lBQy9ILHlKQUF5SjtZQUN6SiwrSkFBK0o7WUFDL0osbUZBQW1GO1lBQ25GLE1BQU07WUFDTixnQ0FBZ0M7WUFFaEMsNkNBQTZDO1lBRXpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0NBQWtDLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUk7Z0JBQ3BILFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEVBQUUsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTtnQkFDOUksVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsRUFBQyxDQUFDLENBQUM7WUFFekcsNENBQTRDO1FBRzVDLENBQUM7UUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUE7UUFFMUUsRUFBRSxDQUFBLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxLQUFLLE1BQU0sQ0FBQyxDQUFBLENBQUM7WUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsWUFBWSxHQUFHLG1CQUFtQixDQUFBO1FBQ3pDLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFBLENBQUM7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsMkJBQTJCLENBQUE7UUFDakQsQ0FBQztRQUNELElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEtBQUssV0FBVyxDQUFDLENBQUEsQ0FBQztZQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyx3QkFBd0IsQ0FBQTtRQUM5QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxVQUFVLENBQUMsQ0FBQSxDQUFDO1lBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLHVCQUF1QixDQUFBO1FBQzdDLENBQUM7SUFFUCxDQUFDO0lBRUQsb0NBQU8sR0FBUCxVQUFRLEVBQUUsRUFBRSxLQUFLO1FBQWpCLGlCQWtCQztRQWpCQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0UsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN4RCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxDQUFDO1FBRXZDLElBQUksT0FBTyxHQUF1QjtZQUNoQyxPQUFPLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUU7WUFDN0MsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtTQUN4QyxDQUFDO1FBRUYsSUFBSSxDQUFDLFlBQVk7YUFDZCxTQUFTLENBQUMseUJBQU0sRUFBRSxPQUFPLENBQUM7YUFDMUIsSUFBSSxDQUFDLFVBQUMsWUFBMkI7WUFDaEMsT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxDQUFDO1FBQXZDLENBQXVDLENBQ3hDLENBQUM7SUFDTixDQUFDO0lBRU0sc0NBQVMsR0FBaEIsVUFBaUIsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1FBRW5DLEVBQUUsQ0FBQyxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUU3RCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXpCLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5QyxxQkFBcUI7WUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDNUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDbkQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDL0MsK0NBQStDO1lBRS9DLDRFQUE0RTtZQUM1RSwrRUFBK0U7WUFDL0Usd0NBQXdDO1lBQ3hDLG9EQUFvRDtZQUNwRCwrREFBK0Q7WUFDL0QsdURBQXVEO1lBQ3ZELGdGQUFnRjtZQUNoRixvRkFBb0Y7WUFDcEYsSUFBSTtZQUVKLDZDQUE2QztRQUMvQyxDQUFDO0lBQ0gsQ0FBQztJQUVELHdDQUFXLEdBQVgsVUFBWSxFQUFFLEVBQUUsS0FBSztRQUNuQixtQ0FBbUM7UUFEckMsaUJBa0NDO1FBL0JDLElBQUksT0FBTyxHQUFHO1lBQ1osS0FBSyxFQUFFLGNBQWM7WUFDckIsT0FBTyxFQUFFLHVCQUF1QjtZQUNoQyxZQUFZLEVBQUUsS0FBSztZQUNuQixnQkFBZ0IsRUFBRSxJQUFJO1NBQ3ZCLENBQUM7UUFFRixpQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQWU7WUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDdEMsRUFBRSxDQUFDLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3hELEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQztnQkFDRCxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQUM7b0JBQ3hDLG1EQUFtRDtvQkFFbkQsT0FBTzt5QkFDSixLQUFLLENBQUM7d0JBQ0wsS0FBSyxFQUFFLGVBQWU7d0JBQ3RCLE9BQU8sRUFBRSwwQkFBMEI7d0JBQ25DLFlBQVksRUFBRSxJQUFJO3FCQUNuQixDQUFDLENBQUE7Z0JBQ04sQ0FBQztZQUNILENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixNQUFNLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQWUsR0FBZixVQUFnQixTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUs7UUFDckMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ2pELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLENBQUM7WUFDekMsQ0FBQztRQUNILENBQUM7UUFFRCxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FDVCw4QkFBOEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUN2RSxDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQzNELElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5RCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELHdDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztRQUN0QixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUMxRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ25ELENBQUM7UUFDRCxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUV6RSxDQUFDO0lBRUQsZ0RBQW1CLEdBQW5CO1FBQUEsaUJBMEJDO1FBekJDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQ3RDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2pDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2pDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1FBQy9CLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1FBQzlDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ3pDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxDQUFDO1FBRW5DLElBQUksT0FBTyxHQUF1QjtZQUNoQyxPQUFPLEVBQUU7Z0JBQ1AsTUFBTSxFQUFFLFVBQVU7Z0JBQ2xCLFVBQVUsRUFBRSxjQUFjO2dCQUMxQixJQUFJLEVBQUUsUUFBUTtnQkFDZCxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLEdBQUcsRUFBRSxPQUFPO2FBQ2I7WUFDRCxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCO1NBQ3hDLENBQUM7UUFFRixJQUFJLENBQUMsWUFBWTthQUNkLFNBQVMsQ0FBQyx5Q0FBYyxFQUFFLE9BQU8sQ0FBQzthQUNsQyxJQUFJLENBQUMsVUFBQyxZQUEyQixJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksT0FBTztRQUNqQixFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN0QixNQUFNLENBQUM7UUFDVCxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRS9FLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxhQUFhLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFbkYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUU1RSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTVFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUU3RSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNFLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDdkMsQ0FBQztJQUNILENBQUM7SUFFRCxtQ0FBTSxHQUFOLFVBQU8sSUFBSTtRQUFYLGlCQWlGQztRQWhGQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RCLElBQUksV0FBVyxHQUFHLElBQUksS0FBSyxFQUF1QixDQUFDO1FBQ25ELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFPLE9BQU8sQ0FBQyxDQUFDO1FBQ2xELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFPLE9BQU8sQ0FBQyxDQUFDO1FBRW5ELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUV6QixJQUFJLEVBQUUsR0FDTixFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQTtZQUNuRCxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBRXJCLElBQUksRUFBRSxHQUNOO2dCQUNFLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRTtnQkFDekIsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLENBQUM7YUFDVCxDQUFBO1lBQ0QsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVyQixJQUFJLEVBQUUsR0FDTjtnQkFDRSxNQUFNLEVBQUUsT0FBTztnQkFDZixTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ3pCLE9BQU8sRUFBRSxDQUFDO2dCQUNWLFFBQVEsRUFBRSxHQUFHO2dCQUNiLEtBQUssRUFBRSxDQUFDO2FBQ1QsQ0FBQTtZQUNELFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFckIsSUFBSSxZQUFZLEdBQUcsSUFBSSxxQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRTlDLFlBQVk7aUJBQ1gsSUFBSSxFQUFFO2lCQUNOLElBQUksQ0FBQztnQkFDSixxQ0FBcUM7Z0JBQ3JDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsVUFBQyxDQUFDO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxFQUFFLEdBQ0osRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUE7WUFDcEQsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVyQixJQUFJLEVBQUUsR0FDTjtnQkFDRSxNQUFNLEVBQUUsTUFBTTtnQkFDZCxTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLENBQUM7YUFDVCxDQUFBO1lBQ0QsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVyQixJQUFJLEVBQUUsR0FDTjtnQkFDRSxNQUFNLEVBQUUsT0FBTztnQkFDZixTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRTtnQkFDNUIsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLENBQUM7YUFDVCxDQUFBO1lBQ0QsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVyQixJQUFJLFlBQVksR0FBRyxJQUFJLHFCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUMsWUFBWTtpQkFDWCxJQUFJLEVBQUU7aUJBQ04sSUFBSSxDQUFDO2dCQUNKLHFDQUFxQztnQkFDckMsS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDeEIsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLENBQUM7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0lBQ0gsQ0FBQztJQUVELHlDQUFZLEdBQVosVUFBYSxRQUFRO1FBQXJCLGlCQTZCQztRQTNCQyxJQUFJLE9BQU8sR0FBRztZQUNaLEtBQUssRUFBRSxlQUFlO1lBQ3RCLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDekIsQ0FBQztRQUVBLGlCQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBZTtZQUNwQyxFQUFFLENBQUEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQztnQkFDakIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztnQkFDL0IsS0FBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsUUFBUSxDQUFDO2dCQUNwRSxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2dCQUNwRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7Z0JBQ3pDLE9BQU8sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsQ0FBQTtnQkFDbkMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQzdCLElBQUksUUFBUSxHQUFHLFVBQVUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLFFBQVEsRUFBQyxRQUFRLEVBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3pGLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDaEYsQ0FBQztZQUNELElBQUksQ0FBQSxDQUFDO2dCQUNELE1BQU0sQ0FBQztZQUNYLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUVILENBQUM7SUFFRCxzQ0FBUyxHQUFULFVBQVUsUUFBUTtRQUFsQixpQkF1Q0M7UUFyQ0MsSUFBSSxPQUFPLEdBQUc7WUFDWixLQUFLLEVBQUUsWUFBWTtZQUNuQixZQUFZLEVBQUUsS0FBSztZQUNuQixnQkFBZ0IsRUFBRSxJQUFJO1NBQ3pCLENBQUM7UUFFQSxpQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQWU7WUFDcEMsRUFBRSxDQUFBLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLENBQUE7Z0JBQ3pDLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLFFBQVEsQ0FBQztnQkFDcEUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztnQkFDcEUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3BELElBQUksT0FBTyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO2dCQUN6QyxPQUFPLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUE7Z0JBQ25DLEVBQUUsQ0FBQSxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsQ0FBQSxDQUFDO29CQUNqQixLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDMUIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDN0IsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFBO29CQUNyQixLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDNUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLFFBQVEsRUFBQyxRQUFRLEVBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3pGLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ2hGLENBQUM7Z0JBRUQsSUFBSSxDQUFBLENBQUM7b0JBQ0gsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFBO2dCQUN0QixDQUFDO1lBRUgsQ0FBQztZQUNELElBQUksQ0FBQSxDQUFDO2dCQUNELE1BQU0sQ0FBQztZQUNYLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUlILENBQUM7SUFDSCx5QkFBQztBQUFELENBQUMsQUFwakJELElBb2pCQztBQXhoQnVCO0lBQXJCLGdCQUFTLENBQUMsU0FBUyxDQUFDOzhCQUFVLGlCQUFVO21EQUFDO0FBNUIvQixrQkFBa0I7SUFOOUIsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxzQkFBc0I7UUFDaEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSxtQ0FBbUM7UUFDaEQsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7S0FDaEQsQ0FBQztxQ0FnQzRCLHlCQUFnQjtRQUNoQixvQ0FBZ0I7UUFDNUIsV0FBSTtRQUNJLGdDQUFZO1FBQ2YsNkJBQVM7UUFDVCw2QkFBUztRQUNOLDhCQUFZO1FBQ1osaUNBQWtCO1FBQ2QsdUJBQWdCO1FBQ2IsdUNBQW1CO1FBQzFCLGdDQUFZO1FBQ2YsNkJBQVM7UUFDSCwyQkFBUTtRQUNsQix1QkFBYztRQUNOLHFDQUFhO1FBQ2xCLGlCQUFRO1FBQ0MscUNBQWlCO1FBQ2hCLHNDQUFrQjtRQUNoQyxXQUFJO0dBakRULGtCQUFrQixDQW9qQjlCO0FBcGpCWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQge1xyXG4vLyAgICAgUmVzcG9uc2UsXHJcbi8vIH0gZnJvbSAnLi4vLi4vLi4vLi4vcGxhdGZvcm1zL2FuZHJvaWQvYXBwL2J1aWxkL2ludGVybWVkaWF0ZXMvYXNzZXRzL2RlYnVnL2FwcC90bnNfbW9kdWxlcy9AYW5ndWxhci9odHRwL3NyYy9zdGF0aWNfcmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBPcmRlckFwcHJvdmFsIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvb3JkZXJhcHByb3ZhbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgR2V0T3JkZXIgfSBmcm9tICcuLi8uLi8uLi9TZXJ2aWNlcy9nZXRvcmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGlhbG9nU2hpcHBpbmcgfSBmcm9tICcuLi8uLi9EaWFsb2dfc2hpcHBpbmdhZGRyZXNzL2RpYWxvZ3NoaXBwaW5nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERpYWxvZyB9IGZyb20gJy4uLy4uL0RpYWxvZy9kaWFsb2cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2F2ZVByb2R1Y3RzIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvcHJvZHVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VuZE9yZGVyIH0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvc2VuZG9yZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gICAgU2F2ZUN1c3RvbWVyLFxyXG4gICAgU2F2ZUN1c3RvbWVyRGV0YWlscyxcclxuICAgIFNhdmVOb3RlcyxcclxuICAgIFNhdmVPcmRlclNlcnZpY2UsXHJcbiAgICBTYXZlU2hpcHBpbmcsXHJcbiAgICBTYXZlVG90YWwsXHJcbn0gZnJvbSAnLi4vLi4vLi4vU2VydmljZXMvc2F2ZW9yZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPcmRlciwgTm90ZSB9IGZyb20gJy4uLy4uLy4uL01vZGVscy9vcmRlci5tb2RlbCc7XHJcbmltcG9ydCB7IFByb2R1Y3QsT2ZmZXIgfSBmcm9tICcuLi8uLi8uLi9Nb2RlbHMvcHJvZHVjdC5tb2RlbCc7XHJcbmltcG9ydCB7IEFkZHJlc3MsIEN1c3RvbWVyIH0gZnJvbSAnLi4vLi4vLi4vTW9kZWxzL2N1c3RvbWVyLm1vZGVsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q29udGFpbmVyUmVmLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd1aS9wYWdlJztcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dTZXJ2aWNlLCBNb2RhbERpYWxvZ09wdGlvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2cnO1xyXG5pbXBvcnQgeyBwcm9tcHQsIGlucHV0VHlwZSwgY29uZmlybSB9IGZyb20gXCJ1aS9kaWFsb2dzXCI7XHJcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSAndWkvZGlhbG9ncyc7XHJcbmltcG9ydCB7IEFuaW1hdGlvbiAsIEFuaW1hdGlvbkRlZmluaXRpb259IGZyb20gXCJ1aS9hbmltYXRpb25cIjtcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgVmlldyB9IGZyb20gXCJ1aS9jb3JlL3ZpZXdcIjtcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tICdhcHBsaWNhdGlvbic7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU2F2ZVVwZGF0ZU9yZGVySWQsIFNhdmVVcGRhdGVPcmRlcl9JZCB9IGZyb20gJy4uLy4uLy4uL1NlcnZpY2VzL3NhdmVvcmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGV4dEZpZWxkIH0gZnJvbSAndWkvdGV4dC1maWVsZCc7XHJcbmltcG9ydCB7IHN1YnNjcmliZU9uIH0gZnJvbSAncnhqcy9vcGVyYXRvci9zdWJzY3JpYmVPbic7XHJcbmltcG9ydCB7IEh0dHAgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xyXG4vLyBpbXBvcnQgeyBPZmZlciB9IGZyb20gJy4uLy4uLy4uLy4uL3BsYXRmb3Jtcy9hbmRyb2lkL2FwcC9zcmMvbWFpbi9hc3NldHMvYXBwL01vZGVscy9wcm9kdWN0Lm1vZGVsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJtYW5hZ2VyLW9yZGVyLXJldmlld1wiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwibWFuYWdlcm9yZGVycmV2aWV3LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJtYW5hZ2Vyb3JkZXJyZXZpZXcuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFuYWdlck9yZGVyUmV2aWV3IHtcclxuICB0aXRsZTogc3RyaW5nID0gXCJPcmRlciBSZXZpZXdcIjtcclxuICBjdXN0b21lciA9IG5ldyBDdXN0b21lcigpO1xyXG4gIGFkZHJlc3MgPSBuZXcgQWRkcmVzcygpO1xyXG4gIHByb2R1Y3REZXRhaWxzOiBBcnJheTxPYmplY3Q+ID0gW107XHJcbiAgY3VzdG9tZXJJZDogYW55O1xyXG4gIGN1c3RvbWVyZGV0YWlsczogYW55O1xyXG4gIHByb2R1Y3RzOiBhbnk7XHJcbiAgZ2V0cHJvZHVjdHM6IGFueTtcclxuICBwdWJsaWMgcHJvZHVjdDogUHJvZHVjdDtcclxuICBwdWJsaWMgb3JkZXI6IE9yZGVyO1xyXG4gIHB1YmxpYyBhbGxwcm9kdWN0czogUHJvZHVjdFtdID0gbmV3IEFycmF5KCk7XHJcbiAgb3JkZXJyZW1vdmVkOiBhbnk7XHJcbiAgcHVibGljIGVkaXRTaGlwcGluZyA9IGZhbHNlO1xyXG4gIHRvdGFsX2Ftb3VudDogYW55O1xyXG4gIHRlbXBsaW5ldG90YWw6IGFueTtcclxuICBzaGlwcGluZ21vZGU6IGFueTtcclxuICBwdWJsaWMgaXNGYWJPcGVuID0gZmFsc2U7XHJcbiAgb3JkZXJfZGF0ZTtcclxuICBvcmRlcl9zdGF0dXM7XHJcbiAgb3JkZXJfaWQ7XHJcbiAgZm9ybWF0dGVkT3JkZXJfaWQ7XHJcbiAgb3JkZXJ1cGRhdGVfY3VzdG9tZXJkZXRhaWxzOiBBcnJheTxPYmplY3Q+ID0gW107XHJcbiAgcHVibGljIG5vdGVzOiBOb3RlO1xyXG4gIHB1YmxpYyBhbGxub3RlczogTm90ZVtdID0gbmV3IEFycmF5KCk7XHJcbiAgdGVtcGRhdGU6RGF0ZTtcclxuICBkaXNhYmxlSG9sZEZhYjogYm9vbGVhbiA9IHRydWU7XHJcbiAgZGlzYWJsZUFwcHJvdmVGYWI6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIEBWaWV3Q2hpbGQoXCJuZXdub3RlXCIpIG5ld25vdGU6IEVsZW1lbnRSZWY7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBzYXZlb3JkZXJzZXJ2aWNlOiBTYXZlT3JkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBwYWdlOiBQYWdlLFxyXG4gICAgcHJpdmF0ZSBzYXZlY3VzdG9tZXI6IFNhdmVDdXN0b21lcixcclxuICAgIHByaXZhdGUgc2VuZG9yZGVyOiBTZW5kT3JkZXIsXHJcbiAgICBwcml2YXRlIHNhdmV0b3RhbDogU2F2ZVRvdGFsLFxyXG4gICAgcHJpdmF0ZSBzYXZlcHJvZHVjdHM6IFNhdmVQcm9kdWN0cyxcclxuICAgIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWYsXHJcbiAgICBwcml2YXRlIHNhdmVjdXN0b21lcmRldGFpbHM6IFNhdmVDdXN0b21lckRldGFpbHMsXHJcbiAgICBwcml2YXRlIHNhdmVzaGlwcGluZzogU2F2ZVNoaXBwaW5nLFxyXG4gICAgcHJpdmF0ZSBzYXZlbm90ZXM6IFNhdmVOb3RlcyxcclxuICAgIHByaXZhdGUgZ2V0b3JkZXJzZXJ2aWNlOiBHZXRPcmRlcixcclxuICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBvcmRlcmFwcHJvdmFsOiBPcmRlckFwcHJvdmFsLFxyXG4gICAgcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXHJcbiAgICBwcml2YXRlIHNhdmV1cGRhdGVvcmRlcmlkOiBTYXZlVXBkYXRlT3JkZXJJZCxcclxuICAgIHByaXZhdGUgc2F2ZXVwZGF0ZW9yZGVyX2lkOiBTYXZlVXBkYXRlT3JkZXJfSWQsXHJcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBcclxuICApIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy90aGlzLm9yZGVyRGV0YWlscyA9ICh7aWQ6XCJhc2gzNDI4Mzk4MDkzOFwiLCBzaG9wOlwiS2FubmlnYSBQYXJhbWVzaHdhcmkgU3RvcmVzXCIsIGltZzpcIn4vSW1hZ2VzL2dyZWVuX2RvdC5wbmdcIiwgZGF0ZTpcIjgvMi8xOFwiLCBzaGlwcGluZ21vZGU6XCJLLlAuTiBUcmF2ZWxzXCJ9KVxyXG4gICAgLy90aGlzLmFkZHJlc3MgPSAoe2FyZWE6IFwiTm8uMTIzLCBvcHAuIEVsZGFtJ3MgUm9hZCwgQW5uYVNhbGFpXCIsIGNpdHk6XCJDaGVubmFpXCIsIHBpbmNvZGU6XCI2MTExMTFcIiwgbW9iaWxlOlwiOTg3NDU2MzIxMFwifSlcclxuICAgIC8vdGhpcy5wcm9kdWN0RGV0YWlscy5wdXNoKHtuYW1lOlwiS3Jpc2huYSdzIEN1c3RhcmQgUG93ZGVyXCIsIGlkOiBcImhld3E4N2U5ODc4MlwiLCBpbWc6XCJyZXM6Ly9zdG9yZVwiLCBvZmZlcjpcIkJ1eSAxIEdldCAyIEZyZWVcIiwgcmFjazpcIjVcIiwgb3JkZXI6XCI1XCJ9KVxyXG4gICAgdGhpcy5jdXN0b21lci5zaG9wbmFtZSA9IFwiQUJDIERlcGFydG1lbnRhbCBzdG9yZVwiO1xyXG4gICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IFwibmFtZVwiO1xyXG4gICAgdGhpcy5hZGRyZXNzLmRvb3Jfbm8gPSBcIjdcIjtcclxuICAgIHRoaXMuYWRkcmVzcy5zdHJlZXRfbmFtZSA9IFwic29tZSBzdHJlZXRcIjtcclxuICAgIHRoaXMuYWRkcmVzcy5hcmVhID1cInNvbWUgYXJlYVwiO1xyXG4gICAgdGhpcy5hZGRyZXNzLmNpdHkgPSBcImNoZW5uYWlcIjtcclxuICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IFwiY2hlbm5haVwiO1xyXG4gICAgdGhpcy5hZGRyZXNzLnN0YXRlID0gXCJUYW1pbCBOYWR1XCI7XHJcbiAgICB0aGlzLmFkZHJlc3MucGluID0gNjU4OTkwO1xyXG5cclxuICAgIGNvbnN0IGlkID0gdGhpcy5yb3V0ZS5zbmFwc2hvdC5wYXJhbXNbXCJpZFwiXTtcclxuICAgIHRoaXMuZ2V0b3JkZXJzZXJ2aWNlXHJcbiAgICAgIC5nZXRvcmRlcmJ5aWQoaWQpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLmdldGFsbG9yZGVycyhkYXRhLmpzb24oKSksXHJcbiAgICAgICAgICAgICAgICAgIGVycj0+Y29uc29sZS5sb2coXCJHZXQgb3JkZXIgYnkgaWQgZXJyb3I6IFwiICsgZXJyKSk7XHJcblxyXG4gICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgIGRhdGEuY2FuY2VsID0gdHJ1ZTtcclxuICAgICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMuYmFja1RvUHJldmlvdXNQYWdlKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0YWxsb3JkZXJzKGRhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiQWxsIERhdGE6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgY29uc29sZS5sb2coXCJwcm9kdWN0cy5sZW5ndGg6IFwiICsgZGF0YVswXS5saW5lX2l0ZW1zLmxlbmd0aCk7XHJcbiAgICAvL2RhdGEgPSBkYXRhLl9ib2R5O1xyXG4gICAgdGhpcy5vcmRlcl9pZCA9IGRhdGFbMF0uX2lkLiRvaWQ7XHJcbiAgICB0aGlzLmZvcm1hdHRlZE9yZGVyX2lkID0gZGF0YVswXS5vcmRlcl9pZDtcclxuICAgIGNvbnNvbGUubG9nKFwiZm9ybWF0dGVkIG9yZGVyIGlkXCIrdGhpcy5mb3JtYXR0ZWRPcmRlcl9pZCk7XHJcbiAgICB0aGlzLnNhdmV1cGRhdGVvcmRlcmlkLnNldFNjb3BlKHRoaXMub3JkZXJfaWQpO1xyXG4gICAgdGhpcy5zYXZldXBkYXRlb3JkZXJfaWQuc2V0U2NvcGUoZGF0YVswXS5vcmRlcl9pZCk7XHJcbiAgICB0aGlzLm9yZGVyX2RhdGUgPSAgZGF0YVswXS5vcmRlcl9kYXRlO1xyXG4gICAgLy90aGlzLnRlbXBkYXRlID0gZGF0YVswXS5vcmRlcl9kYXRlO1xyXG4gICAgLy90aGlzLm9yZGVyX2RhdGUgPSB0aGlzLnRlbXBkYXRlLmdldERhdGUoKStcIi9cIit0aGlzLnRlbXBkYXRlLmdldE1vbnRoKCk7XHJcbiAgICB2YXIgdGVtcHN0YXR1cyA9IGRhdGFbMF0uc3RhdHVzO1xyXG4gICAgdGhpcy5jdXN0b21lci5zaG9wbmFtZSA9IGRhdGFbMF0uc2hvcF9uYW1lO1xyXG4gICAgY29uc29sZS5sb2coXCJzaG9wbmFtZTogXCIrIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUpO1xyXG4gICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IGRhdGFbMF0uY3VzdG9tZXJfbmFtZTtcclxuICAgIHRoaXMuYWRkcmVzcy5zaGlwcGluZ21vZGUgPSBkYXRhWzBdLnByZWZlcnJlZF9zaGlwcGluZzsgICAgXHJcbiAgICB0aGlzLmFkZHJlc3MuYXJlYSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5hcmVhO1xyXG4gICAgdGhpcy5hZGRyZXNzLmNpdHkgPSBkYXRhWzBdLnNoaXBwaW5nX2FkZHJlc3MuY2l0eTtcclxuICAgIHRoaXMuYWRkcmVzcy5kb29yX25vID0gZGF0YVswXS5zaGlwcGluZ19hZGRyZXNzLmRvb3Jfbm87XHJcbiAgICB0aGlzLmFkZHJlc3MucGluID0gZGF0YVswXS5zaGlwcGluZ19hZGRyZXNzLnBpbjtcclxuICAgIHRoaXMuYWRkcmVzcy5zdHJlZXRfbmFtZSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5zdHJlZXRfbmFtZTtcclxuICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5kaXN0cmljdDtcclxuICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5zdGF0ZTtcclxuICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IHRoaXMuYWRkcmVzcztcclxuICAgIHRoaXMudG90YWxfYW1vdW50ID0gTnVtYmVyKGRhdGFbMF0udG90YWxfYW1vdW50KTtcclxuICAgIGNvbnNvbGUubG9nKFwiQ1VTVE9NRVIgPT09PT0+IFwiK0pTT04uc3RyaW5naWZ5KHRoaXMuY3VzdG9tZXIpKTtcclxuICAgIGNvbnNvbGUubG9nKFwiQUREUkVTUyA9PT09PT4gXCIrSlNPTi5zdHJpbmdpZnkodGhpcy5hZGRyZXNzKSk7XHJcbiAgICBcclxuICAgIFxyXG5cclxuICAgIGlmKGRhdGFbMF0ubm90ZXMpe1xyXG4gICAgICAvL2NvbnNvbGUubG9nKFwiTXkgTm90ZXNcIilcclxuICAgICAgLy9jb25zb2xlLmxvZyhkYXRhWzBdLmN1c3RvbWVyX25hbWUgKyBcIiA6IFwiICsgZGF0YVswXS5ub3Rlcy5sZW5ndGgpXHJcbiAgICAgIGZvcihsZXQgYT0wOyBhPGRhdGFbMF0ubm90ZXMubGVuZ3RoOyBhKyspe1xyXG4gICAgICAgIHRoaXMubm90ZXMgPSBuZXcgTm90ZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJNeSBOb3RlczogXCIgK2RhdGFbMF0ubm90ZXNbYV0ubm90ZSlcclxuICAgICAgICB0aGlzLm5vdGVzLmNyZWF0ZWRfZGF0ZSA9IGRhdGFbMF0ubm90ZXNbYV0uY3JlYXRlZF9kYXRlO1xyXG4gICAgICAgIHRoaXMubm90ZXMubm90ZSA9IGRhdGFbMF0ubm90ZXNbYV0ubm90ZTtcclxuICAgICAgICB0aGlzLm5vdGVzLmNyZWF0ZWRfYnkgPSBkYXRhWzBdLm5vdGVzW2FdLmNyZWF0ZWRfYnk7XHJcblxyXG4gICAgICAgIHRoaXMuYWxsbm90ZXMucHVzaCh0aGlzLm5vdGVzKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYoZGF0YVswXS5ub3Rlcy5sZW5ndGggPT0gMCl7XHJcbiAgICAgICAgaWYoZGF0YVswXS5jdXJyZW50X25vdGUpe1xyXG4gICAgICAgICAgdGhpcy5ub3RlcyA9IG5ldyBOb3RlKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ3VycmVudCBOb3RlczogXCIgK2RhdGFbMF0uY3VycmVudF9ub3RlLm5vdGUpXHJcbiAgICAgICAgICAgIHRoaXMubm90ZXMuY3JlYXRlZF9kYXRlID0gZGF0YVswXS5jdXJyZW50X25vdGUuY3JlYXRlZF9kYXRlO1xyXG4gICAgICAgICAgICB0aGlzLm5vdGVzLm5vdGUgPSBkYXRhWzBdLmN1cnJlbnRfbm90ZS5ub3RlO1xyXG4gICAgICAgICAgICB0aGlzLm5vdGVzLmNyZWF0ZWRfYnkgPSBkYXRhWzBdLmN1cnJlbnRfbm90ZS5jcmVhdGVkX2J5O1xyXG4gICAgXHJcbiAgICAgICAgICAgIHRoaXMuYWxsbm90ZXMucHVzaCh0aGlzLm5vdGVzKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXt9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gZWxzZXt9XHJcbiAgIFxyXG5cclxuICAgIHRoaXMub3JkZXJ1cGRhdGVfY3VzdG9tZXJkZXRhaWxzLnB1c2goe29yZGVyZGF0ZTp0aGlzLm9yZGVyX2RhdGUsIGVtcGxveWVlaWQ6ZGF0YVswXS5lbXBsb3llZV9pZC4kb2lkLFxyXG4gICAgICAgICAgICAgIGN1c3RvbWVyaWQ6ZGF0YVswXS5jdXN0b21lcl9pZC4kb2lkLCBjdXN0b21lcm5hbWU6ZGF0YVswXS5jdXN0b21lcl9uYW1lLCBzaG9wbmFtZTpkYXRhWzBdLnNob3BfbmFtZSxcclxuICAgICAgICAgICAgICBzaGlwcGluZ2FkZHJlc3M6ZGF0YVswXS5zaGlwcGluZ19hZGRyZXNzLCBiaWxsaW5nYWRkcmVzczpkYXRhWzBdLmJpbGxpbmdfYWRkcmVzcywgcHJlZmVycmVkc2hpcHBpbmc6IGRhdGFbMF0ucHJlZmVycmVkX3NoaXBwaW5nLFxyXG4gICAgICAgICAgICAgIHRvdGFsYW1vdW50OmRhdGFbMF0udG90YWxfYW1vdW50IH0pXHJcbiAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLm9yZGVydXBkYXRlX2N1c3RvbWVyZGV0YWlscykpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRhdGFbMF0ubGluZV9pdGVtcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgdGhpcy5wcm9kdWN0ID0gbmV3IFByb2R1Y3QoKTtcclxuICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIHN0YXJ0XHJcblxyXG4gICAgICAgICAgLy8gdGhpcy5wcm9kdWN0Lm9mZmVyID0gbmV3IE9mZmVyKCk7XHJcblxyXG4gICAgICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gZW5kXHJcbiAgICBcclxuICAgICAgICAgIHRoaXMucHJvZHVjdC5wcm9kdWN0X2lkID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLnByb2R1Y3RfaWQuJG9pZDsgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnByb2R1Y3Quc2t1X2lkID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLnNrdV9pZC4kb2lkO1xyXG4gICAgICAgICAgdGhpcy5wcm9kdWN0LnByb2R1Y3RfbmFtZSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X25hbWU7XHJcbiAgICAgICAgICB0aGlzLnByb2R1Y3QubGluZV90b3RhbCA9IE51bWJlcihkYXRhWzBdLmxpbmVfaXRlbXNbaV0ubGluZV90b3RhbCk7XHJcbiAgICAgICAgICB0aGlzLnByb2R1Y3QucmFja19xdWFudGl0eSA9IE51bWJlcihkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucmFja19xdWFudGl0eSk7XHJcbiAgICAgICAgICB0aGlzLnByb2R1Y3QucXVhbnRpdHkgPSBOdW1iZXIoZGF0YVswXS5saW5lX2l0ZW1zW2ldLnF1YW50aXR5KTtcclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIlBST0RVQ1Q6IFwiKyBKU09OLnN0cmluZ2lmeSh0aGlzLnByb2R1Y3QpKTtcclxuXHJcbiAgICAgICAgIFxyXG5cclxuICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIHN0YXJ0XHJcblxyXG4gICAgICAgICAgLy8gaWYoZGF0YVswXS5saW5lX2l0ZW1zW2ldLmRpc2NvdW50X2lkKXsgICAgICAgICAgICBcclxuICAgICAgICAgIC8vICAgY29uc29sZS5sb2coXCJIYXMgRGlzY291bnRcIikgICBcclxuICAgICAgICAgIC8vICAgdmFyIG5vZGlzY291bnQgPSBmYWxzZTtcclxuICAgICAgICAgIC8vICAgdGhpcy5wcm9kdWN0LmRpc2NvdW50X2lkID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLmRpc2NvdW50X2lkLiRvaWQ7XHJcbiAgICAgICAgICAvLyAgIHRoaXMucHJvZHVjdC5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5ID0gIGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5O1xyXG4gICAgICAgICAgLy8gICB0aGlzLnByb2R1Y3Qub2ZmZXIuYmFzZV9xdWFudGl0eSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9kZXRhaWxzWzBdLmJhc2VfcXVhbnRpdHk7XHJcbiAgICAgICAgICAvLyAgIHRoaXMucHJvZHVjdC5vZmZlci5vZmZlcl9xdWFudGl0eSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9kZXRhaWxzWzBdLm9mZmVyX3F1YW50aXR5O1xyXG4gICAgICAgICAgLy8gICB0aGlzLnByb2R1Y3Qub2ZmZXIuZGlzY291bnRfcHJvZHVjdF9uYW1lID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLmRpc2NvdW50X2RldGFpbHNbMV0uZGlzY291bnRfcHJvZHVjdF9uYW1lO1xyXG4gICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgLy8gZWxzZXsgICAgICAgICAgICBcclxuICAgICAgICAgIC8vICAgY29uc29sZS5sb2coXCJIYXMgTm8gRGlzY291bnRcIilcclxuICAgICAgICAgIC8vICAgdGhpcy5wcm9kdWN0LmRpc2NvdW50X2lkID0gXCJcIjtcclxuICAgICAgICAgIC8vICAgdmFyIG5vZGlzY291bnQgPSB0cnVlO1xyXG4gICAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIGVuZFxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnByb2R1Y3QucHJvZHVjdF9wcmljZSA9IE51bWJlcihkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucHJvZHVjdF9wcmljZSk7XHJcblxyXG5cclxuICAgICAgICAgIHRoaXMuYWxscHJvZHVjdHMucHVzaCh0aGlzLnByb2R1Y3QpO1xyXG4gICAgICAgICBcclxuICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG5cclxuICAgICAgICAvLyAgIGlmKG5vZGlzY291bnQgPT09IGZhbHNlKXtcclxuICAgICAgICAvLyAgICAgY29uc29sZS5sb2coXCJJbnNlcnRpbmcgZGlzY291bnQgcHJvZHVjdC4uXCIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe3Byb2R1Y3RfaWQ6IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X2lkLiRvaWQsIHNrdV9pZDogZGF0YVswXS5saW5lX2l0ZW1zW2ldLnNrdV9pZC4kb2lkLCBcclxuICAgICAgICAvLyAgICAgICBxdWFudGl0eTogZGF0YVswXS5saW5lX2l0ZW1zW2ldLnF1YW50aXR5LCByYWNrX3F1YW50aXR5OiBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucmFja19xdWFudGl0eSwgcHJvZHVjdF9uYW1lOiBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucHJvZHVjdF9uYW1lLCBcclxuICAgICAgICAvLyAgICAgICBsaW5lX3RvdGFsOiBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ubGluZV90b3RhbCwgZGlzY291bnRfaWQ6IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9pZC4kb2lkLCBwcm9kdWN0X3ByaWNlOiBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucHJvZHVjdF9wcmljZSxcclxuICAgICAgICAvLyAgICAgZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eTpkYXRhWzBdLmxpbmVfaXRlbXNbaV0uZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eX0pO1xyXG4gICAgICAgIC8vICAgfVxyXG4gICAgICAgIC8vIGVsc2UgaWYobm9kaXNjb3VudCA9PT0gdHJ1ZSl7XHJcblxyXG4gICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIGVuZFxyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbnNlcnRpbmcgTm8gRGlzY291bnQgUHJvZHVjdC4uLlwiKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0RGV0YWlscy5wdXNoKHtwcm9kdWN0X2lkOiBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucHJvZHVjdF9pZC4kb2lkLCBza3VfaWQ6IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5za3VfaWQuJG9pZCwgXHJcbiAgICAgICAgICAgICAgcXVhbnRpdHk6IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5xdWFudGl0eSwgcmFja19xdWFudGl0eTogZGF0YVswXS5saW5lX2l0ZW1zW2ldLnJhY2tfcXVhbnRpdHksIHByb2R1Y3RfbmFtZTogZGF0YVswXS5saW5lX2l0ZW1zW2ldLnByb2R1Y3RfbmFtZSwgXHJcbiAgICAgICAgICAgICAgbGluZV90b3RhbDogZGF0YVswXS5saW5lX2l0ZW1zW2ldLmxpbmVfdG90YWwsIHByb2R1Y3RfcHJpY2U6IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X3ByaWNlfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIC8vIH0gLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIkFsbCBQcm9kdWN0IGRldGFpbHM6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9kdWN0RGV0YWlscykpXHJcblxyXG4gICAgICAgIGlmKHRlbXBzdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gXCJob2xkXCIpey8vb25Ib2xkXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInN0YXR1czogSG9sZFwiKTtcclxuICAgICAgICAgIHRoaXMub3JkZXJfc3RhdHVzID0gXCJ+L0ltYWdlcy9ob2xkLnBuZ1wiXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYodGVtcHN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSBcInBlbmRpbmdcIil7Ly9ub3RfcmV2aWV3ZWQgYnkgc2FsZXNfbWFuYWdlclxyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJzdGF0dXM6IG5vdF9yZXZpZXdlZFwiKTtcclxuICAgICAgICAgIHRoaXMub3JkZXJfc3RhdHVzID0gXCJ+L0ltYWdlcy9ub3RfcmV2aWV3ZWQucG5nXCJcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZih0ZW1wc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09IFwicHJvY2Vzc2VkXCIpey8vQ29tcGxldGVkXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInN0YXR1czogY29tcGxldGVkXCIpO1xyXG4gICAgICAgICAgdGhpcy5vcmRlcl9zdGF0dXMgPSBcIn4vSW1hZ2VzL2NvbXBsZXRlZC5wbmdcIlxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKHRlbXBzdGF0dXMudG9Mb3dlckNhc2UoKSA9PT0gXCJhcHByb3ZlZFwiKXsvL0FwcHJvdmVkICYgUHJvY2Vzc2luZ1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJzdGF0dXM6IGFwcHJvdmVkXCIpO1xyXG4gICAgICAgICAgdGhpcy5vcmRlcl9zdGF0dXMgPSBcIn4vSW1hZ2VzL2FwcHJvdmVkLnBuZ1wiXHJcbiAgICAgICAgfVxyXG4gICAgICAgICBcclxuICB9XHJcblxyXG4gIGVkaXRxdHkoaWQsIGluZGV4KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImlkOiBcIiArIGlkICsgXCJpbmRleDogXCIgKyBpbmRleCk7XHJcbiAgICBjb25zb2xlLmxvZyhcImlkOiBcIiArIEpTT04uc3RyaW5naWZ5KHRoaXMuYWxscHJvZHVjdHMpKTtcclxuICAgIGNvbnNvbGUubG9nKFwib3JkZXJxdHkgaW4gZWRpdHF0eTogXCIgKyB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcInF1YW50aXR5XCJdKTtcclxuICAgIHZhciByYWNrZWRpdCA9IHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wicmFja19xdWFudGl0eVwiXTtcclxuICAgIHZhciBvcmRlcmVkaXQgPSB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcInF1YW50aXR5XCJdO1xyXG4gICAgY29uc29sZS5sb2coXCJvcmRlcmVkaXQ6IFwiICsgb3JkZXJlZGl0KTtcclxuXHJcbiAgICBsZXQgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xyXG4gICAgICBjb250ZXh0OiB7IHJhY2s6IHJhY2tlZGl0LCBvcmRlcjogb3JkZXJlZGl0IH0sXHJcbiAgICAgIHZpZXdDb250YWluZXJSZWY6IHRoaXMudmlld0NvbnRhaW5lclJlZlxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1vZGFsU2VydmljZVxyXG4gICAgICAuc2hvd01vZGFsKERpYWxvZywgb3B0aW9ucylcclxuICAgICAgLnRoZW4oKGRpYWxvZ1Jlc3VsdDogQXJyYXk8c3RyaW5nPikgPT5cclxuICAgICAgICB0aGlzLnNldHJlc3VsdChkaWFsb2dSZXN1bHQsIGlkLCBpbmRleClcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRyZXN1bHQocXVhbnRpdHksIHByb2R1Y3RpZCwgaW5kZXgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwicmVzdWx0OiBcIiArIHF1YW50aXR5KTtcclxuXHJcbiAgICBpZiAocXVhbnRpdHkgPT0gXCJudWxsXCIpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdmFyIHJhY2sgPSBxdWFudGl0eVswXTtcclxuICAgICAgdmFyIG9yZGVyID0gcXVhbnRpdHlbMV07XHJcblxyXG4gICAgICBjb25zb2xlLmxvZyhcInJlczE6IFwiICsgcXVhbnRpdHlbMF0gKyBcInJlczI6IFwiICsgcXVhbnRpdHlbMV0pO1xyXG5cclxuICAgICAgY29uc29sZS5sb2coXCJOZXcgVmFsdWVcIik7XHJcblxyXG4gICAgICB0aGlzLmNoYW5nZWxpbmV0b3RhbChwcm9kdWN0aWQsIGluZGV4LCBvcmRlcik7XHJcbiAgICAgIC8vdGhpcy5jaGFuZ2V0b3RhbCgpO1xyXG4gICAgICB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcInJhY2tfcXVhbnRpdHlcIl0gPSByYWNrO1xyXG4gICAgICB0aGlzLmFsbHByb2R1Y3RzW2luZGV4XVtcInF1YW50aXR5XCJdID0gb3JkZXI7XHJcbiAgICAgIHRoaXMucHJvZHVjdERldGFpbHNbaW5kZXhdW1wicmFja19xdWFudGl0eVwiXSA9IHJhY2s7XHJcbiAgICAgIHRoaXMucHJvZHVjdERldGFpbHNbaW5kZXhdW1wicXVhbnRpdHlcIl0gPSBvcmRlcjtcclxuICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gc3RhcnRcclxuXHJcbiAgICAgIC8vIGxldCBiYXNlUXVhbnRpdHkgPSBwYXJzZUludCh0aGlzLmFsbHByb2R1Y3RzW2luZGV4XS5vZmZlci5iYXNlX3F1YW50aXR5KTtcclxuICAgICAgLy8gbGV0IG9mZmVyX1F1YW50aXR5ID0gcGFyc2VJbnQodGhpcy5hbGxwcm9kdWN0c1tpbmRleF0ub2ZmZXIub2ZmZXJfcXVhbnRpdHkpO1xyXG4gICAgICAvLyBpZihwYXJzZUludChvcmRlcikgPj0gYmFzZVF1YW50aXR5ICl7XHJcbiAgICAgIC8vICAgICB2YXIgcmVtYWluZGVyID0gcGFyc2VJbnQob3JkZXIpJWJhc2VRdWFudGl0eTtcclxuICAgICAgLy8gICAgIHZhciBxdW90aWVudCA9IE1hdGguZmxvb3IocGFyc2VJbnQob3JkZXIpL2Jhc2VRdWFudGl0eSk7XHJcbiAgICAgIC8vICAgICB2YXIgb2ZmZXJRdWFudGl0eSA9IChxdW90aWVudCAqIG9mZmVyX1F1YW50aXR5KTtcclxuICAgICAgLy8gICAgIHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wiZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eVwiXSA9IG9mZmVyUXVhbnRpdHkrXCJcIjsgXHJcbiAgICAgIC8vICAgICB0aGlzLnByb2R1Y3REZXRhaWxzW2luZGV4XVtcImRpc2NvdW50X3Byb2R1Y3RfcXVhbnRpdHlcIl0gPSBvZmZlclF1YW50aXR5K1wiXCIgICBcclxuICAgICAgLy8gfVxyXG5cclxuICAgICAgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50IC0gZW5kXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW1vdmVvcmRlcihpZCwgaW5kZXgpIHtcclxuICAgIC8vdGhpcy5hbGxwcm9kdWN0cy5zcGxpY2UoaW5kZXgsMSk7XHJcblxyXG4gICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgIHRpdGxlOiBcIkFyZSBZb3UgU3VyZVwiLFxyXG4gICAgICBtZXNzYWdlOiBcIldhbnQgdG8gcmVtb3ZlIG9yZGVyP1wiLFxyXG4gICAgICBva0J1dHRvblRleHQ6IFwiWWVzXCIsXHJcbiAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiTm9cIlxyXG4gICAgfTtcclxuXHJcbiAgICBjb25maXJtKG9wdGlvbnMpLnRoZW4oKHJlc3VsdDogYm9vbGVhbikgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhcInJlbW92ZW9yZGVyOiBcIiArIHJlc3VsdCk7XHJcbiAgICAgIGlmIChyZXN1bHQgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICB0aGlzLmFsbHByb2R1Y3RzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgdGhpcy5wcm9kdWN0RGV0YWlscy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiT3JkZXIgTGVuZ3RoOiBcIiArIHRoaXMuYWxscHJvZHVjdHMubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmNoYW5nZXRvdGFsKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJwcm9kdWN0cyBjYW5ub3QgYmUgZW1wdHlcIik7XHJcbiAgICAgICAgICAvL3RoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcHJvZHVjdGxpc3RcIl0pO1xyXG5cclxuICAgICAgICAgIGRpYWxvZ3NcclxuICAgICAgICAgICAgLmFsZXJ0KHtcclxuICAgICAgICAgICAgICB0aXRsZTogXCJEZWxldGluZyBBbGwhXCIsXHJcbiAgICAgICAgICAgICAgbWVzc2FnZTogXCJQcm9kdWN0cyBDYW5ub3QgYmUgRW1wdHlcIixcclxuICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT2tcIlxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2hhbmdlbGluZXRvdGFsKHByb2R1Y3RpZCwgaW5kZXgsIG9yZGVyKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuYWxscHJvZHVjdHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKHRoaXMuYWxscHJvZHVjdHNbaV1bXCJwcm9kdWN0X2lkXCJdID09IHByb2R1Y3RpZCkge1xyXG4gICAgICAgIHZhciB0ZW1wcHJpY2UgPSBOdW1iZXIodGhpcy5hbGxwcm9kdWN0c1tpXVtcInByb2R1Y3RfcHJpY2VcIl0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwidGVtcHByaWNlOiBcIiArIHRlbXBwcmljZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnRlbXBsaW5ldG90YWwgPSB0ZW1wcHJpY2UgKiBvcmRlcjtcclxuICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICBcIkxpbmUgdG90YWwgaW4gY2hhbmdlIHRvdGFsOiBcIiArIHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wibGluZV90b3RhbFwiXVxyXG4gICAgKTtcclxuICAgIHRoaXMuYWxscHJvZHVjdHNbaW5kZXhdW1wibGluZV90b3RhbFwiXSA9IHRoaXMudGVtcGxpbmV0b3RhbDtcclxuICAgIHRoaXMucHJvZHVjdERldGFpbHNbaW5kZXhdW1wibGluZV90b3RhbFwiXSA9IHRoaXMudGVtcGxpbmV0b3RhbDtcclxuICAgIHRoaXMuY2hhbmdldG90YWwoKTtcclxuICB9XHJcblxyXG4gIGNoYW5nZXRvdGFsKCkge1xyXG4gICAgdGhpcy50b3RhbF9hbW91bnQgPSAwO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmFsbHByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGkgKyBcIjogXCIgKyB0aGlzLmFsbHByb2R1Y3RzW2ldW1wicHJvZHVjdF9pZFwiXSk7XHJcbiAgICAgIHZhciB0ZW1wdG90YWwgPSB0aGlzLmFsbHByb2R1Y3RzW2ldW1wibGluZV90b3RhbFwiXTtcclxuICAgICAgdGhpcy50b3RhbF9hbW91bnQgPSB0ZW1wdG90YWwgKyB0aGlzLnRvdGFsX2Ftb3VudDtcclxuICAgICAgY29uc29sZS5sb2coXCJ0b3RhbGFtb3VudDogXCIgKyB0aGlzLnRvdGFsX2Ftb3VudCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLm9yZGVydXBkYXRlX2N1c3RvbWVyZGV0YWlsc1swXVtcInRvdGFsYW1vdW50XCJdID0gdGhpcy50b3RhbF9hbW91bnQ7XHJcblxyXG4gIH1cclxuXHJcbiAgZWRpdHNoaXBwaW5nYWRkcmVzcygpIHtcclxuICAgIHZhciBkb29ybm9lZGl0ID0gdGhpcy5hZGRyZXNzLmRvb3Jfbm87XHJcbiAgICB2YXIgYXJlYWVkaXQgPSB0aGlzLmFkZHJlc3MuYXJlYTtcclxuICAgIHZhciBjaXR5ZWRpdCA9IHRoaXMuYWRkcmVzcy5jaXR5O1xyXG4gICAgdmFyIHBpbmVkaXQgPSB0aGlzLmFkZHJlc3MucGluO1xyXG4gICAgdmFyIHN0cmVldG5hbWVlZGl0ID0gdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lO1xyXG4gICAgdmFyIGRpc3RyaWN0ZWRpdCA9IHRoaXMuYWRkcmVzcy5kaXN0cmljdDtcclxuICAgIHZhciBzdGF0ZWVkaXQgPSB0aGlzLmFkZHJlc3Muc3RhdGU7XHJcbiAgICBjb25zb2xlLmxvZyhcInN0YXRlOiBcIiArIHN0YXRlZWRpdCk7XHJcblxyXG4gICAgbGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcclxuICAgICAgY29udGV4dDoge1xyXG4gICAgICAgIGRvb3JubzogZG9vcm5vZWRpdCxcclxuICAgICAgICBzdHJlZXRuYW1lOiBzdHJlZXRuYW1lZWRpdCxcclxuICAgICAgICBhcmVhOiBhcmVhZWRpdCxcclxuICAgICAgICBjaXR5OiBjaXR5ZWRpdCxcclxuICAgICAgICBkaXN0cmljdDogZGlzdHJpY3RlZGl0LFxyXG4gICAgICAgIHN0YXRlOiBzdGF0ZWVkaXQsXHJcbiAgICAgICAgcGluOiBwaW5lZGl0XHJcbiAgICAgIH0sXHJcbiAgICAgIHZpZXdDb250YWluZXJSZWY6IHRoaXMudmlld0NvbnRhaW5lclJlZlxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1vZGFsU2VydmljZVxyXG4gICAgICAuc2hvd01vZGFsKERpYWxvZ1NoaXBwaW5nLCBvcHRpb25zKVxyXG4gICAgICAudGhlbigoZGlhbG9nUmVzdWx0OiBBcnJheTxzdHJpbmc+KSA9PiB0aGlzLmVkaXRhZGRyZXNzKGRpYWxvZ1Jlc3VsdCkpO1xyXG4gIH1cclxuXHJcbiAgZWRpdGFkZHJlc3MoYWRkcmVzcykge1xyXG4gICAgaWYgKGFkZHJlc3MgPT0gXCJudWxsXCIpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfSBcclxuICAgIGVsc2Uge1xyXG4gICAgICB0aGlzLmFkZHJlc3MuZG9vcl9ubyA9IGFkZHJlc3NbMF07XHJcbiAgICAgIHRoaXMub3JkZXJ1cGRhdGVfY3VzdG9tZXJkZXRhaWxzWzBdW1wic2hpcHBpbmdhZGRyZXNzXCJdW1wiZG9vcl9ub1wiXSA9IGFkZHJlc3NbMF07XHJcblxyXG4gICAgICB0aGlzLmFkZHJlc3Muc3RyZWV0X25hbWUgPSBhZGRyZXNzWzFdO1xyXG4gICAgICB0aGlzLm9yZGVydXBkYXRlX2N1c3RvbWVyZGV0YWlsc1swXVtcInNoaXBwaW5nYWRkcmVzc1wiXVtcInN0cmVldF9uYW1lXCJdID0gYWRkcmVzc1sxXTtcclxuXHJcbiAgICAgIHRoaXMuYWRkcmVzcy5hcmVhID0gYWRkcmVzc1syXTtcclxuICAgICAgdGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHNbMF1bXCJzaGlwcGluZ2FkZHJlc3NcIl1bXCJhcmVhXCJdID0gYWRkcmVzc1syXTtcclxuXHJcbiAgICAgIHRoaXMuYWRkcmVzcy5jaXR5ID0gYWRkcmVzc1szXTtcclxuICAgICAgdGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHNbMF1bXCJzaGlwcGluZ2FkZHJlc3NcIl1bXCJjaXR5XCJdID0gYWRkcmVzc1szXTtcclxuXHJcbiAgICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IGFkZHJlc3NbNF07XHJcbiAgICAgIHRoaXMub3JkZXJ1cGRhdGVfY3VzdG9tZXJkZXRhaWxzWzBdW1wic2hpcHBpbmdhZGRyZXNzXCJdW1wiZGlzdHJpY3RcIl0gPSBhZGRyZXNzWzRdO1xyXG5cclxuICAgICAgdGhpcy5hZGRyZXNzLnN0YXRlID0gYWRkcmVzc1s1XTtcclxuICAgICAgdGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHNbMF1bXCJzaGlwcGluZ2FkZHJlc3NcIl1bXCJzdGF0ZVwiXSA9IGFkZHJlc3NbNV07XHJcblxyXG4gICAgICB0aGlzLmFkZHJlc3MucGluID0gYWRkcmVzc1s2XTtcclxuICAgICAgdGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHNbMF1bXCJzaGlwcGluZ2FkZHJlc3NcIl1bXCJwaW5cIl0gPSBhZGRyZXNzWzZdO1xyXG4gICAgICB0aGlzLmN1c3RvbWVyLmFkZHJlc3MgPSB0aGlzLmFkZHJlc3M7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmYWJUYXAoYXJncykge1xyXG4gICAgdmFyIGZhYiA9IGFyZ3Mub2JqZWN0O1xyXG4gICAgbGV0IGRlZmluaXRpb25zID0gbmV3IEFycmF5PEFuaW1hdGlvbkRlZmluaXRpb24+KCk7XHJcbiAgICB2YXIgYnV0dG9uID0gdGhpcy5wYWdlLmdldFZpZXdCeUlkPFZpZXc+KFwiZGlhbDFcIik7XHJcbiAgICB2YXIgYnV0dG9uMiA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZDxWaWV3PihcImRpYWwyXCIpO1xyXG5cclxuICAgIGlmICh0aGlzLmlzRmFiT3BlbiA9PSB0cnVlKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbGV0IGExOiBBbmltYXRpb25EZWZpbml0aW9uICA9XHJcbiAgICAgICAgeyB0YXJnZXQ6IGZhYiwgcm90YXRlOiAwLCBkdXJhdGlvbjogNDAwLCBkZWxheTogMCB9XHJcbiAgICAgICAgZGVmaW5pdGlvbnMucHVzaChhMSk7XHJcblxyXG4gICAgICAgIGxldCBhMjogQW5pbWF0aW9uRGVmaW5pdGlvbiA9IFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHRhcmdldDogYnV0dG9uLFxyXG4gICAgICAgICAgdHJhbnNsYXRlOiB7IHg6IDAsIHk6IDAgfSxcclxuICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICBkdXJhdGlvbjogNDAwLFxyXG4gICAgICAgICAgZGVsYXk6IDBcclxuICAgICAgICB9XHJcbiAgICAgICAgZGVmaW5pdGlvbnMucHVzaChhMik7XHJcblxyXG4gICAgICAgIGxldCBhMzogQW5pbWF0aW9uRGVmaW5pdGlvbiA9IFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHRhcmdldDogYnV0dG9uMixcclxuICAgICAgICAgIHRyYW5zbGF0ZTogeyB4OiAwLCB5OiAwIH0sXHJcbiAgICAgICAgICBvcGFjaXR5OiAwLFxyXG4gICAgICAgICAgZHVyYXRpb246IDQ0MCxcclxuICAgICAgICAgIGRlbGF5OiAwXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRlZmluaXRpb25zLnB1c2goYTMpO1xyXG5cclxuICAgICAgICBsZXQgYW5pbWF0aW9uU2V0ID0gbmV3IEFuaW1hdGlvbihkZWZpbml0aW9ucyk7XHJcblxyXG4gICAgICAgIGFuaW1hdGlvblNldFxyXG4gICAgICAgIC5wbGF5KClcclxuICAgICAgICAudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiQW5pbWF0aW9ucyBmaW5pc2hlZFwiKTtcclxuICAgICAgICAgIHRoaXMuaXNGYWJPcGVuID0gZmFsc2U7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGUubWVzc2FnZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IFxyXG4gICAgXHJcbiAgICBlbHNlIHtcclxuICAgICAgbGV0IGE0OiBBbmltYXRpb25EZWZpbml0aW9uICA9XHJcbiAgICAgICAgeyB0YXJnZXQ6IGZhYiwgcm90YXRlOiA0NSwgZHVyYXRpb246IDQwMCwgZGVsYXk6IDAgfVxyXG4gICAgICAgIGRlZmluaXRpb25zLnB1c2goYTQpO1xyXG5cclxuICAgICAgICBsZXQgYTU6IEFuaW1hdGlvbkRlZmluaXRpb24gID1cclxuICAgICAgICB7XHJcbiAgICAgICAgICB0YXJnZXQ6IGJ1dHRvbixcclxuICAgICAgICAgIHRyYW5zbGF0ZTogeyB4OiAwLCB5OiAtNTQgfSxcclxuICAgICAgICAgIG9wYWNpdHk6IDEsXHJcbiAgICAgICAgICBkdXJhdGlvbjogNDAwLFxyXG4gICAgICAgICAgZGVsYXk6IDBcclxuICAgICAgICB9XHJcbiAgICAgICAgZGVmaW5pdGlvbnMucHVzaChhNSk7XHJcblxyXG4gICAgICAgIGxldCBhNjogQW5pbWF0aW9uRGVmaW5pdGlvbiAgPVxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHRhcmdldDogYnV0dG9uMixcclxuICAgICAgICAgIHRyYW5zbGF0ZTogeyB4OiAwLCB5OiAtMTAwIH0sXHJcbiAgICAgICAgICBvcGFjaXR5OiAxLFxyXG4gICAgICAgICAgZHVyYXRpb246IDQ0MCxcclxuICAgICAgICAgIGRlbGF5OiAwXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRlZmluaXRpb25zLnB1c2goYTYpO1xyXG5cclxuICAgICAgICBsZXQgYW5pbWF0aW9uU2V0ID0gbmV3IEFuaW1hdGlvbihkZWZpbml0aW9ucyk7XHJcbiAgICAgICAgYW5pbWF0aW9uU2V0XHJcbiAgICAgICAgLnBsYXkoKVxyXG4gICAgICAgIC50aGVuKCgpPT4ge1xyXG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkFuaW1hdGlvbnMgZmluaXNoZWRcIik7XHJcbiAgICAgICAgICB0aGlzLmlzRmFiT3BlbiA9IHRydWU7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goKGUpPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZS5tZXNzYWdlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGFwcHJvdmVvcmRlcihzaGlwbW9kZSl7XHJcblxyXG4gICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgIHRpdGxlOiBcIkFwcHJvdmUgT3JkZXJcIixcclxuICAgICAgb2tCdXR0b25UZXh0OiBcIlllc1wiLFxyXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5vXCJcclxuICB9O1xyXG5cclxuICAgIGNvbmZpcm0ob3B0aW9ucykudGhlbigocmVzdWx0OiBib29sZWFuKSA9PiB7XHJcbiAgICAgIGlmKHJlc3VsdCA9PSB0cnVlKXtcclxuICAgICAgICB0aGlzLmRpc2FibGVBcHByb3ZlRmFiID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHNbMF1bXCJwcmVmZXJyZWRzaGlwcGluZ1wiXSA9IHNoaXBtb2RlO1xyXG4gICAgICAgIHRoaXMuc2F2ZWN1c3RvbWVyZGV0YWlscy5zZXRTY29wZSh0aGlzLm9yZGVydXBkYXRlX2N1c3RvbWVyZGV0YWlscyk7ICAgIFxyXG4gICAgICAgIHRoaXMuc2F2ZW9yZGVyc2VydmljZS5zZXRTY29wZSh0aGlzLnByb2R1Y3REZXRhaWxzKTtcclxuICAgICAgICB2YXIgbmV3bm90ZSA9IHRoaXMubmV3bm90ZS5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIG5ld25vdGUgPSBuZXdub3RlLnRleHQ7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJOZXcgbm90ZTogXCIgKyBuZXdub3RlKVxyXG4gICAgICAgIHRoaXMuc2F2ZW5vdGVzLnNldFNjb3BlKG5ld25vdGUpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiQXBwcm92ZSBPcmRlclwiKTsgICAgICAgIFxyXG4gICAgICAgIHRoaXMuc2VuZG9yZGVyLnVwZGF0ZW9yZGVyKCk7XHJcbiAgICAgICAgdmFyIGFwcHJvdmFsID0gXCJhcHByb3ZlZFwiO1xyXG4gICAgICAgIHRoaXMub3JkZXJhcHByb3ZhbC5hcHByb3Zlb3JkZXIodGhpcy5vcmRlcl9pZCxhcHByb3ZhbCx0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdKTtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL21hbmFnZXJvcmRlcmxpc3RcIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGVsc2V7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICB9KTtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgaG9sZG9yZGVyKHNoaXBtb2RlKXtcclxuXHJcbiAgICBsZXQgb3B0aW9ucyA9IHtcclxuICAgICAgdGl0bGU6IFwiSG9sZCBPcmRlclwiLFxyXG4gICAgICBva0J1dHRvblRleHQ6IFwiWWVzXCIsXHJcbiAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiTm9cIlxyXG4gIH07XHJcblxyXG4gICAgY29uZmlybShvcHRpb25zKS50aGVuKChyZXN1bHQ6IGJvb2xlYW4pID0+IHtcclxuICAgICAgaWYocmVzdWx0ID09IHRydWUpeyAgICAgICAgXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTaGlwcGluZyBNb2RlOiBcIiArIHNoaXBtb2RlKVxyXG4gICAgICAgIHRoaXMub3JkZXJ1cGRhdGVfY3VzdG9tZXJkZXRhaWxzWzBdW1wicHJlZmVycmVkc2hpcHBpbmdcIl0gPSBzaGlwbW9kZTtcclxuICAgICAgICB0aGlzLnNhdmVjdXN0b21lcmRldGFpbHMuc2V0U2NvcGUodGhpcy5vcmRlcnVwZGF0ZV9jdXN0b21lcmRldGFpbHMpOyAgICBcclxuICAgICAgICB0aGlzLnNhdmVvcmRlcnNlcnZpY2Uuc2V0U2NvcGUodGhpcy5wcm9kdWN0RGV0YWlscyk7ICAgICAgICBcclxuICAgICAgICB2YXIgbmV3bm90ZSA9IHRoaXMubmV3bm90ZS5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIG5ld25vdGUgPSBuZXdub3RlLnRleHQ7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJOZXcgbm90ZTogXCIgKyBuZXdub3RlKVxyXG4gICAgICAgIGlmKG5ld25vdGUgIT09IFwiXCIpe1xyXG4gICAgICAgICAgdGhpcy5zYXZlbm90ZXMuc2V0U2NvcGUobmV3bm90ZSk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkhvbGQgT3JkZXJcIik7XHJcbiAgICAgICAgICB0aGlzLnNlbmRvcmRlci51cGRhdGVvcmRlcigpO1xyXG4gICAgICAgICAgdmFyIGFwcHJvdmFsID0gXCJob2xkXCJcclxuICAgICAgICAgIHRoaXMuZGlzYWJsZUhvbGRGYWIgPSBmYWxzZTtcclxuICAgICAgICAgIHRoaXMub3JkZXJhcHByb3ZhbC5hcHByb3Zlb3JkZXIodGhpcy5vcmRlcl9pZCxhcHByb3ZhbCx0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdKTtcclxuICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbWFuYWdlcm9yZGVybGlzdFwiXSwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgYWxlcnQoXCJFbnRlciBub3Rlc1wiKVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgfVxyXG4gICAgICBlbHNle1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgfSk7XHJcblxyXG5cclxuICAgIFxyXG4gIH1cclxufSJdfQ==