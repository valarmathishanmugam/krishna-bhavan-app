"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_model_1 = require("../../Models/customer.model");
var customer_service_1 = require("../../Services/customer.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var dialogs_1 = require("ui/dialogs");
var router_2 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var ApproveCustomerDetail = (function () {
    function ApproveCustomerDetail(customerservice, route, http, routerExtensions, page, location) {
        this.customerservice = customerservice;
        this.route = route;
        this.http = http;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.disableApproveFAB = true;
    }
    ApproveCustomerDetail.prototype.ngOnInit = function () {
        var _this = this;
        //    datas for initial value start here
        this.customer.shopname = "ABC Departmental store";
        this.customer.contactname = "name";
        this.address.door_no = "7";
        this.address.street_name = "some street";
        this.address.area = "some area";
        this.address.city = "chennai";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.address.pin = 658990;
        this.customer.contactnumber = 0987654321;
        //    datas for initial value end here
        var id = this.route.snapshot.params["id"];
        this.customerid = id;
        this.customerservice.getcustomerbyid(id)
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    ApproveCustomerDetail.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
    };
    ApproveCustomerDetail.prototype.gotoshop = function () {
        var _this = this;
        var options = {
            title: "Approve Customer",
            //message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
                var data = { "status": "active" };
                return _this.http.post("https://app.krishnabhavanfoods.in/api/customer/customer_approval/" + _this.customerid, data, { headers: _this.getCommonHeaders() })
                    .subscribe(function (result) {
                    _this.disableApproveFAB = false;
                    console.log("Approve Customer Service Result: " + result);
                }, function (error) {
                    console.log("Approve Customer Service Error: " + error);
                });
            }
            else {
                return;
            }
        });
    };
    ApproveCustomerDetail.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    return ApproveCustomerDetail;
}());
ApproveCustomerDetail = __decorate([
    core_1.Component({
        selector: "approve-customer-detail",
        moduleId: module.id,
        templateUrl: "approve_customer_detail.component.html",
        styleUrls: ["approve_customer_detail.component.css"]
    }),
    __metadata("design:paramtypes", [customer_service_1.CustomerService, router_1.ActivatedRoute, http_1.Http,
        router_2.RouterExtensions, page_1.Page, common_1.Location])
], ApproveCustomerDetail);
exports.ApproveCustomerDetail = ApproveCustomerDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwcm92ZV9jdXN0b21lcl9kZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwcm92ZV9jdXN0b21lcl9kZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsOERBQWdFO0FBQ2hFLG9FQUFrRTtBQUNsRSxzQ0FBMEM7QUFDMUMsMENBQWlEO0FBQ2pELHNDQUF3RDtBQUN4RCxzQ0FBcUM7QUFFckMsc0RBQStEO0FBQy9ELHNEQUFxRDtBQUNyRCwyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQVMzQyxJQUFhLHFCQUFxQjtJQU85QiwrQkFBcUIsZUFBZ0MsRUFBUyxLQUFxQixFQUFVLElBQVUsRUFDM0YsZ0JBQWtDLEVBQVUsSUFBVSxFQUFVLFFBQWtCO1FBRHpFLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFTLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUMzRixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFMOUYsYUFBUSxHQUFHLElBQUkseUJBQVEsRUFBRSxDQUFDO1FBQzFCLFlBQU8sR0FBRyxJQUFJLHdCQUFPLEVBQUUsQ0FBQztRQUN4QixzQkFBaUIsR0FBWSxJQUFJLENBQUM7SUFLbEMsQ0FBQztJQUVDLHdDQUFRLEdBQVI7UUFBQSxpQkEyQkM7UUExQkMsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLHdCQUF3QixDQUFDO1FBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFFLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDO1FBQ3pDLHNDQUFzQztRQUd0QyxJQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFFckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDO2FBQ3ZDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztRQUVyRCxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxnQ0FBa0IsQ0FBQyx3QkFBd0IsRUFBRSxVQUFDLElBQXlDO1lBQzFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsOENBQWMsR0FBZCxVQUFlLElBQVM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFaEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBRTNDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDbkQsQ0FBQztJQUVELHdDQUFRLEdBQVI7UUFBQSxpQkErQkQ7UUE3QkcsSUFBSSxPQUFPLEdBQUc7WUFDVixLQUFLLEVBQUUsa0JBQWtCO1lBQ3pCLG1DQUFtQztZQUNuQyxZQUFZLEVBQUUsS0FBSztZQUNuQixnQkFBZ0IsRUFBRSxJQUFJO1NBQ3pCLENBQUM7UUFFRixpQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQWU7WUFDbEMsRUFBRSxDQUFBLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFBLENBQUM7Z0JBQ2YsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztnQkFDekQsSUFBSSxJQUFJLEdBQUcsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FFbkIsbUVBQW1FLEdBQUMsS0FBSSxDQUFDLFVBQVUsRUFDakYsSUFBSSxFQUNKLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQ25DO3FCQUNFLFNBQVMsQ0FBQyxVQUFDLE1BQU07b0JBQ2QsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztvQkFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsR0FBRyxNQUFNLENBQUMsQ0FBQTtnQkFDL0QsQ0FBQyxFQUFFLFVBQUMsS0FBSztvQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLEtBQUssQ0FBQyxDQUFBO2dCQUMzRCxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUM7WUFDRCxJQUFJLENBQUEsQ0FBQztnQkFDRCxNQUFNLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsZ0RBQWdCLEdBQWhCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsSUFBSSxjQUFPLEVBQUUsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUNMLDRCQUFDO0FBQUQsQ0FBQyxBQWpHRCxJQWlHQztBQWpHWSxxQkFBcUI7SUFQakMsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx5QkFBeUI7UUFDbkMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSx3Q0FBd0M7UUFDckQsU0FBUyxFQUFFLENBQUMsdUNBQXVDLENBQUM7S0FDdkQsQ0FBQztxQ0FTd0Msa0NBQWUsRUFBZ0IsdUJBQWMsRUFBZ0IsV0FBSTtRQUN6RSx5QkFBZ0IsRUFBZ0IsV0FBSSxFQUFvQixpQkFBUTtHQVJyRixxQkFBcUIsQ0FpR2pDO0FBakdZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFkZHJlc3MsIEN1c3RvbWVyIH0gZnJvbSAnLi4vLi4vTW9kZWxzL2N1c3RvbWVyLm1vZGVsJztcclxuaW1wb3J0IHsgQ3VzdG9tZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vU2VydmljZXMvY3VzdG9tZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMsIFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IGNvbmZpcm0gfSBmcm9tIFwidWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgKiBhcyBkaWFsb2dzIGZyb20gJ3VpL2RpYWxvZ3MnO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJhcHByb3ZlLWN1c3RvbWVyLWRldGFpbFwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcImFwcHJvdmVfY3VzdG9tZXJfZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcImFwcHJvdmVfY3VzdG9tZXJfZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHByb3ZlQ3VzdG9tZXJEZXRhaWx7XHJcbiAgICBcclxuICAgIGN1c3RvbWVyaWQ6IFwiXCI7XHJcbiAgICBjdXN0b21lciA9IG5ldyBDdXN0b21lcigpOyAgICBcclxuICAgIGFkZHJlc3MgPSBuZXcgQWRkcmVzcygpO1xyXG4gICAgZGlzYWJsZUFwcHJvdmVGQUI6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIGN1c3RvbWVyc2VydmljZTogQ3VzdG9tZXJTZXJ2aWNlLHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLCBwcml2YXRlIGh0dHA6IEh0dHAsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uKXtcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG4gICAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICAvLyAgICBkYXRhcyBmb3IgaW5pdGlhbCB2YWx1ZSBzdGFydCBoZXJlXHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5zaG9wbmFtZSA9IFwiQUJDIERlcGFydG1lbnRhbCBzdG9yZVwiO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG5hbWUgPSBcIm5hbWVcIjtcclxuICAgICAgICB0aGlzLmFkZHJlc3MuZG9vcl9ubyA9IFwiN1wiO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5zdHJlZXRfbmFtZSA9IFwic29tZSBzdHJlZXRcIjtcclxuICAgICAgICB0aGlzLmFkZHJlc3MuYXJlYSA9XCJzb21lIGFyZWFcIjtcclxuICAgICAgICB0aGlzLmFkZHJlc3MuY2l0eSA9IFwiY2hlbm5haVwiO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5kaXN0cmljdCA9IFwiY2hlbm5haVwiO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IFwiVGFtaWwgTmFkdVwiO1xyXG4gICAgICAgIHRoaXMuYWRkcmVzcy5waW4gPSA2NTg5OTA7XHJcbiAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bnVtYmVyID0gMDk4NzY1NDMyMTtcclxuICAgICAgICAvLyAgICBkYXRhcyBmb3IgaW5pdGlhbCB2YWx1ZSBlbmQgaGVyZVxyXG5cclxuXHJcbiAgICAgICAgY29uc3QgaWQgPSB0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tZXJpZCA9IGlkO1xyXG5cclxuICAgICAgICB0aGlzLmN1c3RvbWVyc2VydmljZS5nZXRjdXN0b21lcmJ5aWQoaWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMucGFyc2VDdXN0b21lcnMoZGF0YS5qc29uKCkpKTtcclxuXHJcbiAgICAgICAgYXBwbGljYXRpb24uYW5kcm9pZC5vbihBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50LCAoZGF0YTogQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEpID0+IHtcclxuICAgICAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgICAgICAgIC8vdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgICAgICAgIH0pOyAgXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHBhcnNlQ3VzdG9tZXJzKGRhdGE6IGFueSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEuc2hvcF9uYW1lKTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbWVyLnNob3BuYW1lID0gZGF0YS5zaG9wX25hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSA9IGRhdGEubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RudW1iZXIgPSBkYXRhLnByaW1hcnlfbW9iaWxlX25vO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIuZ3N0bm8gPSBkYXRhLmdzdF9ubztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZHJlc3MuY2l0eSA9IGRhdGEuYWRkcmVzc1swXS5jaXR5O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5kb29yX25vID0gZGF0YS5hZGRyZXNzWzBdLmRvb3Jfbm87XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRyZXNzLmFyZWEgPSBkYXRhLmFkZHJlc3NbMF0uYXJlYTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZHJlc3MucGluID0gZGF0YS5hZGRyZXNzWzBdLnBpbjtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZHJlc3Muc3RyZWV0X25hbWUgPSBkYXRhLmFkZHJlc3NbMF0uc3RyZWV0X25hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRyZXNzLmRpc3RyaWN0ID0gZGF0YS5hZGRyZXNzWzBdLmRpc3RyaWN0O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGRhdGEuYWRkcmVzc1swXS5zdGF0ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbWVyLmFkZHJlc3MgPSB0aGlzLmFkZHJlc3M7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGdvdG9zaG9wKCl7XHJcblxyXG4gICAgICAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJBcHByb3ZlIEN1c3RvbWVyXCIsXHJcbiAgICAgICAgICAgIC8vbWVzc2FnZTogXCJXYW50IHRvIHJlbW92ZSBvcmRlcj9cIixcclxuICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIlllc1wiLFxyXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5vXCJcclxuICAgICAgICB9O1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbmZpcm0ob3B0aW9ucykudGhlbigocmVzdWx0OiBib29sZWFuKSA9PiB7XHJcbiAgICAgICAgICAgIGlmKHJlc3VsdCA9PSB0cnVlKXtcclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvYXBwcm92ZWN1c3RvbWVybGlzdFwiXSk7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHtcInN0YXR1c1wiOlwiYWN0aXZlXCJ9O1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICBcImh0dHBzOi8vYXBwLmtyaXNobmFiaGF2YW5mb29kcy5pbi9hcGkvY3VzdG9tZXIvY3VzdG9tZXJfYXBwcm92YWwvXCIrdGhpcy5jdXN0b21lcmlkLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEsIFxyXG4gICAgICAgICAgICAgICAgICAgIHsgaGVhZGVyczogdGhpcy5nZXRDb21tb25IZWFkZXJzKCkgfVxyXG4gICAgICAgICAgICAgICAgICAgICkgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVBcHByb3ZlRkFCID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJBcHByb3ZlIEN1c3RvbWVyIFNlcnZpY2UgUmVzdWx0OiBcIiArIHJlc3VsdClcclxuICAgICAgICAgICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJBcHByb3ZlIEN1c3RvbWVyIFNlcnZpY2UgRXJyb3I6IFwiICsgZXJyb3IpXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29tbW9uSGVhZGVycygpIHtcclxuICAgICAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKCk7XHJcbiAgICAgICAgaGVhZGVycy5zZXQoXCJDb250ZW50LVR5cGVcIiwgXCJhcHBsaWNhdGlvbi9qc29uXCIpO1xyXG4gICAgICAgIHJldHVybiBoZWFkZXJzO1xyXG4gICAgfVxyXG59Il19