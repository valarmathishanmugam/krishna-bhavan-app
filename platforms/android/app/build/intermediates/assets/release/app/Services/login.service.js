"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var loginService = (function () {
    function loginService(http) {
        this.http = http;
        this.scope = false;
    }
    loginService.prototype.login = function (user, password) {
        return this.http.post("https://app.krishnabhavanfoods.in/api/user/login", { "mobile_no": user, "password": password
        }, { headers: this.getCommonHeaders() });
    };
    loginService.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    loginService.prototype.getScope = function () {
        return this.scope;
    };
    loginService.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("LoginDetails " + JSON.stringify(this.scope));
    };
    return loginService;
}());
loginService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], loginService);
exports.loginService = loginService;
