
import {Component,OnInit,AfterViewInit, ViewChild, ElementRef} from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-telerik-ui/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui/sidedrawer/angular";
import { TextField } from "ui/text-field";
import { ToolTip } from "nativescript-tooltip";
import { confirm } from "ui/dialogs";
import {
  AndroidApplication,
  AndroidActivityBackPressedEventData
} from "application";
import * as application from "application";
import { Location } from "@angular/common";
import { Http, Headers, Response } from "@angular/http";

@Component({
  selector: "account-comp",
  moduleId: module.id,
  templateUrl: "account.component.html",
  styleUrls: ["account.component.css"]
})
export class AccountComponent implements OnInit {

  constructor(private routerExtensions: RouterExtensions, private page: Page, private location: Location,
    private http: Http) {
        
    }

  ngOnInit() {
    this.page.actionBarHidden = false;
    application.android.on(
      AndroidApplication.activityBackPressedEvent,
      (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        //alert("Back Pressed")
        //this.location.back();
        //this.exitApp(data.cancel);
      }
    );   
   
  } 
}
