"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var DialogShipping = (function () {
    function DialogShipping(params) {
        this.params = params;
        this.address = [];
        this.doorno = params.context.doorno;
        this.streetname = params.context.streetname;
        this.area = params.context.area;
        this.city = params.context.city;
        this.district = params.context.district;
        this.state = params.context.state;
        this.pin = params.context.pin;
        console.log("Address in Dialog: " + this.doorno + this.streetname + this.area + this.city);
    }
    DialogShipping.prototype.ok = function (doorno, streetname, area, city, district, state, pin) {
        console.log("VAl1: " + doorno + "VAL2: " + streetname);
        if (doorno == "" || streetname == "" || area == "" || city == "" || district == "" || state == "" ||
            pin == "") {
            alert("Enter Value!!!");
        }
        else {
            console.log("Res1: " + this.doorno + "Res2: " + this.streetname);
            this.address.push(doorno);
            this.address.push(streetname);
            this.address.push(area);
            this.address.push(city);
            this.address.push(district);
            this.address.push(state);
            this.address.push(pin);
            this.params.closeCallback(this.address);
        }
    };
    DialogShipping.prototype.close = function (result) {
        console.log("Cancelled");
        console.log("Res: " + result);
        this.params.closeCallback(result);
    };
    return DialogShipping;
}());
DialogShipping = __decorate([
    core_1.Component({
        selector: "dialog-shipping",
        moduleId: module.id,
        templateUrl: "dialogshipping.component.html",
        styleUrls: ["dialogshipping.component.css"]
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams])
], DialogShipping);
exports.DialogShipping = DialogShipping;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nc2hpcHBpbmcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGlhbG9nc2hpcHBpbmcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLGtFQUFzRTtBQVN0RSxJQUFhLGNBQWM7SUFVdkIsd0JBQW9CLE1BQXlCO1FBQXpCLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBRjdDLFlBQU8sR0FBa0IsRUFBRSxDQUFDO1FBR3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUM1QyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7UUFFOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUMsSUFBSSxDQUFDLElBQUksR0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFNUYsQ0FBQztJQUVNLDJCQUFFLEdBQVQsVUFBVSxNQUFNLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBRTFELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFDLE1BQU0sR0FBQyxRQUFRLEdBQUMsVUFBVSxDQUFDLENBQUM7UUFDakQsRUFBRSxDQUFBLENBQUMsTUFBTSxJQUFHLEVBQUUsSUFBSSxVQUFVLElBQUcsRUFBRSxJQUFJLElBQUksSUFBRyxFQUFFLElBQUksSUFBSSxJQUFHLEVBQUUsSUFBSSxRQUFRLElBQUcsRUFBRSxJQUFJLEtBQUssSUFBRyxFQUFFO1lBQzFGLEdBQUcsSUFBRyxFQUFHLENBQUMsQ0FBQSxDQUFDO1lBQ1AsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDNUIsQ0FBQztRQUNELElBQUksQ0FBQSxDQUFDO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDhCQUFLLEdBQVosVUFBYSxNQUFjO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUUsTUFBTSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUNILHFCQUFDO0FBQUQsQ0FBQyxBQWhERCxJQWdEQztBQWhEWSxjQUFjO0lBUDFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsK0JBQStCO1FBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO0tBQzlDLENBQUM7cUNBWThCLGdDQUFpQjtHQVZwQyxjQUFjLENBZ0QxQjtBQWhEWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nUGFyYW1zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJkaWFsb2ctc2hpcHBpbmdcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJkaWFsb2dzaGlwcGluZy5jb21wb25lbnQuaHRtbFwiLFxyXG4gICAgc3R5bGVVcmxzOiBbXCJkaWFsb2dzaGlwcGluZy5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGlhbG9nU2hpcHBpbmcge1xyXG4gICAgZG9vcm5vOiBzdHJpbmc7XHJcbiAgICBzdHJlZXRuYW1lOnN0cmluZztcclxuICAgIGFyZWE6c3RyaW5nO1xyXG4gICAgY2l0eTpzdHJpbmc7XHJcbiAgICBkaXN0cmljdDpzdHJpbmc7XHJcbiAgICBzdGF0ZTpzdHJpbmc7XHJcbiAgICBwaW46c3RyaW5nO1xyXG4gICAgYWRkcmVzczogQXJyYXk8c3RyaW5nPiA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcykge1xyXG4gICAgICAgIHRoaXMuZG9vcm5vID0gcGFyYW1zLmNvbnRleHQuZG9vcm5vO1xyXG4gICAgICAgIHRoaXMuc3RyZWV0bmFtZSA9IHBhcmFtcy5jb250ZXh0LnN0cmVldG5hbWU7XHJcbiAgICAgICAgdGhpcy5hcmVhID0gcGFyYW1zLmNvbnRleHQuYXJlYTtcclxuICAgICAgICB0aGlzLmNpdHkgPSBwYXJhbXMuY29udGV4dC5jaXR5O1xyXG4gICAgICAgIHRoaXMuZGlzdHJpY3QgPSBwYXJhbXMuY29udGV4dC5kaXN0cmljdDtcclxuICAgICAgICB0aGlzLnN0YXRlID0gcGFyYW1zLmNvbnRleHQuc3RhdGU7XHJcbiAgICAgICAgdGhpcy5waW4gPSBwYXJhbXMuY29udGV4dC5waW47XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiQWRkcmVzcyBpbiBEaWFsb2c6IFwiICsgdGhpcy5kb29ybm8gKyB0aGlzLnN0cmVldG5hbWUrdGhpcy5hcmVhKyB0aGlzLmNpdHkpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb2soZG9vcm5vLCBzdHJlZXRuYW1lLCBhcmVhLCBjaXR5LCBkaXN0cmljdCwgc3RhdGUsIHBpbikge1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIlZBbDE6IFwiK2Rvb3JubytcIlZBTDI6IFwiK3N0cmVldG5hbWUpO1xyXG4gICAgICAgIGlmKGRvb3Jubz09IFwiXCIgfHwgc3RyZWV0bmFtZT09IFwiXCIgfHwgYXJlYT09IFwiXCIgfHwgY2l0eT09IFwiXCIgfHwgZGlzdHJpY3Q9PSBcIlwiIHx8IHN0YXRlPT0gXCJcIiB8fCBcclxuICAgICAgICBwaW49PSBcIlwiICl7XHJcbiAgICAgICAgICAgIGFsZXJ0KFwiRW50ZXIgVmFsdWUhISFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVzMTogXCIrdGhpcy5kb29ybm8rXCJSZXMyOiBcIit0aGlzLnN0cmVldG5hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MucHVzaChkb29ybm8pO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MucHVzaChzdHJlZXRuYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5hZGRyZXNzLnB1c2goYXJlYSk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5wdXNoKGNpdHkpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZHJlc3MucHVzaChkaXN0cmljdCk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkcmVzcy5wdXNoKHN0YXRlKTtcclxuICAgICAgICAgICAgdGhpcy5hZGRyZXNzLnB1c2gocGluKTtcclxuICAgICAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjayh0aGlzLmFkZHJlc3MpOyAgICAgXHJcbiAgICAgICAgfSAgIFxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlKHJlc3VsdDogc3RyaW5nKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIkNhbmNlbGxlZFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwiUmVzOiBcIisgcmVzdWx0KTtcclxuICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2socmVzdWx0KTtcclxuICB9XHJcbn0iXX0=