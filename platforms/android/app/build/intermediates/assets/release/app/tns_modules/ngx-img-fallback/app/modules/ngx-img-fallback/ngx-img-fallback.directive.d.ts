/**
 * Created by vadimdez on 28/01/2018.
 */
import { ElementRef, EventEmitter, OnDestroy, Renderer2 } from '@angular/core';
export declare class ImgFallbackDirective implements OnDestroy {
    private el;
    private renderer;
    imgSrc: string;
    loaded: EventEmitter<boolean>;
    private nativeElement;
    private isApplied;
    private ERROR_EVENT_TYPE;
    private LOAD_EVENT_TYPE;
    private cancelOnError;
    private cancelOnLoad;
    constructor(el: ElementRef, renderer: Renderer2);
    ngOnDestroy(): void;
    private onError();
    private onLoad();
    private removeErrorEvent();
    private removeOnLoadEvent();
    private addEvents();
}
