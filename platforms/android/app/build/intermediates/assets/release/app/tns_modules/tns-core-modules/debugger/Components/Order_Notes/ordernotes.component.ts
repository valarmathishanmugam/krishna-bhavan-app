import { Component, Input } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { ActivatedRoute } from '@angular/router';
import { GetOrder } from '../../Services/getorder.service';
import { loginService } from '../../Services/login.service';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";

@Component({
    selector: "order-notes",
    moduleId: module.id,
    templateUrl: "ordernotes.component.html",
    styleUrls: ["ordernotes.component.css"]
})

export class OrderNotes{
    @Input() orderid;

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page,
        private getorderservice: GetOrder,
        private route: ActivatedRoute,
        private loginservice: loginService
      ) {
        console.log("Notes Data1: " +this.orderid)
        this.getorderservice.getorderbyid(this.orderid)
        .subscribe(data => this.getorderdetails(data.json()));
      }

      ngOnInit() {

        
          //var tempcustomer = this.loginservice.getScope();
  
          application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            this.routerExtensions.backToPreviousPage();
          });
      }

      getorderdetails(data) {
        console.log("Notes Data")
        if(data.notes){
            console.log("Notes Length: " + data.length)
        }
      }
}