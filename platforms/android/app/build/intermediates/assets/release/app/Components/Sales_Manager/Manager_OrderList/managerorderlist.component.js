"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var orderapproval_service_1 = require("../../../Services/orderapproval.service");
var customer_model_1 = require("../../../Models/customer.model");
var order_model_1 = require("../../../Models/order.model");
var getorder_service_1 = require("../../../Services/getorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var dialogs_1 = require("ui/dialogs");
var page_1 = require("ui/page");
var common_1 = require("@angular/common");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ManagerOrderList = (function () {
    function ManagerOrderList(routerExtensions, getorder, orderapproval, page, location) {
        this.routerExtensions = routerExtensions;
        this.getorder = getorder;
        this.orderapproval = orderapproval;
        this.page = page;
        this.location = location;
        this.title = "Order List";
        this.orderDetails = [];
        this.allorder = new Array();
        this.original = new Array();
        this.selectedIndex = null;
        this.loader = new LoadingIndicator();
    }
    ManagerOrderList.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = false;
        //this.orderDetails.push({rep:"Ramasamy",id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
        this.getorder
            .getmanagerorders(this.id)
            .subscribe(function (data) { return _this.fetchallorders(data.json()); }, function (err) { return console.log("Getting Manager orders error: " + err); });
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });           
        this.items = new nativescript_drop_down_1.ValueList();
        this.items.push({
            value: "all",
            display: "All Orders"
        }, {
            value: "not_reviewed",
            display: "Waiting For Approval"
        }, {
            value: "hold",
            display: "Hold Orders"
        }, {
            value: "approved",
            display: "Pending File"
        }, {
            value: "completed",
            display: "Dispatched Orders"
        });
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
    };
    ManagerOrderList.prototype.fetchallorders = function (data) {
        this.loader.hide();
        console.log("All manager orders" + data.length);
        for (var i = 0; i < data.length; i++) {
            // if(this.tempuser["role"] === "admin"){
            //     this.order = new Order();
            //     this.order._id = data[i]._id.$oid;
            //     this.order.order_id = data[i].order_id;
            //     this.order.order_date = data[i].order_date;
            //     this.order.total_amount = data[i].total_amount; 
            //     this.customer = new Customer();
            //     this.customer.shopname = data[i].shop_name;
            //     this.order.customer = this.customer;                    
            //     var tempstatus = data[i].status;
            // }
            // else {
            this.order = new order_model_1.Order();
            this.order._id = data[i].id.$oid;
            this.order.order_id = data[i].order_id;
            this.order.order_date = data[i].date;
            //this.tempdate = data[i].order_date;
            //this.order.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();               
            this.order.total_amount = data[i].total_amount;
            this.customer = new customer_model_1.Customer();
            this.customer.shopname = data[i].shop_name;
            this.order.customer = this.customer;
            var tempstatus = data[i].status;
            // }
            if (tempstatus.toLowerCase() === "hold") {
                this.order.status = "~/Images/hold.png";
                this.order.approve = "visible";
            }
            else if (tempstatus.toLowerCase() === "pending") {
                this.order.status = "~/Images/not_reviewed.png";
                this.order.approve = "visible";
            }
            else if (tempstatus.toLowerCase() === "processed") {
                this.order.status = "~/Images/completed.png";
                this.order.approve = "hidden";
            }
            else if (tempstatus.toLowerCase() === "approved") {
                this.order.status = "~/Images/approved.png";
                this.order.approve = "hidden";
            }
            this.allorder.push(this.order);
            this.original.push(this.order);
            this.allorder.reverse();
            //this.allorder.filter(result=> result.order_date)
        }
    };
    ManagerOrderList.prototype.approveorder = function (id, index) {
        //this.routerExtensions.navigate(["/managerorderreview"]);
        var _this = this;
        var options = {
            title: "Approve Order",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                console.log("Approve Status before: " + _this.allorder[index]["status"]);
                _this.updatestatus(index);
                var approval = "approved";
                _this.orderapproval.approveorder(id, approval, _this.id);
            }
            else {
                return;
            }
        });
    };
    ManagerOrderList.prototype.updatestatus = function (index) {
        this.approvein = "hidden";
        this.statusin = "~/Images/approved.png";
        this.allorder[index]["status"] = this.statusin;
        this.allorder[index]["approve"] = this.approvein;
        console.log("Approve Status after: " + this.allorder[index]["status"]);
    };
    ManagerOrderList.prototype.orderreview = function (id, status, order_id) {
        if (status == "~/Images/approved.png") {
            this.routerExtensions.navigateByUrl("orderdetail/" + id + "/" + order_id);
        }
        else if (status == "~/Images/completed.png") {
            this.routerExtensions.navigateByUrl("orderdetail/" + id + "/" + order_id);
        }
        else if (status == "~/Images/not_reviewed.png") {
            this.routerExtensions.navigateByUrl("managerorderreview/" + id);
        }
        else if (status == "~/Images/hold.png") {
            this.routerExtensions.navigateByUrl("managerorderreview/" + id);
        }
    };
    ManagerOrderList.prototype.onchange = function (args) {
        console.log("Changed");
        console.log("Changed Value: " + this.items.getValue(args.newIndex));
        if (this.items.getValue(args.newIndex) === "all") {
            this.allorder = this.original;
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "not_reviewed") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/not_reviewed.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "hold") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/hold.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "approved") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/approved.png"); });
            this.allorder.reverse();
        }
        else if (this.items.getValue(args.newIndex) === "completed") {
            this.allorder = this.original.filter(function (result) { return result.status.includes("~/Images/completed.png"); });
            this.allorder.reverse();
        }
    };
    ManagerOrderList.prototype.onopen = function () {
        console.log("Opened");
    };
    ManagerOrderList.prototype.onclose = function () {
        console.log("Closed");
    };
    return ManagerOrderList;
}());
ManagerOrderList = __decorate([
    core_1.Component({
        selector: "manager-order-list",
        moduleId: module.id,
        templateUrl: "managerorderlist.component.html",
        styleUrls: ["managerorderlist.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, getorder_service_1.GetOrder,
        orderapproval_service_1.OrderApproval, page_1.Page, common_1.Location])
], ManagerOrderList);
exports.ManagerOrderList = ManagerOrderList;
