import { Component, Input } from '@angular/core';
@Component({
    selector: "customer-info",
    moduleId: module.id,
    templateUrl: "customerinfo.component.html",
    styleUrls: ["customerinfo.component.css"]
})

export class CustomerInfo{

    address: object;
    @Input() customerid;
    

    ngOnInit(){
        console.log("InfoID"+ this.customerid);
        this.address = ({shop:"Kanniga Parameshwari Store", gst:"ABCD12345698", area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
      
    }

}