"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var http_1 = require("@angular/http");
var order_model_1 = require("../../Models/order.model");
var login_service_1 = require("../../Services/login.service");
var reports_service_1 = require("../../Services/reports.service");
var ReportDetail = (function () {
    function ReportDetail(page, location, routerExtensions, loginservice, http, reportservice) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.loginservice = loginservice;
        this.http = http;
        this.reportservice = reportservice;
        this.big_total_count = "";
        this.big_total_amount = "";
        this.reports = new Array();
        this.detailvisible = "visible";
        this.bigvisible = "collapse";
    }
    ReportDetail.prototype.ngOnInit = function () {
        var _this = this;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
        this.tempuser = this.loginservice.getScope();
        this.getuserreport();
        this.getdate = this.reportservice.getdate();
        this.setfromdate = this.getdate["from"];
        this.settodate = this.getdate["to"];
        console.log("Tempfromdate: " + this.setfromdate);
    };
    ReportDetail.prototype.getuserreport = function () {
        var _this = this;
        if (this.tempuser.role === "manager") {
            //console.log("Manager Id: " + this.tempuser.id)
            //alert("Manager Id: " + this.tempuser.id)
            this.reportservice.managerReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.getmanagerreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Manager report Error: " + error);
            });
        }
        else if (this.tempuser.role === "rep") {
            //console.log("Rep Id: " + this.tempuser.id)
            //alert("Rep Id: " + this.tempuser.id)
            this.reportservice.repReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.salesrepreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Rep report Error: " + error);
            });
        }
        else if (this.tempuser.role === "customer") {
            //console.log("Customer Id: " + this.tempuser.id)
            //alert("Customer Id: " + this.tempuser.id)
            this.reportservice.customerReport(this.tempuser.id)
                .subscribe(function (result) {
                _this.customerreport(result.json());
                //console.log("Customer Result: " + JSON.stringify(result))
            }, function (error) {
                console.log("Customer Error: " + error);
            });
        }
    };
    ReportDetail.prototype.getmanagerreport = function (result) {
        console.log("Manager Result: " + JSON.stringify(result));
        console.log("Dummy Result: " + result);
        console.log("Manager Report Details: " + result.salesrep_orders);
        //alert("Manager Result: " + JSON.stringify(result))
        this.bigvisible = "visible";
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for (var i = 0; i < result.salesrep_orders.length; i++) {
            //console.log("Manager Report Details: " + result.salesrep_orders.length)
            //alert("Manager Report Details: " + result.salesrep_orders.length)
            this.reportdetail = new order_model_1.Report();
            this.reportdetail.emp_id = result.salesrep_orders[i].salesrep_id;
            this.reportdetail.order_count = result.salesrep_orders[i].order_count;
            this.reportdetail.order_total = result.salesrep_orders[i].order_total;
            this.reports.push(this.reportdetail);
        }
    };
    ReportDetail.prototype.salesrepreport = function (result) {
        //console.log("rep Result: " + JSON.stringify(result));
        //alert("rep Result: " + JSON.stringify(result))
        this.bigvisible = "visible";
        this.detailvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
        for (var i = 0; i < result.customer_orders_details.length; i++) {
            console.log("Rep Report Details: " + result.customer_orders_details.length);
            //alert("Manager Report Details: " + result.customer_orders_details.length)
            this.reportdetail = new order_model_1.Report();
            this.reportdetail.emp_id = result.customer_orders_details[i].customer_id;
            this.reportdetail.order_count = result.customer_orders_details[i].order_count;
            this.reportdetail.order_total = result.customer_orders_details[i].order_total;
            this.reports.push(this.reportdetail);
        }
    };
    ReportDetail.prototype.customerreport = function (result) {
        console.log("customer Result: " + JSON.stringify(result));
        //alert("customer Result: " + JSON.stringify(result))+++++++++
        this.bigvisible = "visible";
        this.big_total_count = result.order_count;
        this.big_total_amount = result.order_total;
    };
    ReportDetail.prototype.goback = function () {
        //alert("Back Back...")
        this.routerExtensions.navigate(["/salesrepreport"]);
    };
    return ReportDetail;
}());
ReportDetail = __decorate([
    core_1.Component({
        selector: "report-detail",
        moduleId: module.id,
        templateUrl: "reportdetail.component.html",
        styleUrls: ["reportdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions,
        login_service_1.loginService, http_1.Http, reports_service_1.ReportService])
], ReportDetail);
exports.ReportDetail = ReportDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwb3J0ZGV0YWlsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJlcG9ydGRldGFpbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsc0RBQXFEO0FBQ3JELDJDQUFzRjtBQUN0Rix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBQzNDLHNEQUErRDtBQUUvRCxzQ0FBd0Q7QUFDeEQsd0RBQWtEO0FBQ2xELDhEQUE0RDtBQUM1RCxrRUFBK0Q7QUFTN0QsSUFBYSxZQUFZO0lBa0J2QixzQkFBb0IsSUFBVSxFQUFVLFFBQWtCLEVBQVUsZ0JBQWtDLEVBQzVGLFlBQTBCLEVBQVUsSUFBVSxFQUFVLGFBQTRCO1FBRDFFLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUM1RixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQVI5RixvQkFBZSxHQUFFLEVBQUUsQ0FBQztRQUNwQixxQkFBZ0IsR0FBRSxFQUFFLENBQUM7UUFDckIsWUFBTyxHQUFhLElBQUksS0FBSyxFQUFFLENBQUM7UUFFaEMsa0JBQWEsR0FBRyxTQUFTLENBQUE7UUFDekIsZUFBVSxHQUFHLFVBQVUsQ0FBQTtJQUd5RSxDQUFDO0lBRWpHLCtCQUFRLEdBQVI7UUFBQSxpQkFjQztRQWJDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDNUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzVDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUdELG9DQUFhLEdBQWI7UUFBQSxpQkF5Q0M7UUF4Q0csRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUEsQ0FBQztZQUNqQyxnREFBZ0Q7WUFDaEQsMENBQTBDO1lBRTFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDO2lCQUNqRCxTQUFTLENBQUMsVUFBQyxNQUFNO2dCQUNkLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQTtnQkFDcEMsMkRBQTJEO1lBQzdELENBQUMsRUFBRSxVQUFDLEtBQUs7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsR0FBRyxLQUFLLENBQUMsQ0FBQTtZQUMvQyxDQUFDLENBQUMsQ0FBQztRQUVILENBQUM7UUFFRCxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUEsQ0FBQztZQUNwQyw0Q0FBNEM7WUFDNUMsc0NBQXNDO1lBRXRDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDO2lCQUM3QyxTQUFTLENBQUMsVUFBQyxNQUFNO2dCQUNkLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUE7Z0JBQ2xDLDJEQUEyRDtZQUM3RCxDQUFDLEVBQUUsVUFBQyxLQUFLO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDLENBQUE7WUFDM0MsQ0FBQyxDQUFDLENBQUM7UUFFSCxDQUFDO1FBQ0wsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFBLENBQUM7WUFDekMsaURBQWlEO1lBQ2pELDJDQUEyQztZQUUzQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztpQkFDbEQsU0FBUyxDQUFDLFVBQUMsTUFBTTtnQkFDZCxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO2dCQUNsQywyREFBMkQ7WUFDN0QsQ0FBQyxFQUFFLFVBQUMsS0FBSztnQkFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxDQUFBO1lBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBRUgsQ0FBQztJQUNmLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEIsVUFBaUIsTUFBTTtRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN6RCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ2hFLG9EQUFvRDtRQUNwRCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztRQUMvQixJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDMUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDM0MsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO1lBQ2pELHlFQUF5RTtZQUN4RSxtRUFBbUU7WUFDbkUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG9CQUFNLEVBQUUsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUNqRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUN0RSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUV0RSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkMsQ0FBQztJQUNILENBQUM7SUFFRCxxQ0FBYyxHQUFkLFVBQWUsTUFBTTtRQUNuQix1REFBdUQ7UUFDdkQsZ0RBQWdEO1FBQ2hELElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFBO1FBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUMxQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUMzQyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQztZQUN4RCxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUMzRSwyRUFBMkU7WUFDM0UsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG9CQUFNLEVBQUUsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQ3pFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFDOUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUU5RSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkMsQ0FBQztJQUNILENBQUM7SUFDRCxxQ0FBYyxHQUFkLFVBQWUsTUFBTTtRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUMxRCw4REFBOEQ7UUFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUE7UUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQzFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQzdDLENBQUM7SUFFRCw2QkFBTSxHQUFOO1FBQ0UsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUNQLG1CQUFDO0FBQUQsQ0FBQyxBQXBJQyxJQW9JRDtBQXBJYyxZQUFZO0lBTjFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsZUFBZTtRQUN6QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLDZCQUE2QjtRQUMxQyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQztLQUMxQyxDQUFDO3FDQW1CMEIsV0FBSSxFQUFvQixpQkFBUSxFQUE0Qix5QkFBZ0I7UUFDOUUsNEJBQVksRUFBZ0IsV0FBSSxFQUF5QiwrQkFBYTtHQW5CbkYsWUFBWSxDQW9JMUI7QUFwSWMsb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRGF0ZVBpY2tlciB9IGZyb20gXCJ1aS9kYXRlLXBpY2tlclwiO1xyXG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgeyBSZXBvcnQgfSBmcm9tICcuLi8uLi9Nb2RlbHMvb3JkZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tICcuLi8uLi9TZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUmVwb3J0U2VydmljZSB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL3JlcG9ydHMuc2VydmljZSc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJyZXBvcnQtZGV0YWlsXCIsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6IFwicmVwb3J0ZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcInJlcG9ydGRldGFpbC5jb21wb25lbnQuY3NzXCJdXHJcbiAgfSlcclxuICBleHBvcnQgY2xhc3MgUmVwb3J0RGV0YWlse1xyXG4gICAgZGF0ZTogRGF0ZTtcclxuICAgIGZyb21kYXRlOiBEYXRlO1xyXG4gICAgdG9kYXRlOiBEYXRlO1xyXG4gICAgaXNvZnJvbWRhdGU7XHJcbiAgICBpc290b2RhdGU7XHJcbiAgICBnZXRkYXRlOiBib29sZWFuIHwgYW55O1xyXG4gICAgc2V0ZnJvbWRhdGU7XHJcbiAgICBzZXR0b2RhdGU7XHJcbiAgICBkYXRhO1xyXG4gICAgdGVtcHVzZXI7XHJcbiAgICBiaWdfdG90YWxfY291bnQ9IFwiXCI7XHJcbiAgICBiaWdfdG90YWxfYW1vdW50ID1cIlwiO1xyXG4gICAgcmVwb3J0czogUmVwb3J0W10gPSBuZXcgQXJyYXkoKTtcclxuICAgIHJlcG9ydGRldGFpbDogUmVwb3J0OyBcclxuICAgIGRldGFpbHZpc2libGUgPSBcInZpc2libGVcIlxyXG4gICAgYmlndmlzaWJsZSA9IFwiY29sbGFwc2VcIlxyXG4gIFxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgICBwcml2YXRlIGxvZ2luc2VydmljZTogbG9naW5TZXJ2aWNlLCBwcml2YXRlIGh0dHA6IEh0dHAsIHByaXZhdGUgcmVwb3J0c2VydmljZTogUmVwb3J0U2VydmljZSl7fVxyXG4gIFxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgIGFwcGxpY2F0aW9uLmFuZHJvaWQub24oQW5kcm9pZEFwcGxpY2F0aW9uLmFjdGl2aXR5QmFja1ByZXNzZWRFdmVudCwgKGRhdGE6IEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhKSA9PiB7XHJcbiAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAvL3RoaXMubG9jYXRpb24uYmFjaygpO1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMudGVtcHVzZXIgPSB0aGlzLmxvZ2luc2VydmljZS5nZXRTY29wZSgpO1xyXG4gICAgICB0aGlzLmdldHVzZXJyZXBvcnQoKTtcclxuXHJcbiAgICAgIHRoaXMuZ2V0ZGF0ZSA9IHRoaXMucmVwb3J0c2VydmljZS5nZXRkYXRlKCk7XHJcbiAgICAgIHRoaXMuc2V0ZnJvbWRhdGUgPSB0aGlzLmdldGRhdGVbXCJmcm9tXCJdXHJcbiAgICAgIHRoaXMuc2V0dG9kYXRlID0gdGhpcy5nZXRkYXRlW1widG9cIl1cclxuICAgICAgY29uc29sZS5sb2coXCJUZW1wZnJvbWRhdGU6IFwiICsgdGhpcy5zZXRmcm9tZGF0ZSlcclxuICAgIH1cclxuXHJcblxyXG4gICAgZ2V0dXNlcnJlcG9ydCgpe1xyXG4gICAgICAgIGlmKHRoaXMudGVtcHVzZXIucm9sZSA9PT0gXCJtYW5hZ2VyXCIpe1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiTWFuYWdlciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgICAgICAvL2FsZXJ0KFwiTWFuYWdlciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG5cclxuICAgICAgICAgICAgdGhpcy5yZXBvcnRzZXJ2aWNlLm1hbmFnZXJSZXBvcnQodGhpcy50ZW1wdXNlci5pZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldG1hbmFnZXJyZXBvcnQocmVzdWx0Lmpzb24oKSlcclxuICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJDdXN0b21lciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSlcclxuICAgICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTWFuYWdlciByZXBvcnQgRXJyb3I6IFwiICsgZXJyb3IpXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAgICAgICBlbHNlIGlmKHRoaXMudGVtcHVzZXIucm9sZSA9PT0gXCJyZXBcIil7XHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiUmVwIElkOiBcIiArIHRoaXMudGVtcHVzZXIuaWQpXHJcbiAgICAgICAgICAgICAgICAvL2FsZXJ0KFwiUmVwIElkOiBcIiArIHRoaXMudGVtcHVzZXIuaWQpXHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXBvcnRzZXJ2aWNlLnJlcFJlcG9ydCh0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYWxlc3JlcHJlcG9ydChyZXN1bHQuanNvbigpKVxyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJDdXN0b21lciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSlcclxuICAgICAgICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJSZXAgcmVwb3J0IEVycm9yOiBcIiArIGVycm9yKVxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgZWxzZSBpZih0aGlzLnRlbXB1c2VyLnJvbGUgPT09IFwiY3VzdG9tZXJcIil7XHJcbiAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiQ3VzdG9tZXIgSWQ6IFwiICsgdGhpcy50ZW1wdXNlci5pZClcclxuICAgICAgICAgICAgICAgIC8vYWxlcnQoXCJDdXN0b21lciBJZDogXCIgKyB0aGlzLnRlbXB1c2VyLmlkKVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcG9ydHNlcnZpY2UuY3VzdG9tZXJSZXBvcnQodGhpcy50ZW1wdXNlci5pZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VzdG9tZXJyZXBvcnQocmVzdWx0Lmpzb24oKSlcclxuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiQ3VzdG9tZXIgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpXHJcbiAgICAgICAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ3VzdG9tZXIgRXJyb3I6IFwiICsgZXJyb3IpXHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0bWFuYWdlcnJlcG9ydChyZXN1bHQpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiTWFuYWdlciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJEdW1teSBSZXN1bHQ6IFwiICsgcmVzdWx0KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIk1hbmFnZXIgUmVwb3J0IERldGFpbHM6IFwiICsgcmVzdWx0LnNhbGVzcmVwX29yZGVycylcclxuICAgICAgICAvL2FsZXJ0KFwiTWFuYWdlciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSlcclxuICAgICAgICB0aGlzLmJpZ3Zpc2libGUgPSBcInZpc2libGVcIjtcclxuICAgICAgICB0aGlzLmRldGFpbHZpc2libGUgPSBcInZpc2libGVcIjtcclxuICAgICAgICB0aGlzLmJpZ190b3RhbF9jb3VudCA9IHJlc3VsdC5vcmRlcl9jb3VudDtcclxuICAgICAgICB0aGlzLmJpZ190b3RhbF9hbW91bnQgPSByZXN1bHQub3JkZXJfdG90YWw7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7aTxyZXN1bHQuc2FsZXNyZXBfb3JkZXJzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgLy9jb25zb2xlLmxvZyhcIk1hbmFnZXIgUmVwb3J0IERldGFpbHM6IFwiICsgcmVzdWx0LnNhbGVzcmVwX29yZGVycy5sZW5ndGgpXHJcbiAgICAgICAgICAvL2FsZXJ0KFwiTWFuYWdlciBSZXBvcnQgRGV0YWlsczogXCIgKyByZXN1bHQuc2FsZXNyZXBfb3JkZXJzLmxlbmd0aClcclxuICAgICAgICAgIHRoaXMucmVwb3J0ZGV0YWlsID0gbmV3IFJlcG9ydCgpO1xyXG4gICAgICAgICAgdGhpcy5yZXBvcnRkZXRhaWwuZW1wX2lkID0gcmVzdWx0LnNhbGVzcmVwX29yZGVyc1tpXS5zYWxlc3JlcF9pZDtcclxuICAgICAgICAgIHRoaXMucmVwb3J0ZGV0YWlsLm9yZGVyX2NvdW50ID0gcmVzdWx0LnNhbGVzcmVwX29yZGVyc1tpXS5vcmRlcl9jb3VudDtcclxuICAgICAgICAgIHRoaXMucmVwb3J0ZGV0YWlsLm9yZGVyX3RvdGFsID0gcmVzdWx0LnNhbGVzcmVwX29yZGVyc1tpXS5vcmRlcl90b3RhbDtcclxuICAgICAgXHJcbiAgICAgICAgICB0aGlzLnJlcG9ydHMucHVzaCh0aGlzLnJlcG9ydGRldGFpbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBzYWxlc3JlcHJlcG9ydChyZXN1bHQpe1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJyZXAgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpO1xyXG4gICAgICAgIC8vYWxlcnQoXCJyZXAgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpXHJcbiAgICAgICAgdGhpcy5iaWd2aXNpYmxlID0gXCJ2aXNpYmxlXCJcclxuICAgICAgICB0aGlzLmRldGFpbHZpc2libGUgPSBcInZpc2libGVcIjtcclxuICAgICAgICB0aGlzLmJpZ190b3RhbF9jb3VudCA9IHJlc3VsdC5vcmRlcl9jb3VudDtcclxuICAgICAgICB0aGlzLmJpZ190b3RhbF9hbW91bnQgPSByZXN1bHQub3JkZXJfdG90YWw7XHJcbiAgICAgICAgZm9yKHZhciBpPTA7aTxyZXN1bHQuY3VzdG9tZXJfb3JkZXJzX2RldGFpbHMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJSZXAgUmVwb3J0IERldGFpbHM6IFwiICsgcmVzdWx0LmN1c3RvbWVyX29yZGVyc19kZXRhaWxzLmxlbmd0aClcclxuICAgICAgICAgIC8vYWxlcnQoXCJNYW5hZ2VyIFJlcG9ydCBEZXRhaWxzOiBcIiArIHJlc3VsdC5jdXN0b21lcl9vcmRlcnNfZGV0YWlscy5sZW5ndGgpXHJcbiAgICAgICAgICB0aGlzLnJlcG9ydGRldGFpbCA9IG5ldyBSZXBvcnQoKTtcclxuICAgICAgICAgIHRoaXMucmVwb3J0ZGV0YWlsLmVtcF9pZCA9IHJlc3VsdC5jdXN0b21lcl9vcmRlcnNfZGV0YWlsc1tpXS5jdXN0b21lcl9pZDtcclxuICAgICAgICAgIHRoaXMucmVwb3J0ZGV0YWlsLm9yZGVyX2NvdW50ID0gcmVzdWx0LmN1c3RvbWVyX29yZGVyc19kZXRhaWxzW2ldLm9yZGVyX2NvdW50O1xyXG4gICAgICAgICAgdGhpcy5yZXBvcnRkZXRhaWwub3JkZXJfdG90YWwgPSByZXN1bHQuY3VzdG9tZXJfb3JkZXJzX2RldGFpbHNbaV0ub3JkZXJfdG90YWw7XHJcbiAgICAgIFxyXG4gICAgICAgICAgdGhpcy5yZXBvcnRzLnB1c2godGhpcy5yZXBvcnRkZXRhaWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBjdXN0b21lcnJlcG9ydChyZXN1bHQpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiY3VzdG9tZXIgUmVzdWx0OiBcIiArIEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpO1xyXG4gICAgICAgIC8vYWxlcnQoXCJjdXN0b21lciBSZXN1bHQ6IFwiICsgSlNPTi5zdHJpbmdpZnkocmVzdWx0KSkrKysrKysrKytcclxuICAgICAgICB0aGlzLmJpZ3Zpc2libGUgPSBcInZpc2libGVcIlxyXG4gICAgICAgIHRoaXMuYmlnX3RvdGFsX2NvdW50ID0gcmVzdWx0Lm9yZGVyX2NvdW50O1xyXG4gICAgICAgIHRoaXMuYmlnX3RvdGFsX2Ftb3VudCA9IHJlc3VsdC5vcmRlcl90b3RhbDtcclxuICAgICAgfVxyXG5cclxuICAgICAgZ29iYWNrKCl7XHJcbiAgICAgICAgLy9hbGVydChcIkJhY2sgQmFjay4uLlwiKVxyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvc2FsZXNyZXByZXBvcnRcIl0pO1xyXG4gICAgICB9XHJcbn0iXX0=