"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_model_1 = require("../../Models/customer.model");
var customer_service_1 = require("../../Services/customer.service");
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ApproveCustomerList = (function () {
    function ApproveCustomerList(customerservice, page, location, routerExtensions) {
        this.customerservice = customerservice;
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.customers = new Array();
        this.loader = new LoadingIndicator();
    }
    ApproveCustomerList.prototype.ngOnInit = function () {
        var _this = this;
        //code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        //code for loading indicator end here
        this.getcustomer();
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    ApproveCustomerList.prototype.getcustomer = function () {
        var _this = this;
        this.customerservice.getunapprovedcustomer()
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
    };
    ApproveCustomerList.prototype.parseCustomers = function (data) {
        this.loader.hide();
        //console.log("data: " + JSON.stringify(data));
        console.log(data[0]._id.$oid);
        for (var i = 0; i < data.length; i++) {
            this.customer = new customer_model_1.Customer();
            this.customer.id = data[i]._id.$oid;
            this.customer.shopname = data[i].shop_name;
            this.customer.contactname = data[i].name;
            this.customers.push(this.customer);
        }
    };
    return ApproveCustomerList;
}());
ApproveCustomerList = __decorate([
    core_1.Component({
        selector: "approve-customer-list",
        moduleId: module.id,
        templateUrl: "approve_customer_list.component.html",
        styleUrls: ["approve_customer_list.component.css"]
    }),
    __metadata("design:paramtypes", [customer_service_1.CustomerService, page_1.Page, common_1.Location, router_1.RouterExtensions])
], ApproveCustomerList);
exports.ApproveCustomerList = ApproveCustomerList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwcm92ZV9jdXN0b21lcl9saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcHJvdmVfY3VzdG9tZXJfbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4REFBdUQ7QUFDdkQsb0VBQWtFO0FBQ2xFLHNDQUEwQztBQUMxQyxzREFBcUQ7QUFDckQsMkNBQXNGO0FBQ3RGLHlDQUEyQztBQUMzQywwQ0FBMkM7QUFDM0Msc0RBQStEO0FBQy9ELElBQUksZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7QUFTdEYsSUFBYSxtQkFBbUI7SUFNNUIsNkJBQXFCLGVBQWdDLEVBQVUsSUFBVSxFQUFVLFFBQWtCLEVBQVUsZ0JBQWtDO1FBQTVILG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUoxSSxjQUFTLEdBQWUsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUUzQyxXQUFNLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRSxDQUFDO0lBSWhDLENBQUM7SUFFQyxzQ0FBUSxHQUFSO1FBQUEsaUJBa0NDO1FBakNHLHVDQUF1QztRQUN2QyxJQUFJLE9BQU8sR0FBRztZQUNaLE9BQU8sRUFBRSxZQUFZO1lBQ3JCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixVQUFVLEVBQUUsS0FBSztnQkFDakIsR0FBRyxFQUFFLEdBQUc7Z0JBQ1Isb0JBQW9CLEVBQUUsU0FBUztnQkFDL0IscUJBQXFCLEVBQUUsSUFBSTtnQkFDM0IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLGlCQUFpQixFQUFFLENBQUM7YUFDckI7WUFDRCxHQUFHLEVBQUU7Z0JBQ0gsT0FBTyxFQUFFLHlCQUF5QjtnQkFDbEMsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsYUFBYSxFQUFFLElBQUk7Z0JBQ25CLEtBQUssRUFBRSxTQUFTO2dCQUNoQixJQUFJLEVBQUMsRUFBRSxDQUFDLGlDQUFpQzthQUMxQztTQUNGLENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQixxQ0FBcUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDMUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQUEsaUJBR0Q7UUFGRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFO2FBQzNDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBQ0QsNENBQWMsR0FBZCxVQUFlLElBQVM7UUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNuQiwrQ0FBK0M7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTlCLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBRWxDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSx5QkFBUSxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUE7WUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztZQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBRXpDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QyxDQUFDO0lBRUwsQ0FBQztJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQWxFRCxJQWtFQztBQWxFWSxtQkFBbUI7SUFQL0IsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx1QkFBdUI7UUFDakMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSxzQ0FBc0M7UUFDbkQsU0FBUyxFQUFFLENBQUMscUNBQXFDLENBQUM7S0FDckQsQ0FBQztxQ0FRd0Msa0NBQWUsRUFBZ0IsV0FBSSxFQUFvQixpQkFBUSxFQUE0Qix5QkFBZ0I7R0FOeEksbUJBQW1CLENBa0UvQjtBQWxFWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDdXN0b21lciB9IGZyb20gJy4uLy4uL01vZGVscy9jdXN0b21lci5tb2RlbCc7XHJcbmltcG9ydCB7IEN1c3RvbWVyU2VydmljZSB9IGZyb20gJy4uLy4uL1NlcnZpY2VzL2N1c3RvbWVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlJztcclxuaW1wb3J0IHsgQW5kcm9pZEFwcGxpY2F0aW9uLCBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvbiBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxudmFyIExvYWRpbmdJbmRpY2F0b3IgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWxvYWRpbmctaW5kaWNhdG9yLW5ld1wiKS5Mb2FkaW5nSW5kaWNhdG9yO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJhcHByb3ZlLWN1c3RvbWVyLWxpc3RcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJhcHByb3ZlX2N1c3RvbWVyX2xpc3QuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wiYXBwcm92ZV9jdXN0b21lcl9saXN0LmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHByb3ZlQ3VzdG9tZXJMaXN0e1xyXG4gICAgXHJcbiAgICBwdWJsaWMgY3VzdG9tZXJzOiBDdXN0b21lcltdID0gbmV3IEFycmF5KCk7XHJcbiAgICBwdWJsaWMgY3VzdG9tZXI6IEN1c3RvbWVyO1xyXG4gICAgbG9hZGVyID0gbmV3IExvYWRpbmdJbmRpY2F0b3IoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcHJpdmF0ZSBjdXN0b21lcnNlcnZpY2U6IEN1c3RvbWVyU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKXtcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG4gICAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICAgIC8vY29kZSBmb3IgbG9hZGluZyBpbmRpY2F0b3Igc3RhcnQgaGVyZVxyXG4gICAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdMb2FkaW5nLi4uJyxcclxuICAgICAgICAgICAgcHJvZ3Jlc3M6IDAuNjUsXHJcbiAgICAgICAgICAgIGFuZHJvaWQ6IHtcclxuICAgICAgICAgICAgICBpbmRldGVybWluYXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgIGNhbmNlbGFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIG1heDogMTAwLFxyXG4gICAgICAgICAgICAgIHByb2dyZXNzTnVtYmVyRm9ybWF0OiBcIiUxZC8lMmRcIixcclxuICAgICAgICAgICAgICBwcm9ncmVzc1BlcmNlbnRGb3JtYXQ6IDAuNTMsXHJcbiAgICAgICAgICAgICAgcHJvZ3Jlc3NTdHlsZTogMSxcclxuICAgICAgICAgICAgICBzZWNvbmRhcnlQcm9ncmVzczogMVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBpb3M6IHtcclxuICAgICAgICAgICAgICBkZXRhaWxzOiBcIkFkZGl0aW9uYWwgZGV0YWlsIG5vdGUhXCIsXHJcbiAgICAgICAgICAgICAgc3F1YXJlOiBmYWxzZSxcclxuICAgICAgICAgICAgICBtYXJnaW46IDEwLFxyXG4gICAgICAgICAgICAgIGRpbUJhY2tncm91bmQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgY29sb3I6IFwiIzRCOUVENlwiLFxyXG4gICAgICAgICAgICAgIG1vZGU6XCJcIiAvLyBzZWUgaU9TIHNwZWNpZmljIG9wdGlvbnMgYmVsb3dcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMubG9hZGVyLnNob3cob3B0aW9ucyk7IFxyXG4gICAgICAgICAgLy9jb2RlIGZvciBsb2FkaW5nIGluZGljYXRvciBlbmQgaGVyZVxyXG4gICAgICAgIHRoaXMuZ2V0Y3VzdG9tZXIoKTtcclxuICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2tUb1ByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgICAgfSk7ICAgICBcclxuICAgICAgICAgIFxyXG4gICAgICB9XHJcblxyXG4gICAgICBnZXRjdXN0b21lcigpIHtcclxuICAgICAgICB0aGlzLmN1c3RvbWVyc2VydmljZS5nZXR1bmFwcHJvdmVkY3VzdG9tZXIoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnBhcnNlQ3VzdG9tZXJzKGRhdGEuanNvbigpKSk7XHJcbiAgICB9IFxyXG4gICAgcGFyc2VDdXN0b21lcnMoZGF0YTogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXIuaGlkZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJkYXRhOiBcIiArIEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhWzBdLl9pZC4kb2lkKTtcclxuICAgIFxyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICBcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lciA9IG5ldyBDdXN0b21lcigpO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbWVyLmlkID0gZGF0YVtpXS5faWQuJG9pZFxyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbWVyLnNob3BuYW1lID0gZGF0YVtpXS5zaG9wX25hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG5hbWUgPSBkYXRhW2ldLm5hbWU7XHJcbiAgICBcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21lcnMucHVzaCh0aGlzLmN1c3RvbWVyKTtcclxuICAgICAgICB9ICBcclxuICAgIFxyXG4gICAgfVxyXG59Il19