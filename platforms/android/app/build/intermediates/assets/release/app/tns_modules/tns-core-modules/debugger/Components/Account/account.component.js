"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var router_1 = require("nativescript-angular/router");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var AccountComponent = (function () {
    function AccountComponent(routerExtensions, page, location, http) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.http = http;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = false;
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            //this.exitApp(data.cancel);
        });
    };
    return AccountComponent;
}());
AccountComponent = __decorate([
    core_1.Component({
        selector: "account-comp",
        moduleId: module.id,
        templateUrl: "account.component.html",
        styleUrls: ["account.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page, common_1.Location,
        http_1.Http])
], AccountComponent);
exports.AccountComponent = AccountComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhY2NvdW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHNDQUFvRjtBQUNwRixzREFBcUQ7QUFDckQsc0RBQStEO0FBTS9ELDJDQUdxQjtBQUNyQix5Q0FBMkM7QUFDM0MsMENBQTJDO0FBQzNDLHNDQUF3RDtBQVF4RCxJQUFhLGdCQUFnQjtJQUUzQiwwQkFBb0IsZ0JBQWtDLEVBQVUsSUFBVSxFQUFVLFFBQWtCLEVBQzVGLElBQVU7UUFEQSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDNUYsU0FBSSxHQUFKLElBQUksQ0FBTTtJQUVsQixDQUFDO0lBRUgsbUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUNsQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FDcEIsZ0NBQWtCLENBQUMsd0JBQXdCLEVBQzNDLFVBQUMsSUFBeUM7WUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2Qiw0QkFBNEI7UUFDOUIsQ0FBQyxDQUNGLENBQUM7SUFFSixDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQUFDLEFBcEJELElBb0JDO0FBcEJZLGdCQUFnQjtJQU41QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7UUFDeEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSx3QkFBd0I7UUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7S0FDckMsQ0FBQztxQ0FHc0MseUJBQWdCLEVBQWdCLFdBQUksRUFBb0IsaUJBQVE7UUFDdEYsV0FBSTtHQUhULGdCQUFnQixDQW9CNUI7QUFwQlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCB7Q29tcG9uZW50LE9uSW5pdCxBZnRlclZpZXdJbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWZ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdGVsZXJpay11aS9zaWRlZHJhd2VyXCI7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXRlbGVyaWstdWkvc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gXCJ1aS90ZXh0LWZpZWxkXCI7XHJcbmltcG9ydCB7IFRvb2xUaXAgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXRvb2x0aXBcIjtcclxuaW1wb3J0IHsgY29uZmlybSB9IGZyb20gXCJ1aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7XHJcbiAgQW5kcm9pZEFwcGxpY2F0aW9uLFxyXG4gIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhXHJcbn0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcclxuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiYWNjb3VudC1jb21wXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCJhY2NvdW50LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJhY2NvdW50LmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIEFjY291bnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXHJcbiAgICBwcml2YXRlIGh0dHA6IEh0dHApIHtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gZmFsc2U7XHJcbiAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKFxyXG4gICAgICBBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50LFxyXG4gICAgICAoZGF0YTogQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEpID0+IHtcclxuICAgICAgICBkYXRhLmNhbmNlbCA9IHRydWU7XHJcbiAgICAgICAgLy9hbGVydChcIkJhY2sgUHJlc3NlZFwiKVxyXG4gICAgICAgIC8vdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICAgICAgLy90aGlzLmV4aXRBcHAoZGF0YS5jYW5jZWwpO1xyXG4gICAgICB9XHJcbiAgICApOyAgIFxyXG4gICBcclxuICB9IFxyXG59XHJcbiJdfQ==