import { Component, Input } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { CustomerService } from "../../../Services/customer.service";
import { Customer, Address } from "../../../Models/customer.model";
import { Page } from "tns-core-modules/ui/page/page";
import { SearchBar } from "ui/search-bar";
import { isAndroid } from "platform";
import {
  AndroidApplication,
  AndroidActivityBackPressedEventData
} from "application";
import * as application from "application";
import { Location } from "@angular/common";
import { loginService } from "../../../Services/login.service";
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;


@Component({
  selector: "Customer-List",
  moduleId: module.id,
  templateUrl: "CustomerList.component.html",
  styleUrls: ["CustomerList.component.css"]
})
export class CustomerList {
  title: string;
  customerDetails: Array<Object> = [];
  public customers: Customer[] = new Array();
  public customer: Customer;
  public currentdate: Date;
  public original: Customer[] = new Array();
  public tempuser;
  public repnamevisible = "visible";
  loader = new LoadingIndicator();
  // enableMessage = 'collapse';
  constructor(
    private routerExtensions: RouterExtensions,
    private customerservice: CustomerService,
    private page: Page,
    private location: Location,
    private loginservice: loginService
  ) {}

  ngOnInit() {
    //code for loading indicator start here
   let options = {
      message: 'Loading...',
      progress: 0.65,
      android: {
        indeterminate: true,
        cancelable: false,
        max: 100,
        progressNumberFormat: "%1d/%2d",
        progressPercentFormat: 0.53,
        progressStyle: 1,
        secondaryProgress: 1
      },
      ios: {
        details: "Additional detail note!",
        square: false,
        margin: 10,
        dimBackground: true,
        color: "#4B9ED6",
        mode:"" // see iOS specific options below
      }
    };
     
    this.loader.show(options);
    //code for loading indicator end here
    this.title = "Customer List";
    this.page.actionBarHidden = false;
    this.getcustomer();
    this.currentdate = new Date();
    console.log(
      this.currentdate.getDate() +
        "/" +
        (this.currentdate.getMonth() + 1) +
        "/" +
        this.currentdate.getFullYear()
    );

    application.android.on(
      AndroidApplication.activityBackPressedEvent,
      (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        //alert("Back Pressed")
        //this.location.back();
        this.routerExtensions.backToPreviousPage();
      }
    );

    this.tempuser = this.loginservice.getScope();
    if (this.tempuser["role"] === "rep") {
      this.repnamevisible = "collapse";
    }
  }

  getcustomer() {
    this.customerservice
      .getcustomer()
      .subscribe(data => this.parseCustomers(data.json()));
  }

  parseCustomers(data: any) {    
    //console.log("data: " + JSON.stringify(data));
    //console.log("Customer list: "+"length: "+ data.length + JSON.stringify(data));


  //  commented by valarmathi
    // for (let i = 0; i < data.length; i++) {
    //   this.customer = new Customer();
    //   this.customer.id = data[i].id.$oid;
    //   if (data[i].customer_name) {
    //     this.customer.contactname = data[i].customer_name;
    //   } else if (data[i].name) {
    //     this.customer.contactname = data[i].name;
    //   }
    //   if (data[i].slaesrep_name) {
    //     this.customer.salesrepname = data[i].slaesrep_name;
    //   }
    //   this.customer.contactnumber = data[i].mobile_no;
    //   this.customer.shopname = data[i].shopname;

    //   console.log(
    //     "cus_id: " +
    //       this.customer.id +
    //       "cus_name: " +
    //       this.customer.shopname +
    //       "rep: " +
    //       this.customer.contactname +
    //       "mobile: " +
    //       this.customer.contactnumber
    //   );
    //   this.customers.push(this.customer);
    //   this.original.push(this.customer);
    // }

    // created by valarmathi
  //   if (typeof data === 'string') {
  //     console.log("THER IS NO DATAS");
  //     this.loader.hide();
  //     this.enableMessage = 'visible';
  // }
  // else { 
    // console.log("THER IS DATAS"); 
    this.loader.hide();
    // this.enableMessage = 'collapse';
    for(let i = 0; i < data.length; i++){
      this.customer = new Customer();
      this.customer.id = data[i]._id.$oid;
      this.customer.contactname = data[i].name;
      this.customer.contactnumber = data[i].primary_mobile_no;
      this.customer.shopname = data[i].shop_name;
      this.customer.address = new Address();
     
      this.customer.address.city = data[i].address[0].city;
      this.customer.address.district = data[i].address[0].district;
      console.log(
            "cus_id: " +
              this.customer.id +
              "cus_name: " +
              this.customer.shopname +
              "rep: " +
              this.customer.contactname +
              "mobile: " +
              this.customer.contactnumber
          );
          this.customers.push(this.customer);
          this.original.push(this.customer);

    }
  //  }
  }

  public onSubmit(args) {
    let searchBar = <SearchBar>args.object;
    searchBar.dismissSoftInput();
    let searchValue = searchBar.text.toLowerCase();
    this.customers = this.original.filter(
      result =>
        result.contactname.toLowerCase().includes(searchValue) ||
        result.shopname.toLowerCase().includes(searchValue)
    );
  }

  public onTextChanged(args) {
    let searchBar = <SearchBar>args.object;
    let searchValue = searchBar.text.toLowerCase();
    console.log("searchValue: " + searchValue);
    // alert(searchValue);
    this.customers = this.original.filter(
      result =>
        result.contactname.toLowerCase().includes(searchValue) ||
        result.shopname.toLowerCase().includes(searchValue)
    );
  }

  public onClear(args) {
    let searchBar = <SearchBar>args.object;
    searchBar.dismissSoftInput();
    searchBar.text = "";
  }

  searchBarLoaded(args) {
    console.log("search bar loaded");
    let searchBar = <SearchBar>args.object;
    if (isAndroid) {
      searchBar.android.clearFocus();
    }
  }
}
