import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    selector: "dialog-shipping",
    moduleId: module.id,
    templateUrl: "dialogshipping.component.html",
    styleUrls: ["dialogshipping.component.css"]
})

export class DialogShipping {
    doorno: string;
    streetname:string;
    area:string;
    city:string;
    district:string;
    state:string;
    pin:string;
    address: Array<string> = [];

    constructor(private params: ModalDialogParams) {
        this.doorno = params.context.doorno;
        this.streetname = params.context.streetname;
        this.area = params.context.area;
        this.city = params.context.city;
        this.district = params.context.district;
        this.state = params.context.state;
        this.pin = params.context.pin;

        console.log("Address in Dialog: " + this.doorno + this.streetname+this.area+ this.city);

    }

    public ok(doorno, streetname, area, city, district, state, pin) {

        console.log("VAl1: "+doorno+"VAL2: "+streetname);
        if(doorno== "" || streetname== "" || area== "" || city== "" || district== "" || state== "" || 
        pin== "" ){
            alert("Enter Value!!!");
        }
        else{
            console.log("Res1: "+this.doorno+"Res2: "+this.streetname);
            this.address.push(doorno);
            this.address.push(streetname);
            this.address.push(area);
            this.address.push(city);
            this.address.push(district);
            this.address.push(state);
            this.address.push(pin);
            this.params.closeCallback(this.address);     
        }   
  }

  public close(result: string) {
    console.log("Cancelled");
    console.log("Res: "+ result);
    this.params.closeCallback(result);
  }
}