"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ManagerCustomerDetail = (function () {
    function ManagerCustomerDetail() {
        this.title = "Customer Detail";
        this.name = "PunniyaMoorthy";
        this.shop = "Kanniga Parameshwari Store";
        this.rep = "Ramasamy";
        this.region = "Thiruvallur Region";
    }
    ManagerCustomerDetail.prototype.ngOnInit = function () {
        this.address = ({ area: "No.123, opp. Eldam's Road, AnnaSalai", city: "Chennai", pincode: "611111", mobile: "9874563210" });
    };
    return ManagerCustomerDetail;
}());
ManagerCustomerDetail = __decorate([
    core_1.Component({
        selector: "manager-customer-detail",
        moduleId: module.id,
        templateUrl: "managercustomerdetail.component.html",
        styleUrls: ["managercustomerdetail.component.css"]
    })
], ManagerCustomerDetail);
exports.ManagerCustomerDetail = ManagerCustomerDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlcmN1c3RvbWVyZGV0YWlsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1hbmFnZXJjdXN0b21lcmRldGFpbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFRMUMsSUFBYSxxQkFBcUI7SUFQbEM7UUFTSSxVQUFLLEdBQVcsaUJBQWlCLENBQUM7UUFFbEMsU0FBSSxHQUFXLGdCQUFnQixDQUFDO1FBQ2hDLFNBQUksR0FBVyw0QkFBNEIsQ0FBQztRQUM1QyxRQUFHLEdBQVEsVUFBVSxDQUFDO1FBQ3RCLFdBQU0sR0FBUSxvQkFBb0IsQ0FBQTtJQUt0QyxDQUFDO0lBSkcsd0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFDLElBQUksRUFBRSxzQ0FBc0MsRUFBRSxJQUFJLEVBQUMsU0FBUyxFQUFFLE9BQU8sRUFBQyxRQUFRLEVBQUUsTUFBTSxFQUFDLFlBQVksRUFBQyxDQUFDLENBQUE7SUFDdEgsQ0FBQztJQUVULDRCQUFDO0FBQUQsQ0FBQyxBQVpELElBWUM7QUFaWSxxQkFBcUI7SUFQakMsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx5QkFBeUI7UUFDbkMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSxzQ0FBc0M7UUFDbkQsU0FBUyxFQUFFLENBQUMscUNBQXFDLENBQUM7S0FDckQsQ0FBQztHQUVXLHFCQUFxQixDQVlqQztBQVpZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwibWFuYWdlci1jdXN0b21lci1kZXRhaWxcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJtYW5hZ2VyY3VzdG9tZXJkZXRhaWwuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wibWFuYWdlcmN1c3RvbWVyZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBNYW5hZ2VyQ3VzdG9tZXJEZXRhaWx7XHJcblxyXG4gICAgdGl0bGU6IHN0cmluZyA9IFwiQ3VzdG9tZXIgRGV0YWlsXCI7XHJcbiAgICBhZGRyZXNzOiBvYmplY3Q7XHJcbiAgICBuYW1lOiBzdHJpbmcgPSBcIlB1bm5peWFNb29ydGh5XCI7XHJcbiAgICBzaG9wOiBzdHJpbmcgPSBcIkthbm5pZ2EgUGFyYW1lc2h3YXJpIFN0b3JlXCI7XHJcbiAgICByZXA6c3RyaW5nPVwiUmFtYXNhbXlcIjtcclxuICAgIHJlZ2lvbjpzdHJpbmc9XCJUaGlydXZhbGx1ciBSZWdpb25cIlxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICB0aGlzLmFkZHJlc3MgPSAoe2FyZWE6IFwiTm8uMTIzLCBvcHAuIEVsZGFtJ3MgUm9hZCwgQW5uYVNhbGFpXCIsIGNpdHk6XCJDaGVubmFpXCIsIHBpbmNvZGU6XCI2MTExMTFcIiwgbW9iaWxlOlwiOTg3NDU2MzIxMFwifSlcclxuICAgICAgICB9XHJcblxyXG59Il19