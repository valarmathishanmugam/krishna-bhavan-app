import { RadSideDrawerComponent } from 'nativescript-telerik-ui/sidedrawer/angular';
import { AfterViewInit, ChangeDetectorRef, Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Router, NavigationEnd } from "@angular/router";
import * as connectivity from 'connectivity';
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from 'nativescript-telerik-ui/sidedrawer';
import { Page } from 'tns-core-modules/ui/page';

@Component({
    selector: "action-bar",
    moduleId: module.id,
    templateUrl: "actionbar.component.html",
    styles: [`
    ActionBar {
        color: #fff;
    }
    `]
})

export class ActionBar implements OnInit, AfterViewInit {

    @Input() actionbartitle;
    
    constructor(@Inject(Page) private _page: Page, private _router: Router, private routerExtensions: RouterExtensions, private _changeDetectionRef: ChangeDetectorRef){
        _page.on("loaded", this.onLoaded, this);
    }
  
    orderdetails(){
        this.routerExtensions.navigate(["/managerorderlist"]);
    }

    private _mainContentText: string;
    
        @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
        private drawer: RadSideDrawer;
        private _sideDrawerTransition: DrawerTransitionBase;
    
        ngAfterViewInit() {
            this.drawer = this.drawerComponent.sideDrawer;
            this._changeDetectionRef.detectChanges();
        }
    
        ngOnInit() {
            this.mainContentText = "SideDrawer for NativeScript can be easily setup in the HTML definition of your page by defining tkDrawerContent and tkMainContent. The component has a default transition and position and also exposes notifications related to changes in its state. Swipe from left to open side drawer.";
            this._router.events.subscribe(e => {
              if (e instanceof NavigationEnd) {
                this.drawer.closeDrawer();
              }
            });
        
        }
    
        get mainContentText() {
            return this._mainContentText;
        }
    
        set mainContentText(value: string) {
            this._mainContentText = value;
        }
    
        public openDrawer() {
            this.drawer.showDrawer();
        }
    
        public onCloseDrawerTap() {
           this.drawer.closeDrawer();
        }

        public onLoaded(args) {
            this._sideDrawerTransition = new SlideInOnTopTransition();
        }
}