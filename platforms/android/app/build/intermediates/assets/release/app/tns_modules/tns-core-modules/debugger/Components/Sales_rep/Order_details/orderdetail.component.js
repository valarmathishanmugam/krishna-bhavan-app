"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var order_model_1 = require("../../../Models/order.model");
var customer_model_1 = require("../../../Models/customer.model");
var getorder_service_1 = require("../../../Services/getorder.service");
var product_model_1 = require("../../../Models/product.model");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var router_2 = require("@angular/router");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var OrderDetail = (function () {
    function OrderDetail(routerExtensions, page, getorderservice, route, loginservice, location) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.getorderservice = getorderservice;
        this.route = route;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Order Review";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.allnotes = new Array();
    }
    OrderDetail.prototype.ngOnInit = function () {
        var _this = this;
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        var id = this.route.snapshot.params["id"];
        this.formattedOrderId = this.route.snapshot.params["id2"];
        this.getorderservice
            .getorderbyid(id)
            .subscribe(function (data) { return _this.getallorders(data.json()); });
        var tempcustomer = this.loginservice.getScope();
        this.checkcustomer(tempcustomer);
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    OrderDetail.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            this.ifcustomer = "visible";
        }
    };
    OrderDetail.prototype.getallorders = function (data) {
        console.log("All Data: " + JSON.stringify(data));
        console.log("products.length: " + data[0].line_items.length);
        //data = data._body;
        this.order_id = data[0]._id.$oid;
        this.order_date = data[0].order_date;
        /*
        console.log("Actual Date: " + this.tempdate);
        this.order_date = new Date();
        this.order_date = this.tempdate.getDate();
        console.log("Changed Date: " + this.order_date);
        */
        //this.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
        var tempstatus = data[0].status;
        this.customer.shopname = data[0].shop_name;
        console.log("shopname: " + this.customer.shopname);
        this.customer.contactname = data[0].customer_name;
        this.address.shippingmode = data[0].preferred_shipping;
        this.address.area = data[0].shipping_address.area;
        this.address.city = data[0].shipping_address.city;
        this.address.door_no = data[0].shipping_address.door_no;
        this.address.pin = data[0].shipping_address.pin;
        this.address.street_name = data[0].shipping_address.street_name;
        this.address.district = data[0].shipping_address.district;
        this.address.state = data[0].shipping_address.state;
        this.customer.address = this.address;
        this.total_amount = data[0].total_amount;
        if (data[0].notes) {
            //console.log("My Notes")
            console.log(data[0].customer_name + " : " + data[0].notes.length);
            for (var a = 0; a < data[0].notes.length; a++) {
                this.notes = new order_model_1.Note();
                console.log("My Notes: " + data[0].notes[a].note);
                this.notes.created_date = data[0].notes[a].created_date;
                this.notes.note = data[0].notes[a].note;
                this.notes.created_by = data[0].notes[a].created_by;
                this.allnotes.push(this.notes);
            }
            if (data[0].notes.length == 0) {
                if (data[0].current_note) {
                    this.notes = new order_model_1.Note();
                    console.log("Current Notes: " + data[0].current_note.note);
                    this.notes.created_date = data[0].current_note.created_date;
                    this.notes.note = data[0].current_note.note;
                    this.notes.created_by = data[0].current_note.created_by;
                    this.allnotes.push(this.notes);
                }
                else { }
            }
        }
        // else{}
        for (var i = 0; i < data[0].line_items.length; i++) {
            this.product = new product_model_1.Product();
            // this.product.offer = new Offer(); // commented by valar for hide discount
            this.product.product_id = data[0].line_items[i].product_id.$oid;
            console.log("productname: " + data[0].line_items[i].product_name);
            this.product.sku_id = data[0].line_items[i].sku_id.$oid;
            this.product.product_name = data[0].line_items[i].product_name;
            this.product.line_total = data[0].line_items[i].line_total;
            this.product.rack_quantity = data[0].line_items[i].rack_quantity;
            this.product.quantity = data[0].line_items[i].quantity;
            // commented by valar for hide discount - start
            // if(data[0].line_items[i].discount_id){
            //   var nodiscount = false;
            //   this.product.discount_id = data[0].line_items[i].discount_id.$oid;
            //   if(data[0].line_items[i].discount_details){
            //   this.product.discount_product_quantity = data[0].line_items[i].discount_product_quantity;
            //   this.product.offer.discount_product_name = data[0].line_items[i].discount_details[1].discount_product_name;
            //   }
            // }
            // else{
            //   var nodiscount = true;
            // }
            // commented by valar for hide discount - end
            this.product.product_price = data[0].line_items[i].product_price;
            this.allproducts.push(this.product);
        }
        console.log("All Product details: " + JSON.stringify(this.productDetails));
        if (tempstatus.toLowerCase() === "hold") {
            console.log("status: Hold");
            this.order_status = "~/Images/hold.png";
        }
        else if (tempstatus.toLowerCase() === "pending") {
            console.log("status: not_reviewed");
            this.order_status = "~/Images/not_reviewed.png";
        }
        else if (tempstatus.toLowerCase() === "processed") {
            console.log("status: completed");
            this.order_status = "~/Images/completed.png";
        }
        else if (tempstatus.toLowerCase() === "approved") {
            console.log("status: approved");
            this.order_status = "~/Images/approved.png";
        }
    };
    return OrderDetail;
}());
OrderDetail = __decorate([
    core_1.Component({
        selector: "order-detail",
        moduleId: module.id,
        templateUrl: "orderdetail.component.html",
        styleUrls: ["orderdetail.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        page_1.Page,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        login_service_1.loginService,
        common_1.Location])
], OrderDetail);
exports.OrderDetail = OrderDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXJkZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3JkZXJkZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUVBQStEO0FBQy9ELDJEQUEwRDtBQUMxRCxpRUFBbUU7QUFDbkUsdUVBQThEO0FBQzlELCtEQUE4RDtBQUM5RCxzQ0FBMEM7QUFDMUMsc0RBQStEO0FBQy9ELGdDQUErQjtBQUMvQiwwQ0FBaUQ7QUFDakQsMkNBQXNGO0FBQ3RGLHlDQUEyQztBQUMzQywwQ0FBMkM7QUFTM0MsSUFBYSxXQUFXO0lBdUJwQixxQkFDVSxnQkFBa0MsRUFDbEMsSUFBVSxFQUNWLGVBQXlCLEVBQ3pCLEtBQXFCLEVBQ3JCLFlBQTBCLEVBQzFCLFFBQWtCO1FBTGxCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLG9CQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLGFBQVEsR0FBUixRQUFRLENBQVU7UUEzQjVCLFVBQUssR0FBVyxjQUFjLENBQUM7UUFDL0IsYUFBUSxHQUFHLElBQUkseUJBQVEsRUFBRSxDQUFDO1FBQzFCLFlBQU8sR0FBRyxJQUFJLHdCQUFPLEVBQUUsQ0FBQztRQUN4QixtQkFBYyxHQUFrQixFQUFFLENBQUM7UUFPNUIsZ0JBQVcsR0FBYyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBUXJDLGFBQVEsR0FBVyxJQUFJLEtBQUssRUFBRSxDQUFDO0lBVW5DLENBQUM7SUFFSiw4QkFBUSxHQUFSO1FBQUEsaUJBa0JDO1FBakJDLDRKQUE0SjtRQUM1Six3SEFBd0g7UUFDeEgsbUpBQW1KO1FBQ25KLElBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxlQUFlO2FBQ2pCLFlBQVksQ0FBQyxFQUFFLENBQUM7YUFDaEIsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO1FBQ25ELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUE7UUFDL0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUVqQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxnQ0FBa0IsQ0FBQyx3QkFBd0IsRUFBRSxVQUFDLElBQXlDO1lBQzVHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsbUNBQWEsR0FBYixVQUFjLElBQUk7UUFDZCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFBLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUE7UUFDaEMsQ0FBQztRQUNELElBQUksQ0FBQSxDQUFDO1lBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUE7UUFDL0IsQ0FBQztJQUNMLENBQUM7SUFFRCxrQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNqRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDN0Qsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO1FBQ3RDOzs7OztVQUtFO1FBQ0oseUVBQXlFO1FBQ3ZFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7UUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztRQUNsRCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO1FBQ3hELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUM7UUFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztRQUNoRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1FBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7UUFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7UUFFekMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUM7WUFDaEIseUJBQXlCO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUNqRSxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxrQkFBSSxFQUFFLENBQUM7Z0JBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQ2hELElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO2dCQUN4RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7Z0JBRXBELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxDQUFDO1lBQ0QsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDOUIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFBLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxrQkFBSSxFQUFFLENBQUM7b0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQTtvQkFDekQsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUM7b0JBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO29CQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztvQkFFeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQyxDQUFDO2dCQUNELElBQUksQ0FBQSxDQUFDLENBQUEsQ0FBQztZQUNSLENBQUM7UUFDRCxDQUFDO1FBQ0QsU0FBUztRQUlMLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNuRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksdUJBQU8sRUFBRSxDQUFDO1lBRTdCLDRFQUE0RTtZQUU1RSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDeEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7WUFDL0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFDM0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDakUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDckQsK0NBQStDO1lBRWpELHlDQUF5QztZQUN6Qyw0QkFBNEI7WUFDNUIsdUVBQXVFO1lBQ3ZFLGdEQUFnRDtZQUNoRCw4RkFBOEY7WUFDOUYsZ0hBQWdIO1lBQ2hILE1BQU07WUFDTixJQUFJO1lBQ0osUUFBUTtZQUNSLDJCQUEyQjtZQUMzQixJQUFJO1lBRUYsNkNBQTZDO1lBRS9DLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDO1lBRWpFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV0QyxDQUFDO1FBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBO1FBRTFFLEVBQUUsQ0FBQSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFlBQVksR0FBRyxtQkFBbUIsQ0FBQTtRQUN6QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQSxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsWUFBWSxHQUFHLDJCQUEyQixDQUFBO1FBQ2pELENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxLQUFLLFdBQVcsQ0FBQyxDQUFBLENBQUM7WUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsd0JBQXdCLENBQUE7UUFDOUMsQ0FBQztRQUNELElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEtBQUssVUFBVSxDQUFDLENBQUEsQ0FBQztZQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLFlBQVksR0FBRyx1QkFBdUIsQ0FBQTtRQUM3QyxDQUFDO0lBQ1AsQ0FBQztJQUVMLGtCQUFDO0FBQUQsQ0FBQyxBQTVLRCxJQTRLQztBQTVLWSxXQUFXO0lBUHZCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsY0FBYztRQUN4QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLDRCQUE0QjtRQUN6QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztLQUMzQyxDQUFDO3FDQTBCOEIseUJBQWdCO1FBQzVCLFdBQUk7UUFDTywyQkFBUTtRQUNsQix1QkFBYztRQUNQLDRCQUFZO1FBQ2hCLGlCQUFRO0dBN0JuQixXQUFXLENBNEt2QjtBQTVLWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGxvZ2luU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL1NlcnZpY2VzL2xvZ2luLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPcmRlciwgTm90ZSB9IGZyb20gJy4uLy4uLy4uL01vZGVscy9vcmRlci5tb2RlbCc7XHJcbmltcG9ydCB7IEFkZHJlc3MsIEN1c3RvbWVyIH0gZnJvbSAnLi4vLi4vLi4vTW9kZWxzL2N1c3RvbWVyLm1vZGVsJztcclxuaW1wb3J0IHsgR2V0T3JkZXIgfSBmcm9tICcuLi8uLi8uLi9TZXJ2aWNlcy9nZXRvcmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvZHVjdCxPZmZlciB9IGZyb20gJy4uLy4uLy4uL01vZGVscy9wcm9kdWN0Lm1vZGVsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd1aS9wYWdlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBbmRyb2lkQXBwbGljYXRpb24sIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhIH0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcIm9yZGVyLWRldGFpbFwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcIm9yZGVyZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcIm9yZGVyZGV0YWlsLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBPcmRlckRldGFpbHtcclxuXHJcbiAgICB0aXRsZTogc3RyaW5nID0gXCJPcmRlciBSZXZpZXdcIjtcclxuICAgIGN1c3RvbWVyID0gbmV3IEN1c3RvbWVyKCk7XHJcbiAgICBhZGRyZXNzID0gbmV3IEFkZHJlc3MoKTtcclxuICAgIHByb2R1Y3REZXRhaWxzOiBBcnJheTxPYmplY3Q+ID0gW107XHJcbiAgICBjdXN0b21lcklkOiBhbnk7XHJcbiAgICBjdXN0b21lcmRldGFpbHM6IGFueTtcclxuICAgIHByb2R1Y3RzOiBhbnk7XHJcbiAgICBnZXRwcm9kdWN0czogYW55O1xyXG4gICAgcHVibGljIHByb2R1Y3Q6IFByb2R1Y3Q7XHJcbiAgICBwdWJsaWMgb3JkZXI6IE9yZGVyO1xyXG4gICAgcHVibGljIGFsbHByb2R1Y3RzOiBQcm9kdWN0W10gPSBuZXcgQXJyYXkoKTtcclxuICAgIHRvdGFsX2Ftb3VudDogYW55O1xyXG4gICAgb3JkZXJfZGF0ZTtcclxuICAgIHRlbXBkYXRlOiBEYXRlO1xyXG4gICAgb3JkZXJfc3RhdHVzO1xyXG4gICAgb3JkZXJfaWQ7XHJcbiAgICBwdWJsaWMgaWZjdXN0b21lcjtcclxuICAgIHB1YmxpYyBub3RlczogTm90ZTtcclxuICAgIHB1YmxpYyBhbGxub3RlczogTm90ZVtdID0gbmV3IEFycmF5KCk7XHJcbiAgICBmb3JtYXR0ZWRPcmRlcklkOiBzdHJpbmc7XHJcbiAgXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgICBwcml2YXRlIHBhZ2U6IFBhZ2UsXHJcbiAgICAgIHByaXZhdGUgZ2V0b3JkZXJzZXJ2aWNlOiBHZXRPcmRlcixcclxuICAgICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgIHByaXZhdGUgbG9naW5zZXJ2aWNlOiBsb2dpblNlcnZpY2UsIFxyXG4gICAgICBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvblxyXG4gICAgKSB7fVxyXG4gIFxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgIC8vdGhpcy5vcmRlckRldGFpbHMgPSAoe2lkOlwiYXNoMzQyODM5ODA5MzhcIiwgc2hvcDpcIkthbm5pZ2EgUGFyYW1lc2h3YXJpIFN0b3Jlc1wiLCBpbWc6XCJ+L0ltYWdlcy9ncmVlbl9kb3QucG5nXCIsIGRhdGU6XCI4LzIvMThcIiwgc2hpcHBpbmdtb2RlOlwiSy5QLk4gVHJhdmVsc1wifSlcclxuICAgICAgLy90aGlzLmFkZHJlc3MgPSAoe2FyZWE6IFwiTm8uMTIzLCBvcHAuIEVsZGFtJ3MgUm9hZCwgQW5uYVNhbGFpXCIsIGNpdHk6XCJDaGVubmFpXCIsIHBpbmNvZGU6XCI2MTExMTFcIiwgbW9iaWxlOlwiOTg3NDU2MzIxMFwifSlcclxuICAgICAgLy90aGlzLnByb2R1Y3REZXRhaWxzLnB1c2goe25hbWU6XCJLcmlzaG5hJ3MgQ3VzdGFyZCBQb3dkZXJcIiwgaWQ6IFwiaGV3cTg3ZTk4NzgyXCIsIGltZzpcInJlczovL3N0b3JlXCIsIG9mZmVyOlwiQnV5IDEgR2V0IDIgRnJlZVwiLCByYWNrOlwiNVwiLCBvcmRlcjpcIjVcIn0pXHJcbiAgICAgIGNvbnN0IGlkID0gdGhpcy5yb3V0ZS5zbmFwc2hvdC5wYXJhbXNbXCJpZFwiXTtcclxuICAgICAgdGhpcy5mb3JtYXR0ZWRPcmRlcklkID0gdGhpcy5yb3V0ZS5zbmFwc2hvdC5wYXJhbXNbXCJpZDJcIl07XHJcbiAgICAgIHRoaXMuZ2V0b3JkZXJzZXJ2aWNlXHJcbiAgICAgICAgLmdldG9yZGVyYnlpZChpZClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5nZXRhbGxvcmRlcnMoZGF0YS5qc29uKCkpKTtcclxuICAgICAgICB2YXIgdGVtcGN1c3RvbWVyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKVxyXG4gICAgICAgIHRoaXMuY2hlY2tjdXN0b21lcih0ZW1wY3VzdG9tZXIpO1xyXG5cclxuICAgICAgICBhcHBsaWNhdGlvbi5hbmRyb2lkLm9uKEFuZHJvaWRBcHBsaWNhdGlvbi5hY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnQsIChkYXRhOiBBbmRyb2lkQWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50RGF0YSkgPT4ge1xyXG4gICAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgICAgLy9hbGVydChcIkJhY2sgUHJlc3NlZFwiKVxyXG4gICAgICAgICAgLy90aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja2N1c3RvbWVyKGRhdGEpe1xyXG4gICAgICAgIGlmKGRhdGEucm9sZSA9PT0gJ2N1c3RvbWVyJyl7XHJcbiAgICAgICAgICAgIHRoaXMuaWZjdXN0b21lciA9IFwiY29sbGFwc2VcIlxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICB0aGlzLmlmY3VzdG9tZXIgPSBcInZpc2libGVcIlxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICBcclxuICAgIGdldGFsbG9yZGVycyhkYXRhKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiQWxsIERhdGE6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgICBjb25zb2xlLmxvZyhcInByb2R1Y3RzLmxlbmd0aDogXCIgKyBkYXRhWzBdLmxpbmVfaXRlbXMubGVuZ3RoKTtcclxuICAgICAgLy9kYXRhID0gZGF0YS5fYm9keTtcclxuICAgICAgdGhpcy5vcmRlcl9pZCA9IGRhdGFbMF0uX2lkLiRvaWQ7XHJcbiAgICAgIHRoaXMub3JkZXJfZGF0ZSA9ICBkYXRhWzBdLm9yZGVyX2RhdGU7XHJcbiAgICAgIC8qXHJcbiAgICAgIGNvbnNvbGUubG9nKFwiQWN0dWFsIERhdGU6IFwiICsgdGhpcy50ZW1wZGF0ZSk7XHJcbiAgICAgIHRoaXMub3JkZXJfZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgIHRoaXMub3JkZXJfZGF0ZSA9IHRoaXMudGVtcGRhdGUuZ2V0RGF0ZSgpO1xyXG4gICAgICBjb25zb2xlLmxvZyhcIkNoYW5nZWQgRGF0ZTogXCIgKyB0aGlzLm9yZGVyX2RhdGUpO1xyXG4gICAgICAqL1xyXG4gICAgLy90aGlzLm9yZGVyX2RhdGUgPSB0aGlzLnRlbXBkYXRlLmdldERhdGUoKStcIi9cIit0aGlzLnRlbXBkYXRlLmdldE1vbnRoKCk7XHJcbiAgICAgIHZhciB0ZW1wc3RhdHVzID0gZGF0YVswXS5zdGF0dXM7XHJcbiAgICAgIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUgPSBkYXRhWzBdLnNob3BfbmFtZTtcclxuICAgICAgY29uc29sZS5sb2coXCJzaG9wbmFtZTogXCIrIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUpO1xyXG4gICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RuYW1lID0gZGF0YVswXS5jdXN0b21lcl9uYW1lO1xyXG4gICAgICB0aGlzLmFkZHJlc3Muc2hpcHBpbmdtb2RlID0gZGF0YVswXS5wcmVmZXJyZWRfc2hpcHBpbmc7ICAgIFxyXG4gICAgICB0aGlzLmFkZHJlc3MuYXJlYSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5hcmVhO1xyXG4gICAgICB0aGlzLmFkZHJlc3MuY2l0eSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5jaXR5O1xyXG4gICAgICB0aGlzLmFkZHJlc3MuZG9vcl9ubyA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5kb29yX25vO1xyXG4gICAgICB0aGlzLmFkZHJlc3MucGluID0gZGF0YVswXS5zaGlwcGluZ19hZGRyZXNzLnBpbjtcclxuICAgICAgdGhpcy5hZGRyZXNzLnN0cmVldF9uYW1lID0gZGF0YVswXS5zaGlwcGluZ19hZGRyZXNzLnN0cmVldF9uYW1lO1xyXG4gICAgICB0aGlzLmFkZHJlc3MuZGlzdHJpY3QgPSBkYXRhWzBdLnNoaXBwaW5nX2FkZHJlc3MuZGlzdHJpY3Q7XHJcbiAgICAgIHRoaXMuYWRkcmVzcy5zdGF0ZSA9IGRhdGFbMF0uc2hpcHBpbmdfYWRkcmVzcy5zdGF0ZTtcclxuICAgICAgdGhpcy5jdXN0b21lci5hZGRyZXNzID0gdGhpcy5hZGRyZXNzO1xyXG4gICAgICB0aGlzLnRvdGFsX2Ftb3VudCA9IGRhdGFbMF0udG90YWxfYW1vdW50O1xyXG5cclxuICAgICAgaWYoZGF0YVswXS5ub3Rlcyl7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIk15IE5vdGVzXCIpXHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YVswXS5jdXN0b21lcl9uYW1lICsgXCIgOiBcIiArIGRhdGFbMF0ubm90ZXMubGVuZ3RoKVxyXG4gICAgICAgIGZvcihsZXQgYT0wOyBhPGRhdGFbMF0ubm90ZXMubGVuZ3RoOyBhKyspe1xyXG4gICAgICAgICAgdGhpcy5ub3RlcyA9IG5ldyBOb3RlKCk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIk15IE5vdGVzOiBcIiArZGF0YVswXS5ub3Rlc1thXS5ub3RlKVxyXG4gICAgICAgICAgdGhpcy5ub3Rlcy5jcmVhdGVkX2RhdGUgPSBkYXRhWzBdLm5vdGVzW2FdLmNyZWF0ZWRfZGF0ZTtcclxuICAgICAgICAgIHRoaXMubm90ZXMubm90ZSA9IGRhdGFbMF0ubm90ZXNbYV0ubm90ZTtcclxuICAgICAgICAgIHRoaXMubm90ZXMuY3JlYXRlZF9ieSA9IGRhdGFbMF0ubm90ZXNbYV0uY3JlYXRlZF9ieTtcclxuXHJcbiAgICAgICAgICB0aGlzLmFsbG5vdGVzLnB1c2godGhpcy5ub3Rlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKGRhdGFbMF0ubm90ZXMubGVuZ3RoID09IDApe1xyXG4gICAgICAgIGlmKGRhdGFbMF0uY3VycmVudF9ub3RlKXtcclxuICAgICAgICAgIHRoaXMubm90ZXMgPSBuZXcgTm90ZSgpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkN1cnJlbnQgTm90ZXM6IFwiICtkYXRhWzBdLmN1cnJlbnRfbm90ZS5ub3RlKVxyXG4gICAgICAgICAgICB0aGlzLm5vdGVzLmNyZWF0ZWRfZGF0ZSA9IGRhdGFbMF0uY3VycmVudF9ub3RlLmNyZWF0ZWRfZGF0ZTtcclxuICAgICAgICAgICAgdGhpcy5ub3Rlcy5ub3RlID0gZGF0YVswXS5jdXJyZW50X25vdGUubm90ZTtcclxuICAgICAgICAgICAgdGhpcy5ub3Rlcy5jcmVhdGVkX2J5ID0gZGF0YVswXS5jdXJyZW50X25vdGUuY3JlYXRlZF9ieTtcclxuICBcclxuICAgICAgICAgICAgdGhpcy5hbGxub3Rlcy5wdXNoKHRoaXMubm90ZXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle31cclxuICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC8vIGVsc2V7fVxyXG5cclxuICAgICBcclxuICBcclxuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YVswXS5saW5lX2l0ZW1zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdCA9IG5ldyBQcm9kdWN0KCk7XHJcblxyXG4gICAgICAgICAgICAvLyB0aGlzLnByb2R1Y3Qub2ZmZXIgPSBuZXcgT2ZmZXIoKTsgLy8gY29tbWVudGVkIGJ5IHZhbGFyIGZvciBoaWRlIGRpc2NvdW50XHJcblxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3QucHJvZHVjdF9pZCA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X2lkLiRvaWQ7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicHJvZHVjdG5hbWU6IFwiKyBkYXRhWzBdLmxpbmVfaXRlbXNbaV0ucHJvZHVjdF9uYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0LnNrdV9pZCA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5za3VfaWQuJG9pZDtcclxuICAgICAgICAgICAgdGhpcy5wcm9kdWN0LnByb2R1Y3RfbmFtZSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X25hbWU7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdC5saW5lX3RvdGFsID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLmxpbmVfdG90YWw7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdC5yYWNrX3F1YW50aXR5ID0gZGF0YVswXS5saW5lX2l0ZW1zW2ldLnJhY2tfcXVhbnRpdHk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZHVjdC5xdWFudGl0eSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5xdWFudGl0eTtcclxuICAgICAgICAgICAgICAvLyBjb21tZW50ZWQgYnkgdmFsYXIgZm9yIGhpZGUgZGlzY291bnQgLSBzdGFydFxyXG5cclxuICAgICAgICAgICAgLy8gaWYoZGF0YVswXS5saW5lX2l0ZW1zW2ldLmRpc2NvdW50X2lkKXtcclxuICAgICAgICAgICAgLy8gICB2YXIgbm9kaXNjb3VudCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyAgIHRoaXMucHJvZHVjdC5kaXNjb3VudF9pZCA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9pZC4kb2lkO1xyXG4gICAgICAgICAgICAvLyAgIGlmKGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9kZXRhaWxzKXtcclxuICAgICAgICAgICAgLy8gICB0aGlzLnByb2R1Y3QuZGlzY291bnRfcHJvZHVjdF9xdWFudGl0eSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5kaXNjb3VudF9wcm9kdWN0X3F1YW50aXR5O1xyXG4gICAgICAgICAgICAvLyAgIHRoaXMucHJvZHVjdC5vZmZlci5kaXNjb3VudF9wcm9kdWN0X25hbWUgPSBkYXRhWzBdLmxpbmVfaXRlbXNbaV0uZGlzY291bnRfZGV0YWlsc1sxXS5kaXNjb3VudF9wcm9kdWN0X25hbWU7XHJcbiAgICAgICAgICAgIC8vICAgfVxyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2V7XHJcbiAgICAgICAgICAgIC8vICAgdmFyIG5vZGlzY291bnQgPSB0cnVlO1xyXG4gICAgICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgICAgICAgIC8vIGNvbW1lbnRlZCBieSB2YWxhciBmb3IgaGlkZSBkaXNjb3VudCAtIGVuZFxyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnByb2R1Y3QucHJvZHVjdF9wcmljZSA9IGRhdGFbMF0ubGluZV9pdGVtc1tpXS5wcm9kdWN0X3ByaWNlO1xyXG4gIFxyXG4gICAgICAgICAgICB0aGlzLmFsbHByb2R1Y3RzLnB1c2godGhpcy5wcm9kdWN0KTsgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkFsbCBQcm9kdWN0IGRldGFpbHM6IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9kdWN0RGV0YWlscykpXHJcbiAgXHJcbiAgICAgICAgICBpZih0ZW1wc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09IFwiaG9sZFwiKXsvL29uSG9sZFxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInN0YXR1czogSG9sZFwiKTtcclxuICAgICAgICAgICAgdGhpcy5vcmRlcl9zdGF0dXMgPSBcIn4vSW1hZ2VzL2hvbGQucG5nXCJcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGVsc2UgaWYodGVtcHN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSBcInBlbmRpbmdcIil7Ly9ub3RfcmV2aWV3ZWQgYnkgc2FsZXNfbWFuYWdlclxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInN0YXR1czogbm90X3Jldmlld2VkXCIpO1xyXG4gICAgICAgICAgICB0aGlzLm9yZGVyX3N0YXR1cyA9IFwifi9JbWFnZXMvbm90X3Jldmlld2VkLnBuZ1wiICAgIFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgZWxzZSBpZih0ZW1wc3RhdHVzLnRvTG93ZXJDYXNlKCkgPT09IFwicHJvY2Vzc2VkXCIpey8vQ29tcGxldGVkXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwic3RhdHVzOiBjb21wbGV0ZWRcIik7XHJcbiAgICAgICAgICAgIHRoaXMub3JkZXJfc3RhdHVzID0gXCJ+L0ltYWdlcy9jb21wbGV0ZWQucG5nXCJcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGVsc2UgaWYodGVtcHN0YXR1cy50b0xvd2VyQ2FzZSgpID09PSBcImFwcHJvdmVkXCIpey8vQXBwcm92ZWQgJiBQcm9jZXNzaW5nXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwic3RhdHVzOiBhcHByb3ZlZFwiKTtcclxuICAgICAgICAgICAgdGhpcy5vcmRlcl9zdGF0dXMgPSBcIn4vSW1hZ2VzL2FwcHJvdmVkLnBuZ1wiXHJcbiAgICAgICAgICB9ICAgXHJcbiAgICB9XHJcbiAgXHJcbn0iXX0=