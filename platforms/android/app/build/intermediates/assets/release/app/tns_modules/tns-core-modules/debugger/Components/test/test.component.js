"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Connectivity = require("tns-core-modules/connectivity");
var router_1 = require("nativescript-angular/router");
var Test = (function () {
    function Test(zone, routerExtensions) {
        this.zone = zone;
        this.routerExtensions = routerExtensions;
        this.connectionType = "Unknown";
    }
    Test.prototype.ngOnInit = function () {
        var _this = this;
        this.connectionType = this.connectionToString(Connectivity.getConnectionType());
        Connectivity.startMonitoring(function (connectionType) {
            _this.connectionType = _this.connectionToString(connectionType);
        });
    };
    Test.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "No Connection!";
            case Connectivity.connectionType.wifi:
                return "Connected to WiFi!";
            case Connectivity.connectionType.mobile:
                return "Connected to Cellular!";
            default:
                return "Unknown";
        }
    };
    return Test;
}());
Test = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: "test.component.html"
    }),
    __metadata("design:paramtypes", [core_1.NgZone, router_1.RouterExtensions])
], Test);
exports.Test = Test;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0ZXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHNDQUEwRDtBQUUxRCw0REFBOEQ7QUFDOUQsc0RBQStEO0FBTy9ELElBQWEsSUFBSTtJQUdULGNBQTJCLElBQVksRUFBVSxnQkFBa0M7UUFBeEQsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDL0UsSUFBSSxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUM7SUFDcEMsQ0FBQztJQUVNLHVCQUFRLEdBQWY7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7UUFDaEYsWUFBWSxDQUFDLGVBQWUsQ0FBQyxVQUFBLGNBQWM7WUFDbkMsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0saUNBQWtCLEdBQXpCLFVBQTBCLGNBQXNCO1FBQzVDLE1BQU0sQ0FBQSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDcEIsS0FBSyxZQUFZLENBQUMsY0FBYyxDQUFDLElBQUk7Z0JBQ2pDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztZQUM1QixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDakMsTUFBTSxDQUFDLG9CQUFvQixDQUFDO1lBQ2hDLEtBQUssWUFBWSxDQUFDLGNBQWMsQ0FBQyxNQUFNO2dCQUNuQyxNQUFNLENBQUMsd0JBQXdCLENBQUM7WUFDcEM7Z0JBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUN6QixDQUFDO0lBQ0wsQ0FBQztJQUVMLFdBQUM7QUFBRCxDQUFDLEFBM0JMLElBMkJLO0FBM0JRLElBQUk7SUFMaEIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUscUJBQXFCO0tBQ3JDLENBQUM7cUNBS3VDLGFBQU0sRUFBNEIseUJBQWdCO0dBSDlFLElBQUksQ0EyQlo7QUEzQlEsb0JBQUkiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE5nWm9uZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5pbXBvcnQgKiBhcyBDb25uZWN0aXZpdHkgZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9jb25uZWN0aXZpdHknO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcInRlc3QuY29tcG9uZW50Lmh0bWxcIlxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRlc3QgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgcHVibGljIGNvbm5lY3Rpb25UeXBlOiBzdHJpbmc7XHJcbiAgICBcclxuICAgICAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSB6b25lOiBOZ1pvbmUsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb25UeXBlID0gXCJVbmtub3duXCI7XHJcbiAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgcHVibGljIG5nT25Jbml0KCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb25UeXBlID0gdGhpcy5jb25uZWN0aW9uVG9TdHJpbmcoQ29ubmVjdGl2aXR5LmdldENvbm5lY3Rpb25UeXBlKCkpO1xyXG4gICAgICAgICAgICBDb25uZWN0aXZpdHkuc3RhcnRNb25pdG9yaW5nKGNvbm5lY3Rpb25UeXBlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb25UeXBlID0gdGhpcy5jb25uZWN0aW9uVG9TdHJpbmcoY29ubmVjdGlvblR5cGUpOyAgXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgIHB1YmxpYyBjb25uZWN0aW9uVG9TdHJpbmcoY29ubmVjdGlvblR5cGU6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgICAgIHN3aXRjaChjb25uZWN0aW9uVHlwZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUubm9uZTpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJObyBDb25uZWN0aW9uIVwiO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUud2lmaTpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJDb25uZWN0ZWQgdG8gV2lGaSFcIjtcclxuICAgICAgICAgICAgICAgIGNhc2UgQ29ubmVjdGl2aXR5LmNvbm5lY3Rpb25UeXBlLm1vYmlsZTpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJDb25uZWN0ZWQgdG8gQ2VsbHVsYXIhXCI7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIlVua25vd25cIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgfSJdfQ==