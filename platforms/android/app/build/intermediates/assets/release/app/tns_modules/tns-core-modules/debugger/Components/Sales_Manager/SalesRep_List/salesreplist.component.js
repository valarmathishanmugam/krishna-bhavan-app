"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var SalesRepList = (function () {
    function SalesRepList(routerExtensions, page, location) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.title = "Sales Rep List";
        this.salerepdetails = [];
    }
    SalesRepList.prototype.ngOnInit = function () {
        var _this = this;
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        this.salerepdetails.push({ region: "Trichy District", name: "Parthasarathy", img: "~/Images/person.png", phone: "9874563210" });
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
    };
    SalesRepList.prototype.salesrepdetail = function () {
        this.routerExtensions.navigate(["/salesrepdetail"]);
    };
    return SalesRepList;
}());
SalesRepList = __decorate([
    core_1.Component({
        selector: "sales-rep-list",
        moduleId: module.id,
        templateUrl: "salesreplist.component.html",
        styleUrls: ["salesreplist.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page, common_1.Location])
], SalesRepList);
exports.SalesRepList = SalesRepList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNyZXBsaXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNhbGVzcmVwbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsc0RBQStEO0FBQy9ELHNEQUFxRDtBQUNyRCwyQ0FBc0Y7QUFDdEYseUNBQTJDO0FBQzNDLDBDQUEyQztBQVMzQyxJQUFhLFlBQVk7SUFLekIsc0JBQW9CLGdCQUFrQyxFQUFVLElBQVUsRUFBVSxRQUFrQjtRQUFsRixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFIbEcsVUFBSyxHQUFXLGdCQUFnQixDQUFDO1FBQ2pDLG1CQUFjLEdBQWtCLEVBQUUsQ0FBQztJQUVpRSxDQUFDO0lBRXZHLCtCQUFRLEdBQVI7UUFBQSxpQkFlQztRQWRDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1FBRTdILFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdDQUFrQixDQUFDLHdCQUF3QixFQUFFLFVBQUMsSUFBeUM7WUFDNUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsdUJBQXVCO1lBQ3ZCLHVCQUF1QjtZQUN2QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFQyxxQ0FBYyxHQUFkO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUwsbUJBQUM7QUFBRCxDQUFDLEFBNUJELElBNEJDO0FBNUJZLFlBQVk7SUFQeEIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxnQkFBZ0I7UUFDMUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSw2QkFBNkI7UUFDMUMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7S0FDNUMsQ0FBQztxQ0FPb0MseUJBQWdCLEVBQWdCLFdBQUksRUFBb0IsaUJBQVE7R0FMekYsWUFBWSxDQTRCeEI7QUE1Qlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2UnO1xyXG5pbXBvcnQgeyBBbmRyb2lkQXBwbGljYXRpb24sIEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhIH0gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcInNhbGVzLXJlcC1saXN0XCIsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6IFwic2FsZXNyZXBsaXN0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcInNhbGVzcmVwbGlzdC5jb21wb25lbnQuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2FsZXNSZXBMaXN0e1xyXG5cclxuICAgIHRpdGxlOiBzdHJpbmcgPSBcIlNhbGVzIFJlcCBMaXN0XCI7XHJcbiAgICBzYWxlcmVwZGV0YWlsczogQXJyYXk8T2JqZWN0PiA9IFtdO1xyXG5cclxuY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uKXt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gICAgdGhpcy5zYWxlcmVwZGV0YWlscy5wdXNoKHsgcmVnaW9uOlwiVHJpY2h5IERpc3RyaWN0XCIsIG5hbWU6IFwiUGFydGhhc2FyYXRoeVwiLCBpbWc6XCJ+L0ltYWdlcy9wZXJzb24ucG5nXCIsIHBob25lOlwiOTg3NDU2MzIxMFwiIH0pO1xyXG4gIFxyXG4gICAgYXBwbGljYXRpb24uYW5kcm9pZC5vbihBbmRyb2lkQXBwbGljYXRpb24uYWN0aXZpdHlCYWNrUHJlc3NlZEV2ZW50LCAoZGF0YTogQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGEpID0+IHtcclxuICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAvL2FsZXJ0KFwiQmFjayBQcmVzc2VkXCIpXHJcbiAgICAgIC8vdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgICBzYWxlc3JlcGRldGFpbCgpe1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3NhbGVzcmVwZGV0YWlsXCJdKTtcclxuICAgIH1cclxuXHJcbn0iXX0=