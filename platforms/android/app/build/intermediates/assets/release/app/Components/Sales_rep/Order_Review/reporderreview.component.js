"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var dialogshipping_component_1 = require("../../Dialog_shippingaddress/dialogshipping.component");
var dialog_component_1 = require("../../Dialog/dialog.component");
var product_service_1 = require("../../../Services/product.service");
var sendorder_service_1 = require("../../../Services/sendorder.service");
var product_model_1 = require("../../../Models/product.model");
var customer_model_1 = require("../../../Models/customer.model");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var dialogs_1 = require("ui/dialogs");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var dialogs = require("ui/dialogs");
var common_1 = require("@angular/common");
// import { Product } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';
var RepOrderReview = (function () {
    function RepOrderReview(routerExtensions, saveorderservice, page, savecustomer, sendorder, savetotal, saveproducts, modalService, viewContainerRef, savecustomerdetails, saveshipping, savenotes, loginservice, location) {
        this.routerExtensions = routerExtensions;
        this.saveorderservice = saveorderservice;
        this.page = page;
        this.savecustomer = savecustomer;
        this.sendorder = sendorder;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.savecustomerdetails = savecustomerdetails;
        this.saveshipping = saveshipping;
        this.savenotes = savenotes;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Order Review";
        //orderDetails: object;
        //address: object;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.editShipping = false;
        this.disableFAB = true;
        this.formattedData = [];
    }
    RepOrderReview.prototype.ngOnInit = function () {
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        this.products = this.saveorderservice.getScope();
        this.getproducts = this.saveproducts.getScope();
        //console.log(this.productId);
        console.log("Id: " + this.products[0]["product_id"] + "name: " + this.products[0]["product_name"] + "orderqty: " + this.products[0]["quantity"] + this.products[0]["line_total"]);
        this.customerId = this.savecustomer.getScope();
        this.customerdetails = this.savecustomerdetails.getScope();
        this.total_amount = this.savetotal.getScope();
        console.log("Customer Id: " + this.customerId);
        this.getcustomerdetails(this.customerdetails);
        this.getorder(this.products);
        var tempcustomer = this.loginservice.getScope();
        this.checkcustomer(tempcustomer);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });
    };
    RepOrderReview.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            this.ifcustomer = "visible";
        }
    };
    RepOrderReview.prototype.gotoshop = function (note, ship) {
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Is Your Shipping Address Correct?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.disableFAB = false;
                console.log("notes: " + note);
                _this.savenotes.setScope(note);
                _this.saveshipping.setScope(ship);
                _this.savecustomerdetails.setScope(_this.customer);
                _this.sendorder.placeorder();
            }
            else {
                return;
            }
        });
    };
    RepOrderReview.prototype.getcustomerdetails = function (customerdetails) {
        this.customer.shopname = customerdetails.shopname;
        this.customer.contactname = customerdetails.contactname;
        this.customer.contactnumber = customerdetails.contactnumber;
        this.address.area = customerdetails.address.area;
        this.address.city = customerdetails.address.city;
        this.address.door_no = customerdetails.address.door_no;
        this.address.pin = customerdetails.address.pin;
        this.address.street_name = customerdetails.address.street_name;
        this.address.district = customerdetails.address.district;
        this.address.state = customerdetails.address.state;
        this.customer.address = this.address;
    };
    RepOrderReview.prototype.addorder = function (customerid, productid) {
        console.log("product id in order review:" + productid);
        //this.productDetails.push({name:"Krishna's Custard Powder", id: productid, img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
    };
    RepOrderReview.prototype.getorder = function (products) {
        console.log("products.length: " + products.length);
        console.log(products);
        // commented by valar for hide discount - start
        // if(products[0].offer){
        //     products[0]["offer_quantity"] = products[0].offer.offer_quantity;
        //     products[0]["base_quantity"] = products[0].offer.base_quantity;
        //     products[0]["discount_product_name"]=products[0].offer.discount_product_name;
        // }
        // commented by valar for hide discount - end
        for (var i = 0; i < products.length; i++) {
            this.product = new product_model_1.Product();
            // this.product.offer = new Offer();  // commented by valar for hide discount
            this.product.product_id = products[i]["product_id"];
            this.product.sku_id = products[i]["sku_id"];
            this.product.product_name = products[i]["product_name"];
            this.product.line_total = products[i]["line_total"];
            this.product.rack_quantity = products[i]["rack_quantity"];
            this.product.quantity = products[i]["quantity"];
            // commented by valar for hide discount - start
            if (products[i]["discount_id"]) {
                this.product.discount_id = products[i]["discount_id"];
                //     this.product.offer.offer_quantity = products[i]["offer_quantity"]+"";
                //     this.product.offer.base_quantity = products[i]["base_quantity"];
                //     this.product.discount_product_id = products[i]["discount_product_id"];
                //     this.product.discount_product_quantity = products[i]["discount_product_quantity"];
                //     this.product.offer.discount_product_name = products[i]["discount_product_name"];
                //     console.log( this.product.discount_product_quantity);
            }
            // commented by valar for hide discount - start
            this.product.product_price = products[i]["product_price"];
            console.log("product property" + JSON.stringify(this.product));
            this.allproducts.push(this.product);
        }
    };
    RepOrderReview.prototype.editqty = function (id, index) {
        var _this = this;
        console.log("id: " + id + "index: " + index);
        console.log("id: " + JSON.stringify(this.allproducts));
        console.log("orderqty in editqty: " + this.allproducts[index]["quantity"]);
        var rackedit = this.allproducts[index]["rack_quantity"];
        var orderedit = this.allproducts[index]["quantity"];
        console.log("orderedit: " + orderedit);
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, id, index); });
    };
    RepOrderReview.prototype.setresult = function (quantity, productid, index) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            console.log("New Value");
            this.allproducts[index]["rack_quantity"] = rack;
            this.allproducts[index]["quantity"] = order;
            // commented by valar for hide discount - start
            // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
            // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
            // if(parseInt(order) >= baseQuantity ){
            //     var remainder = parseInt(order)%baseQuantity;
            //     var quotient = Math.floor(parseInt(order)/baseQuantity);
            //     var offerQuantity = (quotient * offer_Quantity);
            //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+"";       
            // }
            // commented by valar for hide discount - end
            this.changelinetotal(productid, index, order);
            this.changetotal();
        }
    };
    RepOrderReview.prototype.removeorder = function (id, index) {
        //this.allproducts.splice(index,1);
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            console.log("removeorder: " + result);
            if (result == true) {
                _this.allproducts.splice(index, 1);
                console.log("Order Length: " + _this.allproducts.length);
                _this.changetotal();
                if (_this.allproducts.length == 0) {
                    console.log("Array null");
                    _this.routerExtensions.navigate(["/productlist"], { clearHistory: true });
                    dialogs.alert({
                        title: "Empty",
                        message: "Shop Products",
                        okButtonText: "Ok"
                    }).then(function () {
                        console.log("Products");
                        //this.routerExtensions.navigate(["/productlist"]);
                    });
                }
                else {
                    return;
                }
            }
            else {
                return;
            }
        });
    };
    RepOrderReview.prototype.changelinetotal = function (productid, index, order) {
        for (var i = 0; i < this.getproducts.length; i++) {
            if (this.getproducts[i]["product_id"] == productid) {
                var tempprice = this.getproducts[i]["product_price"];
                console.log("tempprice: " + tempprice);
            }
        }
        this.templinetotal = tempprice * order;
        console.log("Line total in change total: " + this.allproducts[index]["line_total"]);
        this.allproducts[index]["line_total"] = this.templinetotal;
    };
    RepOrderReview.prototype.changetotal = function () {
        this.total_amount = 0;
        for (var i = 0; i < this.allproducts.length; i++) {
            console.log(i + ": " + this.allproducts[i]["product_id"]);
            var temptotal = this.allproducts[i]["line_total"];
            this.total_amount = temptotal + this.total_amount;
            console.log("totalamount: " + this.total_amount);
        }
        // commented by valar for hide discount - start
        // this.formattedData = [];
        // for(let j=0;j<this.allproducts.length;j++){
        //     if(this.allproducts[j].discount_id){
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price,
        //             'discount_id': this.allproducts[j].discount_id,
        //             'discount_product_quantity': this.allproducts[j].discount_product_quantity
        //         }); 
        //     }
        //     else {
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price
        //         });   
        //     }     
        // }
        // console.log("all product datas=====>"+JSON.stringify(this.formattedData));
        // commented by valar for hide discount - end
        this.saveorderservice.setScope(this.allproducts);
        this.savetotal.setScope(this.total_amount);
    };
    RepOrderReview.prototype.editshippingaddress = function () {
        var _this = this;
        var doornoedit = this.address.door_no;
        var areaedit = this.address.area;
        var cityedit = this.address.city;
        var pinedit = this.address.pin;
        var streetnameedit = this.address.street_name;
        var districtedit = this.address.district;
        var stateedit = this.address.state;
        console.log("state: " + stateedit);
        var options = {
            context: { doorno: doornoedit, streetname: streetnameedit, area: areaedit, city: cityedit, district: districtedit,
                state: stateedit, pin: pinedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialogshipping_component_1.DialogShipping, options)
            .then(function (dialogResult) { return _this.editaddress(dialogResult); });
    };
    RepOrderReview.prototype.editaddress = function (address) {
        if (address == "null") {
            return;
        }
        else {
            this.address.door_no = address[0];
            this.address.street_name = address[1];
            this.address.area = address[2];
            this.address.city = address[3];
            this.address.district = address[4];
            this.address.state = address[5];
            this.address.pin = address[6];
            this.customer.address = this.address;
            this.savecustomerdetails.setScope(this.customer);
        }
    };
    return RepOrderReview;
}());
RepOrderReview = __decorate([
    core_1.Component({
        selector: "order-review",
        moduleId: module.id,
        templateUrl: "reporderreview.component.html",
        styleUrls: ["reporderreview.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, saveorder_service_1.SaveOrderService,
        page_1.Page, saveorder_service_1.SaveCustomer, sendorder_service_1.SendOrder,
        saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef,
        saveorder_service_1.SaveCustomerDetails, saveorder_service_1.SaveShipping,
        saveorder_service_1.SaveNotes, login_service_1.loginService, common_1.Location])
], RepOrderReview);
exports.RepOrderReview = RepOrderReview;
