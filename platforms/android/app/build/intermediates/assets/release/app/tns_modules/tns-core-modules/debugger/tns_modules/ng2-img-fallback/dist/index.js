"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
/**
 * Created by vadimdez on 04/06/16.
 */
__export(require('./src/ng2-img-fallback.module'));
__export(require('./src/ng2-img-fallback.directive'));
//# sourceMappingURL=index.js.map