"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var customer_service_1 = require("../../../Services/customer.service");
var customer_model_1 = require("../../../Models/customer.model");
var page_1 = require("tns-core-modules/ui/page/page");
var platform_1 = require("platform");
var application_1 = require("application");
var application = require("application");
var common_1 = require("@angular/common");
var login_service_1 = require("../../../Services/login.service");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var CustomerList = (function () {
    // enableMessage = 'collapse';
    function CustomerList(routerExtensions, customerservice, page, location, loginservice) {
        this.routerExtensions = routerExtensions;
        this.customerservice = customerservice;
        this.page = page;
        this.location = location;
        this.loginservice = loginservice;
        this.customerDetails = [];
        this.customers = new Array();
        this.original = new Array();
        this.repnamevisible = "visible";
        this.loader = new LoadingIndicator();
    }
    CustomerList.prototype.ngOnInit = function () {
        var _this = this;
        //code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        //code for loading indicator end here
        this.title = "Customer List";
        this.page.actionBarHidden = false;
        this.getcustomer();
        this.currentdate = new Date();
        console.log(this.currentdate.getDate() +
            "/" +
            (this.currentdate.getMonth() + 1) +
            "/" +
            this.currentdate.getFullYear());
        application.android.on(application_1.AndroidApplication.activityBackPressedEvent, function (data) {
            data.cancel = true;
            //alert("Back Pressed")
            //this.location.back();
            _this.routerExtensions.backToPreviousPage();
        });
        this.tempuser = this.loginservice.getScope();
        if (this.tempuser["role"] === "rep") {
            this.repnamevisible = "collapse";
        }
    };
    CustomerList.prototype.getcustomer = function () {
        var _this = this;
        this.customerservice
            .getcustomer()
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
    };
    CustomerList.prototype.parseCustomers = function (data) {
        //console.log("data: " + JSON.stringify(data));
        //console.log("Customer list: "+"length: "+ data.length + JSON.stringify(data));
        //  commented by valarmathi
        // for (let i = 0; i < data.length; i++) {
        //   this.customer = new Customer();
        //   this.customer.id = data[i].id.$oid;
        //   if (data[i].customer_name) {
        //     this.customer.contactname = data[i].customer_name;
        //   } else if (data[i].name) {
        //     this.customer.contactname = data[i].name;
        //   }
        //   if (data[i].slaesrep_name) {
        //     this.customer.salesrepname = data[i].slaesrep_name;
        //   }
        //   this.customer.contactnumber = data[i].mobile_no;
        //   this.customer.shopname = data[i].shopname;
        //   console.log(
        //     "cus_id: " +
        //       this.customer.id +
        //       "cus_name: " +
        //       this.customer.shopname +
        //       "rep: " +
        //       this.customer.contactname +
        //       "mobile: " +
        //       this.customer.contactnumber
        //   );
        //   this.customers.push(this.customer);
        //   this.original.push(this.customer);
        // }
        // created by valarmathi
        //   if (typeof data === 'string') {
        //     console.log("THER IS NO DATAS");
        //     this.loader.hide();
        //     this.enableMessage = 'visible';
        // }
        // else { 
        // console.log("THER IS DATAS"); 
        this.loader.hide();
        // this.enableMessage = 'collapse';
        for (var i = 0; i < data.length; i++) {
            this.customer = new customer_model_1.Customer();
            this.customer.id = data[i]._id.$oid;
            this.customer.contactname = data[i].name;
            this.customer.contactnumber = data[i].primary_mobile_no;
            this.customer.shopname = data[i].shop_name;
            this.customer.address = new customer_model_1.Address();
            this.customer.address.city = data[i].address[0].city;
            this.customer.address.district = data[i].address[0].district;
            console.log("cus_id: " +
                this.customer.id +
                "cus_name: " +
                this.customer.shopname +
                "rep: " +
                this.customer.contactname +
                "mobile: " +
                this.customer.contactnumber);
            this.customers.push(this.customer);
            this.original.push(this.customer);
        }
        //  }
    };
    CustomerList.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.customers = this.original.filter(function (result) {
            return result.contactname.toLowerCase().includes(searchValue) ||
                result.shopname.toLowerCase().includes(searchValue);
        });
    };
    CustomerList.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        console.log("searchValue: " + searchValue);
        // alert(searchValue);
        this.customers = this.original.filter(function (result) {
            return result.contactname.toLowerCase().includes(searchValue) ||
                result.shopname.toLowerCase().includes(searchValue);
        });
    };
    CustomerList.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
    };
    CustomerList.prototype.searchBarLoaded = function (args) {
        console.log("search bar loaded");
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return CustomerList;
}());
CustomerList = __decorate([
    core_1.Component({
        selector: "Customer-List",
        moduleId: module.id,
        templateUrl: "CustomerList.component.html",
        styleUrls: ["CustomerList.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        customer_service_1.CustomerService,
        page_1.Page,
        common_1.Location,
        login_service_1.loginService])
], CustomerList);
exports.CustomerList = CustomerList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tZXJMaXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkN1c3RvbWVyTGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBaUQ7QUFDakQsc0RBQStEO0FBQy9ELHVFQUFxRTtBQUNyRSxpRUFBbUU7QUFDbkUsc0RBQXFEO0FBRXJELHFDQUFxQztBQUNyQywyQ0FHcUI7QUFDckIseUNBQTJDO0FBQzNDLDBDQUEyQztBQUMzQyxpRUFBK0Q7QUFDL0QsSUFBSSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztBQVN0RixJQUFhLFlBQVk7SUFVdkIsOEJBQThCO0lBQzlCLHNCQUNVLGdCQUFrQyxFQUNsQyxlQUFnQyxFQUNoQyxJQUFVLEVBQ1YsUUFBa0IsRUFDbEIsWUFBMEI7UUFKMUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFkcEMsb0JBQWUsR0FBa0IsRUFBRSxDQUFDO1FBQzdCLGNBQVMsR0FBZSxJQUFJLEtBQUssRUFBRSxDQUFDO1FBR3BDLGFBQVEsR0FBZSxJQUFJLEtBQUssRUFBRSxDQUFDO1FBRW5DLG1CQUFjLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLFdBQU0sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7SUFRN0IsQ0FBQztJQUVKLCtCQUFRLEdBQVI7UUFBQSxpQkFvREM7UUFuREMsdUNBQXVDO1FBQ3hDLElBQUksT0FBTyxHQUFHO1lBQ1gsT0FBTyxFQUFFLFlBQVk7WUFDckIsUUFBUSxFQUFFLElBQUk7WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsYUFBYSxFQUFFLElBQUk7Z0JBQ25CLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixHQUFHLEVBQUUsR0FBRztnQkFDUixvQkFBb0IsRUFBRSxTQUFTO2dCQUMvQixxQkFBcUIsRUFBRSxJQUFJO2dCQUMzQixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsaUJBQWlCLEVBQUUsQ0FBQzthQUNyQjtZQUNELEdBQUcsRUFBRTtnQkFDSCxPQUFPLEVBQUUseUJBQXlCO2dCQUNsQyxNQUFNLEVBQUUsS0FBSztnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBQyxFQUFFLENBQUMsaUNBQWlDO2FBQzFDO1NBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzFCLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLGVBQWUsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUM5QixPQUFPLENBQUMsR0FBRyxDQUNULElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO1lBQ3hCLEdBQUc7WUFDSCxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ2pDLEdBQUc7WUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUNqQyxDQUFDO1FBRUYsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQ3BCLGdDQUFrQixDQUFDLHdCQUF3QixFQUMzQyxVQUFDLElBQXlDO1lBQ3hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFDdkIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUNGLENBQUM7UUFFRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDN0MsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxjQUFjLEdBQUcsVUFBVSxDQUFDO1FBQ25DLENBQUM7SUFDSCxDQUFDO0lBRUQsa0NBQVcsR0FBWDtRQUFBLGlCQUlDO1FBSEMsSUFBSSxDQUFDLGVBQWU7YUFDakIsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCxxQ0FBYyxHQUFkLFVBQWUsSUFBUztRQUN0QiwrQ0FBK0M7UUFDL0MsZ0ZBQWdGO1FBR2xGLDJCQUEyQjtRQUN6QiwwQ0FBMEM7UUFDMUMsb0NBQW9DO1FBQ3BDLHdDQUF3QztRQUN4QyxpQ0FBaUM7UUFDakMseURBQXlEO1FBQ3pELCtCQUErQjtRQUMvQixnREFBZ0Q7UUFDaEQsTUFBTTtRQUNOLGlDQUFpQztRQUNqQywwREFBMEQ7UUFDMUQsTUFBTTtRQUNOLHFEQUFxRDtRQUNyRCwrQ0FBK0M7UUFFL0MsaUJBQWlCO1FBQ2pCLG1CQUFtQjtRQUNuQiwyQkFBMkI7UUFDM0IsdUJBQXVCO1FBQ3ZCLGlDQUFpQztRQUNqQyxrQkFBa0I7UUFDbEIsb0NBQW9DO1FBQ3BDLHFCQUFxQjtRQUNyQixvQ0FBb0M7UUFDcEMsT0FBTztRQUNQLHdDQUF3QztRQUN4Qyx1Q0FBdUM7UUFDdkMsSUFBSTtRQUVKLHdCQUF3QjtRQUMxQixvQ0FBb0M7UUFDcEMsdUNBQXVDO1FBQ3ZDLDBCQUEwQjtRQUMxQixzQ0FBc0M7UUFDdEMsSUFBSTtRQUNKLFVBQVU7UUFDUixpQ0FBaUM7UUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNuQixtQ0FBbUM7UUFDbkMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLHlCQUFRLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQztZQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksd0JBQU8sRUFBRSxDQUFDO1lBRXRDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FDTCxVQUFVO2dCQUNSLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDaEIsWUFBWTtnQkFDWixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ3RCLE9BQU87Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXO2dCQUN6QixVQUFVO2dCQUNWLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUM5QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUV4QyxDQUFDO1FBQ0gsS0FBSztJQUNMLENBQUM7SUFFTSwrQkFBUSxHQUFmLFVBQWdCLElBQUk7UUFDbEIsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM3QixJQUFJLFdBQVcsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQ25DLFVBQUEsTUFBTTtZQUNKLE9BQUEsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO2dCQUN0RCxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFEbkQsQ0FDbUQsQ0FDdEQsQ0FBQztJQUNKLENBQUM7SUFFTSxvQ0FBYSxHQUFwQixVQUFxQixJQUFJO1FBQ3ZCLElBQUksU0FBUyxHQUFjLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkMsSUFBSSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsQ0FBQztRQUMzQyxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FDbkMsVUFBQSxNQUFNO1lBQ0osT0FBQSxNQUFNLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7Z0JBQ3RELE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQURuRCxDQUNtRCxDQUN0RCxDQUFDO0lBQ0osQ0FBQztJQUVNLDhCQUFPLEdBQWQsVUFBZSxJQUFJO1FBQ2pCLElBQUksU0FBUyxHQUFjLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDN0IsU0FBUyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELHNDQUFlLEdBQWYsVUFBZ0IsSUFBSTtRQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakMsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxFQUFFLENBQUMsQ0FBQyxvQkFBUyxDQUFDLENBQUMsQ0FBQztZQUNkLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNILENBQUM7SUFDSCxtQkFBQztBQUFELENBQUMsQUExTEQsSUEwTEM7QUExTFksWUFBWTtJQU54QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGVBQWU7UUFDekIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFdBQVcsRUFBRSw2QkFBNkI7UUFDMUMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7S0FDMUMsQ0FBQztxQ0FhNEIseUJBQWdCO1FBQ2pCLGtDQUFlO1FBQzFCLFdBQUk7UUFDQSxpQkFBUTtRQUNKLDRCQUFZO0dBaEJ6QixZQUFZLENBMEx4QjtBQTFMWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBDdXN0b21lclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vU2VydmljZXMvY3VzdG9tZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBDdXN0b21lciwgQWRkcmVzcyB9IGZyb20gXCIuLi8uLi8uLi9Nb2RlbHMvY3VzdG9tZXIubW9kZWxcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xyXG5pbXBvcnQgeyBTZWFyY2hCYXIgfSBmcm9tIFwidWkvc2VhcmNoLWJhclwiO1xyXG5pbXBvcnQgeyBpc0FuZHJvaWQgfSBmcm9tIFwicGxhdGZvcm1cIjtcclxuaW1wb3J0IHtcclxuICBBbmRyb2lkQXBwbGljYXRpb24sXHJcbiAgQW5kcm9pZEFjdGl2aXR5QmFja1ByZXNzZWRFdmVudERhdGFcclxufSBmcm9tIFwiYXBwbGljYXRpb25cIjtcclxuaW1wb3J0ICogYXMgYXBwbGljYXRpb24gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBsb2dpblNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vU2VydmljZXMvbG9naW4uc2VydmljZVwiO1xyXG52YXIgTG9hZGluZ0luZGljYXRvciA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtbG9hZGluZy1pbmRpY2F0b3ItbmV3XCIpLkxvYWRpbmdJbmRpY2F0b3I7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiQ3VzdG9tZXItTGlzdFwiLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6IFwiQ3VzdG9tZXJMaXN0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJDdXN0b21lckxpc3QuY29tcG9uZW50LmNzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tZXJMaXN0IHtcclxuICB0aXRsZTogc3RyaW5nO1xyXG4gIGN1c3RvbWVyRGV0YWlsczogQXJyYXk8T2JqZWN0PiA9IFtdO1xyXG4gIHB1YmxpYyBjdXN0b21lcnM6IEN1c3RvbWVyW10gPSBuZXcgQXJyYXkoKTtcclxuICBwdWJsaWMgY3VzdG9tZXI6IEN1c3RvbWVyO1xyXG4gIHB1YmxpYyBjdXJyZW50ZGF0ZTogRGF0ZTtcclxuICBwdWJsaWMgb3JpZ2luYWw6IEN1c3RvbWVyW10gPSBuZXcgQXJyYXkoKTtcclxuICBwdWJsaWMgdGVtcHVzZXI7XHJcbiAgcHVibGljIHJlcG5hbWV2aXNpYmxlID0gXCJ2aXNpYmxlXCI7XHJcbiAgbG9hZGVyID0gbmV3IExvYWRpbmdJbmRpY2F0b3IoKTtcclxuICAvLyBlbmFibGVNZXNzYWdlID0gJ2NvbGxhcHNlJztcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgY3VzdG9tZXJzZXJ2aWNlOiBDdXN0b21lclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHBhZ2U6IFBhZ2UsXHJcbiAgICBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbixcclxuICAgIHByaXZhdGUgbG9naW5zZXJ2aWNlOiBsb2dpblNlcnZpY2VcclxuICApIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy9jb2RlIGZvciBsb2FkaW5nIGluZGljYXRvciBzdGFydCBoZXJlXHJcbiAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICBtZXNzYWdlOiAnTG9hZGluZy4uLicsXHJcbiAgICAgIHByb2dyZXNzOiAwLjY1LFxyXG4gICAgICBhbmRyb2lkOiB7XHJcbiAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcclxuICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcclxuICAgICAgICBtYXg6IDEwMCxcclxuICAgICAgICBwcm9ncmVzc051bWJlckZvcm1hdDogXCIlMWQvJTJkXCIsXHJcbiAgICAgICAgcHJvZ3Jlc3NQZXJjZW50Rm9ybWF0OiAwLjUzLFxyXG4gICAgICAgIHByb2dyZXNzU3R5bGU6IDEsXHJcbiAgICAgICAgc2Vjb25kYXJ5UHJvZ3Jlc3M6IDFcclxuICAgICAgfSxcclxuICAgICAgaW9zOiB7XHJcbiAgICAgICAgZGV0YWlsczogXCJBZGRpdGlvbmFsIGRldGFpbCBub3RlIVwiLFxyXG4gICAgICAgIHNxdWFyZTogZmFsc2UsXHJcbiAgICAgICAgbWFyZ2luOiAxMCxcclxuICAgICAgICBkaW1CYWNrZ3JvdW5kOiB0cnVlLFxyXG4gICAgICAgIGNvbG9yOiBcIiM0QjlFRDZcIixcclxuICAgICAgICBtb2RlOlwiXCIgLy8gc2VlIGlPUyBzcGVjaWZpYyBvcHRpb25zIGJlbG93XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICAgXHJcbiAgICB0aGlzLmxvYWRlci5zaG93KG9wdGlvbnMpO1xyXG4gICAgLy9jb2RlIGZvciBsb2FkaW5nIGluZGljYXRvciBlbmQgaGVyZVxyXG4gICAgdGhpcy50aXRsZSA9IFwiQ3VzdG9tZXIgTGlzdFwiO1xyXG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5nZXRjdXN0b21lcigpO1xyXG4gICAgdGhpcy5jdXJyZW50ZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICBjb25zb2xlLmxvZyhcclxuICAgICAgdGhpcy5jdXJyZW50ZGF0ZS5nZXREYXRlKCkgK1xyXG4gICAgICAgIFwiL1wiICtcclxuICAgICAgICAodGhpcy5jdXJyZW50ZGF0ZS5nZXRNb250aCgpICsgMSkgK1xyXG4gICAgICAgIFwiL1wiICtcclxuICAgICAgICB0aGlzLmN1cnJlbnRkYXRlLmdldEZ1bGxZZWFyKClcclxuICAgICk7XHJcblxyXG4gICAgYXBwbGljYXRpb24uYW5kcm9pZC5vbihcclxuICAgICAgQW5kcm9pZEFwcGxpY2F0aW9uLmFjdGl2aXR5QmFja1ByZXNzZWRFdmVudCxcclxuICAgICAgKGRhdGE6IEFuZHJvaWRBY3Rpdml0eUJhY2tQcmVzc2VkRXZlbnREYXRhKSA9PiB7XHJcbiAgICAgICAgZGF0YS5jYW5jZWwgPSB0cnVlO1xyXG4gICAgICAgIC8vYWxlcnQoXCJCYWNrIFByZXNzZWRcIilcclxuICAgICAgICAvL3RoaXMubG9jYXRpb24uYmFjaygpO1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrVG9QcmV2aW91c1BhZ2UoKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuXHJcbiAgICB0aGlzLnRlbXB1c2VyID0gdGhpcy5sb2dpbnNlcnZpY2UuZ2V0U2NvcGUoKTtcclxuICAgIGlmICh0aGlzLnRlbXB1c2VyW1wicm9sZVwiXSA9PT0gXCJyZXBcIikge1xyXG4gICAgICB0aGlzLnJlcG5hbWV2aXNpYmxlID0gXCJjb2xsYXBzZVwiO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0Y3VzdG9tZXIoKSB7XHJcbiAgICB0aGlzLmN1c3RvbWVyc2VydmljZVxyXG4gICAgICAuZ2V0Y3VzdG9tZXIoKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5wYXJzZUN1c3RvbWVycyhkYXRhLmpzb24oKSkpO1xyXG4gIH1cclxuXHJcbiAgcGFyc2VDdXN0b21lcnMoZGF0YTogYW55KSB7ICAgIFxyXG4gICAgLy9jb25zb2xlLmxvZyhcImRhdGE6IFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgLy9jb25zb2xlLmxvZyhcIkN1c3RvbWVyIGxpc3Q6IFwiK1wibGVuZ3RoOiBcIisgZGF0YS5sZW5ndGggKyBKU09OLnN0cmluZ2lmeShkYXRhKSk7XHJcblxyXG5cclxuICAvLyAgY29tbWVudGVkIGJ5IHZhbGFybWF0aGlcclxuICAgIC8vIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykge1xyXG4gICAgLy8gICB0aGlzLmN1c3RvbWVyID0gbmV3IEN1c3RvbWVyKCk7XHJcbiAgICAvLyAgIHRoaXMuY3VzdG9tZXIuaWQgPSBkYXRhW2ldLmlkLiRvaWQ7XHJcbiAgICAvLyAgIGlmIChkYXRhW2ldLmN1c3RvbWVyX25hbWUpIHtcclxuICAgIC8vICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RuYW1lID0gZGF0YVtpXS5jdXN0b21lcl9uYW1lO1xyXG4gICAgLy8gICB9IGVsc2UgaWYgKGRhdGFbaV0ubmFtZSkge1xyXG4gICAgLy8gICAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG5hbWUgPSBkYXRhW2ldLm5hbWU7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vICAgaWYgKGRhdGFbaV0uc2xhZXNyZXBfbmFtZSkge1xyXG4gICAgLy8gICAgIHRoaXMuY3VzdG9tZXIuc2FsZXNyZXBuYW1lID0gZGF0YVtpXS5zbGFlc3JlcF9uYW1lO1xyXG4gICAgLy8gICB9XHJcbiAgICAvLyAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG51bWJlciA9IGRhdGFbaV0ubW9iaWxlX25vO1xyXG4gICAgLy8gICB0aGlzLmN1c3RvbWVyLnNob3BuYW1lID0gZGF0YVtpXS5zaG9wbmFtZTtcclxuXHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKFxyXG4gICAgLy8gICAgIFwiY3VzX2lkOiBcIiArXHJcbiAgICAvLyAgICAgICB0aGlzLmN1c3RvbWVyLmlkICtcclxuICAgIC8vICAgICAgIFwiY3VzX25hbWU6IFwiICtcclxuICAgIC8vICAgICAgIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUgK1xyXG4gICAgLy8gICAgICAgXCJyZXA6IFwiICtcclxuICAgIC8vICAgICAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG5hbWUgK1xyXG4gICAgLy8gICAgICAgXCJtb2JpbGU6IFwiICtcclxuICAgIC8vICAgICAgIHRoaXMuY3VzdG9tZXIuY29udGFjdG51bWJlclxyXG4gICAgLy8gICApO1xyXG4gICAgLy8gICB0aGlzLmN1c3RvbWVycy5wdXNoKHRoaXMuY3VzdG9tZXIpO1xyXG4gICAgLy8gICB0aGlzLm9yaWdpbmFsLnB1c2godGhpcy5jdXN0b21lcik7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgLy8gY3JlYXRlZCBieSB2YWxhcm1hdGhpXHJcbiAgLy8gICBpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKSB7XHJcbiAgLy8gICAgIGNvbnNvbGUubG9nKFwiVEhFUiBJUyBOTyBEQVRBU1wiKTtcclxuICAvLyAgICAgdGhpcy5sb2FkZXIuaGlkZSgpO1xyXG4gIC8vICAgICB0aGlzLmVuYWJsZU1lc3NhZ2UgPSAndmlzaWJsZSc7XHJcbiAgLy8gfVxyXG4gIC8vIGVsc2UgeyBcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiVEhFUiBJUyBEQVRBU1wiKTsgXHJcbiAgICB0aGlzLmxvYWRlci5oaWRlKCk7XHJcbiAgICAvLyB0aGlzLmVuYWJsZU1lc3NhZ2UgPSAnY29sbGFwc2UnO1xyXG4gICAgZm9yKGxldCBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspe1xyXG4gICAgICB0aGlzLmN1c3RvbWVyID0gbmV3IEN1c3RvbWVyKCk7XHJcbiAgICAgIHRoaXMuY3VzdG9tZXIuaWQgPSBkYXRhW2ldLl9pZC4kb2lkO1xyXG4gICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RuYW1lID0gZGF0YVtpXS5uYW1lO1xyXG4gICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RudW1iZXIgPSBkYXRhW2ldLnByaW1hcnlfbW9iaWxlX25vO1xyXG4gICAgICB0aGlzLmN1c3RvbWVyLnNob3BuYW1lID0gZGF0YVtpXS5zaG9wX25hbWU7XHJcbiAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcyA9IG5ldyBBZGRyZXNzKCk7XHJcbiAgICAgXHJcbiAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcy5jaXR5ID0gZGF0YVtpXS5hZGRyZXNzWzBdLmNpdHk7XHJcbiAgICAgIHRoaXMuY3VzdG9tZXIuYWRkcmVzcy5kaXN0cmljdCA9IGRhdGFbaV0uYWRkcmVzc1swXS5kaXN0cmljdDtcclxuICAgICAgY29uc29sZS5sb2coXHJcbiAgICAgICAgICAgIFwiY3VzX2lkOiBcIiArXHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21lci5pZCArXHJcbiAgICAgICAgICAgICAgXCJjdXNfbmFtZTogXCIgK1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIuc2hvcG5hbWUgK1xyXG4gICAgICAgICAgICAgIFwicmVwOiBcIiArXHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21lci5jb250YWN0bmFtZSArXHJcbiAgICAgICAgICAgICAgXCJtb2JpbGU6IFwiICtcclxuICAgICAgICAgICAgICB0aGlzLmN1c3RvbWVyLmNvbnRhY3RudW1iZXJcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLmN1c3RvbWVycy5wdXNoKHRoaXMuY3VzdG9tZXIpO1xyXG4gICAgICAgICAgdGhpcy5vcmlnaW5hbC5wdXNoKHRoaXMuY3VzdG9tZXIpO1xyXG5cclxuICAgIH1cclxuICAvLyAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uU3VibWl0KGFyZ3MpIHtcclxuICAgIGxldCBzZWFyY2hCYXIgPSA8U2VhcmNoQmFyPmFyZ3Mub2JqZWN0O1xyXG4gICAgc2VhcmNoQmFyLmRpc21pc3NTb2Z0SW5wdXQoKTtcclxuICAgIGxldCBzZWFyY2hWYWx1ZSA9IHNlYXJjaEJhci50ZXh0LnRvTG93ZXJDYXNlKCk7XHJcbiAgICB0aGlzLmN1c3RvbWVycyA9IHRoaXMub3JpZ2luYWwuZmlsdGVyKFxyXG4gICAgICByZXN1bHQgPT5cclxuICAgICAgICByZXN1bHQuY29udGFjdG5hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hWYWx1ZSkgfHxcclxuICAgICAgICByZXN1bHQuc2hvcG5hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hWYWx1ZSlcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25UZXh0Q2hhbmdlZChhcmdzKSB7XHJcbiAgICBsZXQgc2VhcmNoQmFyID0gPFNlYXJjaEJhcj5hcmdzLm9iamVjdDtcclxuICAgIGxldCBzZWFyY2hWYWx1ZSA9IHNlYXJjaEJhci50ZXh0LnRvTG93ZXJDYXNlKCk7XHJcbiAgICBjb25zb2xlLmxvZyhcInNlYXJjaFZhbHVlOiBcIiArIHNlYXJjaFZhbHVlKTtcclxuICAgIC8vIGFsZXJ0KHNlYXJjaFZhbHVlKTtcclxuICAgIHRoaXMuY3VzdG9tZXJzID0gdGhpcy5vcmlnaW5hbC5maWx0ZXIoXHJcbiAgICAgIHJlc3VsdCA9PlxyXG4gICAgICAgIHJlc3VsdC5jb250YWN0bmFtZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKHNlYXJjaFZhbHVlKSB8fFxyXG4gICAgICAgIHJlc3VsdC5zaG9wbmFtZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKHNlYXJjaFZhbHVlKVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNsZWFyKGFyZ3MpIHtcclxuICAgIGxldCBzZWFyY2hCYXIgPSA8U2VhcmNoQmFyPmFyZ3Mub2JqZWN0O1xyXG4gICAgc2VhcmNoQmFyLmRpc21pc3NTb2Z0SW5wdXQoKTtcclxuICAgIHNlYXJjaEJhci50ZXh0ID0gXCJcIjtcclxuICB9XHJcblxyXG4gIHNlYXJjaEJhckxvYWRlZChhcmdzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcInNlYXJjaCBiYXIgbG9hZGVkXCIpO1xyXG4gICAgbGV0IHNlYXJjaEJhciA9IDxTZWFyY2hCYXI+YXJncy5vYmplY3Q7XHJcbiAgICBpZiAoaXNBbmRyb2lkKSB7XHJcbiAgICAgIHNlYXJjaEJhci5hbmRyb2lkLmNsZWFyRm9jdXMoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19