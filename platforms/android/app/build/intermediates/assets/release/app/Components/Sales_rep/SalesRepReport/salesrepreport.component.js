"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var router_1 = require("nativescript-angular/router");
var login_service_1 = require("../../../Services/login.service");
var http_1 = require("@angular/http");
var reports_service_1 = require("../../../Services/reports.service");
var SalesRepReport = (function () {
    function SalesRepReport(page, location, routerExtensions, loginservice, http, reportservice) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.loginservice = loginservice;
        this.http = http;
        this.reportservice = reportservice;
        this.big_total_count = "";
        this.big_total_amount = "";
        this.reports = new Array();
        this.detailvisible = "visible";
        this.bigvisible = "collapse";
    }
    SalesRepReport.prototype.ngOnInit = function () {
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   data.cancel = true;
        //   //alert("Back Pressed")
        //   //this.location.back();
        //   this.routerExtensions.backToPreviousPage();
        // });
        this.tempuser = this.loginservice.getScope();
    };
    SalesRepReport.prototype.onPickerLoaded = function (args) {
        var datePicker = args.object;
        this.date = new Date();
        datePicker.minDate = new Date(1989, 0, 29);
        datePicker.maxDate = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
        //datePicker.maxDate = new Date(2048, 3, 24);
        datePicker.year = this.date.getFullYear();
        datePicker.month = this.date.getMonth() + 1;
        datePicker.day = this.date.getDate();
    };
    SalesRepReport.prototype.fromdatefunc = function (args) {
        //console.log("Date changed");
        //console.log("New value: " + args.value);
        this.fromdate = args.value;
        this.setfromdate = this.fromdate.getDate() + "/" + (this.fromdate.getMonth() + 1) + "/" + this.fromdate.getFullYear();
        //console.log("fromstring: " + this.fromdate.getDate() +"/" + (this.fromdate.getMonth()+1)+ "/" + this.fromdate.getFullYear());
        this.isofromdate = this.fromdate.toISOString();
    };
    SalesRepReport.prototype.todatefunc = function (args) {
        //console.log("Date changed");
        //console.log("New value: " + args.value);  
        this.todate = args.value;
        this.settodate = this.todate.getDate() + "/" + (this.todate.getMonth() + 1) + "/" + this.todate.getFullYear();
        //console.log("tostring: " + this.todate.getDate() +"/" + (this.todate.getMonth()+1)+ "/" + this.todate.getFullYear());
        //console.log("To Iso value: " + this.todate.toISOString());
        this.isotodate = this.todate.toISOString();
    };
    SalesRepReport.prototype.submit = function () {
        this.getdate = { "start_date": this.isofromdate, "end_date": this.isotodate };
        console.log("start date and end date for report" + this.getdate);
        this.tempset = { "from": this.setfromdate, "to": this.settodate };
        this.reportservice.setScope(this.getdate);
        this.reportservice.setdate(this.tempset);
        this.routerExtensions.navigate(["/reportdetail"]);
        //this.data = {"start_date": this.isofromdate, "end_date": this.isotodate}
        this.data = { "start_date": "2018-02-01T10:35:48.343", "end_date": "2018-05-07T10:35:48.000" };
        /*
          if(this.tempuser.role === "manager"){
            //console.log("Manager Id: " + this.tempuser.id)
            //alert("Manager Id: " + this.tempuser.id)
            this.http.post("https://app.krishnabhavanfoods.in/api/orders/getmanagerreports/id/"+this.tempuser.id,
                this.data,
                { headers: this.getCommonHeaders() }
                ).subscribe((result) => {
                  this.getmanagerreport(result.json())
                  //console.log("Customer Result: " + JSON.stringify(result))
                }, (error) => {
                  console.log("Manager report Error: " + error)
                });
              }
        
              else if(this.tempuser.role === "rep"){
                //console.log("Rep Id: " + this.tempuser.id)
                //alert("Rep Id: " + this.tempuser.id)
                this.http.post("https://app.krishnabhavanfoods.in/api/orders/getsalesrepreports/id/"+this.tempuser.id,
                    this.data,
                    { headers: this.getCommonHeaders() }
                    ).subscribe((result) => {
                      this.salesrepreport(result.json())
                      //console.log("Customer Result: " + JSON.stringify(result))
                    }, (error) => {
                      console.log("Rep report Error: " + error)
                    });
                  }
              else if(this.tempuser.role === "customer"){
                //console.log("Customer Id: " + this.tempuser.id)
                //alert("Customer Id: " + this.tempuser.id)
                this.http.post("https://app.krishnabhavanfoods.in/api/orders/getcustomerreports/id/"+this.tempuser.id,
                    this.data,
                    { headers: this.getCommonHeaders() }
                    ).subscribe((result) => {
                      this.customerreport(result.json())
                      //console.log("Customer Result: " + JSON.stringify(result))
                    }, (error) => {
                      console.log("Customer Error: " + error)
                    });
        
                  }
        
                  */
    };
    return SalesRepReport;
}());
SalesRepReport = __decorate([
    core_1.Component({
        selector: "rep-report",
        moduleId: module.id,
        templateUrl: "salesrepreport.component.html",
        styleUrls: ["salesrepreport.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions,
        login_service_1.loginService, http_1.Http, reports_service_1.ReportService])
], SalesRepReport);
exports.SalesRepReport = SalesRepReport;
