"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ManagerCustomerDetail = (function () {
    function ManagerCustomerDetail() {
        this.title = "Customer Detail";
        this.name = "PunniyaMoorthy";
        this.shop = "Kanniga Parameshwari Store";
        this.rep = "Ramasamy";
        this.region = "Thiruvallur Region";
    }
    ManagerCustomerDetail.prototype.ngOnInit = function () {
        this.address = ({ area: "No.123, opp. Eldam's Road, AnnaSalai", city: "Chennai", pincode: "611111", mobile: "9874563210" });
    };
    return ManagerCustomerDetail;
}());
ManagerCustomerDetail = __decorate([
    core_1.Component({
        selector: "manager-customer-detail",
        moduleId: module.id,
        templateUrl: "managercustomerdetail.component.html",
        styleUrls: ["managercustomerdetail.component.css"]
    })
], ManagerCustomerDetail);
exports.ManagerCustomerDetail = ManagerCustomerDetail;
