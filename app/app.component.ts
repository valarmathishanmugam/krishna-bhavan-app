import { loginService } from './Services/login.service';

import { SaveCustomer, SaveOrderService } from './Services/saveorder.service';
import { RadSideDrawerComponent } from 'nativescript-telerik-ui/sidedrawer/angular';
import { ChangeDetectorRef, Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
// import { RouterExtensions } from 'nativescript-angular/router';
import { RouterExtensions } from "nativescript-angular";

import { Router } from '@angular/router';
import * as connectivity from 'connectivity';
import { RadSideDrawer } from 'nativescript-telerik-ui/sidedrawer';
import { Page } from 'tns-core-modules/ui/page/page';
import * as dialogs from 'ui/dialogs';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import * as PushNotifications from "nativescript-push-notifications";
import  * as frameModule from "ui/frame";
import {exit} from 'nativescript-exit';
import { isAndroid } from "platform";



@Component({
  selector: "main",
  moduleId: module.id,
  templateUrl: "app.component.html",
  styles: [`
  ActionBar, .sideStackLayout{
      color: #fff;
  }
  .navdata{
    padding-left: 5%;
    padding-top: 10%;
    font-size: 20;
  }
  `]
})

export class AppComponent implements OnInit, AfterViewInit {

public connectionType: string;
tempuser;
temperaryuser;
products: any;
gesture = "false"
offerlistvisible = "visible"
productlistvisible = "visible"
customerlistvisible = "visible"
managerorderlist = "visible"
orderlist = "visible"
approvecus ="visible"
reportlistvisible = "visible"
customerid: any;

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private drawer: RadSideDrawer;
      
      constructor(private _router: Router, private _changeDetectionRef: ChangeDetectorRef, private routerExtensions: RouterExtensions,
        private page: Page, private saveorderservice: SaveOrderService, private router:Router,
         private savecustomer: SaveCustomer, private loginservice: loginService, private location: Location) {
       page.bindingContext = { actionbartitle: "Krishna Bhavan" }; 
        
        this.connectionType = "Unknown";
      }
      ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
        application.android.on(AndroidApplication.activityBackPressedEvent, 
            (data: AndroidActivityBackPressedEventData) => {
            data.cancel = true;
            this.drawer.closeDrawer();
            // alert("Back Pressed")
            //this.location.back();
            this.temperaryuser = this.loginservice.getScope();
            if(this.temperaryuser["role"] === "rep"){
                if (this._router.url === '/customerlist') {
                   this.getDialog();
                }
                else {
                    this.routerExtensions.back();
                }
            }
            else if(this.temperaryuser["role"] === "manager"){
                if (this._router.url === '/managerorderlist') {
                    this.getDialog();
                }
                else {
                    this.routerExtensions.back();
                }
            }
            else if(this.temperaryuser["role"] === "customer"){
                if (this._router.url === '/productlist') {
                    this.getDialog();
                }
                else {
                    this.routerExtensions.back();
                }
            }
        }); 
        
    }
  
  
      public ngOnInit() {
          this.connectionType = this.connectionToString(connectivity.getConnectionType());
          connectivity.startMonitoring(connectionType => {              
                  this.connectionType = this.connectionToString(connectionType);                    
                  if(connectionType == 1 || connectionType == 2){
                      console.log("Connected");
                      this.routerExtensions.navigate(["/login"], { clearHistory: true });
                  }
                  else{
                      console.log("DisConnected");
                      this.routerExtensions.navigate(["/test"], { clearHistory: true });
                  }
              
                });                 
                               
        
      }


      usercustomnavbar(){
        this.tempuser = this.loginservice.getScope();
        if(this.tempuser["role"] === "manager"){
            //this.productlistvisible = "collapse"
            this.orderlist = "collapse"
        }

        else if(this.tempuser["role"] === "rep"){
            this.managerorderlist = "collapse"
            this.approvecus ="collapse"
        }
        else if(this.tempuser["role"] === "customer"){
            console.log("Navbar:" + this.tempuser["role"])
            this.customerlistvisible = "collapse"
            this.reportlistvisible = "collapse"
            this.managerorderlist = "collapse"
            this.approvecus ="collapse"
            
        }
      }
  
      public connectionToString(connectionType: number): string {
          switch(connectionType) {
              case connectivity.connectionType.none:
                  return "No Connection!";
              case connectivity.connectionType.wifi:
                  return "Connected to WiFi!";
              case connectivity.connectionType.mobile:
                  return "Connected to Cellular!";
              default:
                  return "Unknown";
          }
      }   
        
        public openDrawer() {
            this.drawer.toggleDrawerState();
            this.usercustomnavbar();
        }
    
        orderdetails(){

            this.tempuser = this.loginservice.getScope();
            this.products = this.saveorderservice.getScope();
            this.customerid = this.savecustomer.getScope();
            console.log("Cart:" + this.tempuser.role);
            console.log("Products: " + this.products + " Customer: " + this.customerid);
            if(this.customerid !== false && this.products !== false)
            {   
                if(this.customerid.length == 0){
                    //alert("select Products");
                    dialogs.alert({
                        title: "Empty ",
                        message: "Select Customer",
                        okButtonText: "Ok"
                    }).then(function () {
                    });
                }
                else if(this.products.length == 0){
                    //alert("select Products");
                    dialogs.alert({
                        title: "Empty",
                        message: "Add Products",
                        okButtonText: "Ok"
                    }).then(function () {
                    });
                }
                
                else{                    
                    console.log("Not Null")
                    console.log("Length: " + this.products.length)
                    this.routerExtensions.navigate(["/orderreview"]);
                }
            }
            else if(this.tempuser["role"] === "customer"){
                console.log("Go For Shop");
                dialogs.alert({
                    title: "ORDER",
                    message: "Select Products",
                    okButtonText: "Ok"
                }).then(function () {
                });
            }
            else if(this.tempuser["role"] === "rep"){
                console.log("Go For Shop");
                dialogs.alert({
                    title: "ORDER",
                    message: "Select Customer and Products",
                    okButtonText: "Ok"
                }).then(function () {
                });
            }

            else if(this.tempuser["role"] === "manager"){
                console.log("Go For Shop");
                dialogs.alert({
                    title: "ORDER",
                    message: "Select Customer and Products",
                    okButtonText: "Ok"
                }).then(function () {
                });
            }
            
        }
    
        close(){
            this.drawer.closeDrawer();
            //this.routerExtensions.navigate(["/managerorderlist"]);
        }

        exitApp(){
            this.drawer.closeDrawer();
            // if (!this.routerExtensions.canGoBackToPreviousPage()) {              
                let options = {
                    title: "Exit App",
                    message: "Are you sure？",
                    okButtonText: "OK",
                    cancelButtonText: "Cancel"                   
                };
            
                dialogs.confirm(options).then((result: boolean) => {
                    console.log(result);
                    if (result) {
                        
                        console.log("Exit App")
                        //frameModule.topmost().goBack();
                        exit();
                    } else if (typeof result == "undefined" || result == false) {
                       
                        return;
                    }
                });
            
            // }
           
        }
        getDialog(){
            if (!this.routerExtensions.canGoBackToPreviousPage()) {              
                let options = {
                    title: "Exit App",
                    message: "Are you sure？",
                    okButtonText: "OK",
                    cancelButtonText: "Cancel"                   
                };
            
                dialogs.confirm(options).then((result: boolean) => {
                    console.log(result);
                    if (result) {
                       
                        this.exitApp();
                    } else if (typeof result == "undefined" || result == false) {
                        
                        return;
                    }
                });
            
            }
        }   

 }