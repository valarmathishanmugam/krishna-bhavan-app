import { ApproveCustomerDetail } from './Components/Approve_Customer_Detail/approve_customer_detail.component';
import { ApproveCustomerList } from './Components/Approve_Customer/approve_customer_list.component';
import { SalesRepReport } from './Components/Sales_rep/SalesRepReport/salesrepreport.component';
import { ManagerOrderReview } from './Components/Sales_Manager/Manager_OrderReview/managerorderreview.component';
import { ActionBar } from './Components/ActionBar/actionbar.component';
import { Test } from './Components/test/test.component';
import { ProductDetail } from './Components/Product_Detail/productdetail.component';
import { CustomerInfo } from './Components/Customer_Info/customerinfo.component';
import { CustomerOrderList } from './Components/customer/Order_List/cusorderlist.component';
import { CustomerDetail } from './Components/Sales_rep/CustomerDetail/customerdetail.component';
import { RepOrderReview } from './Components/Sales_rep/Order_Review/reporderreview.component';
import { OrderDetail } from './Components/Sales_rep/Order_details/orderdetail.component';
import { RepOrderList } from './Components/Sales_rep/Order_List/reporderlist.component';
import { LoginComponent } from "./Components/Login/login.component";
import { CustomerList } from "./Components/Sales_rep/Customer_List/CustomerList.component";
import { ProductList } from "./Components/Product_List/ProductList.component";
import { ManagerCustomerDetail } from './Components/Sales_Manager/Customer_Detail/managercustomerdetail.component';
import { ManagerOrderList } from './Components/Sales_Manager/Manager_OrderList/managerorderlist.component';
import { CreateCustomer } from './Components/CreateCustomer/createcustomer.component';
import { ProductOffers } from './Components/Product_Offers/productoffers.component';
import { OrderNotes } from './Components/Order_Notes/ordernotes.component';
import { ReportDetail } from './Components/Report_Detail/reportdetail.component';
import { AccountComponent } from './Components/Account/account.component';

export const routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "customerlist", component: CustomerList },
  { path: "productlist", component: ProductList },
  { path: "orderlist", component:RepOrderList },
  { path: "orderdetail/:id/:id2", component:OrderDetail },
  { path: "orderreview", component:RepOrderReview },
  { path: "customerdetail/:id", component:CustomerDetail },
  { path: "customerorderlist", component:CustomerOrderList },
  { path: "customerinfo", component:CustomerInfo },
  { path: "productdetail", component:ProductDetail },
  { path: "managercustomerdetail", component:ManagerCustomerDetail },
  { path: "test", component:Test },
  { path: "managerorderlist", component:ManagerOrderList },
  { path: "actionbar", component: ActionBar },
  { path: "managerorderreview/:id", component: ManagerOrderReview },
  { path: "salesrepreport", component: SalesRepReport },
  { path: "createcustomer", component: CreateCustomer },
  { path: "approvecustomerlist", component: ApproveCustomerList },
  { path: "approvecustomerdetail/:id", component: ApproveCustomerDetail },
  { path: "productoffers", component: ProductOffers },
  { path: "ordernotes", component: OrderNotes },
  { path: "reportdetail", component: ReportDetail },
  { path: "accountdetails", component: AccountComponent }, 
  //{ path: '**', component: LoginComponent }       
];

export const navigatableComponents = [
  LoginComponent,
  CustomerList,
  ProductList,
  RepOrderList,
  OrderDetail,
  RepOrderReview,
  CustomerDetail,
  CustomerOrderList,
  CustomerInfo,
  ProductDetail,
  ManagerCustomerDetail,
  ManagerOrderList,
  Test,
  ActionBar,
  ManagerOrderReview,
  SalesRepReport,
  CreateCustomer,
  ApproveCustomerList,
  ApproveCustomerDetail,
  ProductOffers,
  OrderNotes,
  ReportDetail,
  AccountComponent 
];