import { stretchProperty } from 'tns-core-modules/ui/image';
import {Order} from "./order.model";

export class Product {
    product_id: string;
    sku_id: string;
    product_name: string;
    categoryId: string;
    categoryName: string;
    discount_id: string;
    quantity;
    product_price;
    region: string;
    line_total;
    rack_quantity;
    offer: Offer;
    image;
    discount?: Array<object>;
    discount_product_id?: string;
    discount_product_quantity?: string;
    case_quantity?: string
}
export class Offer {
    base_quantity;
    offer_quantity;
    offervisible: string;    
    discount_product_name?;
}
