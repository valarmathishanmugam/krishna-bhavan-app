import { Customer } from './customer.model';
export class Order{
    _id: string;
    order_date: string;
    total_amount: string;
    line_total: string;
    priority: number;
    status: string;
    preferred_shipping: string;
    change_time: string;
    changed_by: string;
    customer: Customer
    approve:string;
    order_id: string;
}
export class Note{
    note: string;
    created_by: string;
    created_date: string;
}

export class Report{
    emp_id;
    order_count;
    order_total;
}