export class Customer { 
    id: string;
    shopname: string;
    address: Address;
    contactname: string;
    contactnumber: number;
    gstno: number;
    salesrepname: string;
}

export class Address {
     door_no: string;
     street_name: string;
     area: string;
     city: string;
     district: string;
     state: string;
     pin: number;
     contactnumber: number;
     region: string;
     shippingmode: string;
}
