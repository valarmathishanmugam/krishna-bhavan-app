import { Order } from '../Models/order.model';
import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';

import { Customer, Address } from "./../Models/customer.model";


@Injectable()
export class OrderApproval {
   
        constructor(private http: Http) { }
    
        approveorder(id,status,manager_id) { 
            console.log("status of approve order"+status);
            const updatestatus = {"status": status,"sm_approved_by":manager_id}  
            console.log("update status"+updatestatus);         
            return this.http.post("https://app.krishnabhavanfoods.in/api/orders/order_approval/"+ id,updatestatus
            ,
            { headers: this.getCommonHeaders() })
            .subscribe((result) => {
                console.log("dataresult: " + result)
            }, (error) => {
                console.log("Order Approval Error: " + error)
                //this.orderplaceerror();
            });
        }
    
      getCommonHeaders() {
        let headers = new Headers();
        headers.set("Content-Type", "application/json");
        return headers;
      }

      updateorder(){
          
      }
        
}