
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { loginService } from './login.service';


@Injectable()
export class GetOrder{

  url;
  tempuser;

  constructor(private http: Http, private loginservice: loginService) {}  

  getorderbyid(id) {
    //console.log("Order_Id: "+ id)
    return this.http.get("https://app.krishnabhavanfoods.in/api/orders/get_orders/id/"+ id,
        //{ headers: this.getCommonHeaders() }
        );
    }

  getCommonHeaders() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    return headers;
  }

  getmanagerorders(id){
    this.tempuser = this.loginservice.getScope();

    if(id != undefined){
      console.log("requser service id: " + id)
      return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/"+id+"/orders")
    }
    else{
      id = this.tempuser.id
      console.log("user service id: " + id)
      if(this.tempuser.role === "manager"){
        return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getmanager/id/"+this.tempuser.id+"/orders")
      }
      else if(this.tempuser.role === "rep"){
        return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getsalesrep/id/"+this.tempuser.id+"/orders")
      }
      else if(this.tempuser.role === "customer"){
        //console.log("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/"+this.tempuser.id+"/orders")
        return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/"+this.tempuser.id+"/orders")
      }
      // else if(this.tempuser.role === "admin"){
      //   return this.http.get("https://app.krishnabhavanfoods.in/api/orders/getorders/")
      // }
    }   
    
   //return this.http.get("https://app.krishnabhavanfoods.in/api/orders/getorders/");
  }

}
