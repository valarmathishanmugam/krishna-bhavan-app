import { Address, Customer } from '../Models/customer.model';
import { CustomerService } from './customer.service';
import { loginService } from './login.service';
import { Injectable } from '@angular/core';

@Injectable()
export class SaveOrderService {

    public scope: Array<any> | boolean = false;

    constructor() {
    }

    public getScope(): Array<any> | boolean {
        return this.scope;
    }

    public setScope(scope: any): void {
        this.scope = scope;
        console.log("Order Scope: " + JSON.stringify(this.scope));
    }
}

export class SaveCustomer{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;            
        }
        
}

export class SaveEmployeeId{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            console.log("Employee Id: " + scope);
            this.scope = scope;            
        }
}

export class SaveTotal{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
        }
}

export class SaveCustomerDetails{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
            console.log("Details " + JSON.stringify(this.scope));
        }
}

export class SaveShipping {

    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
        }
}
export class SaveNotes {
    
        public scope: Array<any> | boolean = false;
        
            constructor() {
            }
        
            public getScope(): Array<any> | boolean {
                return this.scope;
            }
        
            public setScope(scope: any): void {
                this.scope = scope;
            }
    }

    export class SaveUpdateOrderId {
    
        public scope: Array<any> | boolean = false;
        
            constructor() {
            }
        
            public getScope(): Array<any> | boolean {
                return this.scope;
            }
        
            public setScope(scope: any): void {
                this.scope = scope;
                console.log("Update Order ID: " + this.scope);
            }
    }
    export class SaveUpdateOrder_Id {
        
            public scope: Array<any> | boolean = false;
            
                constructor() {
                }
            
                public getScope(): Array<any> | boolean {
                    return this.scope;
                }
            
                public setScope(scope: any): void {
                    this.scope = scope;
                    console.log("Update New Order ID: " + this.scope);
                }
        }
        
        