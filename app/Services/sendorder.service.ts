import { loginService } from './login.service';
import { Address, Customer } from '../Models/customer.model';
import { Note } from '../Models/order.model';
import {
    SaveCustomer,
    SaveCustomerDetails,
    SaveNotes,
    SaveOrderService,
    SaveShipping,
    SaveTotal,
    SaveEmployeeId,
} from './saveorder.service';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { RouterExtensions } from 'nativescript-angular/router';
import { SaveUpdateOrderId,SaveUpdateOrder_Id } from './saveorder.service';
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;


@Injectable()
export class SendOrder{

  customerid: any;
  shippingaddress: any;
  orderedproducts: any;
  orderedNewProducts: any[] = [];
  total_amount: any;
  shippingmode: any;
  public customer: Customer;
  public address: Address;  
  public currentdate: Date;
  public date: any;
  public isodate: any;
  public notes: any;
  employeeid: any;
  tempuser;
  details: any;
  updateorderid;
  updateOrderDetails;
  current_note:Note; 
  noteArray: any[]=[];
  loader = new LoadingIndicator();

  constructor(private http: Http, private savecustomer: SaveCustomer, private savecustomerdetails: SaveCustomerDetails, 
    private saveorderservice: SaveOrderService, private savetotal: SaveTotal, private saveshipping: SaveShipping,
    private savenotes: SaveNotes, private routerExtensions: RouterExtensions, private loginservice: loginService,
    private saveemployee: SaveEmployeeId, private saveupdateorderid: SaveUpdateOrderId, private saveupdateorder_id: SaveUpdateOrder_Id) {}

    result: any;

    data: any;
  

  placeorder() {
    this.showLoadingIndicator();
    this.tempuser = this.loginservice.getScope();

    this.customerid = this.savecustomer.getScope();
    this.employeeid = this.saveemployee.getScope();
    console.log("Customerid:"+ this.customerid);
    this.shippingaddress = this.savecustomerdetails.getScope();
    this.total_amount = this.savetotal.getScope();
    this.orderedproducts = this.saveorderservice.getScope();
    console.log("OFFER DETAILS=======>"+JSON.stringify(this.orderedproducts));
    
    // for(let i=0;i<this.orderedproducts.length;i++){     
    //   var object = {
    //     "product_id":this.orderedproducts[i].product_id,
    //     "sku_id":this.orderedproducts[i].sku_id,
    //     "product_name":this.orderedproducts[i].product_name,
    //     "line_total":this.orderedproducts[i].line_total,
    //     "rack_quantity":this.orderedproducts[i].rack_quantity,
    //     "quantity":this.orderedproducts[i].quantity,
    //     "discount_id":this.orderedproducts[i].discount_id,
    //     "discount_product_quantity":this.orderedproducts[i].discount_product_quantity,
    //     "product_price":this.orderedproducts[i].product_price
    //   }
    //   this.orderedNewProducts.push(object);    
     

    // }
    // console.log("OFFER DETAILS=======>"+JSON.stringify(this.orderedNewProducts));
   
    for(let i=0; i < this.orderedproducts.length; i++) {      
      this.orderedproducts[i]["product_id"] = String(this.orderedproducts[i]["product_id"]);
      this.orderedproducts[i]["sku_id"] = String(this.orderedproducts[i]["sku_id"]);
      this.orderedproducts[i]["product_name"] = String(this.orderedproducts[i]["product_name"]);            
      this.orderedproducts[i]["line_total"] = String(this.orderedproducts[i]["line_total"]);
      this.orderedproducts[i]["rack_quantity"] = String(this.orderedproducts[i]["rack_quantity"]);
      this.orderedproducts[i]["quantity"] = String(this.orderedproducts[i]["quantity"]); 
       // commented by valar for hide discount - start

      if(this.orderedproducts[i]["discount_id"]){
        this.orderedproducts[i]["discount_id"] = String(this.orderedproducts[i]["discount_id"]);       
      //   this.orderedproducts[i]["discount_product_quantity"] = String(this.orderedproducts[i]["discount_product_quantity"]); 
      }
       // commented by valar for hide discount - end
        this.orderedproducts[i]["product_price"] = String(this.orderedproducts[i]["product_price"]);
      
    }
    
    console.log("Line-Items:"+ this.orderedproducts);
    this.shippingmode = this.saveshipping.getScope();
    console.log("shipmode: " + this.shippingmode)
    this.notes = this.savenotes.getScope();
    console.log("New order Note: " + this.notes)
    this.currentdate = new Date();    
    this.date =(this.currentdate.getMonth() +1) + "/" + (this.currentdate.getDate()) +"/" + (this.currentdate.getFullYear());
    this.isodate = new Date().toISOString();
    console.log("Today Date: " + this.date);

    this.current_note = {
      "note": String(this.notes),
      "created_by": String(this.tempuser.name),
      "created_date": String(this.date)
    }
    this.noteArray.push(this.current_note);
    console.log("note array"+this.noteArray);

    if(this.notes !== "")
    {

    console.log("With notes");
    this.data = { 
      "order_id":'',
      "order_date": this.isodate,
    "employee_id": String(this.employeeid),
    "customer_id": String(this.customerid),
    "customer_name": String(this.shippingaddress.contactname),
    "shop_name": String(this.shippingaddress.shopname),
    "shipping_address": {
      "door_no": String(this.shippingaddress.address.door_no),
      "street_name": String(this.shippingaddress.address.street_name),
      "area": String(this.shippingaddress.address.area),
      "city": String(this.shippingaddress.address.city),
      "district": String(this.shippingaddress.address.district),
      "state": String(this.shippingaddress.address.state),
      "pin": String(this.shippingaddress.address.pin)
    },
    "billing_address": {
      "door_no": String(this.shippingaddress.address.door_no),
      "street_name": String(this.shippingaddress.address.street_name),
      "area": String(this.shippingaddress.address.area),
      "city": String(this.shippingaddress.address.city),
      "district": String(this.shippingaddress.address.district),
      "state": String(this.shippingaddress.address.state),
      "pin": String(this.shippingaddress.address.pin)
    },
    "total_amount": String(this.total_amount),
    "priority": "1",
    "status": "pending",
    "preferred_shipping": this.shippingmode,
    "line_items": this.orderedproducts,
      "notes": this.noteArray,
      "current_note": {
        "note": String(this.notes),
        "created_by": String(this.tempuser.name),
        "created_date": String(this.date)
      },
      "sm_is_approved": "0",
      "sm_approved_by": "none",
      "admin_is_approved": "0",
      "admin_approved_by": "none"
     }
    }
    else{
      console.log("Without notes");
      this.data = { 
        "order_date": this.isodate,
      "employee_id": String(this.employeeid),
      "customer_id": String(this.customerid),
      "customer_name": String(this.shippingaddress.contactname),
      "shop_name": String(this.shippingaddress.shopname),
      "shipping_address": {
        "door_no": String(this.shippingaddress.address.door_no),
        "street_name": String(this.shippingaddress.address.street_name),
        "area": String(this.shippingaddress.address.area),
        "city": String(this.shippingaddress.address.city),
        "district": String(this.shippingaddress.address.district),
        "state": String(this.shippingaddress.address.state),
        "pin": String(this.shippingaddress.address.pin)
      },
      "billing_address": {
        "door_no": String(this.shippingaddress.address.door_no),
        "street_name": String(this.shippingaddress.address.street_name),
        "area": String(this.shippingaddress.address.area),
        "city": String(this.shippingaddress.address.city),
        "district": String(this.shippingaddress.address.district),
        "state": String(this.shippingaddress.address.state),
        "pin": String(this.shippingaddress.address.pin)
      },
      "total_amount": String(this.total_amount),
      "priority": "1",
      "status": "pending",
      "preferred_shipping": this.shippingmode,
      "line_items": this.orderedproducts,
      "sm_is_approved": "0",
      "sm_approved_by": "none",
      "admin_is_approved": "0",
      "admin_approved_by": "none"       
       }
    }
    console.log("In send new order: " + JSON.stringify(this.data));
    return this.http.post(
        
      "https://app.krishnabhavanfoods.in/api/orders/neworder/",
        this.data, 
        { headers: this.getCommonHeaders() }
        )
        //.map(res => this.getresponse(res))
        //.map(res => this.result = JSON.stringify(res)),
        //.subscribe(
         // data => console.log("dataresult: " + JSON.stringify(data))
            //console.log("dataresult: " + JSON.stringify(data)),
            //this.result = data,
         //   err => console.log('ERROR!!! '+ err),
            //() => console.log('Got response from API', this.result)
         // )
          //console.log("mapresult: " + this.result)
          //this.result = this.result[0],
          //console.log("Token: " + this.result)

          .subscribe((result) => {
            this.getresponse(result.json())
        }, (err) => {
            this.orderplaceerror(err);
        });
    }

    updateorder(){
      this.showLoadingIndicator();
      this.tempuser = this.loginservice.getScope();

      this.details = this.savecustomerdetails.getScope(); 
      this.updateorderid = this.saveupdateorderid.getScope();
      this.updateOrderDetails = this.saveupdateorder_id.getScope();
     // this.updateorder_id = this.saveupdateorder_id.getScope();
      
      this.orderedproducts = this.saveorderservice.getScope();
      console.log("Line-Items:"+ JSON.stringify(this.orderedproducts));

      for(let i=0; i < this.orderedproducts.length; i++) {

        this.orderedproducts[i]["product_id"] = String(this.orderedproducts[i]["product_id"]);
        this.orderedproducts[i]["sku_id"] = String(this.orderedproducts[i]["sku_id"]);
        this.orderedproducts[i]["product_name"] = String(this.orderedproducts[i]["product_name"]);            
        this.orderedproducts[i]["line_total"] = String(this.orderedproducts[i]["line_total"]);
        this.orderedproducts[i]["rack_quantity"] = String(this.orderedproducts[i]["rack_quantity"]);
        this.orderedproducts[i]["quantity"] = String(this.orderedproducts[i]["quantity"]);
         // commented by valar for hide discount - start

        if(this.orderedproducts[i]["discount_id"]){
          this.orderedproducts[i]["discount_id"] = String(this.orderedproducts[i]["discount_id"]);
        }

         // commented by valar for hide discount - end
        this.orderedproducts[i]["product_price"] = String(this.orderedproducts[i]["product_price"]);
      }

      this.current_note = {
        "note": String(this.notes),
        "created_by": String(this.tempuser.name),
        "created_date": String(this.date)
      }
      this.noteArray.push(this.current_note);
      console.log("note array"+this.noteArray);

      this.shippingmode = this.saveshipping.getScope();
      this.notes = this.savenotes.getScope();
      console.log("Updated notes: " + this.notes)
      this.currentdate = new Date();    
      this.date = (this.currentdate.getMonth() +1) + "/" + (this.currentdate.getDate()) +"/" + (this.currentdate.getFullYear());
      console.log("Update Order billing address: " + JSON.stringify(this.details[0]["billing_address"]));
  
      this.data = { 
        "order_id":  String(this.saveupdateorder_id.getScope()),
        "order_date":new Date(String(this.details[0]["orderdate"])).toISOString(),
      "employee_id": String(this.details[0]["employeeid"]),
      "customer_id": String(this.details[0]["customerid"]),
      "customer_name": String(this.details[0]["customername"]),
      "shop_name": String(this.details[0]["shopname"]),
      "shipping_address": {
        "door_no": String(this.details[0]["shippingaddress"]["door_no"]),
        "street_name": String(this.details[0]["shippingaddress"]["street_name"]),
        "area": String(this.details[0]["shippingaddress"]["area"]),
        "city": String(this.details[0]["shippingaddress"]["city"]),
        "district": String(this.details[0]["shippingaddress"]["district"]),
        "state": String(this.details[0]["shippingaddress"]["state"]),
        "pin": String(this.details[0]["shippingaddress"]["pin"])
      },
      "billing_address": this.details[0]["billingaddress"],
      "total_amount": String(this.details[0]["totalamount"]),
      "priority": "1",
      "status": "pending",
      "preferred_shipping": this.details[0]["preferredshipping"],
      "line_items": this.orderedproducts,
      "current_note": {
        "note": String(this.notes),
        "created_by": String(this.tempuser.name),
        "created_date": String(this.date)
        },
       "notes": this.noteArray,
        "sm_is_approved": "0",
        "sm_approved_by": "none",
        "admin_is_approved": "0",
        "admin_approved_by": "none",
        "change_log" : [ 
          {
              "changed_by" : String(this.tempuser.name),
              "change_time" : "0"
          }]
  
       }
      console.log("In send upate order: " + JSON.stringify(this.data));

      return this.http.post(
        
        "https://app.krishnabhavanfoods.in/api/orders/updateorder/"+this.updateorderid,
          this.data, 
          { headers: this.getCommonHeaders() }
          )
          //.map(res => this.getresponse(res))
          //.map(res => this.result = JSON.stringify(res)),
          //.subscribe(
           // data => console.log("dataresult: " + JSON.stringify(data))
              //console.log("dataresult: " + JSON.stringify(data)),
              //this.result = data,
           //   err => console.log('ERROR!!! '+ err),
              //() => console.log('Got response from API', this.result)
           // )
            //console.log("mapresult: " + this.result)
            //this.result = this.result[0],
            //console.log("Token: " + this.result)
  
            .subscribe((result) => {
              this.getupdateresponse(result.json())
          }, (err) => {
              this.orderplaceerror(err);
          });
    }

  getCommonHeaders() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    return headers;
  }

  getresponse(result){
    this.loader.hide();
    console.log("New Order Response: " + result);
    //console.log("Status: " + result["status"]);
    //console.log("Response: " + JSON.stringify(result));
    alert("Order Placed")     
    if(this.tempuser["role"] === "customer"){
      this.routerExtensions.navigate(["/orderlist"], { clearHistory: true });
      var a = false;
      
      this.saveorderservice.setScope(a); 
    } 
    else if(this.tempuser["role"] === "rep"){
      this.routerExtensions.navigate(["/orderlist"], { clearHistory: true });
      var a = false;
      this.savecustomerdetails.setScope(a);      
      this.saveorderservice.setScope(a); 
      this.savecustomer.setScope(a);
    }  
    else if(this.tempuser["role"] === "manager"){
      this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
      var a = false;
      this.savecustomerdetails.setScope(a);      
      this.saveorderservice.setScope(a); 
      this.savecustomer.setScope(a);
    }      
    
  }

  getupdateresponse(result){
    this.loader.hide();
    console.log("Updated Result: " + JSON.stringify(result));
    var a = false;
    this.savecustomerdetails.setScope(a);      
    this.saveorderservice.setScope(a); 
    this.savecustomer.setScope(a);
    alert("Order Updated");
  }

  orderplaceerror(error){
    console.log("In order place Err Func: " + error);
    alert("Order not Placed. Try again!")
  }
  showLoadingIndicator(){
   let options = {
      message: 'Loading...',
      progress: 0.65,
      android: {
        indeterminate: true,
        cancelable: false,
        max: 100,
        progressNumberFormat: "%1d/%2d",
        progressPercentFormat: 0.53,
        progressStyle: 1,
        secondaryProgress: 1
      },
      ios: {
        details: "Additional detail note!",
        square: false,
        margin: 10,
        dimBackground: true,
        color: "#4B9ED6",
        mode:"" // see iOS specific options below
      }
    };
     
    this.loader.show(options); 
  }
}
