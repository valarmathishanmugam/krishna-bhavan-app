"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SaveOrderService = (function () {
    function SaveOrderService() {
        this.scope = false;
    }
    SaveOrderService.prototype.getScope = function () {
        return this.scope;
    };
    SaveOrderService.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("Order Scope: " + JSON.stringify(this.scope));
    };
    return SaveOrderService;
}());
SaveOrderService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], SaveOrderService);
exports.SaveOrderService = SaveOrderService;
var SaveCustomer = (function () {
    function SaveCustomer() {
        this.scope = false;
    }
    SaveCustomer.prototype.getScope = function () {
        return this.scope;
    };
    SaveCustomer.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveCustomer;
}());
exports.SaveCustomer = SaveCustomer;
var SaveEmployeeId = (function () {
    function SaveEmployeeId() {
        this.scope = false;
    }
    SaveEmployeeId.prototype.getScope = function () {
        return this.scope;
    };
    SaveEmployeeId.prototype.setScope = function (scope) {
        console.log("Employee Id: " + scope);
        this.scope = scope;
    };
    return SaveEmployeeId;
}());
exports.SaveEmployeeId = SaveEmployeeId;
var SaveTotal = (function () {
    function SaveTotal() {
        this.scope = false;
    }
    SaveTotal.prototype.getScope = function () {
        return this.scope;
    };
    SaveTotal.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveTotal;
}());
exports.SaveTotal = SaveTotal;
var SaveCustomerDetails = (function () {
    function SaveCustomerDetails() {
        this.scope = false;
    }
    SaveCustomerDetails.prototype.getScope = function () {
        return this.scope;
    };
    SaveCustomerDetails.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("Details " + JSON.stringify(this.scope));
    };
    return SaveCustomerDetails;
}());
exports.SaveCustomerDetails = SaveCustomerDetails;
var SaveShipping = (function () {
    function SaveShipping() {
        this.scope = false;
    }
    SaveShipping.prototype.getScope = function () {
        return this.scope;
    };
    SaveShipping.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveShipping;
}());
exports.SaveShipping = SaveShipping;
var SaveNotes = (function () {
    function SaveNotes() {
        this.scope = false;
    }
    SaveNotes.prototype.getScope = function () {
        return this.scope;
    };
    SaveNotes.prototype.setScope = function (scope) {
        this.scope = scope;
    };
    return SaveNotes;
}());
exports.SaveNotes = SaveNotes;
var SaveUpdateOrderId = (function () {
    function SaveUpdateOrderId() {
        this.scope = false;
    }
    SaveUpdateOrderId.prototype.getScope = function () {
        return this.scope;
    };
    SaveUpdateOrderId.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("Update Order ID: " + this.scope);
    };
    return SaveUpdateOrderId;
}());
exports.SaveUpdateOrderId = SaveUpdateOrderId;
var SaveUpdateOrder_Id = (function () {
    function SaveUpdateOrder_Id() {
        this.scope = false;
    }
    SaveUpdateOrder_Id.prototype.getScope = function () {
        return this.scope;
    };
    SaveUpdateOrder_Id.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("Update New Order ID: " + this.scope);
    };
    return SaveUpdateOrder_Id;
}());
exports.SaveUpdateOrder_Id = SaveUpdateOrder_Id;
