import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

@Injectable()
export class ReportService {

    public scope: Array<any> | boolean = false;
    public tempdate: Array<any> | boolean = false;

    constructor(private http: Http) {
    }

    public getScope(): Array<any> | boolean {
        return this.scope;
    }

    public setScope(scope: any): void {
        this.scope = scope;
        console.log("Report Scope: " + JSON.stringify(this.scope));    
    }

    public getdate(): Array<any> | boolean {
        return this.tempdate;
    }

    public setdate(tempdate: any): void {
        this.tempdate = tempdate; 
    }

    public managerReport(id){
       
        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getmanagerreports/id/"+id,
                this.scope, 
                { headers: this.getCommonHeaders() });
    }

    public repReport(id){

        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getsalesrepreports/id/"+id,
                    this.scope, 
                    { headers: this.getCommonHeaders() }
                    )
    }

    public customerReport(id){

        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getcustomerreports/id/"+id,
                    this.scope, 
                    { headers: this.getCommonHeaders() }
                    )
    }

    getCommonHeaders() {
        let headers = new Headers();
        headers.set("Content-Type", "application/json");
        return headers;
      }

}