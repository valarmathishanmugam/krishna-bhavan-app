import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs/Observable";
import { Http } from '@angular/http';

import { Customer, Address } from "./../Models/customer.model";
import { loginService } from "./login.service";


@Injectable()
export class CustomerService {

    id : string;
    private getcustomersUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers/active";    
    private getcustomersbyidUrl;
    public tempuser;
    
        constructor(private http: Http, private loginservice: loginService) {
            
         }
    
        getcustomer() {          
            this.tempuser = this.loginservice.getScope();
            let tempid = this.tempuser.id;

            if(this.tempuser["role"] === "manager"){
                //alert("Manager: " + tempid);
                // commented for get all active customers list by valarmathi
                //return this.http.get("https://app.krishnabhavanfoods.in/api/employee/customer/getmanagerreportees/id/"+tempid);
                return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/active");
                 
            }
    
            else if(this.tempuser["role"] === "rep"){
                //alert("Rep: " + tempid);
                 // commented for get all active customers list by valarmathi
                //return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getrepreportees/id/"+tempid);
                return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/active");
            }
            
            //return this.http.get(this.getcustomersUrl);
        }  
        
        getcustomerbyid(id){
             this.getcustomersbyidUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers/id/"+id;
             console.log(this.getcustomersbyidUrl);
             return this.http.get(this.getcustomersbyidUrl);
        }

        getunapprovedcustomer(){
            console.log("aprroval changed")
            this.tempuser = this.loginservice.getScope();
            // commented by valarmathi
            // return this.http.get("https://app.krishnabhavanfoods.in/api/employee/getcustomers/toapprove/manager/id/"+this.tempuser.id);
            return this.http.get("https://app.krishnabhavanfoods.in/api/customer/getcustomers/toapprove");
        }

}