import { Injectable } from "@angular/core";
//import { Observable as RxObservable } from "rxjs/Observable";
import { Http } from '@angular/http';



@Injectable()
export class ProductService {

    id : string;
    private getproductsUrl = "https://app.krishnabhavanfoods.in/api/product/getproducts";    
    private getcustomersbyidUrl;

    
        constructor(private http: Http) { }
    
        getproduct() {            
            return this.http.get(this.getproductsUrl);
        } 
        
        getDiscounts(){
            return this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts");
        }

        

}

export class SaveProducts{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
        }
}

export class SaveOffers{
    public scope: Array<any> | boolean = false;
    
        constructor() {
        }
    
        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
        }
}