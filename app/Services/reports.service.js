"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ReportService = (function () {
    function ReportService(http) {
        this.http = http;
        this.scope = false;
        this.tempdate = false;
    }
    ReportService.prototype.getScope = function () {
        return this.scope;
    };
    ReportService.prototype.setScope = function (scope) {
        this.scope = scope;
        console.log("Report Scope: " + JSON.stringify(this.scope));
    };
    ReportService.prototype.getdate = function () {
        return this.tempdate;
    };
    ReportService.prototype.setdate = function (tempdate) {
        this.tempdate = tempdate;
    };
    ReportService.prototype.managerReport = function (id) {
        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getmanagerreports/id/" + id, this.scope, { headers: this.getCommonHeaders() });
    };
    ReportService.prototype.repReport = function (id) {
        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getsalesrepreports/id/" + id, this.scope, { headers: this.getCommonHeaders() });
    };
    ReportService.prototype.customerReport = function (id) {
        return this.http.post("https://app.krishnabhavanfoods.in/api/orders/getcustomerreports/id/" + id, this.scope, { headers: this.getCommonHeaders() });
    };
    ReportService.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    return ReportService;
}());
ReportService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ReportService);
exports.ReportService = ReportService;
