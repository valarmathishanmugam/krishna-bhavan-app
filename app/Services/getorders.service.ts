import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs/Observable";
import { Http } from '@angular/http';

import { Customer, Address } from "./../Models/customer.model";


@Injectable()
export class GetOrderService {

    public scope: Array<any> | boolean = false;

    id : string;
    private getcustomersUrl = "https://app.krishnabhavanfoods.in/api/customer/getcustomers";    
    private getcustomersbyidUrl;

    
        constructor(private http: Http) { }
    
        getorders() {            
            return this.http.get(this.getcustomersUrl);
        }  

        public getScope(): Array<any> | boolean {
            return this.scope;
        }
    
        public setScope(scope: any): void {
            this.scope = scope;
        }
    }