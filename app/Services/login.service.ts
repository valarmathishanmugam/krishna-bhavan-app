import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";


@Injectable()
export class loginService {

  public scope: Array<any> | boolean = false;
  

  constructor(private http: Http) {}

  login(user,password) {

    return this.http.post(
        "https://app.krishnabhavanfoods.in/api/user/login",
        { "mobile_no": user, "password": password 
        }, 
        { headers: this.getCommonHeaders() }
        )}

  getCommonHeaders() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");
    return headers;
  }

  
    public getScope(): Array<any> | boolean {
        return this.scope;
    }

    public setScope(scope: any): void {
        this.scope = scope;
        console.log("LoginDetails " + JSON.stringify(this.scope));
    }
}
