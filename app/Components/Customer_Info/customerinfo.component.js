"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CustomerInfo = (function () {
    function CustomerInfo() {
    }
    CustomerInfo.prototype.ngOnInit = function () {
        console.log("InfoID" + this.customerid);
        this.address = ({ shop: "Kanniga Parameshwari Store", gst: "ABCD12345698", area: "No.123, opp. Eldam's Road, AnnaSalai", city: "Chennai", pincode: "611111", mobile: "9874563210" });
    };
    return CustomerInfo;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], CustomerInfo.prototype, "customerid", void 0);
CustomerInfo = __decorate([
    core_1.Component({
        selector: "customer-info",
        moduleId: module.id,
        templateUrl: "customerinfo.component.html",
        styleUrls: ["customerinfo.component.css"]
    })
], CustomerInfo);
exports.CustomerInfo = CustomerInfo;
