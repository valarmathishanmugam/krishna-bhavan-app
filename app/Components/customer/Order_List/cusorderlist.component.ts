import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
    selector: "cus-order-list",
    moduleId: module.id,
    templateUrl: "cusorderlist.component.html"
})

export class CustomerOrderList{

    orderDetails: Array<Object> = [];
    
    constructor(private routerExtensions: RouterExtensions){}

        ngOnInit(){
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.orderDetails.push({id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
        }

        orderdetail(){
            this.routerExtensions.navigate(["/orderdetail"]);
        }

}