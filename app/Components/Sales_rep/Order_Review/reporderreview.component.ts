import { loginService } from '../../../Services/login.service';
import { DialogShipping } from '../../Dialog_shippingaddress/dialogshipping.component';
import { Dialog } from '../../Dialog/dialog.component';
import { SaveProducts } from '../../../Services/product.service';
import { SendOrder } from '../../../Services/sendorder.service';
import { Offer, Product } from '../../../Models/product.model';
import { Customer, Address } from '../../../Models/customer.model';
import {
    SaveCustomer,
    SaveCustomerDetails,
    SaveNotes,
    SaveOrderService,
    SaveShipping,
    SaveTotal
    
} from '../../../Services/saveorder.service';
import { Component, AfterViewInit, ViewContainerRef, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { prompt, inputType, confirm } from "ui/dialogs";
import { Placeholder } from 'tns-core-modules/ui/placeholder';
import { Order } from '../../../Models/order.model';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import * as dialogs from 'ui/dialogs';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from 'application';
import { Location } from '@angular/common';
// import { Product } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';

@Component({
    selector: "order-review",
    moduleId: module.id,
    templateUrl: "reporderreview.component.html",
    styleUrls: ["reporderreview.component.css"]
})

export class RepOrderReview implements OnInit{

    title: string = "Order Review";
    //orderDetails: object;
    //address: object;
    customer = new Customer();
    address = new Address();
    productDetails: Array<Object> = [];
    customerId: any;
    customerdetails: any;
    products: any;
    getproducts: any;
    public product: Product;
    public order: Order;
    public allproducts: Product[] = new Array();
    orderremoved: any;
    public editShipping = false;
    total_amount: any;
    templinetotal: any;
    shippingmode: any;
    disableFAB = true;
    public ifcustomer;
    formattedData:any [] = [];

    constructor(private routerExtensions: RouterExtensions, private saveorderservice: SaveOrderService, 
        private page: Page, private savecustomer: SaveCustomer, private sendorder: SendOrder, 
        private savetotal: SaveTotal, private saveproducts: SaveProducts,
        private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef,
        private savecustomerdetails: SaveCustomerDetails, private saveshipping: SaveShipping, 
        private savenotes: SaveNotes, private loginservice: loginService, private location: Location){}

    ngOnInit(){
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        this.products = this.saveorderservice.getScope();
        this.getproducts = this.saveproducts.getScope();
        //console.log(this.productId);
            console.log("Id: "+ this.products[0]["product_id"] + "name: "+ this.products[0]["product_name"] + "orderqty: "+ this.products[0]["quantity"]+ this.products[0]["line_total"]);
            this.customerId = this.savecustomer.getScope();
            this.customerdetails = this.savecustomerdetails.getScope();
            this.total_amount = this.savetotal.getScope();
            console.log("Customer Id: " + this.customerId);
            this.getcustomerdetails(this.customerdetails);
            this.getorder(this.products);
            var tempcustomer = this.loginservice.getScope()
            this.checkcustomer(tempcustomer);

            // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            //     data.cancel = true;
            //     //alert("Back Pressed")
            //     //this.location.back();
            //     this.routerExtensions.backToPreviousPage();
            //   });
        }
    
        checkcustomer(data){
            if(data.role === 'customer'){
                this.ifcustomer = "collapse"
            }
            else{
                this.ifcustomer = "visible"
            }
        }
    gotoshop(note,ship){
        let options = {
            title: "Are You Sure",
            message: "Is Your Shipping Address Correct?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        confirm(options).then((result: boolean) => {           
            if(result == true){
                this.disableFAB = false;
                console.log("notes: " + note);
                this.savenotes.setScope(note);
                this.saveshipping.setScope(ship);
                this.savecustomerdetails.setScope(this.customer)
                this.sendorder.placeorder();
            }
            else{
                return;
            }
        });        
    }

    getcustomerdetails(customerdetails){

        this.customer.shopname = customerdetails.shopname;
        this.customer.contactname = customerdetails.contactname;
        this.customer.contactnumber = customerdetails.contactnumber;
        this.address.area = customerdetails.address.area;
        this.address.city = customerdetails.address.city;
        this.address.door_no = customerdetails.address.door_no;
        this.address.pin = customerdetails.address.pin;
        this.address.street_name = customerdetails.address.street_name;
        this.address.district = customerdetails.address.district;
        this.address.state = customerdetails.address.state;
        this.customer.address = this.address;        

    }

    addorder(customerid, productid){
        console.log("product id in order review:"+ productid);
        //this.productDetails.push({name:"Krishna's Custard Powder", id: productid, img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
    }

    getorder(products){
        console.log("products.length: "+ products.length);
        console.log(products);
         // commented by valar for hide discount - start

        // if(products[0].offer){
        //     products[0]["offer_quantity"] = products[0].offer.offer_quantity;
        //     products[0]["base_quantity"] = products[0].offer.base_quantity;
        //     products[0]["discount_product_name"]=products[0].offer.discount_product_name;
        // }

         // commented by valar for hide discount - end
        for(let i=0; i < products.length; i++) {
            
            this.product = new Product();
            // this.product.offer = new Offer();  // commented by valar for hide discount

            this.product.product_id = products[i]["product_id"];
            this.product.sku_id = products[i]["sku_id"];
            this.product.product_name = products[i]["product_name"];            
            this.product.line_total = products[i]["line_total"];
            this.product.rack_quantity = products[i]["rack_quantity"];
            this.product.quantity = products[i]["quantity"];
             // commented by valar for hide discount - start

             if(products[i]["discount_id"]){
                this.product.discount_id = products[i]["discount_id"];
            //     this.product.offer.offer_quantity = products[i]["offer_quantity"]+"";
            //     this.product.offer.base_quantity = products[i]["base_quantity"];
            //     this.product.discount_product_id = products[i]["discount_product_id"];
            //     this.product.discount_product_quantity = products[i]["discount_product_quantity"];
            //     this.product.offer.discount_product_name = products[i]["discount_product_name"];
                
            //     console.log( this.product.discount_product_quantity);
             }

             // commented by valar for hide discount - start
            this.product.product_price = products[i]["product_price"];
           
            console.log("product property"+JSON.stringify(this.product));
            this.allproducts.push(this.product);
        } 
    }

    editqty(id,index){

        console.log("id: " +id + "index: " +index );
        console.log("id: " + JSON.stringify(this.allproducts));
        console.log("orderqty in editqty: " +this.allproducts[index]["quantity"]);
        var rackedit = this.allproducts[index]["rack_quantity"];
        var orderedit = this.allproducts[index]["quantity"];
        console.log("orderedit: " + orderedit );

        let options: ModalDialogOptions = {
            context: { rack: rackedit, order: orderedit},
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(Dialog, options)
        .then((dialogResult: Array<string>) => this.setresult(dialogResult,id,index));

    }

    public setresult(quantity,productid,index){
        console.log("result: "+ quantity);

        if(quantity == "null"){
            return;
        }
        else{
        var rack = quantity[0];
        var order = quantity[1];

        console.log("res1: "+quantity[0] + "res2: "+ quantity[1]);

        console.log("New Value");      
        
        this.allproducts[index]["rack_quantity"] = rack;
        this.allproducts[index]["quantity"] = order;
         // commented by valar for hide discount - start

        // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
        // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
        // if(parseInt(order) >= baseQuantity ){
        //     var remainder = parseInt(order)%baseQuantity;
        //     var quotient = Math.floor(parseInt(order)/baseQuantity);
        //     var offerQuantity = (quotient * offer_Quantity);
        //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+"";       
        // }

         // commented by valar for hide discount - end
        this.changelinetotal(productid,index,order);
        this.changetotal();

    }
}

    removeorder(id,index){
        //this.allproducts.splice(index,1);

        let options = {
            title: "Are You Sure",
            message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        
        confirm(options).then((result: boolean) => {
            console.log("removeorder: " + result);
            if(result == true){
                this.allproducts.splice(index,1);
                console.log("Order Length: " + this.allproducts.length);
                this.changetotal();
                if(this.allproducts.length == 0){
                    console.log("Array null");
                    this.routerExtensions.navigate(["/productlist"], { clearHistory: true });
                    
                    dialogs.alert({
                        title: "Empty",
                        message: "Shop Products",
                        okButtonText: "Ok"
                    }).then(function () {
                        console.log("Products");
                        //this.routerExtensions.navigate(["/productlist"]);
                    });
                }
                else{
                    return;
                }
            }
            else{
                return;
            }
        });
    }

    

    changelinetotal(productid,index,order){

        for(var i=0 ; i < this.getproducts.length ; i++ ) {
            if(this.getproducts[i]["product_id"] == productid){
                var tempprice = this.getproducts[i]["product_price"];
                console.log("tempprice: "+tempprice);
            }
        }

        this.templinetotal = tempprice * order;
        console.log("Line total in change total: " + this.allproducts[index]["line_total"])
        this.allproducts[index]["line_total"] = this.templinetotal;        
        
    }

    changetotal(){
        this.total_amount = 0;
        for(var i=0 ; i < this.allproducts.length ; i++ ) {
            console.log(i +": " + this.allproducts[i]["product_id"]);
            var temptotal = this.allproducts[i]["line_total"];
            this.total_amount = temptotal + this.total_amount;
            console.log("totalamount: " + this.total_amount);
        }
         // commented by valar for hide discount - start

        // this.formattedData = [];
        // for(let j=0;j<this.allproducts.length;j++){
        //     if(this.allproducts[j].discount_id){
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price,
        //             'discount_id': this.allproducts[j].discount_id,
        //             'discount_product_quantity': this.allproducts[j].discount_product_quantity
        //         }); 
        //     }
        //     else {
        //         this.formattedData.push({
        //             'product_id':this.allproducts[j].product_id,
        //             'sku_id': this.allproducts[j].sku_id,
        //             'product_name': this.allproducts[j].product_name,
        //             'line_total': this.allproducts[j].line_total,
        //             'rack_quantity': this.allproducts[j].rack_quantity,
        //             'quantity': this.allproducts[j].quantity,
        //             'product_price': this.allproducts[j].product_price
                   
        //         });   

        //     }     
        // }
        // console.log("all product datas=====>"+JSON.stringify(this.formattedData));

         // commented by valar for hide discount - end

        this.saveorderservice.setScope(this.allproducts);
        this.savetotal.setScope(this.total_amount);
    }

    editshippingaddress(){        
           
        var doornoedit = this.address.door_no;
        var areaedit = this.address.area;
        var cityedit = this.address.city;
        var pinedit = this.address.pin;
        var streetnameedit = this.address.street_name;
        var districtedit = this.address.district;
        var stateedit = this.address.state;
        console.log("state: " + stateedit );

        let options: ModalDialogOptions = {
            context: { doorno: doornoedit, streetname:streetnameedit, area:areaedit, city:cityedit, district:districtedit,
                 state:stateedit, pin:pinedit },
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(DialogShipping, options)
        .then((dialogResult: Array<string>) => this.editaddress(dialogResult));
    }

    editaddress(address){

        if(address == "null"){
            return;
        }
        else{
            this.address.door_no = address[0];
            this.address.street_name = address[1];
            this.address.area = address[2];
            this.address.city = address[3];
            this.address.district = address[4];
            this.address.state = address[5];
            this.address.pin = address[6]; 
            this.customer.address = this.address;
            this.savecustomerdetails.setScope(this.customer);
        }

    }
}