"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../../Services/login.service");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var customer_service_1 = require("../../../Services/customer.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var customer_model_1 = require("../../../Models/customer.model");
var router_2 = require("nativescript-angular/router");
var common_1 = require("@angular/common");
var page_1 = require("tns-core-modules/ui/page/page");
var CustomerDetail = (function () {
    //address: Address;
    function CustomerDetail(route, customerservice, routerExtensions, savecustomer, savecustomerdetails, loginservice, saveorderservice, location, page) {
        this.route = route;
        this.customerservice = customerservice;
        this.routerExtensions = routerExtensions;
        this.savecustomer = savecustomer;
        this.savecustomerdetails = savecustomerdetails;
        this.loginservice = loginservice;
        this.saveorderservice = saveorderservice;
        this.location = location;
        this.page = page;
        this.title = "Customer Detail";
        //contactname: string = "PunniyaMoorthy";
        //shopname: string = "Kanniga Parameshwari Store";
        //contactnumber = "9123654789";
        //gstno = data.gst_no;
        //city = "Chennai";
        //door_no = "No.123";
        //area = "Amman Nagar";
        //pin = "600019";
        //street_name = "2nd Street";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.addordervisible = "visible";
    }
    CustomerDetail.prototype.ngOnInit = function () {
        var _this = this;
        // defalut datas 
        this.customer.shopname = "Abc Enterprices";
        this.customer.contactname = "Logesh";
        this.customer.contactnumber = 0987654321;
        this.address.city = "chennai";
        this.address.door_no = "4";
        this.address.area = "main area";
        this.address.pin = 639008;
        this.address.street_name = "main street";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.customer.address = this.address;
        //this.address = ({doorno:"No.123", street_name:"",area: ", opp. Eldam's Road, AnnaSalai", city:"Chennai", pin:"611111", contactnumber:"9874563210"})
        var id = this.route.snapshot.params["id"];
        //const id = "abcd";
        this.customerid = id;
        this.savecustomer.setScope(this.customerid);
        this.customerservice.getcustomerbyid(id)
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        console.log(id);
        this.savecustomer.setScope(id);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });
    };
    CustomerDetail.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
        this.savecustomerdetails.setScope(this.customer);
    };
    CustomerDetail.prototype.gotoshop = function () {
        this.routerExtensions.navigate(["/productlist"]);
        var a = false;
        this.saveorderservice.setScope(a);
    };
    return CustomerDetail;
}());
CustomerDetail = __decorate([
    core_1.Component({
        selector: "customer-detail",
        moduleId: module.id,
        templateUrl: "customerdetail.component.html",
        styleUrls: ["customerdetail.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, customer_service_1.CustomerService, router_2.RouterExtensions,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveCustomerDetails, login_service_1.loginService,
        saveorder_service_1.SaveOrderService, common_1.Location, page_1.Page])
], CustomerDetail);
exports.CustomerDetail = CustomerDetail;
