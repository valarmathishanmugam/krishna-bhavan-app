"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var customer_service_1 = require("../../../Services/customer.service");
var customer_model_1 = require("../../../Models/customer.model");
var page_1 = require("tns-core-modules/ui/page/page");
var platform_1 = require("platform");
var common_1 = require("@angular/common");
var login_service_1 = require("../../../Services/login.service");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var CustomerList = (function () {
    // enableMessage = 'collapse';
    function CustomerList(routerExtensions, customerservice, page, location, loginservice) {
        this.routerExtensions = routerExtensions;
        this.customerservice = customerservice;
        this.page = page;
        this.location = location;
        this.loginservice = loginservice;
        this.customerDetails = [];
        this.customers = new Array();
        this.original = new Array();
        this.repnamevisible = "visible";
        this.loader = new LoadingIndicator();
    }
    CustomerList.prototype.ngOnInit = function () {
        //code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        //code for loading indicator end here
        this.title = "Customer List";
        this.page.actionBarHidden = false;
        this.getcustomer();
        this.currentdate = new Date();
        console.log(this.currentdate.getDate() +
            "/" +
            (this.currentdate.getMonth() + 1) +
            "/" +
            this.currentdate.getFullYear());
        // application.android.on(
        //   AndroidApplication.activityBackPressedEvent,
        //   (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   }
        // );
        this.tempuser = this.loginservice.getScope();
        if (this.tempuser["role"] === "rep") {
            this.repnamevisible = "collapse";
        }
    };
    CustomerList.prototype.getcustomer = function () {
        var _this = this;
        this.customerservice
            .getcustomer()
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
    };
    CustomerList.prototype.parseCustomers = function (data) {
        //console.log("data: " + JSON.stringify(data));
        //console.log("Customer list: "+"length: "+ data.length + JSON.stringify(data));
        //  commented by valarmathi
        // for (let i = 0; i < data.length; i++) {
        //   this.customer = new Customer();
        //   this.customer.id = data[i].id.$oid;
        //   if (data[i].customer_name) {
        //     this.customer.contactname = data[i].customer_name;
        //   } else if (data[i].name) {
        //     this.customer.contactname = data[i].name;
        //   }
        //   if (data[i].slaesrep_name) {
        //     this.customer.salesrepname = data[i].slaesrep_name;
        //   }
        //   this.customer.contactnumber = data[i].mobile_no;
        //   this.customer.shopname = data[i].shopname;
        //   console.log(
        //     "cus_id: " +
        //       this.customer.id +
        //       "cus_name: " +
        //       this.customer.shopname +
        //       "rep: " +
        //       this.customer.contactname +
        //       "mobile: " +
        //       this.customer.contactnumber
        //   );
        //   this.customers.push(this.customer);
        //   this.original.push(this.customer);
        // }
        // created by valarmathi
        //   if (typeof data === 'string') {
        //     console.log("THER IS NO DATAS");
        //     this.loader.hide();
        //     this.enableMessage = 'visible';
        // }
        // else { 
        // console.log("THER IS DATAS"); 
        this.loader.hide();
        // this.enableMessage = 'collapse';
        for (var i = 0; i < data.length; i++) {
            this.customer = new customer_model_1.Customer();
            this.customer.id = data[i]._id.$oid;
            this.customer.contactname = data[i].name;
            this.customer.contactnumber = data[i].primary_mobile_no;
            this.customer.shopname = data[i].shop_name;
            this.customer.address = new customer_model_1.Address();
            this.customer.address.city = data[i].address[0].city;
            this.customer.address.district = data[i].address[0].district;
            console.log("cus_id: " +
                this.customer.id +
                "cus_name: " +
                this.customer.shopname +
                "rep: " +
                this.customer.contactname +
                "mobile: " +
                this.customer.contactnumber);
            this.customers.push(this.customer);
            this.original.push(this.customer);
        }
        //  }
    };
    CustomerList.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.customers = this.original.filter(function (result) {
            return result.contactname.toLowerCase().includes(searchValue) ||
                result.shopname.toLowerCase().includes(searchValue);
        });
    };
    CustomerList.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        console.log("searchValue: " + searchValue);
        // alert(searchValue);
        this.customers = this.original.filter(function (result) {
            return result.contactname.toLowerCase().includes(searchValue) ||
                result.shopname.toLowerCase().includes(searchValue);
        });
    };
    CustomerList.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
    };
    CustomerList.prototype.searchBarLoaded = function (args) {
        console.log("search bar loaded");
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return CustomerList;
}());
CustomerList = __decorate([
    core_1.Component({
        selector: "Customer-List",
        moduleId: module.id,
        templateUrl: "CustomerList.component.html",
        styleUrls: ["CustomerList.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        customer_service_1.CustomerService,
        page_1.Page,
        common_1.Location,
        login_service_1.loginService])
], CustomerList);
exports.CustomerList = CustomerList;
