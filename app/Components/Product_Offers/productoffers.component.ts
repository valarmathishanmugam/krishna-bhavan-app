import { loginService } from '../../Services/login.service';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { Dialog } from '../Dialog/dialog.component';
import { ProductService, SaveProducts, SaveOffers } from '../../Services/product.service';
import { SaveOrderService, SaveCustomer, SaveTotal } from '../../Services/saveorder.service';
import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { prompt, inputType } from "ui/dialogs";
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { ActivatedRoute } from '@angular/router';
import { Product, Offer } from "../../Models/product.model";
import { SearchBar } from "ui/search-bar";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { Http } from '@angular/http';
import {isAndroid} from "platform";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;





@Component({
    selector:"product-offers",
    moduleId: module.id,
    templateUrl:"productoffers.component.html",
    styleUrls: ["productoffers.component.css"]
})

export class ProductOffers{

    title: string = "Product List";
    productDetails: Array<Object> = [];
    productsOrdered: Array<Object> = [];
    orderedproducts: any;
    customerid: any;
    productid: Array<string> = [];
    orderquantity: Array<string> = [];
    rackquantity: Array<string> = [];
    productname: Array<string> = [];
    productprice: Array<string> = [];
    productdiscount: Array<string> = [];
    total: any;
    line_total: Array<string> = [];
    temp: any;
    totalAmount: any;
    //qtyedit: string =""
    boolean:boolean = false;
    index:any;
    loader = new LoadingIndicator();
    
    public searchPhrase: string;

    public products: Product[] = new Array();
    public productIn: Product;
    public offers: Offer;

    public original: Product[] = new Array();

    public addvisibility;
    public editvisibility;

    public ifcustomer;
    public tempuser;
    public tempoffer;
    // enableMessage = 'collapse';

    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, 
        private page: Page, private saveorderservice: SaveOrderService, private productservice: ProductService, 
        private savecustomer: SaveCustomer, private savetotal: SaveTotal, private saveproducts: SaveProducts,
        private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef,
        private loginservice: loginService, private http: Http, private saveoffers: SaveOffers, private location: Location){

            
            //let searchBar = <SearchBar>args.object;
            //searchBar.dismissSoftInput();
        }

    ngOnInit(){

       
        // code for loading indicator start here
       let options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
              indeterminate: true,
              cancelable: false,
              max: 100,
              progressNumberFormat: "%1d/%2d",
              progressPercentFormat: 0.53,
              progressStyle: 1,
              secondaryProgress: 1
            },
            ios: {
              details: "Additional detail note!",
              square: false,
              margin: 10,
              dimBackground: true,
              color: "#4B9ED6",
              mode: ""// see iOS specific options below
            }
          };
           
          this.loader.show(options);
          // code for loading indicator end here

        //application.android.off(AndroidApplication.activityBackPressedEvent);
        this.page.actionBarHidden = false;
        this.ifcustomer = "visible"
        //this.getproducts();
        this.productservice.getDiscounts()
        .subscribe((data) => {this.loadDiscounts(data.json())});
        this.tempuser = this.loginservice.getScope()
        this.checkcustomer(this.tempuser);
        
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     // if(this.tempuser.role == 'rep'){
        //     //     this.routerExtensions.navigate(["/customerlist"]);
        //     // }
        //     this.routerExtensions.backToPreviousPage();
        //   });
    }
    loadDiscounts(data){           
        // if (typeof data === 'string') {
        //     this.loader.hide();
        //     this.enableMessage = 'visible';
        // }
        // else {   
            this.loader.hide();
            // this.enableMessage = 'collapse';
            for(let i=0;i<data.length;i++){
            console.log("discount"+i);
            if(data[i].image_name != ""){     
                data[i].image_name = "https://app.krishnabhavanfoods.in/"+ data[i].image_name;
            }
            else{
                data[i].image_name = "~/Images/product_img.png"
            }
            data[i].valid_from = data[i].valid_from.slice(0, 11);
            data[i].valid_to = data[i].valid_to.slice(0, 11);
            
            this.products.push(data[i]);
            console.log("data inside the product array"+ JSON.stringify(this.products));
            }
        // }
        
    }

    checkcustomer(data){
        if(data.role === 'customer'){
            this.ifcustomer = "collapse"
        }
        else{
            return;
        }
    }
    getproducts(){
        this.productservice.getproduct()
        .subscribe(data => this.parseProducts(data.json()));
    }
   
    

    parseProducts(data: any) {
        console.log("data.length:" + data.length);
    
        var tempdiscounttotal = 0;
        for(let i=0; i < data.length; i++) {    
            
            if(data[i].discount){       
                
                //console.log("Offer Region: " + data[i].discount["0"].region)
                //console.log("User Region: " + this.tempuser.region)

                for(var a=0; a < 1; a++){
                    console.log("data.length:" + data.length);

                    if(data[i].discount[a].region === this.tempuser.region){
                        console.log("Offer Inside Region: " + data[i].discount[a].region)
                        var tempproducts = data[i];
                        //console.log("Discount Product Details: " + JSON.stringify(data[i])) 
                        var tempid = data[i].discount[a].discount_id.$oid;
                        this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" +tempid)
                        //.subscribe(data => this.getdiscountdetails(tempproducts,data.json()));
                        .subscribe(data =>this.saveoffers.setScope(data));
                        
                        this.tempoffer = this.saveoffers.getScope();

                        console.log("TempOffer: " + JSON.stringify(this.tempoffer))
                        this.productIn = new Product();
                        this.productIn.product_id = data[i]._id.$oid;
                        this.productIn.product_name = data[i].name;
                        this.productIn.categoryName = data[i].category_name;
                        this.productIn.product_price = data[i].price[0].price; 
                        
                        this.offers = new Offer();
                        this.offers.base_quantity = "2";
                        this.offers.offer_quantity = "1";
                        this.productIn.offer = this.offers; 
            
                        this.products.push(this.productIn);
                        
                        tempdiscounttotal = tempdiscounttotal + 1;   
                        //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    }
                    
                    else{}
                }
                //this.original.push(this.productIn);
            }
            else{}     
            
            
        }  
        
        console.log("tempdiscounttotal: " + tempdiscounttotal)
        this.saveproducts.setScope(this.products);

        this.orderedproducts = this.saveorderservice.getScope();
        
        console.log("Ordered products: " + this.orderedproducts);

        if(this.orderedproducts !== false){
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else{
            return;
        }
    }

    getdiscountdetails(productdetails,data){
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity)

        this.tempoffer.base_quantity = data.base_quantity;
        this.tempoffer.offer_quantity = data.offer_quantity;
        /*
        console.log("Discount Product Details: " + JSON.stringify(productdetails))
        
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity)

        this.productIn = new Product();        
        this.productIn.product_id = productdetails._id.$oid;
        this.productIn.product_name = productdetails.name;
        this.productIn.categoryName = productdetails.category_name;
        this.productIn.product_price = productdetails.price[0].price; 
        
        console.log("Discount Product Name: " + productdetails.name)

        this.offers = new Offer();
        this.offers.base_quantity = data.base_quantity;
        this.offers.offer_quantity = data.offer_quantity;
        this.productIn.offer = this.offers; 

        this.products.push(this.productIn);
        */
    }

    addtocart(productid,index){
        console.log("id: "+ productid);
        this.boolean = false;
        for(var i=0 ; i < this.productsOrdered.length ; i++ ) {
            if(this.productsOrdered[i]["product_id"] == productid){
                console.log("already inserted");

                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: "+ rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }

        let options: ModalDialogOptions = {
            context: { rack: rackedit, order: orderedit},
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(Dialog, options)
        .then((dialogResult: Array<string>) => this.setresult(dialogResult,productid));

    }

    public setresult(quantity,productid){
        console.log("result: "+ quantity);

        if(quantity == "null"){
            return;
        }
        else{
        var rack = quantity[0];
        var order = quantity[1];

        console.log("res1: "+quantity[0] + "res2: "+ quantity[1]);


        for(var i = 0; i < this.products.length; i++){
            if(this.products[i]["product_id"] == productid){
                this.temp = order;
                this.total = this.products[i]["product_price"];
                this.total = this.total*this.temp;
                //console.log("this.line_total: "+ this.total);
                if(this.boolean === false){
                    this.rackQty(productid, rack, order, this.total);
                }
                else{
                    this.rackQtyedit(rack, order, this.total, this.index);
                }
                console.log("promptid: "+ this.products[i]["product_id"]);
                this.products[i]["rackquantity"] = rack;
                this.products[i]["quantity"] = order;
            }
        }
    }
}

    rackQty(productid, rack, order, line_total){
        console.log("Inside rack Qty");
        this.productid.push(productid);        
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);

        for(let i=0; i < this.products.length; i++) {

            if(this.products[i].product_id == productid){

                this.productname.push(this.products[i].product_name);
                this.productprice.push(this.products[i].product_price);
                this.productdiscount.push(this.products[i].discount_id);
            }
        }

        //console.log("line_total: " + this.line_total);        
        console.log("ProductId: "+ this.productid + "orderquantity: "+ this.orderquantity + "rackquantity: "+ this.rackquantity+ "line_total: "+ this.line_total);
        
        var temp =  (this.productid.length) - 1;     
        this.productsOrdered.push({product_id:this.productid[temp], sku_id:this.productid[temp], 
            quantity:this.orderquantity[temp], rack_quantity:this.rackquantity[temp], product_name:this.productname[temp], 
            line_total:this.line_total[temp], discount_id:this.productdiscount[temp], product_price:this.productprice[temp]});
    
        this.gettotal();
    }

    rackQtyedit(rack, order, line_total,index){
        console.log("Inside rack Qty edit");
        
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        
        this.gettotal();
    }

    gettotal(){
        this.totalAmount = 0;
        for(var i=0 ; i < this.productsOrdered.length ; i++ ) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal+this.totalAmount;
            //console.log("totalamount: " + this.totalAmount);
        }
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    }

    productdetail(){
        //this.routerExtensions.navigate(["/productdetail"]);
    }

    updateproducts(orderedproducts){
        
        for(var a=0; a < this.products.length; a++){
            for(var i=0; i < orderedproducts.length; i++){
                if(this.products[a]["product_id"] == orderedproducts[i]["product_id"]){
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    }
    
    public onSubmit(args) {        
        let searchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        let searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(result=> result.product_name.toLowerCase().includes(searchValue)  || 
        result.categoryName.toLowerCase().includes(searchValue));
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }
    
    public onTextChanged(args) {
        let searchBar = <SearchBar>args.object;
        let searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(result=> result.product_name.toLowerCase().includes(searchValue) || 
        result.categoryName.toLowerCase().includes(searchValue));
    }
    
    public onClear(args) {
        let searchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }

    searchBarLoaded(args){
        let searchBar = <SearchBar>args.object;
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }
    

}