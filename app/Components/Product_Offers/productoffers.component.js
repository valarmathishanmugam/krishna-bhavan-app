"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../Services/login.service");
var dialog_component_1 = require("../Dialog/dialog.component");
var product_service_1 = require("../../Services/product.service");
var saveorder_service_1 = require("../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var product_model_1 = require("../../Models/product.model");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var http_1 = require("@angular/http");
var platform_1 = require("platform");
var common_1 = require("@angular/common");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ProductOffers = (function () {
    // enableMessage = 'collapse';
    function ProductOffers(route, routerExtensions, page, saveorderservice, productservice, savecustomer, savetotal, saveproducts, modalService, viewContainerRef, loginservice, http, saveoffers, location) {
        this.route = route;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.saveorderservice = saveorderservice;
        this.productservice = productservice;
        this.savecustomer = savecustomer;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.loginservice = loginservice;
        this.http = http;
        this.saveoffers = saveoffers;
        this.location = location;
        this.title = "Product List";
        this.productDetails = [];
        this.productsOrdered = [];
        this.productid = [];
        this.orderquantity = [];
        this.rackquantity = [];
        this.productname = [];
        this.productprice = [];
        this.productdiscount = [];
        this.line_total = [];
        //qtyedit: string =""
        this.boolean = false;
        this.loader = new LoadingIndicator();
        this.products = new Array();
        this.original = new Array();
        //let searchBar = <SearchBar>args.object;
        //searchBar.dismissSoftInput();
    }
    ProductOffers.prototype.ngOnInit = function () {
        var _this = this;
        // code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        // code for loading indicator end here
        //application.android.off(AndroidApplication.activityBackPressedEvent);
        this.page.actionBarHidden = false;
        this.ifcustomer = "visible";
        //this.getproducts();
        this.productservice.getDiscounts()
            .subscribe(function (data) { _this.loadDiscounts(data.json()); });
        this.tempuser = this.loginservice.getScope();
        this.checkcustomer(this.tempuser);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     // if(this.tempuser.role == 'rep'){
        //     //     this.routerExtensions.navigate(["/customerlist"]);
        //     // }
        //     this.routerExtensions.backToPreviousPage();
        //   });
    };
    ProductOffers.prototype.loadDiscounts = function (data) {
        // if (typeof data === 'string') {
        //     this.loader.hide();
        //     this.enableMessage = 'visible';
        // }
        // else {   
        this.loader.hide();
        // this.enableMessage = 'collapse';
        for (var i = 0; i < data.length; i++) {
            console.log("discount" + i);
            if (data[i].image_name != "") {
                data[i].image_name = "https://app.krishnabhavanfoods.in/" + data[i].image_name;
            }
            else {
                data[i].image_name = "~/Images/product_img.png";
            }
            data[i].valid_from = data[i].valid_from.slice(0, 11);
            data[i].valid_to = data[i].valid_to.slice(0, 11);
            this.products.push(data[i]);
            console.log("data inside the product array" + JSON.stringify(this.products));
        }
        // }
    };
    ProductOffers.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            return;
        }
    };
    ProductOffers.prototype.getproducts = function () {
        var _this = this;
        this.productservice.getproduct()
            .subscribe(function (data) { return _this.parseProducts(data.json()); });
    };
    ProductOffers.prototype.parseProducts = function (data) {
        var _this = this;
        console.log("data.length:" + data.length);
        var tempdiscounttotal = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].discount) {
                //console.log("Offer Region: " + data[i].discount["0"].region)
                //console.log("User Region: " + this.tempuser.region)
                for (var a = 0; a < 1; a++) {
                    console.log("data.length:" + data.length);
                    if (data[i].discount[a].region === this.tempuser.region) {
                        console.log("Offer Inside Region: " + data[i].discount[a].region);
                        var tempproducts = data[i];
                        //console.log("Discount Product Details: " + JSON.stringify(data[i])) 
                        var tempid = data[i].discount[a].discount_id.$oid;
                        this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" + tempid)
                            .subscribe(function (data) { return _this.saveoffers.setScope(data); });
                        this.tempoffer = this.saveoffers.getScope();
                        console.log("TempOffer: " + JSON.stringify(this.tempoffer));
                        this.productIn = new product_model_1.Product();
                        this.productIn.product_id = data[i]._id.$oid;
                        this.productIn.product_name = data[i].name;
                        this.productIn.categoryName = data[i].category_name;
                        this.productIn.product_price = data[i].price[0].price;
                        this.offers = new product_model_1.Offer();
                        this.offers.base_quantity = "2";
                        this.offers.offer_quantity = "1";
                        this.productIn.offer = this.offers;
                        this.products.push(this.productIn);
                        tempdiscounttotal = tempdiscounttotal + 1;
                        //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    }
                    else { }
                }
                //this.original.push(this.productIn);
            }
            else { }
        }
        console.log("tempdiscounttotal: " + tempdiscounttotal);
        this.saveproducts.setScope(this.products);
        this.orderedproducts = this.saveorderservice.getScope();
        console.log("Ordered products: " + this.orderedproducts);
        if (this.orderedproducts !== false) {
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else {
            return;
        }
    };
    ProductOffers.prototype.getdiscountdetails = function (productdetails, data) {
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity);
        this.tempoffer.base_quantity = data.base_quantity;
        this.tempoffer.offer_quantity = data.offer_quantity;
        /*
        console.log("Discount Product Details: " + JSON.stringify(productdetails))
        
        console.log(data._id.$oid + " Buy " + data.base_quantity + " Get " + data.offer_quantity)

        this.productIn = new Product();
        this.productIn.product_id = productdetails._id.$oid;
        this.productIn.product_name = productdetails.name;
        this.productIn.categoryName = productdetails.category_name;
        this.productIn.product_price = productdetails.price[0].price;
        
        console.log("Discount Product Name: " + productdetails.name)

        this.offers = new Offer();
        this.offers.base_quantity = data.base_quantity;
        this.offers.offer_quantity = data.offer_quantity;
        this.productIn.offer = this.offers;

        this.products.push(this.productIn);
        */
    };
    ProductOffers.prototype.addtocart = function (productid, index) {
        var _this = this;
        console.log("id: " + productid);
        this.boolean = false;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            if (this.productsOrdered[i]["product_id"] == productid) {
                console.log("already inserted");
                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: " + rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, productid); });
    };
    ProductOffers.prototype.setresult = function (quantity, productid) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            for (var i = 0; i < this.products.length; i++) {
                if (this.products[i]["product_id"] == productid) {
                    this.temp = order;
                    this.total = this.products[i]["product_price"];
                    this.total = this.total * this.temp;
                    //console.log("this.line_total: "+ this.total);
                    if (this.boolean === false) {
                        this.rackQty(productid, rack, order, this.total);
                    }
                    else {
                        this.rackQtyedit(rack, order, this.total, this.index);
                    }
                    console.log("promptid: " + this.products[i]["product_id"]);
                    this.products[i]["rackquantity"] = rack;
                    this.products[i]["quantity"] = order;
                }
            }
        }
    };
    ProductOffers.prototype.rackQty = function (productid, rack, order, line_total) {
        console.log("Inside rack Qty");
        this.productid.push(productid);
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);
        for (var i = 0; i < this.products.length; i++) {
            if (this.products[i].product_id == productid) {
                this.productname.push(this.products[i].product_name);
                this.productprice.push(this.products[i].product_price);
                this.productdiscount.push(this.products[i].discount_id);
            }
        }
        //console.log("line_total: " + this.line_total);        
        console.log("ProductId: " + this.productid + "orderquantity: " + this.orderquantity + "rackquantity: " + this.rackquantity + "line_total: " + this.line_total);
        var temp = (this.productid.length) - 1;
        this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
            quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
            line_total: this.line_total[temp], discount_id: this.productdiscount[temp], product_price: this.productprice[temp] });
        this.gettotal();
    };
    ProductOffers.prototype.rackQtyedit = function (rack, order, line_total, index) {
        console.log("Inside rack Qty edit");
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        this.gettotal();
    };
    ProductOffers.prototype.gettotal = function () {
        this.totalAmount = 0;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal + this.totalAmount;
            //console.log("totalamount: " + this.totalAmount);
        }
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    };
    ProductOffers.prototype.productdetail = function () {
        //this.routerExtensions.navigate(["/productdetail"]);
    };
    ProductOffers.prototype.updateproducts = function (orderedproducts) {
        for (var a = 0; a < this.products.length; a++) {
            for (var i = 0; i < orderedproducts.length; i++) {
                if (this.products[a]["product_id"] == orderedproducts[i]["product_id"]) {
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    };
    ProductOffers.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductOffers.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
    };
    ProductOffers.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductOffers.prototype.searchBarLoaded = function (args) {
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return ProductOffers;
}());
ProductOffers = __decorate([
    core_1.Component({
        selector: "product-offers",
        moduleId: module.id,
        templateUrl: "productoffers.component.html",
        styleUrls: ["productoffers.component.css"]
    }),
    __metadata("design:paramtypes", [router_2.ActivatedRoute, router_1.RouterExtensions,
        page_1.Page, saveorder_service_1.SaveOrderService, product_service_1.ProductService,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef,
        login_service_1.loginService, http_1.Http, product_service_1.SaveOffers, common_1.Location])
], ProductOffers);
exports.ProductOffers = ProductOffers;
