"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customer_model_1 = require("../../Models/customer.model");
var customer_service_1 = require("../../Services/customer.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var dialogs_1 = require("ui/dialogs");
var router_2 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var ApproveCustomerDetail = (function () {
    function ApproveCustomerDetail(customerservice, route, http, routerExtensions, page, location) {
        this.customerservice = customerservice;
        this.route = route;
        this.http = http;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.location = location;
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.disableApproveFAB = true;
    }
    ApproveCustomerDetail.prototype.ngOnInit = function () {
        var _this = this;
        //    datas for initial value start here
        this.customer.shopname = "ABC Departmental store";
        this.customer.contactname = "name";
        this.address.door_no = "7";
        this.address.street_name = "some street";
        this.address.area = "some area";
        this.address.city = "chennai";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.address.pin = 658990;
        this.customer.contactnumber = 0987654321;
        //    datas for initial value end here
        var id = this.route.snapshot.params["id"];
        this.customerid = id;
        this.customerservice.getcustomerbyid(id)
            .subscribe(function (data) { return _this.parseCustomers(data.json()); });
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });  
    };
    ApproveCustomerDetail.prototype.parseCustomers = function (data) {
        console.log(data.shop_name);
        this.customer.shopname = data.shop_name;
        this.customer.contactname = data.name;
        this.customer.contactnumber = data.primary_mobile_no;
        this.customer.gstno = data.gst_no;
        this.address.city = data.address[0].city;
        this.address.door_no = data.address[0].door_no;
        this.address.area = data.address[0].area;
        this.address.pin = data.address[0].pin;
        this.address.street_name = data.address[0].street_name;
        this.address.district = data.address[0].district;
        this.address.state = data.address[0].state;
        this.customer.address = this.address;
    };
    ApproveCustomerDetail.prototype.gotoshop = function () {
        var _this = this;
        var options = {
            title: "Approve Customer",
            //message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.routerExtensions.navigate(["/approvecustomerlist"]);
                var data = { "status": "active" };
                return _this.http.post("https://app.krishnabhavanfoods.in/api/customer/customer_approval/" + _this.customerid, data, { headers: _this.getCommonHeaders() })
                    .subscribe(function (result) {
                    _this.disableApproveFAB = false;
                    console.log("Approve Customer Service Result: " + result);
                }, function (error) {
                    console.log("Approve Customer Service Error: " + error);
                });
            }
            else {
                return;
            }
        });
    };
    ApproveCustomerDetail.prototype.getCommonHeaders = function () {
        var headers = new http_1.Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };
    return ApproveCustomerDetail;
}());
ApproveCustomerDetail = __decorate([
    core_1.Component({
        selector: "approve-customer-detail",
        moduleId: module.id,
        templateUrl: "approve_customer_detail.component.html",
        styleUrls: ["approve_customer_detail.component.css"]
    }),
    __metadata("design:paramtypes", [customer_service_1.CustomerService, router_1.ActivatedRoute, http_1.Http,
        router_2.RouterExtensions, page_1.Page, common_1.Location])
], ApproveCustomerDetail);
exports.ApproveCustomerDetail = ApproveCustomerDetail;
