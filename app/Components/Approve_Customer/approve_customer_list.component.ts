import { Customer } from '../../Models/customer.model';
import { CustomerService } from '../../Services/customer.service';
import { Component } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import { RouterExtensions } from 'nativescript-angular/router';
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;

@Component({
    selector: "approve-customer-list",
    moduleId: module.id,
    templateUrl: "approve_customer_list.component.html",
    styleUrls: ["approve_customer_list.component.css"]
})

export class ApproveCustomerList{
    
    public customers: Customer[] = new Array();
    public customer: Customer;
    loader = new LoadingIndicator();

    constructor( private customerservice: CustomerService, private page: Page, private location: Location, private routerExtensions: RouterExtensions){
        
    }
    
      ngOnInit() {
          //code for loading indicator start here
          let options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
              indeterminate: true,
              cancelable: false,
              max: 100,
              progressNumberFormat: "%1d/%2d",
              progressPercentFormat: 0.53,
              progressStyle: 1,
              secondaryProgress: 1
            },
            ios: {
              details: "Additional detail note!",
              square: false,
              margin: 10,
              dimBackground: true,
              color: "#4B9ED6",
              mode:"" // see iOS specific options below
            }
          };
           
          this.loader.show(options); 
          //code for loading indicator end here
        this.getcustomer();
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });     
          
      }

      getcustomer() {
        this.customerservice.getunapprovedcustomer()
        .subscribe(data => this.parseCustomers(data.json()));
    } 
    parseCustomers(data: any) {
        this.loader.hide();
        //console.log("data: " + JSON.stringify(data));
        console.log(data[0]._id.$oid);
    
        for(let i = 0; i < data.length; i++) {
    
            this.customer = new Customer();
            this.customer.id = data[i]._id.$oid
            this.customer.shopname = data[i].shop_name;
            this.customer.contactname = data[i].name;
    
            this.customers.push(this.customer);
        }  
    
    }
}