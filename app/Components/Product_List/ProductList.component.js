"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_service_1 = require("../../Services/login.service");
var dialog_component_1 = require("../Dialog/dialog.component");
var product_service_1 = require("../../Services/product.service");
var saveorder_service_1 = require("../../Services/saveorder.service");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("@angular/router");
var product_model_1 = require("../../Models/product.model");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var platform_1 = require("platform");
var http_1 = require("@angular/http");
var common_1 = require("@angular/common");
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;
var ProductList = (function () {
    // enableMessage = 'collapse';
    function ProductList(route, routerExtensions, http, page, saveorderservice, productservice, savecustomer, savetotal, saveproducts, modalService, viewContainerRef, loginservice, location) {
        this.route = route;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.page = page;
        this.saveorderservice = saveorderservice;
        this.productservice = productservice;
        this.savecustomer = savecustomer;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.loginservice = loginservice;
        this.location = location;
        this.title = "Product List";
        this.productDetails = [];
        this.productsOrdered = [];
        this.productid = [];
        this.orderquantity = [];
        this.rackquantity = [];
        this.productname = [];
        this.productprice = [];
        this.productdiscount = [];
        // commented by valar
        this.discount_product_id = [];
        this.discount_product_name = [];
        this.offer_quantity = [];
        this.base_quantity = [];
        this.discount_product_quantity = [];
        this.line_total = [];
        //qtyedit: string =""
        this.boolean = false;
        this.Productproperty = [];
        this.loader = new LoadingIndicator();
        this.products = new Array();
        this.original = new Array();
        this.offervisible = "collapse";
        this.customerselected = "collapse";
        this.totalvisible = "collapse";
        //let searchBar = <SearchBar>args.object;
        //searchBar.dismissSoftInput();
    }
    ProductList.prototype.ngOnInit = function () {
        // code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                details: "Additional detail note!",
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#4B9ED6",
                mode: "" // see iOS specific options below
            }
        };
        this.loader.show(options);
        // code for loading indicator end here
        this.page.actionBarHidden = false;
        this.ifcustomer = "visible";
        /*
        this.productDetails.push({id: 1, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 2, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 3, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 4, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 5, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        */
        //const id = this.route.snapshot.params["id"];
        //console.log(id);
        this.getproducts();
        this.tempuser = this.loginservice.getScope();
        this.checkcustomer(this.tempuser);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });   
        this.getcustomer = this.savecustomer.getScope();
        if (this.getcustomer !== false && this.getcustomer.length != 0) {
            console.log("Customer Length: " + this.getcustomer.length);
            this.customerselected = "visible";
            if (this.tempuser.role !== 'customer') {
                this.totalvisible = "visible";
            }
        }
    };
    ProductList.prototype.checkcustomer = function (data) {
        if (data.role === 'customer') {
            this.ifcustomer = "collapse";
        }
        else {
            return;
        }
    };
    ProductList.prototype.getproducts = function () {
        var _this = this;
        this.productservice.getproduct()
            .subscribe(function (data) { return _this.parseProducts(data.json()); });
    };
    ProductList.prototype.parseProducts = function (data) {
        this.loader.hide();
        console.log("data.length:" + data.length);
        for (var i = 0; i < data.length; i++) {
            this.offervisible = "collapse";
            var tempid = "";
            //this.addvisibility[i] = "visible";
            //this.editvisibility[i] = "collapse";
            if (data[i].discount) {
                for (var a = 0; a < 1; a++) {
                    console.log("data.length:" + data.length);
                    // created by valarmathi
                    this.offervisible = "visible";
                    console.log("Offer Inside Region: " + data[i].discount[a].region);
                    tempid = data[i].discount[a]._id.$oid;
                    if (data[i].discount) {
                        this.offers = new product_model_1.Offer();
                        this.offers.offervisible = "visible";
                        this.offers.base_quantity = data[i].discount[a].base_quantity;
                        this.offers.offer_quantity = data[i].discount[a].offer_quantity;
                    }
                    // commented by valarmathi
                    // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
                    //     var tempUserRegion = this.tempuser.region.find(item => item == data[i].discount[a].region);
                    // }
                    // else{
                    //     var tempUserRegion = this.tempuser.region;
                    // }
                    // console.log("USER REGION =====>"+tempUserRegion);
                    // if(data[i].discount[a].region === tempUserRegion){
                    //     this.offervisible = "visible";
                    //     console.log("Offer Inside Region: " + data[i].discount[a].region)
                    //     tempid = data[i].discount[a].discount_id.$oid;
                    //     // this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" +tempid)
                    //     // .subscribe(datas => {
                    //     //     this.offerDetails = datas;
                    //     //     console.log("product offer details"+this.offerDetails.json());
                    //     // });
                    //     //.subscribe(data =>this.saveoffers.setScope(data));
                    //     //this.tempoffer = this.saveoffers.getScope();
                    //     //console.log("TempOffer: " + JSON.stringify(this.tempoffer))     
                    //     //this.productIn.discount_id = tempid; 
                    //     if(data[i].discount_list){
                    //         this.offers = new Offer();
                    //         this.offers.offervisible = "visible"
                    //         this.offers.base_quantity = data[i].discount_list[0][0].base_quantity;
                    //         this.offers.offer_quantity = data[i].discount_list[0][0].offer_quantity;
                    //     }                 
                    //     //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    // }
                    // else{
                    //     //this.productIn.discount_id = ""; 
                    //     this.offers = new Offer();
                    //     this.offers.offervisible = "collapse"
                    // }
                }
            }
            else {
                //this.productIn.discount_id = ""; 
                this.offers = new product_model_1.Offer();
                this.offers.offervisible = "collapse";
            }
            this.productIn = new product_model_1.Product();
            this.productIn.product_id = data[i]._id.$oid;
            this.productIn.product_name = data[i].name;
            this.productIn.categoryName = data[i].category_name;
            this.productIn.case_quantity = data[i].case_quantity;
            //this.productIn.discount_id = "5a91165f9733343884af261d";
            this.productIn.discount_id = tempid;
            // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
            //   for(let j=0;j<this.tempuser.region.length;j++){
            //     for(let k=0;k<data[i].price.length;k++){
            //         if(this.tempuser.region[j] == data[i].price[k].name){
            //             this.productIn.product_price = data[i].price[k].price;
            //         }
            //     }
            //   }
            // }
            // else if(this.tempuser.role == "customer"){
            //     for(let m=0;m<data[i].price.length;m++){
            //         if(this.tempuser.region == data[i].price[m].name){
            //             this.productIn.product_price = data[i].price[m].price;
            //         }
            //     }
            // }
            this.productIn.product_price = data[i].price[0].price;
            if (data[i].discount) {
                this.productIn.discount = data[i].discount[0];
            }
            if (data[i].image_name) {
                //console.log(data[i].name + " : " + data[i].image_name)           
                this.productIn.image = "https://app.krishnabhavanfoods.in/" + data[i].image_name;
            }
            else {
                //console.log(data[i].name + " : " + "Noo")
                this.productIn.image = "~/Images/product_img.png";
            }
            this.productIn.offer = this.offers;
            this.products.push(this.productIn);
            this.original.push(this.productIn);
        }
        this.saveproducts.setScope(this.products);
        this.orderedproducts = this.saveorderservice.getScope();
        console.log("Ordered products: " + this.orderedproducts);
        if (this.orderedproducts !== false) {
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else {
            return;
        }
        // }
    };
    ProductList.prototype.addtocart = function (productid, index) {
        var _this = this;
        console.log("id: " + productid);
        this.boolean = false;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            if (this.productsOrdered[i]["product_id"] == productid) {
                console.log("already inserted");
                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: " + rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) { return _this.setresult(dialogResult, productid); });
    };
    ProductList.prototype.setresult = function (quantity, productid) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            rack = String(rack);
            var order = quantity[1];
            order = String(order);
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            for (var i = 0; i < this.products.length; i++) {
                if (this.products[i]["product_id"] == productid) {
                    this.temp = order;
                    this.total = this.products[i]["product_price"];
                    this.total = this.total * this.temp;
                    //this.total = String(this.total);
                    //console.log("this.line_total: "+ this.total);
                    if (this.boolean === false) {
                        this.rackQty(productid, rack, order, this.total);
                    }
                    else {
                        this.rackQtyedit(rack, order, this.total, this.index, productid);
                    }
                    console.log("promptid: " + this.products[i]["product_id"]);
                    this.products[i]["rackquantity"] = rack;
                    this.products[i]["quantity"] = order;
                }
            }
        }
    };
    ProductList.prototype.rackQty = function (productid, rack, order, line_total) {
        console.log("Inside rack Qty");
        this.productid.push(productid);
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);
        for (var i = 0; i < this.products.length; i++) {
            if (this.products[i].product_id == productid) {
                this.productname.push(this.products[i].product_name);
                this.productprice.push(String(this.products[i].product_price));
                if (this.products[i].discount) {
                    var nodiscount = false;
                    console.log("GET Discount values ==>" + JSON.stringify(this.products[i].discount['_id'].$oid));
                    this.productdiscount.push(this.products[i].discount['_id'].$oid);
                    console.log("DISCOUNT LIST" + JSON.stringify(this.products[i].discount));
                    // commented by valar for hide discount - start
                    //  this.discount_product_id.push(this.products[i].discount['discount_product_id'].$oid);
                    //  this.base_quantity.push(this.products[i].discount['base_quantity'] +"");
                    //  this.offer_quantity.push(this.products[i].discount['offer_quantity']+"");
                    //  if(this.products[i].discount['discount_product_name']){
                    //  this.discount_product_name.push(this.products[i].discount['discount_product_name']);
                    //  }
                    //  else {
                    //     this.discount_product_name.push("");   
                    //  }
                    //  let baseQuantity = parseInt(this.products[i].discount['base_quantity']);
                    //  let offer_quantity = parseInt(this.products[i].discount['offer_quantity']);
                    //  if(order >= baseQuantity){
                    //      var remainder = order%baseQuantity;
                    //      var quotient = Math.floor(order/baseQuantity);
                    //      var offerQuantity = (quotient * offer_quantity);
                    //      this.discount_product_quantity.push(offerQuantity+"");                   
                    //  }
                    // commented by valar for hide discount - end                 
                }
                else {
                    var nodiscount = true;
                }
            }
        }
        //console.log("line_total: " + this.line_total);        
        var temp = (this.productid.length) - 1;
        console.log("temp value" + temp);
        if (nodiscount === false) {
            console.log("Inserting discount product");
            for (var d = 0; d < this.products.length; d++) {
                if (this.products[d].product_id == this.productid[temp]) {
                    var discount_id = this.products[d].discount_id;
                    var temp1 = this.productdiscount.indexOf(discount_id);
                    console.log("temp1 value" + temp1);
                }
            }
            this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
                quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
                line_total: this.line_total[temp], discount_id: this.productdiscount[temp1], product_price: this.productprice[temp]
                // commented by valar for hide discount - start
                //  ,discount_product_id:this.discount_product_id[temp1],discount_product_quantity:this.discount_product_quantity[temp1],discount_product_name:this.discount_product_name[temp1],offer_quantity:this.offer_quantity[temp1],base_quantity:this.base_quantity[temp1]
                // commented by valar for hide discount - end
            });
        }
        else if (nodiscount === true) {
            console.log("Inserting No Discount Product");
            this.productsOrdered.push({ product_id: this.productid[temp], sku_id: this.productid[temp],
                quantity: this.orderquantity[temp], rack_quantity: this.rackquantity[temp], product_name: this.productname[temp],
                line_total: this.line_total[temp], product_price: this.productprice[temp] });
        }
        this.gettotal();
    };
    ProductList.prototype.rackQtyedit = function (rack, order, line_total, index, productid) {
        console.log("Inside rack Qty edit");
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        // commented by valar for hide discount - start
        //  for(let i=0; i < this.products.length; i++) { 
        //      if(this.products[i].product_id == productid){
        //          if(this.products[i].discount){ 
        //             let baseQuantity = parseInt(this.products[i].discount['base_quantity']); 
        //             let offer_Quantity = parseInt(this.products[i].discount['offer_quantity']);
        //              if(order >= baseQuantity){
        //                  var remainder = order%baseQuantity;
        //                  var quotient = Math.floor(order/baseQuantity);
        //                  var offerQuantity = (quotient * offer_Quantity);
        //                  this.productsOrdered[index]["discount_product_quantity"] = offerQuantity+"";                        
        //              }    
        //       }
        //     }
        //  }
        // commented by valar for hide discount - end
        this.gettotal();
    };
    ProductList.prototype.gettotal = function () {
        this.totalAmount = 0;
        for (var i = 0; i < this.productsOrdered.length; i++) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal + this.totalAmount;
            console.log("totalamont: " + this.totalAmount);
            //this.totalAmount = String(this.totalAmount);
        }
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    };
    ProductList.prototype.productdetail = function () {
        //this.routerExtensions.navigate(["/productdetail"]);
    };
    ProductList.prototype.updateproducts = function (orderedproducts) {
        for (var a = 0; a < this.products.length; a++) {
            for (var i = 0; i < orderedproducts.length; i++) {
                if (this.products[a]["product_id"] == orderedproducts[i]["product_id"]) {
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    };
    ProductList.prototype.onSubmit = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        var searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductList.prototype.onTextChanged = function (args) {
        var searchBar = args.object;
        var searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(function (result) { return result.product_name.toLowerCase().includes(searchValue) ||
            result.categoryName.toLowerCase().includes(searchValue); });
    };
    ProductList.prototype.onClear = function (args) {
        var searchBar = args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    ProductList.prototype.searchBarLoaded = function (args) {
        var searchBar = args.object;
        if (platform_1.isAndroid) {
            searchBar.android.clearFocus();
        }
    };
    return ProductList;
}());
ProductList = __decorate([
    core_1.Component({
        selector: "product-list",
        moduleId: module.id,
        templateUrl: "ProductList.component.html",
        styleUrls: ["ProductList.component.css"]
    }),
    __metadata("design:paramtypes", [router_2.ActivatedRoute, router_1.RouterExtensions, http_1.Http,
        page_1.Page, saveorder_service_1.SaveOrderService, product_service_1.ProductService,
        saveorder_service_1.SaveCustomer, saveorder_service_1.SaveTotal, product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService, core_1.ViewContainerRef, login_service_1.loginService,
        common_1.Location])
], ProductList);
exports.ProductList = ProductList;
