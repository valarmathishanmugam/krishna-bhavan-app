import { stack } from 'tns-core-modules/ui/frame';
import { loginService } from '../../Services/login.service';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { Dialog } from '../Dialog/dialog.component';
import { ProductService, SaveProducts } from '../../Services/product.service';
import { SaveOrderService, SaveCustomer, SaveTotal } from '../../Services/saveorder.service';
import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { prompt, inputType } from "ui/dialogs";
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { ActivatedRoute } from '@angular/router';
import { Product, Offer } from "../../Models/product.model";
import { SearchBar } from "ui/search-bar";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import {isAndroid} from "platform";
import { Http } from '@angular/http';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from 'application';
import { Location } from '@angular/common';
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;

@Component({
    selector:"product-list",
    moduleId: module.id,
    templateUrl:"ProductList.component.html",
    styleUrls: ["ProductList.component.css"]
})

export class ProductList{

    title: string = "Product List";
    productDetails: Array<Object> = [];
    productsOrdered: Array<Object> = [];
    orderedproducts: any;
    customerid: any;
    productid: Array<string> = [];
    orderquantity: Array<string> = [];
    rackquantity: Array<string> = [];
    productname: Array<string> = [];
    productprice: Array<string> = [];
    productdiscount: Array<string> = [];

    // commented by valar
    discount_product_id: Array<string> = [];
    discount_product_name: Array<string> = [];
    offer_quantity: Array<string> = [];
    base_quantity: Array<string> = [];
    discount_product_quantity: Array<string> = [];
   
    total: any;
    line_total: Array<string> = [];
    temp: any;
    totalAmount: any | string;
    //qtyedit: string =""
    boolean:boolean = false;
    index:any;
    Productproperty: any[] = [];

    loader = new LoadingIndicator();
    
    public searchPhrase: string;

    public products: Product[] = new Array();
    public productIn: Product;

    public original: Product[] = new Array();

    public addvisibility;
    public editvisibility;

    public ifcustomer;
    public offers: Offer;
    public tempuser;
    public getcustomer;
    public offervisible = "collapse";
    public customerselected = "collapse";
    public totalvisible = "collapse"
    offerDetails;
    // enableMessage = 'collapse';

    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private http: Http,
        private page: Page, private saveorderservice: SaveOrderService, private productservice: ProductService, 
        private savecustomer: SaveCustomer, private savetotal: SaveTotal, private saveproducts: SaveProducts,
        private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef, private loginservice: loginService,
        private location: Location){

            //let searchBar = <SearchBar>args.object;
            //searchBar.dismissSoftInput();
        }

    ngOnInit(){
        // code for loading indicator start here
        var options = {
            message: 'Loading...',
            progress: 0.65,
            android: {
              indeterminate: true,
              cancelable: false,
              max: 100,
              progressNumberFormat: "%1d/%2d",
              progressPercentFormat: 0.53,
              progressStyle: 1,
              secondaryProgress: 1
            },
            ios: {
              details: "Additional detail note!",
              square: false,
              margin: 10,
              dimBackground: true,
              color: "#4B9ED6",
              mode:"" // see iOS specific options below
            }
          };
           
          this.loader.show(options);
          // code for loading indicator end here

        this.page.actionBarHidden = false;
        this.ifcustomer = "visible"
        /*
        this.productDetails.push({id: 1, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 2, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 3, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 4, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        this.productDetails.push({id: 5, name:"Krishna's Custard Powder", category: "Desserts", img:"~/Images/product.png", offer:"Buy 1 Get 2 Free", orderqty:""})
        */
        //const id = this.route.snapshot.params["id"];
        //console.log(id);
        this.getproducts();
        this.tempuser = this.loginservice.getScope()        
        this.checkcustomer(this.tempuser);
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });   
          
          this.getcustomer = this.savecustomer.getScope();
          if(this.getcustomer !== false && this.getcustomer.length != 0){              
          console.log("Customer Length: " + this.getcustomer.length)
            this.customerselected = "visible";
            if(this.tempuser.role !== 'customer'){
                this.totalvisible = "visible";
            }
          }
    }

    checkcustomer(data){
        if(data.role === 'customer'){
            this.ifcustomer = "collapse"
        }
        else{
            return;
        }
    }
    getproducts(){
        this.productservice.getproduct()
        .subscribe(data => this.parseProducts(data.json()));
    }

    parseProducts(data: any) {
       
        this.loader.hide();           
        
        console.log("data.length:" + data.length);
    
        for(let i=0; i < data.length; i++) {

            this.offervisible = "collapse";
            var tempid ="";
            //this.addvisibility[i] = "visible";
            //this.editvisibility[i] = "collapse";

          

            if(data[i].discount){       

                for(var a=0; a < 1; a++){
                    console.log("data.length:" + data.length);
                    // created by valarmathi
                    this.offervisible = "visible";
                        console.log("Offer Inside Region: " + data[i].discount[a].region)
                        tempid = data[i].discount[a]._id.$oid;
                        if(data[i].discount){
                            this.offers = new Offer();
                            this.offers.offervisible = "visible"
                            this.offers.base_quantity = data[i].discount[a].base_quantity;
                            this.offers.offer_quantity = data[i].discount[a].offer_quantity;
                        }    
              

                    // commented by valarmathi
                    // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
                    //     var tempUserRegion = this.tempuser.region.find(item => item == data[i].discount[a].region);
                    // }
                    // else{
                    //     var tempUserRegion = this.tempuser.region;
                    // }
                
                    
                    // console.log("USER REGION =====>"+tempUserRegion);
                    // if(data[i].discount[a].region === tempUserRegion){
                    //     this.offervisible = "visible";
                    //     console.log("Offer Inside Region: " + data[i].discount[a].region)
                    //     tempid = data[i].discount[a].discount_id.$oid;
                    //     // this.http.get("https://app.krishnabhavanfoods.in/api/discount/getdiscounts/id/" +tempid)
                    //     // .subscribe(datas => {
                    //     //     this.offerDetails = datas;
                    //     //     console.log("product offer details"+this.offerDetails.json());
                    //     // });
                    //     //.subscribe(data =>this.saveoffers.setScope(data));
                        
                    //     //this.tempoffer = this.saveoffers.getScope();
                    //     //console.log("TempOffer: " + JSON.stringify(this.tempoffer))     
                    //     //this.productIn.discount_id = tempid; 
                    //     if(data[i].discount_list){
                    //         this.offers = new Offer();
                    //         this.offers.offervisible = "visible"
                    //         this.offers.base_quantity = data[i].discount_list[0][0].base_quantity;
                    //         this.offers.offer_quantity = data[i].discount_list[0][0].offer_quantity;
                    //     }                 
                        
                          
                    //     //console.log("***Buy" + this.tempoffer.base_quantity + "Get" + this.tempoffer.offer_quantity)    
                    // }
                    
                    // else{
                    //     //this.productIn.discount_id = ""; 
                    //     this.offers = new Offer();
                    //     this.offers.offervisible = "collapse"
                    // }

                  

                }
            }
            else{
                //this.productIn.discount_id = ""; 
                this.offers = new Offer();
                this.offers.offervisible = "collapse"
            }   

          

            this.productIn = new Product();
            this.productIn.product_id = data[i]._id.$oid;
            this.productIn.product_name = data[i].name;
            this.productIn.categoryName = data[i].category_name;
            this.productIn.case_quantity = data[i].case_quantity;
            //this.productIn.discount_id = "5a91165f9733343884af261d";

            this.productIn.discount_id = tempid; 

            // if(this.tempuser.role == "rep" || this.tempuser.role == "manager" ){
            //   for(let j=0;j<this.tempuser.region.length;j++){
            //     for(let k=0;k<data[i].price.length;k++){
            //         if(this.tempuser.region[j] == data[i].price[k].name){
            //             this.productIn.product_price = data[i].price[k].price;
            //         }
            //     }
            //   }
            // }
            // else if(this.tempuser.role == "customer"){
            //     for(let m=0;m<data[i].price.length;m++){
            //         if(this.tempuser.region == data[i].price[m].name){
            //             this.productIn.product_price = data[i].price[m].price;
            //         }
            //     }

            // }
            this.productIn.product_price = data[i].price[0].price;
          
            if(data[i].discount){
               
                this.productIn.discount = data[i].discount[0];
            }

            
           
            if(data[i].image_name){     
                //console.log(data[i].name + " : " + data[i].image_name)           
                this.productIn.image = "https://app.krishnabhavanfoods.in/"+ data[i].image_name;
            }
            else{
                //console.log(data[i].name + " : " + "Noo")
                this.productIn.image = "~/Images/product_img.png"
            }
            this.productIn.offer = this.offers;
            this.products.push(this.productIn);
            this.original.push(this.productIn);
            
        }  
        this.saveproducts.setScope(this.products);

        this.orderedproducts = this.saveorderservice.getScope();
        
        console.log("Ordered products: " + this.orderedproducts);

        if(this.orderedproducts !== false){
            this.updateproducts(this.orderedproducts);
            this.productsOrdered = this.orderedproducts;
        }
        else{
            return;
        }
    // }
    }

    addtocart(productid,index){
        console.log("id: "+ productid);        
        this.boolean = false;
        for(var i=0 ; i < this.productsOrdered.length ; i++ ) {
            if(this.productsOrdered[i]["product_id"] == productid){
                console.log("already inserted");

                var rackedit = this.productsOrdered[i]["rack_quantity"];
                var orderedit = this.productsOrdered[i]["quantity"];
                console.log("rack: "+ rackedit + "order: " + orderedit);
                this.boolean = true;
                this.index = i;
            }
        }

        let options: ModalDialogOptions = {
            context: { rack: rackedit, order: orderedit},
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(Dialog, options)
        .then((dialogResult: Array<string>) => this.setresult(dialogResult,productid));

    }

    public setresult(quantity,productid){
        console.log("result: "+ quantity);

        if(quantity == "null"){
            return;
        }
        else{
        var rack = quantity[0];
        rack = String(rack);
        var order = quantity[1];
        order = String(order);
        console.log("res1: "+quantity[0] + "res2: "+ quantity[1]);


        for(var i = 0; i < this.products.length; i++){
            if(this.products[i]["product_id"] == productid){
                this.temp = order;
                this.total = this.products[i]["product_price"];
                this.total = this.total*this.temp;
                //this.total = String(this.total);
                //console.log("this.line_total: "+ this.total);
                if(this.boolean === false){
                    this.rackQty(productid, rack, order, this.total);
                }
                else{
                    this.rackQtyedit(rack, order, this.total, this.index,productid);
                }
                console.log("promptid: "+ this.products[i]["product_id"]);
                this.products[i]["rackquantity"] = rack;
                this.products[i]["quantity"] = order;
            }
        }
    }
}

    rackQty(productid, rack, order, line_total){
        console.log("Inside rack Qty");
        this.productid.push(productid);        
        this.orderquantity.push(order);
        this.rackquantity.push(rack);
        this.line_total.push(line_total);

        for(let i=0; i < this.products.length; i++) {
 
            if(this.products[i].product_id == productid){

                this.productname.push(this.products[i].product_name);
                this.productprice.push(String(this.products[i].product_price));

               

                if(this.products[i].discount){
                    var nodiscount = false;
                    console.log("GET Discount values ==>"+JSON.stringify(this.products[i].discount['_id'].$oid));
                    this.productdiscount.push(this.products[i].discount['_id'].$oid);
                    
                    console.log("DISCOUNT LIST"+JSON.stringify(this.products[i].discount));
                             // commented by valar for hide discount - start

                    //  this.discount_product_id.push(this.products[i].discount['discount_product_id'].$oid);
                    //  this.base_quantity.push(this.products[i].discount['base_quantity'] +"");
                    //  this.offer_quantity.push(this.products[i].discount['offer_quantity']+"");
                    //  if(this.products[i].discount['discount_product_name']){
                    //  this.discount_product_name.push(this.products[i].discount['discount_product_name']);
                    //  }
                    //  else {
                    //     this.discount_product_name.push("");   
                    //  }
                    //  let baseQuantity = parseInt(this.products[i].discount['base_quantity']);
                    //  let offer_quantity = parseInt(this.products[i].discount['offer_quantity']);
                    //  if(order >= baseQuantity){
                    //      var remainder = order%baseQuantity;
                    //      var quotient = Math.floor(order/baseQuantity);
                    //      var offerQuantity = (quotient * offer_quantity);
                    //      this.discount_product_quantity.push(offerQuantity+"");                   
                    //  }

                      // commented by valar for hide discount - end                 
                    
                }
                else{
                    var nodiscount = true;
                }

                
            }
        }

        //console.log("line_total: " + this.line_total);        
       
        var temp =  (this.productid.length) - 1; 
        console.log("temp value"+temp); 
        
        
        if(nodiscount === false){
            console.log("Inserting discount product");
            for(var d=0;d<this.products.length;d++){               
                if(this.products[d].product_id == this.productid[temp]){
                    var discount_id = this.products[d].discount_id;
                    var temp1 =  this.productdiscount.indexOf(discount_id);
                    console.log("temp1 value"+temp1); 
                }

            }
            
            this.productsOrdered.push({product_id:this.productid[temp], sku_id:this.productid[temp], 
                quantity:this.orderquantity[temp], rack_quantity:this.rackquantity[temp], product_name:this.productname[temp], 
                line_total:this.line_total[temp], discount_id:this.productdiscount[temp1], product_price:this.productprice[temp]
                
                 // commented by valar for hide discount - start

                //  ,discount_product_id:this.discount_product_id[temp1],discount_product_quantity:this.discount_product_quantity[temp1],discount_product_name:this.discount_product_name[temp1],offer_quantity:this.offer_quantity[temp1],base_quantity:this.base_quantity[temp1]

                // commented by valar for hide discount - end
            });
        }
        else if(nodiscount === true){
         

            console.log("Inserting No Discount Product");

            this.productsOrdered.push({product_id:this.productid[temp], sku_id:this.productid[temp], 
                quantity:this.orderquantity[temp], rack_quantity:this.rackquantity[temp], product_name:this.productname[temp], 
                line_total:this.line_total[temp], product_price:this.productprice[temp]});
        
        }  
        this.gettotal();
    }

    rackQtyedit(rack, order, line_total,index,productid){
        console.log("Inside rack Qty edit");
        
        console.log("already inserted");
        this.productsOrdered[index]["rack_quantity"] = rack;
        this.productsOrdered[index]["quantity"] = order;
        this.productsOrdered[index]["line_total"] = line_total;
        
         // commented by valar for hide discount - start


        //  for(let i=0; i < this.products.length; i++) { 
        //      if(this.products[i].product_id == productid){
        //          if(this.products[i].discount){ 
        //             let baseQuantity = parseInt(this.products[i].discount['base_quantity']); 
        //             let offer_Quantity = parseInt(this.products[i].discount['offer_quantity']);
        //              if(order >= baseQuantity){
        //                  var remainder = order%baseQuantity;
        //                  var quotient = Math.floor(order/baseQuantity);
        //                  var offerQuantity = (quotient * offer_Quantity);
        //                  this.productsOrdered[index]["discount_product_quantity"] = offerQuantity+"";                        
        //              }    
                    
        //       }

        //     }
        //  }
         // commented by valar for hide discount - end
        this.gettotal();
    }

    gettotal(){
        this.totalAmount = 0;
        for(var i=0 ; i < this.productsOrdered.length ; i++ ) {
            //console.log(i +": " + this.productsOrdered[i]["id"]);
            var temptotal = this.productsOrdered[i]["line_total"];
            this.totalAmount = temptotal + this.totalAmount;            
            console.log("totalamont: " + this.totalAmount);
            //this.totalAmount = String(this.totalAmount);
        }
       
        this.saveorderservice.setScope(this.productsOrdered);
        this.savetotal.setScope(this.totalAmount);
    }

    productdetail(){
        //this.routerExtensions.navigate(["/productdetail"]);
    }

    updateproducts(orderedproducts){
        
        for(var a=0; a < this.products.length; a++){
            for(var i=0; i < orderedproducts.length; i++){
                if(this.products[a]["product_id"] == orderedproducts[i]["product_id"]){
                    console.log("Ordered products name: " + orderedproducts[i]["product_name"]);
                    this.products[a]["rackquantity"] = orderedproducts[i]["rack_quantity"];
                    this.products[a]["quantity"] = orderedproducts[i]["quantity"];
                    this.totalAmount = this.savetotal.getScope();
                }
            }
        }
    }
    
    public onSubmit(args) {        
        let searchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        let searchValue = searchBar.text.toLowerCase();
        this.products = this.original.filter(result=> result.product_name.toLowerCase().includes(searchValue)  || 
        result.categoryName.toLowerCase().includes(searchValue));
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }
    
    public onTextChanged(args) {
        let searchBar = <SearchBar>args.object;
        let searchValue = searchBar.text.toLowerCase();
        //console.log("searchValue: " + searchValue);
        //alert(searchValue);
        this.products = this.original.filter(result=> result.product_name.toLowerCase().includes(searchValue) || 
        result.categoryName.toLowerCase().includes(searchValue));
    }
    
    public onClear(args) {
        let searchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        searchBar.text = "";
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }

    searchBarLoaded(args){
        let searchBar = <SearchBar>args.object;
        if(isAndroid){        
            searchBar.android.clearFocus();
        }
    }

}