"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var common_1 = require("@angular/common");
var ProductDetail = (function () {
    function ProductDetail(page, location, routerExtensions) {
        this.page = page;
        this.location = location;
        this.routerExtensions = routerExtensions;
        this.title = "Product Detail";
        this.description = "Ready to fry Items, very crispy & tasty,\n     traditionally made. Easy to fry on a daily basis Can be fried in Micro oven.";
    }
    ProductDetail.prototype.ngOnInit = function () {
        this.productDetails = ({ name: "Krishna's Custard Powder", category: "Desserts", img: "~/Images/product.png", offer: "Buy 1 Get 2 Free" });
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //     data.cancel = true;
        //     //alert("Back Pressed")
        //     //this.location.back();
        //     this.routerExtensions.backToPreviousPage();
        //   });  
    };
    return ProductDetail;
}());
ProductDetail = __decorate([
    core_1.Component({
        selector: "product-detail",
        moduleId: module.id,
        templateUrl: "productdetail.component.html",
        styleUrls: ["productdetail.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page, common_1.Location, router_1.RouterExtensions])
], ProductDetail);
exports.ProductDetail = ProductDetail;
