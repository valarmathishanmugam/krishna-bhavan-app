"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {
//     Response,
// } from '../../../../platforms/android/app/build/intermediates/assets/debug/app/tns_modules/@angular/http/src/static_response';
var orderapproval_service_1 = require("../../../Services/orderapproval.service");
var getorder_service_1 = require("../../../Services/getorder.service");
var dialogshipping_component_1 = require("../../Dialog_shippingaddress/dialogshipping.component");
var dialog_component_1 = require("../../Dialog/dialog.component");
var product_service_1 = require("../../../Services/product.service");
var sendorder_service_1 = require("../../../Services/sendorder.service");
var saveorder_service_1 = require("../../../Services/saveorder.service");
var order_model_1 = require("../../../Models/order.model");
var product_model_1 = require("../../../Models/product.model");
var customer_model_1 = require("../../../Models/customer.model");
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var dialogs_1 = require("ui/dialogs");
var dialogs = require("ui/dialogs");
var animation_1 = require("ui/animation");
var router_2 = require("@angular/router");
var common_1 = require("@angular/common");
var saveorder_service_2 = require("../../../Services/saveorder.service");
var http_1 = require("@angular/http");
// import { Offer } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';
var ManagerOrderReview = (function () {
    function ManagerOrderReview(routerExtensions, saveorderservice, page, savecustomer, sendorder, savetotal, saveproducts, modalService, viewContainerRef, savecustomerdetails, saveshipping, savenotes, getorderservice, route, orderapproval, location, saveupdateorderid, saveupdateorder_id, http) {
        this.routerExtensions = routerExtensions;
        this.saveorderservice = saveorderservice;
        this.page = page;
        this.savecustomer = savecustomer;
        this.sendorder = sendorder;
        this.savetotal = savetotal;
        this.saveproducts = saveproducts;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.savecustomerdetails = savecustomerdetails;
        this.saveshipping = saveshipping;
        this.savenotes = savenotes;
        this.getorderservice = getorderservice;
        this.route = route;
        this.orderapproval = orderapproval;
        this.location = location;
        this.saveupdateorderid = saveupdateorderid;
        this.saveupdateorder_id = saveupdateorder_id;
        this.http = http;
        this.title = "Order Review";
        this.customer = new customer_model_1.Customer();
        this.address = new customer_model_1.Address();
        this.productDetails = [];
        this.allproducts = new Array();
        this.editShipping = false;
        this.isFabOpen = false;
        this.orderupdate_customerdetails = [];
        this.allnotes = new Array();
        this.disableHoldFab = true;
        this.disableApproveFab = true;
    }
    ManagerOrderReview.prototype.ngOnInit = function () {
        var _this = this;
        //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
        //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
        this.customer.shopname = "ABC Departmental store";
        this.customer.contactname = "name";
        this.address.door_no = "7";
        this.address.street_name = "some street";
        this.address.area = "some area";
        this.address.city = "chennai";
        this.address.district = "chennai";
        this.address.state = "Tamil Nadu";
        this.address.pin = 658990;
        var id = this.route.snapshot.params["id"];
        this.getorderservice
            .getorderbyid(id)
            .subscribe(function (data) { return _this.getallorders(data.json()); }, function (err) { return console.log("Get order by id error: " + err); });
        // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   data.cancel = true;
        //   //alert("Back Pressed")
        //   //this.location.back();
        //   this.routerExtensions.backToPreviousPage();
        // });
    };
    ManagerOrderReview.prototype.getallorders = function (data) {
        console.log("All Data: " + JSON.stringify(data));
        console.log("products.length: " + data[0].line_items.length);
        //data = data._body;
        this.order_id = data[0]._id.$oid;
        this.formattedOrder_id = data[0].order_id;
        console.log("formatted order id" + this.formattedOrder_id);
        this.saveupdateorderid.setScope(this.order_id);
        this.saveupdateorder_id.setScope(data[0].order_id);
        this.order_date = data[0].order_date;
        //this.tempdate = data[0].order_date;
        //this.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
        var tempstatus = data[0].status;
        this.customer.shopname = data[0].shop_name;
        console.log("shopname: " + this.customer.shopname);
        this.customer.contactname = data[0].customer_name;
        this.address.shippingmode = data[0].preferred_shipping;
        this.address.area = data[0].shipping_address.area;
        this.address.city = data[0].shipping_address.city;
        this.address.door_no = data[0].shipping_address.door_no;
        this.address.pin = data[0].shipping_address.pin;
        this.address.street_name = data[0].shipping_address.street_name;
        this.address.district = data[0].shipping_address.district;
        this.address.state = data[0].shipping_address.state;
        this.customer.address = this.address;
        this.total_amount = Number(data[0].total_amount);
        console.log("CUSTOMER =====> " + JSON.stringify(this.customer));
        console.log("ADDRESS =====> " + JSON.stringify(this.address));
        if (data[0].notes) {
            //console.log("My Notes")
            //console.log(data[0].customer_name + " : " + data[0].notes.length)
            for (var a = 0; a < data[0].notes.length; a++) {
                this.notes = new order_model_1.Note();
                //console.log("My Notes: " +data[0].notes[a].note)
                this.notes.created_date = data[0].notes[a].created_date;
                this.notes.note = data[0].notes[a].note;
                this.notes.created_by = data[0].notes[a].created_by;
                this.allnotes.push(this.notes);
            }
            if (data[0].notes.length == 0) {
                if (data[0].current_note) {
                    this.notes = new order_model_1.Note();
                    console.log("Current Notes: " + data[0].current_note.note);
                    this.notes.created_date = data[0].current_note.created_date;
                    this.notes.note = data[0].current_note.note;
                    this.notes.created_by = data[0].current_note.created_by;
                    this.allnotes.push(this.notes);
                }
                else { }
            }
        }
        // else{}
        this.orderupdate_customerdetails.push({ orderdate: this.order_date, employeeid: data[0].employee_id.$oid,
            customerid: data[0].customer_id.$oid, customername: data[0].customer_name, shopname: data[0].shop_name,
            shippingaddress: data[0].shipping_address, billingaddress: data[0].billing_address, preferredshipping: data[0].preferred_shipping,
            totalamount: data[0].total_amount });
        console.log(JSON.stringify(this.orderupdate_customerdetails));
        for (var i = 0; i < data[0].line_items.length; i++) {
            this.product = new product_model_1.Product();
            // commented by valar for hide discount - start
            // this.product.offer = new Offer();
            // commented by valar for hide discount - end
            this.product.product_id = data[0].line_items[i].product_id.$oid;
            this.product.sku_id = data[0].line_items[i].sku_id.$oid;
            this.product.product_name = data[0].line_items[i].product_name;
            this.product.line_total = Number(data[0].line_items[i].line_total);
            this.product.rack_quantity = Number(data[0].line_items[i].rack_quantity);
            this.product.quantity = Number(data[0].line_items[i].quantity);
            console.log("PRODUCT: " + JSON.stringify(this.product));
            // commented by valar for hide discount - start
            // if(data[0].line_items[i].discount_id){            
            //   console.log("Has Discount")   
            //   var nodiscount = false;
            //   this.product.discount_id = data[0].line_items[i].discount_id.$oid;
            //   this.product.discount_product_quantity =  data[0].line_items[i].discount_product_quantity;
            //   this.product.offer.base_quantity = data[0].line_items[i].discount_details[0].base_quantity;
            //   this.product.offer.offer_quantity = data[0].line_items[i].discount_details[0].offer_quantity;
            //   this.product.offer.discount_product_name = data[0].line_items[i].discount_details[1].discount_product_name;
            // }
            // else{            
            //   console.log("Has No Discount")
            //   this.product.discount_id = "";
            //   var nodiscount = true;
            // }
            // commented by valar for hide discount - end
            this.product.product_price = Number(data[0].line_items[i].product_price);
            this.allproducts.push(this.product);
            // commented by valar for hide discount - start
            //   if(nodiscount === false){
            //     console.log("Inserting discount product..");
            //     this.productDetails.push({product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid, 
            //       quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name, 
            //       line_total: data[0].line_items[i].line_total, discount_id: data[0].line_items[i].discount_id.$oid, product_price: data[0].line_items[i].product_price,
            //     discount_product_quantity:data[0].line_items[i].discount_product_quantity});
            //   }
            // else if(nodiscount === true){
            // commented by valar for hide discount - end
            console.log("Inserting No Discount Product...");
            this.productDetails.push({ product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid,
                quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name,
                line_total: data[0].line_items[i].line_total, product_price: data[0].line_items[i].product_price });
            // } // commented by valar for hide discount
        }
        console.log("All Product details: " + JSON.stringify(this.productDetails));
        if (tempstatus.toLowerCase() === "hold") {
            console.log("status: Hold");
            this.order_status = "~/Images/hold.png";
        }
        else if (tempstatus.toLowerCase() === "pending") {
            console.log("status: not_reviewed");
            this.order_status = "~/Images/not_reviewed.png";
        }
        else if (tempstatus.toLowerCase() === "processed") {
            console.log("status: completed");
            this.order_status = "~/Images/completed.png";
        }
        else if (tempstatus.toLowerCase() === "approved") {
            console.log("status: approved");
            this.order_status = "~/Images/approved.png";
        }
    };
    ManagerOrderReview.prototype.editqty = function (id, index) {
        var _this = this;
        console.log("id: " + id + "index: " + index);
        console.log("id: " + JSON.stringify(this.allproducts));
        console.log("orderqty in editqty: " + this.allproducts[index]["quantity"]);
        var rackedit = this.allproducts[index]["rack_quantity"];
        var orderedit = this.allproducts[index]["quantity"];
        console.log("orderedit: " + orderedit);
        var options = {
            context: { rack: rackedit, order: orderedit },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService
            .showModal(dialog_component_1.Dialog, options)
            .then(function (dialogResult) {
            return _this.setresult(dialogResult, id, index);
        });
    };
    ManagerOrderReview.prototype.setresult = function (quantity, productid, index) {
        console.log("result: " + quantity);
        if (quantity == "null") {
            return;
        }
        else {
            var rack = quantity[0];
            var order = quantity[1];
            console.log("res1: " + quantity[0] + "res2: " + quantity[1]);
            console.log("New Value");
            this.changelinetotal(productid, index, order);
            //this.changetotal();
            this.allproducts[index]["rack_quantity"] = rack;
            this.allproducts[index]["quantity"] = order;
            this.productDetails[index]["rack_quantity"] = rack;
            this.productDetails[index]["quantity"] = order;
            // commented by valar for hide discount - start
            // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
            // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
            // if(parseInt(order) >= baseQuantity ){
            //     var remainder = parseInt(order)%baseQuantity;
            //     var quotient = Math.floor(parseInt(order)/baseQuantity);
            //     var offerQuantity = (quotient * offer_Quantity);
            //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+""; 
            //     this.productDetails[index]["discount_product_quantity"] = offerQuantity+""   
            // }
            // commented by valar for hide discount - end
        }
    };
    ManagerOrderReview.prototype.removeorder = function (id, index) {
        //this.allproducts.splice(index,1);
        var _this = this;
        var options = {
            title: "Are You Sure",
            message: "Want to remove order?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            console.log("removeorder: " + result);
            if (result == true) {
                if (_this.allproducts.length > 1) {
                    _this.allproducts.splice(index, 1);
                    _this.productDetails.splice(index, 1);
                    console.log("Order Length: " + _this.allproducts.length);
                    _this.changetotal();
                }
                else {
                    console.log("products cannot be empty");
                    //this.routerExtensions.navigate(["/productlist"]);
                    dialogs
                        .alert({
                        title: "Deleting All!",
                        message: "Products Cannot be Empty",
                        okButtonText: "Ok"
                    });
                }
            }
            else {
                return;
            }
        });
    };
    ManagerOrderReview.prototype.changelinetotal = function (productid, index, order) {
        for (var i = 0; i < this.allproducts.length; i++) {
            if (this.allproducts[i]["product_id"] == productid) {
                var tempprice = Number(this.allproducts[i]["product_price"]);
                console.log("tempprice: " + tempprice);
            }
        }
        this.templinetotal = tempprice * order;
        console.log("Line total in change total: " + this.allproducts[index]["line_total"]);
        this.allproducts[index]["line_total"] = this.templinetotal;
        this.productDetails[index]["line_total"] = this.templinetotal;
        this.changetotal();
    };
    ManagerOrderReview.prototype.changetotal = function () {
        this.total_amount = 0;
        for (var i = 0; i < this.allproducts.length; i++) {
            console.log(i + ": " + this.allproducts[i]["product_id"]);
            var temptotal = this.allproducts[i]["line_total"];
            this.total_amount = temptotal + this.total_amount;
            console.log("totalamount: " + this.total_amount);
        }
        this.orderupdate_customerdetails[0]["totalamount"] = this.total_amount;
    };
    ManagerOrderReview.prototype.editshippingaddress = function () {
        var _this = this;
        var doornoedit = this.address.door_no;
        var areaedit = this.address.area;
        var cityedit = this.address.city;
        var pinedit = this.address.pin;
        var streetnameedit = this.address.street_name;
        var districtedit = this.address.district;
        var stateedit = this.address.state;
        console.log("state: " + stateedit);
        var options = {
            context: {
                doorno: doornoedit,
                streetname: streetnameedit,
                area: areaedit,
                city: cityedit,
                district: districtedit,
                state: stateedit,
                pin: pinedit
            },
            viewContainerRef: this.viewContainerRef
        };
        this.modalService
            .showModal(dialogshipping_component_1.DialogShipping, options)
            .then(function (dialogResult) { return _this.editaddress(dialogResult); });
    };
    ManagerOrderReview.prototype.editaddress = function (address) {
        if (address == "null") {
            return;
        }
        else {
            this.address.door_no = address[0];
            this.orderupdate_customerdetails[0]["shippingaddress"]["door_no"] = address[0];
            this.address.street_name = address[1];
            this.orderupdate_customerdetails[0]["shippingaddress"]["street_name"] = address[1];
            this.address.area = address[2];
            this.orderupdate_customerdetails[0]["shippingaddress"]["area"] = address[2];
            this.address.city = address[3];
            this.orderupdate_customerdetails[0]["shippingaddress"]["city"] = address[3];
            this.address.district = address[4];
            this.orderupdate_customerdetails[0]["shippingaddress"]["district"] = address[4];
            this.address.state = address[5];
            this.orderupdate_customerdetails[0]["shippingaddress"]["state"] = address[5];
            this.address.pin = address[6];
            this.orderupdate_customerdetails[0]["shippingaddress"]["pin"] = address[6];
            this.customer.address = this.address;
        }
    };
    ManagerOrderReview.prototype.fabTap = function (args) {
        var _this = this;
        var fab = args.object;
        var definitions = new Array();
        var button = this.page.getViewById("dial1");
        var button2 = this.page.getViewById("dial2");
        if (this.isFabOpen == true) {
            var a1 = { target: fab, rotate: 0, duration: 400, delay: 0 };
            definitions.push(a1);
            var a2 = {
                target: button,
                translate: { x: 0, y: 0 },
                opacity: 0,
                duration: 400,
                delay: 0
            };
            definitions.push(a2);
            var a3 = {
                target: button2,
                translate: { x: 0, y: 0 },
                opacity: 0,
                duration: 440,
                delay: 0
            };
            definitions.push(a3);
            var animationSet = new animation_1.Animation(definitions);
            animationSet
                .play()
                .then(function () {
                //console.log("Animations finished");
                _this.isFabOpen = false;
            })
                .catch(function (e) {
                console.log(e.message);
            });
        }
        else {
            var a4 = { target: fab, rotate: 45, duration: 400, delay: 0 };
            definitions.push(a4);
            var a5 = {
                target: button,
                translate: { x: 0, y: -54 },
                opacity: 1,
                duration: 400,
                delay: 0
            };
            definitions.push(a5);
            var a6 = {
                target: button2,
                translate: { x: 0, y: -100 },
                opacity: 1,
                duration: 440,
                delay: 0
            };
            definitions.push(a6);
            var animationSet = new animation_1.Animation(definitions);
            animationSet
                .play()
                .then(function () {
                //console.log("Animations finished");
                _this.isFabOpen = true;
            })
                .catch(function (e) {
                console.log(e.message);
            });
        }
    };
    ManagerOrderReview.prototype.approveorder = function (shipmode) {
        var _this = this;
        var options = {
            title: "Approve Order",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                _this.disableApproveFab = false;
                _this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
                _this.savecustomerdetails.setScope(_this.orderupdate_customerdetails);
                _this.saveorderservice.setScope(_this.productDetails);
                var newnote = _this.newnote.nativeElement;
                newnote = newnote.text;
                console.log("New note: " + newnote);
                _this.savenotes.setScope(newnote);
                console.log("Approve Order");
                _this.sendorder.updateorder();
                var approval = "approved";
                _this.orderapproval.approveorder(_this.order_id, approval, _this.route.snapshot.params["id"]);
                _this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
            }
            else {
                return;
            }
        });
    };
    ManagerOrderReview.prototype.holdorder = function (shipmode) {
        var _this = this;
        var options = {
            title: "Hold Order",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        dialogs_1.confirm(options).then(function (result) {
            if (result == true) {
                console.log("Shipping Mode: " + shipmode);
                _this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
                _this.savecustomerdetails.setScope(_this.orderupdate_customerdetails);
                _this.saveorderservice.setScope(_this.productDetails);
                var newnote = _this.newnote.nativeElement;
                newnote = newnote.text;
                console.log("New note: " + newnote);
                if (newnote !== "") {
                    _this.savenotes.setScope(newnote);
                    console.log("Hold Order");
                    _this.sendorder.updateorder();
                    var approval = "hold";
                    _this.disableHoldFab = false;
                    _this.orderapproval.approveorder(_this.order_id, approval, _this.route.snapshot.params["id"]);
                    _this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
                }
                else {
                    alert("Enter notes");
                }
            }
            else {
                return;
            }
        });
    };
    return ManagerOrderReview;
}());
__decorate([
    core_1.ViewChild("newnote"),
    __metadata("design:type", core_1.ElementRef)
], ManagerOrderReview.prototype, "newnote", void 0);
ManagerOrderReview = __decorate([
    core_1.Component({
        selector: "manager-order-review",
        moduleId: module.id,
        templateUrl: "managerorderreview.component.html",
        styleUrls: ["managerorderreview.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions,
        saveorder_service_1.SaveOrderService,
        page_1.Page,
        saveorder_service_1.SaveCustomer,
        sendorder_service_1.SendOrder,
        saveorder_service_1.SaveTotal,
        product_service_1.SaveProducts,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef,
        saveorder_service_1.SaveCustomerDetails,
        saveorder_service_1.SaveShipping,
        saveorder_service_1.SaveNotes,
        getorder_service_1.GetOrder,
        router_2.ActivatedRoute,
        orderapproval_service_1.OrderApproval,
        common_1.Location,
        saveorder_service_2.SaveUpdateOrderId,
        saveorder_service_2.SaveUpdateOrder_Id,
        http_1.Http])
], ManagerOrderReview);
exports.ManagerOrderReview = ManagerOrderReview;
