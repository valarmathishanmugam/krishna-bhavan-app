// import {
//     Response,
// } from '../../../../platforms/android/app/build/intermediates/assets/debug/app/tns_modules/@angular/http/src/static_response';
import { OrderApproval } from '../../../Services/orderapproval.service';
import { GetOrder } from '../../../Services/getorder.service';
import { DialogShipping } from '../../Dialog_shippingaddress/dialogshipping.component';
import { Dialog } from '../../Dialog/dialog.component';
import { SaveProducts } from '../../../Services/product.service';
import { SendOrder } from '../../../Services/sendorder.service';
import {
    SaveCustomer,
    SaveCustomerDetails,
    SaveNotes,
    SaveOrderService,
    SaveShipping,
    SaveTotal,
} from '../../../Services/saveorder.service';
import { Order, Note } from '../../../Models/order.model';
import { Product,Offer } from '../../../Models/product.model';
import { Address, Customer } from '../../../Models/customer.model';
import { Component, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'ui/page';
import { ModalDialogService, ModalDialogOptions } from 'nativescript-angular/modal-dialog';
import { prompt, inputType, confirm } from "ui/dialogs";
import * as dialogs from 'ui/dialogs';
import { Animation , AnimationDefinition} from "ui/animation";
import { ActivatedRoute } from '@angular/router';

import { View } from "ui/core/view";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from 'application';
import { Location } from '@angular/common';
import { SaveUpdateOrderId, SaveUpdateOrder_Id } from '../../../Services/saveorder.service';
import { TextField } from 'ui/text-field';
import { subscribeOn } from 'rxjs/operator/subscribeOn';
import { Http } from "@angular/http";
// import { Offer } from '../../../../platforms/android/app/src/main/assets/app/Models/product.model';


@Component({
  selector: "manager-order-review",
  moduleId: module.id,
  templateUrl: "managerorderreview.component.html",
  styleUrls: ["managerorderreview.component.css"]
})
export class ManagerOrderReview {
  title: string = "Order Review";
  customer = new Customer();
  address = new Address();
  productDetails: Array<Object> = [];
  customerId: any;
  customerdetails: any;
  products: any;
  getproducts: any;
  public product: Product;
  public order: Order;
  public allproducts: Product[] = new Array();
  orderremoved: any;
  public editShipping = false;
  total_amount: any;
  templinetotal: any;
  shippingmode: any;
  public isFabOpen = false;
  order_date;
  order_status;
  order_id;
  formattedOrder_id;
  orderupdate_customerdetails: Array<Object> = [];
  public notes: Note;
  public allnotes: Note[] = new Array();
  tempdate:Date;
  disableHoldFab: boolean = true;
  disableApproveFab: boolean = true;
  @ViewChild("newnote") newnote: ElementRef;

  constructor(
    private routerExtensions: RouterExtensions,
    private saveorderservice: SaveOrderService,
    private page: Page,
    private savecustomer: SaveCustomer,
    private sendorder: SendOrder,
    private savetotal: SaveTotal,
    private saveproducts: SaveProducts,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private savecustomerdetails: SaveCustomerDetails,
    private saveshipping: SaveShipping,
    private savenotes: SaveNotes,
    private getorderservice: GetOrder,
    private route: ActivatedRoute,
    private orderapproval: OrderApproval,
    private location: Location,
    private saveupdateorderid: SaveUpdateOrderId,
    private saveupdateorder_id: SaveUpdateOrder_Id,
    private http: Http
  ) {}

  ngOnInit() {
    //this.orderDetails = ({id:"ash34283980938", shop:"Kanniga Parameshwari Stores", img:"~/Images/green_dot.png", date:"8/2/18", shippingmode:"K.P.N Travels"})
    //this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
    //this.productDetails.push({name:"Krishna's Custard Powder", id: "hewq87e98782", img:"res://store", offer:"Buy 1 Get 2 Free", rack:"5", order:"5"})
    this.customer.shopname = "ABC Departmental store";
    this.customer.contactname = "name";
    this.address.door_no = "7";
    this.address.street_name = "some street";
    this.address.area ="some area";
    this.address.city = "chennai";
    this.address.district = "chennai";
    this.address.state = "Tamil Nadu";
    this.address.pin = 658990;

    const id = this.route.snapshot.params["id"];
    this.getorderservice
      .getorderbyid(id)
      .subscribe(data => this.getallorders(data.json()),
                  err=>console.log("Get order by id error: " + err));

      // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
      //   data.cancel = true;
      //   //alert("Back Pressed")
      //   //this.location.back();
      //   this.routerExtensions.backToPreviousPage();
      // });
  }

  getallorders(data) {
    console.log("All Data: " + JSON.stringify(data));
    console.log("products.length: " + data[0].line_items.length);
    //data = data._body;
    this.order_id = data[0]._id.$oid;
    this.formattedOrder_id = data[0].order_id;
    console.log("formatted order id"+this.formattedOrder_id);
    this.saveupdateorderid.setScope(this.order_id);
    this.saveupdateorder_id.setScope(data[0].order_id);
    this.order_date =  data[0].order_date;
    //this.tempdate = data[0].order_date;
    //this.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();
    var tempstatus = data[0].status;
    this.customer.shopname = data[0].shop_name;
    console.log("shopname: "+ this.customer.shopname);
    this.customer.contactname = data[0].customer_name;
    this.address.shippingmode = data[0].preferred_shipping;    
    this.address.area = data[0].shipping_address.area;
    this.address.city = data[0].shipping_address.city;
    this.address.door_no = data[0].shipping_address.door_no;
    this.address.pin = data[0].shipping_address.pin;
    this.address.street_name = data[0].shipping_address.street_name;
    this.address.district = data[0].shipping_address.district;
    this.address.state = data[0].shipping_address.state;
    this.customer.address = this.address;
    this.total_amount = Number(data[0].total_amount);
    console.log("CUSTOMER =====> "+JSON.stringify(this.customer));
    console.log("ADDRESS =====> "+JSON.stringify(this.address));
    
    

    if(data[0].notes){
      //console.log("My Notes")
      //console.log(data[0].customer_name + " : " + data[0].notes.length)
      for(let a=0; a<data[0].notes.length; a++){
        this.notes = new Note();
        //console.log("My Notes: " +data[0].notes[a].note)
        this.notes.created_date = data[0].notes[a].created_date;
        this.notes.note = data[0].notes[a].note;
        this.notes.created_by = data[0].notes[a].created_by;

        this.allnotes.push(this.notes);
      }

      if(data[0].notes.length == 0){
        if(data[0].current_note){
          this.notes = new Note();
            console.log("Current Notes: " +data[0].current_note.note)
            this.notes.created_date = data[0].current_note.created_date;
            this.notes.note = data[0].current_note.note;
            this.notes.created_by = data[0].current_note.created_by;
    
            this.allnotes.push(this.notes);
        }
        else{}
        }
    }
    // else{}
   

    this.orderupdate_customerdetails.push({orderdate:this.order_date, employeeid:data[0].employee_id.$oid,
              customerid:data[0].customer_id.$oid, customername:data[0].customer_name, shopname:data[0].shop_name,
              shippingaddress:data[0].shipping_address, billingaddress:data[0].billing_address, preferredshipping: data[0].preferred_shipping,
              totalamount:data[0].total_amount })
    console.log(JSON.stringify(this.orderupdate_customerdetails));

        for (let i = 0; i < data[0].line_items.length; i++) {
          this.product = new Product();
          // commented by valar for hide discount - start

          // this.product.offer = new Offer();

          // commented by valar for hide discount - end
    
          this.product.product_id = data[0].line_items[i].product_id.$oid;          
          this.product.sku_id = data[0].line_items[i].sku_id.$oid;
          this.product.product_name = data[0].line_items[i].product_name;
          this.product.line_total = Number(data[0].line_items[i].line_total);
          this.product.rack_quantity = Number(data[0].line_items[i].rack_quantity);
          this.product.quantity = Number(data[0].line_items[i].quantity);

          console.log("PRODUCT: "+ JSON.stringify(this.product));

         

          // commented by valar for hide discount - start

          // if(data[0].line_items[i].discount_id){            
          //   console.log("Has Discount")   
          //   var nodiscount = false;
          //   this.product.discount_id = data[0].line_items[i].discount_id.$oid;
          //   this.product.discount_product_quantity =  data[0].line_items[i].discount_product_quantity;
          //   this.product.offer.base_quantity = data[0].line_items[i].discount_details[0].base_quantity;
          //   this.product.offer.offer_quantity = data[0].line_items[i].discount_details[0].offer_quantity;
          //   this.product.offer.discount_product_name = data[0].line_items[i].discount_details[1].discount_product_name;
          // }
          // else{            
          //   console.log("Has No Discount")
          //   this.product.discount_id = "";
          //   var nodiscount = true;
          // }

          // commented by valar for hide discount - end
          
          this.product.product_price = Number(data[0].line_items[i].product_price);


          this.allproducts.push(this.product);
         
        // commented by valar for hide discount - start

        //   if(nodiscount === false){
        //     console.log("Inserting discount product..");
        //     this.productDetails.push({product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid, 
        //       quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name, 
        //       line_total: data[0].line_items[i].line_total, discount_id: data[0].line_items[i].discount_id.$oid, product_price: data[0].line_items[i].product_price,
        //     discount_product_quantity:data[0].line_items[i].discount_product_quantity});
        //   }
        // else if(nodiscount === true){

        // commented by valar for hide discount - end

            console.log("Inserting No Discount Product...");
            this.productDetails.push({product_id: data[0].line_items[i].product_id.$oid, sku_id: data[0].line_items[i].sku_id.$oid, 
              quantity: data[0].line_items[i].quantity, rack_quantity: data[0].line_items[i].rack_quantity, product_name: data[0].line_items[i].product_name, 
              line_total: data[0].line_items[i].line_total, product_price: data[0].line_items[i].product_price});
            
        // } // commented by valar for hide discount
        
                       
        }

        console.log("All Product details: " + JSON.stringify(this.productDetails))

        if(tempstatus.toLowerCase() === "hold"){//onHold
          console.log("status: Hold");
          this.order_status = "~/Images/hold.png"
        }
        else if(tempstatus.toLowerCase() === "pending"){//not_reviewed by sales_manager
          console.log("status: not_reviewed");
          this.order_status = "~/Images/not_reviewed.png"
        }
        else if(tempstatus.toLowerCase() === "processed"){//Completed
          console.log("status: completed");
          this.order_status = "~/Images/completed.png"
        }
        else if(tempstatus.toLowerCase() === "approved"){//Approved & Processing
          console.log("status: approved");
          this.order_status = "~/Images/approved.png"
        }
         
  }

  editqty(id, index) {
    console.log("id: " + id + "index: " + index);
    console.log("id: " + JSON.stringify(this.allproducts));
    console.log("orderqty in editqty: " + this.allproducts[index]["quantity"]);
    var rackedit = this.allproducts[index]["rack_quantity"];
    var orderedit = this.allproducts[index]["quantity"];
    console.log("orderedit: " + orderedit);

    let options: ModalDialogOptions = {
      context: { rack: rackedit, order: orderedit },
      viewContainerRef: this.viewContainerRef
    };

    this.modalService
      .showModal(Dialog, options)
      .then((dialogResult: Array<string>) =>
        this.setresult(dialogResult, id, index)
      );
  }

  public setresult(quantity, productid, index) {
    console.log("result: " + quantity);

    if (quantity == "null") {
      return;
    } else {
      var rack = quantity[0];
      var order = quantity[1];

      console.log("res1: " + quantity[0] + "res2: " + quantity[1]);

      console.log("New Value");

      this.changelinetotal(productid, index, order);
      //this.changetotal();
      this.allproducts[index]["rack_quantity"] = rack;
      this.allproducts[index]["quantity"] = order;
      this.productDetails[index]["rack_quantity"] = rack;
      this.productDetails[index]["quantity"] = order;
      // commented by valar for hide discount - start

      // let baseQuantity = parseInt(this.allproducts[index].offer.base_quantity);
      // let offer_Quantity = parseInt(this.allproducts[index].offer.offer_quantity);
      // if(parseInt(order) >= baseQuantity ){
      //     var remainder = parseInt(order)%baseQuantity;
      //     var quotient = Math.floor(parseInt(order)/baseQuantity);
      //     var offerQuantity = (quotient * offer_Quantity);
      //     this.allproducts[index]["discount_product_quantity"] = offerQuantity+""; 
      //     this.productDetails[index]["discount_product_quantity"] = offerQuantity+""   
      // }

      // commented by valar for hide discount - end
    }
  }

  removeorder(id, index) {
    //this.allproducts.splice(index,1);

    let options = {
      title: "Are You Sure",
      message: "Want to remove order?",
      okButtonText: "Yes",
      cancelButtonText: "No"
    };

    confirm(options).then((result: boolean) => {
      console.log("removeorder: " + result);
      if (result == true) {
        if (this.allproducts.length > 1) {
        this.allproducts.splice(index, 1);
        this.productDetails.splice(index, 1);
        console.log("Order Length: " + this.allproducts.length);
        this.changetotal();
        }
        else {
          console.log("products cannot be empty");
          //this.routerExtensions.navigate(["/productlist"]);

          dialogs
            .alert({
              title: "Deleting All!",
              message: "Products Cannot be Empty",
              okButtonText: "Ok"
            })
        }
      } else {
        return;
      }
    });
  }

  changelinetotal(productid, index, order) {
    for (var i = 0; i < this.allproducts.length; i++) {
      if (this.allproducts[i]["product_id"] == productid) {
        var tempprice = Number(this.allproducts[i]["product_price"]);
        console.log("tempprice: " + tempprice);
      }
    }

    this.templinetotal = tempprice * order;
    console.log(
      "Line total in change total: " + this.allproducts[index]["line_total"]
    );
    this.allproducts[index]["line_total"] = this.templinetotal;
    this.productDetails[index]["line_total"] = this.templinetotal;
    this.changetotal();
  }

  changetotal() {
    this.total_amount = 0;
    for (var i = 0; i < this.allproducts.length; i++) {
      console.log(i + ": " + this.allproducts[i]["product_id"]);
      var temptotal = this.allproducts[i]["line_total"];
      this.total_amount = temptotal + this.total_amount;
      console.log("totalamount: " + this.total_amount);
    }
    this.orderupdate_customerdetails[0]["totalamount"] = this.total_amount;

  }

  editshippingaddress() {
    var doornoedit = this.address.door_no;
    var areaedit = this.address.area;
    var cityedit = this.address.city;
    var pinedit = this.address.pin;
    var streetnameedit = this.address.street_name;
    var districtedit = this.address.district;
    var stateedit = this.address.state;
    console.log("state: " + stateedit);

    let options: ModalDialogOptions = {
      context: {
        doorno: doornoedit,
        streetname: streetnameedit,
        area: areaedit,
        city: cityedit,
        district: districtedit,
        state: stateedit,
        pin: pinedit
      },
      viewContainerRef: this.viewContainerRef
    };

    this.modalService
      .showModal(DialogShipping, options)
      .then((dialogResult: Array<string>) => this.editaddress(dialogResult));
  }

  editaddress(address) {
    if (address == "null") {
      return;
    } 
    else {
      this.address.door_no = address[0];
      this.orderupdate_customerdetails[0]["shippingaddress"]["door_no"] = address[0];

      this.address.street_name = address[1];
      this.orderupdate_customerdetails[0]["shippingaddress"]["street_name"] = address[1];

      this.address.area = address[2];
      this.orderupdate_customerdetails[0]["shippingaddress"]["area"] = address[2];

      this.address.city = address[3];
      this.orderupdate_customerdetails[0]["shippingaddress"]["city"] = address[3];

      this.address.district = address[4];
      this.orderupdate_customerdetails[0]["shippingaddress"]["district"] = address[4];

      this.address.state = address[5];
      this.orderupdate_customerdetails[0]["shippingaddress"]["state"] = address[5];

      this.address.pin = address[6];
      this.orderupdate_customerdetails[0]["shippingaddress"]["pin"] = address[6];
      this.customer.address = this.address;
    }
  }

  fabTap(args) {
    var fab = args.object;
    let definitions = new Array<AnimationDefinition>();
    var button = this.page.getViewById<View>("dial1");
    var button2 = this.page.getViewById<View>("dial2");

    if (this.isFabOpen == true) {
        
        let a1: AnimationDefinition  =
        { target: fab, rotate: 0, duration: 400, delay: 0 }
        definitions.push(a1);

        let a2: AnimationDefinition = 
        {
          target: button,
          translate: { x: 0, y: 0 },
          opacity: 0,
          duration: 400,
          delay: 0
        }
        definitions.push(a2);

        let a3: AnimationDefinition = 
        {
          target: button2,
          translate: { x: 0, y: 0 },
          opacity: 0,
          duration: 440,
          delay: 0
        }
        definitions.push(a3);

        let animationSet = new Animation(definitions);

        animationSet
        .play()
        .then(() => {
          //console.log("Animations finished");
          this.isFabOpen = false;
        })
        .catch((e) => {
          console.log(e.message);
        });
    } 
    
    else {
      let a4: AnimationDefinition  =
        { target: fab, rotate: 45, duration: 400, delay: 0 }
        definitions.push(a4);

        let a5: AnimationDefinition  =
        {
          target: button,
          translate: { x: 0, y: -54 },
          opacity: 1,
          duration: 400,
          delay: 0
        }
        definitions.push(a5);

        let a6: AnimationDefinition  =
        {
          target: button2,
          translate: { x: 0, y: -100 },
          opacity: 1,
          duration: 440,
          delay: 0
        }
        definitions.push(a6);

        let animationSet = new Animation(definitions);
        animationSet
        .play()
        .then(()=> {
          //console.log("Animations finished");
          this.isFabOpen = true;
        })
        .catch((e)=> {
          console.log(e.message);
        });
    }
  }

  approveorder(shipmode){

    let options = {
      title: "Approve Order",
      okButtonText: "Yes",
      cancelButtonText: "No"
  };

    confirm(options).then((result: boolean) => {
      if(result == true){
        this.disableApproveFab = false;
        this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
        this.savecustomerdetails.setScope(this.orderupdate_customerdetails);    
        this.saveorderservice.setScope(this.productDetails);
        var newnote = this.newnote.nativeElement;
        newnote = newnote.text;
        console.log("New note: " + newnote)
        this.savenotes.setScope(newnote);
        console.log("Approve Order");        
        this.sendorder.updateorder();
        var approval = "approved";
        this.orderapproval.approveorder(this.order_id,approval,this.route.snapshot.params["id"]);
        this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
      }
      else{
          return;
      }
  });
    
  }

  holdorder(shipmode){

    let options = {
      title: "Hold Order",
      okButtonText: "Yes",
      cancelButtonText: "No"
  };

    confirm(options).then((result: boolean) => {
      if(result == true){        
        console.log("Shipping Mode: " + shipmode)
        this.orderupdate_customerdetails[0]["preferredshipping"] = shipmode;
        this.savecustomerdetails.setScope(this.orderupdate_customerdetails);    
        this.saveorderservice.setScope(this.productDetails);        
        var newnote = this.newnote.nativeElement;
        newnote = newnote.text;
        console.log("New note: " + newnote)
        if(newnote !== ""){
          this.savenotes.setScope(newnote);
          console.log("Hold Order");
          this.sendorder.updateorder();
          var approval = "hold"
          this.disableHoldFab = false;
          this.orderapproval.approveorder(this.order_id,approval,this.route.snapshot.params["id"]);
          this.routerExtensions.navigate(["/managerorderlist"], { clearHistory: true });
        }

        else{
          alert("Enter notes")
        }
        
      }
      else{
          return;
      }
  });


    
  }
}