import { Component } from "@angular/core";
@Component({
    selector: "manager-customer-detail",
    moduleId: module.id,
    templateUrl: "managercustomerdetail.component.html",
    styleUrls: ["managercustomerdetail.component.css"]
})

export class ManagerCustomerDetail{

    title: string = "Customer Detail";
    address: object;
    name: string = "PunniyaMoorthy";
    shop: string = "Kanniga Parameshwari Store";
    rep:string="Ramasamy";
    region:string="Thiruvallur Region"
    ngOnInit(){
        this.address = ({area: "No.123, opp. Eldam's Road, AnnaSalai", city:"Chennai", pincode:"611111", mobile:"9874563210"})
        }

}