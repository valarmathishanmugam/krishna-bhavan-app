import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from 'tns-core-modules/ui/page/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';

@Component({
    selector: "sales-rep-list",
    moduleId: module.id,
    templateUrl: "salesreplist.component.html",
    styleUrls: ["salesreplist.component.css"]
})

export class SalesRepList{

    title: string = "Sales Rep List";
    salerepdetails: Array<Object> = [];

constructor(private routerExtensions: RouterExtensions, private page: Page, private location: Location){}

  ngOnInit() {
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
    this.salerepdetails.push({ region:"Trichy District", name: "Parthasarathy", img:"~/Images/person.png", phone:"9874563210" });
  
    // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
    //   data.cancel = true;
    //   //alert("Back Pressed")
    //   //this.location.back();
    //   this.routerExtensions.backToPreviousPage();
    // });
  }

    salesrepdetail(){
      this.routerExtensions.navigate(["/salesrepdetail"]);
    }

}