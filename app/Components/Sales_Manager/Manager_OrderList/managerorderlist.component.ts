import { OrderApproval } from '../../../Services/orderapproval.service';
import { Customer } from '../../../Models/customer.model';
import { Order } from '../../../Models/order.model';
import { GetOrder } from '../../../Services/getorder.service';
import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { confirm } from "ui/dialogs";
import { Page } from 'ui/page';
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import * as application from "application";
import { Location } from '@angular/common';
import * as page from 'tns-core-modules/ui/page';
import { SelectedIndexChangedEventData, ValueList } from "nativescript-drop-down";
import { Observable } from 'tns-core-modules/data/observable/observable';
var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;


@Component({
    selector: "manager-order-list",
    moduleId: module.id,
    templateUrl: "managerorderlist.component.html",
    styleUrls: ["managerorderlist.component.css"]
})

export class ManagerOrderList{

    title: string = "Order List";
    orderDetails: Array<Object> = [];
    public allorder: Order[] = new Array();
    public original: Order[] = new Array();
    public order: Order
    public customer:Customer
    public approvein;
    public statusin;
    public selectedIndex: number = null;
    public items: ValueList<string>;
    tempdate: Date;
    id: undefined;
    loader = new LoadingIndicator();
    
    constructor(private routerExtensions: RouterExtensions, private getorder: GetOrder,
    private orderapproval: OrderApproval, private page: Page, private location: Location){
        
    }

        ngOnInit(){
            
            this.page.actionBarHidden = false;

            //this.orderDetails.push({rep:"Ramasamy",id:"ash34283980938", shop:"Kanniga Parameshwari Store", img:"~/Images/green_dot.png", date:"8/2/18"})
            this.getorder
            .getmanagerorders(this.id)
            .subscribe(data => this.fetchallorders(data.json()),
                        err=>console.log("Getting Manager orders error: " + err));

            // application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            //     data.cancel = true;
            //     //alert("Back Pressed")
            //     //this.location.back();
            //     this.routerExtensions.backToPreviousPage();
            //   });           
              this.items = new ValueList<string>();
              this.items.push({
                    value:   "all",
                    display: "All Orders"
                },
                {
                    value:   "not_reviewed",
                    display: "Waiting For Approval"
                },
                {
                    value:   "hold",
                    display: "Hold Orders"
                },
                {
                    value:   "approved",
                    display: "Pending File"
                },
                {
                    value:   "completed",
                    display: "Dispatched Orders"
                }
            );

            let options = {
                message: 'Loading...',
                progress: 0.65,
                android: {
                  indeterminate: true,
                  cancelable: false,
                  max: 100,
                  progressNumberFormat: "%1d/%2d",
                  progressPercentFormat: 0.53,
                  progressStyle: 1,
                  secondaryProgress: 1
                },
                ios: {
                  details: "Additional detail note!",
                  square: false,
                  margin: 10,
                  dimBackground: true,
                  color: "#4B9ED6",
                  mode: ""// see iOS specific options below
                }
              };
               
              this.loader.show(options);
            }

        

        fetchallorders(data){
            this.loader.hide();
            console.log("All manager orders" + data.length)
            for (let i = 0; i < data.length; i++) {
                
                // if(this.tempuser["role"] === "admin"){
                //     this.order = new Order();
                //     this.order._id = data[i]._id.$oid;
                //     this.order.order_id = data[i].order_id;
                //     this.order.order_date = data[i].order_date;
                //     this.order.total_amount = data[i].total_amount; 
                //     this.customer = new Customer();
                //     this.customer.shopname = data[i].shop_name;
                //     this.order.customer = this.customer;                    
                //     var tempstatus = data[i].status;
                // }
                // else {
                this.order = new Order();
                this.order._id = data[i].id.$oid;               
                this.order.order_id = data[i].order_id;
                this.order.order_date = data[i].date;
                //this.tempdate = data[i].order_date;
                //this.order.order_date = this.tempdate.getDate()+"/"+this.tempdate.getMonth();               
                this.order.total_amount = data[i].total_amount;
                this.customer = new Customer();
                this.customer.shopname = data[i].shop_name;
                this.order.customer = this.customer;                
                var tempstatus = data[i].status;
                // }

                if(tempstatus.toLowerCase() === "hold"){//onHold
                    this.order.status = "~/Images/hold.png"
                    this.order.approve = "visible"
                  }
                  else if(tempstatus.toLowerCase() === "pending"){//not_reviewed by sales_manager
                    this.order.status = "~/Images/not_reviewed.png"
                    this.order.approve = "visible"
                  }
                  else if(tempstatus.toLowerCase() === "processed"){//Completed
                    this.order.status = "~/Images/completed.png"
                    this.order.approve = "hidden"
                  }
                  else if(tempstatus.toLowerCase() === "approved"){//Approved & Processing
                    this.order.status = "~/Images/approved.png"
                    this.order.approve = "hidden"
                  }   
                
                this.allorder.push(this.order);
                this.original.push(this.order);
                this.allorder.reverse();
                //this.allorder.filter(result=> result.order_date)
              }
        }

        approveorder(id,index){
            //this.routerExtensions.navigate(["/managerorderreview"]);
            
            let options = {
                title: "Approve Order",
                okButtonText: "Yes",
                cancelButtonText: "No"
            };
            
            confirm(options).then((result: boolean) => {
                if(result == true){
                    console.log("Approve Status before: " + this.allorder[index]["status"])
                    
                    this.updatestatus(index);
                    var approval = "approved"
                    this.orderapproval.approveorder(id,approval,this.id);
                    
                }
                else{
                    return;
                }
            });
            
        }

        updatestatus(index){

            this.approvein = "hidden"
            this.statusin = "~/Images/approved.png"
            
            this.allorder[index]["status"] = this.statusin
            this.allorder[index]["approve"] = this.approvein

            console.log("Approve Status after: " + this.allorder[index]["status"])
        }

        orderreview(id, status, order_id){
            if(status == "~/Images/approved.png"){
                this.routerExtensions.navigateByUrl("orderdetail/"+id+"/"+order_id)
            }
            else if(status == "~/Images/completed.png"){
                this.routerExtensions.navigateByUrl("orderdetail/"+id+"/"+order_id)
            }
            else if(status == "~/Images/not_reviewed.png"){
                this.routerExtensions.navigateByUrl("managerorderreview/"+id)
            }
            else if(status == "~/Images/hold.png"){
                this.routerExtensions.navigateByUrl("managerorderreview/"+id)
            }

        }

        onchange(args){
            console.log("Changed");
            console.log("Changed Value: " + this.items.getValue(args.newIndex));
            if(this.items.getValue(args.newIndex) === "all"){
                this.allorder = this.original;
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "not_reviewed"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/not_reviewed.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "hold"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/hold.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "approved"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/approved.png"));
                this.allorder.reverse();
            }
            else if(this.items.getValue(args.newIndex) === "completed"){
                this.allorder = this.original.filter(result=> result.status.includes("~/Images/completed.png"));
                this.allorder.reverse();
            }
        }

        onopen(){
            console.log("Opened");
        }

        onclose(){
            console.log("Closed");
        }

}