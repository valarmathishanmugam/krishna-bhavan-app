"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var ManagerCustomerList = (function () {
    function ManagerCustomerList(routerExtensions) {
        this.routerExtensions = routerExtensions;
        this.title = "Customer List";
        this.customerDetails = [];
        this.rep = "Ramasamy";
    }
    ManagerCustomerList.prototype.ngOnInit = function () {
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
        this.customerDetails.push({ store: "Kanniga Parameshwari Store", name: "Parthasarathy", img: "res://store", address: "Trichy Main Road", phone: "9874563210" });
    };
    ManagerCustomerList.prototype.customerdetail = function () {
        this.routerExtensions.navigate(["/managercustomerdetail"]);
    };
    return ManagerCustomerList;
}());
ManagerCustomerList = __decorate([
    core_1.Component({
        selector: "manager-customer-list",
        moduleId: module.id,
        templateUrl: "managercustomerlist.component.html",
        styleUrls: ["managercustomerlist.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions])
], ManagerCustomerList);
exports.ManagerCustomerList = ManagerCustomerList;
