import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "manager-customer-list",
    moduleId: module.id,
    templateUrl: "managercustomerlist.component.html",
    styleUrls: ["managercustomerlist.component.css"]
})

export class ManagerCustomerList{

    title: string = "Customer List";
    customerDetails: Array<Object> = [];
    rep:string="Ramasamy";

constructor(private routerExtensions: RouterExtensions){}

  ngOnInit() {
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
    this.customerDetails.push({ store:"Kanniga Parameshwari Store", name: "Parthasarathy", img:"res://store",address:"Trichy Main Road", phone:"9874563210" });
  }

  customerdetail(){
    this.routerExtensions.navigate(["/managercustomerdetail"]);
  }
}