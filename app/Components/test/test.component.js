"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Connectivity = require("tns-core-modules/connectivity");
var router_1 = require("nativescript-angular/router");
var Test = (function () {
    function Test(zone, routerExtensions) {
        this.zone = zone;
        this.routerExtensions = routerExtensions;
        this.connectionType = "Unknown";
    }
    Test.prototype.ngOnInit = function () {
        var _this = this;
        this.connectionType = this.connectionToString(Connectivity.getConnectionType());
        Connectivity.startMonitoring(function (connectionType) {
            _this.connectionType = _this.connectionToString(connectionType);
        });
    };
    Test.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "No Connection!";
            case Connectivity.connectionType.wifi:
                return "Connected to WiFi!";
            case Connectivity.connectionType.mobile:
                return "Connected to Cellular!";
            default:
                return "Unknown";
        }
    };
    return Test;
}());
Test = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: "test.component.html"
    }),
    __metadata("design:paramtypes", [core_1.NgZone, router_1.RouterExtensions])
], Test);
exports.Test = Test;
