
import { Component, OnInit, NgZone } from "@angular/core";

import * as Connectivity from 'tns-core-modules/connectivity';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
    moduleId: module.id,
    templateUrl: "test.component.html"
})

export class Test implements OnInit {
    public connectionType: string;
    
        public constructor(private zone: NgZone, private routerExtensions: RouterExtensions) {
            this.connectionType = "Unknown";
        }
    
        public ngOnInit() {
            this.connectionType = this.connectionToString(Connectivity.getConnectionType());
            Connectivity.startMonitoring(connectionType => {
                    this.connectionType = this.connectionToString(connectionType);  
            });
        }
    
        public connectionToString(connectionType: number): string {
            switch(connectionType) {
                case Connectivity.connectionType.none:
                    return "No Connection!";
                case Connectivity.connectionType.wifi:
                    return "Connected to WiFi!";
                case Connectivity.connectionType.mobile:
                    return "Connected to Cellular!";
                default:
                    return "Unknown";
            }
        }
    
    }