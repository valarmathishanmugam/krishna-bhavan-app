import { OrderApproval } from './Services/orderapproval.service';
import { GetOrder } from './Services/getorder.service';
import { SendOrder } from './Services/sendorder.service';
import { ProductService, SaveProducts, SaveOffers } from './Services/product.service';
import { loginService } from './Services/login.service';
import { NgModule } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { routes, navigatableComponents } from "./app.routing";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./Components/Login/login.component";
import { AccountComponent } from "./Components/Account/account.component";

import {registerElement} from "nativescript-angular/element-registry";
import { CustomerService } from './Services/customer.service';
import { NativeScriptHttpModule } from 'nativescript-angular/http';
import { NativeScriptUISideDrawerModule } from 'nativescript-telerik-ui/sidedrawer/angular';
import {
    SaveCustomer,
    SaveCustomerDetails,
    SaveNotes,
    SaveOrderService,
    SaveShipping,
    SaveTotal,
    SaveEmployeeId
    
} from './Services/saveorder.service';
import { Dialog } from './Components/Dialog/dialog.component';
import { DialogShipping } from './Components/Dialog_shippingaddress/dialogshipping.component';
import { ReportService } from './Services/reports.service';
import { SaveUpdateOrderId, SaveUpdateOrder_Id } from './Services/saveorder.service';
// import { LimitToDirective } from './Services/limit-to.directive';




registerElement("CardView", () => require("nativescript-cardview").CardView);
registerElement("CheckBox", () => require("nativescript-checkbox").CheckBox);
registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);
registerElement("DropDown", () => require("nativescript-drop-down").DropDown);

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptUISideDrawerModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    NativeScriptHttpModule
    
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    navigatableComponents,
    Dialog,
    DialogShipping,
    AccountComponent
    // LimitToDirective
  ],
  providers: [
    CustomerService,
    SaveOrderService,    
    loginService,
    ProductService,
    SaveCustomer,
    SendOrder,
    SaveTotal,
    SaveProducts,
    SaveCustomerDetails,
    SaveShipping,
    SaveNotes,
    GetOrder,
    OrderApproval,
    SaveOffers,
    SaveEmployeeId,
    ReportService,
    SaveUpdateOrderId,
    SaveUpdateOrder_Id
    
  ],
  entryComponents: [
    Dialog,
    DialogShipping
],
  bootstrap: [AppComponent]
})
export class AppModule {}
